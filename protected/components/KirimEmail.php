<?php

class KirimEmail {
    public static function getConfig()
    {
		
		$time = time();
		$generated_token = hash_hmac("sha256","bagidata2"."::"."eqLkJv5sauVIHC8iYnxPfTKAQoBcDw37bagidata2"."::".$time,"eqLkJv5sauVIHC8iYnxPfTKAQoBcDw37bagidata2");
		$id = 'bagidata2';
		
		return array(
			'time'=>$time,
			'token'=>$generated_token,
			'id'=>$id,
		);
    }
	
	
	public static function getAllList()
    {
		$config = self::getConfig();
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.kirim.email/v3/list/",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  //CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"Auth-Id: ".$config['id']." ",
			"Auth-Token: ".$config['token']."",
			"Timestamp: ".$config['time']." "
		  ),
		)); 
		
		return self::returnResult($curl);
		
    }
	
	public static function getBroadcastById($campaign_guid)
    {
		$config = self::getConfig();
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.kirim.email/v3/broadcast/".$campaign_guid,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  //CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"Auth-Id: ".$config['id']." ",
			"Auth-Token: ".$config['token']."",
			"Timestamp: ".$config['time']." "
		  ),
		)); 
		
		return self::returnResult($curl);
		
    }
	
	public static function createList($newList)
    {
		$config = self::getConfig();
		//$newList = 'campaign_35';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.kirim.email/v3/list/",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  //CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "name=".$newList."&public_name=".$newList,
		  CURLOPT_HTTPHEADER => array(
			"Auth-Id: ".$config['id']." ",
			"Auth-Token: ".$config['token']."",
			"Timestamp: ".$config['time']." ",
			"Content-Type: application/x-www-form-urlencoded",
		  ),
		));
		
		return self::returnResult($curl);
		
    }
	
	public static function createSubscriberToList($listID,$email)
    {
		$config = self::getConfig();
		/* $email = 'ichsan.must10@gmail.com';
		$listID = '29905'; */
		$postData = 'lists='.$listID.'&email='.$email.'&full_name='.$email;
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.kirim.email/v3/subscriber/",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  //CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		  CURLOPT_HTTPHEADER => array(
			"Auth-Id: ".$config['id']." ",
			"Auth-Token: ".$config['token']."",
			"Timestamp: ".$config['time']." ",
			"Content-Type: application/x-www-form-urlencoded",
		  ),
		));
		
		return self::returnResult($curl);
    }
	
	
	public static function createBroadcast($listID,$sender,$title,$subject,$content)
    {
		$config = self::getConfig();
		$time = strtotime(date('Y-m-d H:i:s'));
		//$startTime = date("H:i:s", strtotime('-10 minutes', $time));
		$endTime = date("Y-m-d H:i:s", strtotime('+10 minutes', $time));
		$datetime = $endTime;
		
		$list = array();
		$list[]= $listID;
		$dataArray = array(
			'title'=>$title,
			'sender'=>$sender,
			'list'=>$list,
			//'send_at'=> date('Y-m-d H:i:s'),
			'send_at'=> $datetime,
			'messages'=>array(
				array(
					'subject'=>$subject,
					'content'=>$content,
				),
			),
		);
		$dataString = json_encode($dataArray);
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.kirim.email/v3/broadcast/",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  //CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  //CURLOPT_POSTFIELDS => "{\n\t\"title\" : \"Tes Campaign from API\",\n\t\"sender\": \"hello@bagidata.com\",\n\t\"list\" : [1],\n\t\"messages\":[\n\t\t\"subject\" : \"Subject #1\",\n\t\t\"content\" : \"Content\"\n\t]\n}",
		  CURLOPT_POSTFIELDS => $dataString,
		  CURLOPT_HTTPHEADER => array(
			"Auth-Id: ".$config['id']." ",
			"Auth-Token: ".$config['token']."",
			"Timestamp: ".$config['time']." ",
			'Content-Length: ' . strlen($dataString),     
		  ),
		));
		
		
		return self::returnResult($curl);
		
	
    }
	
	
	public static function returnResult($curl)
    {
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		$result = array();
		if ($err) {
		  //echo "cURL Error #:" . $err;
		  $result['status'] = 'error';
		  $result['message'] = $err;
		  
		} else {
		  $result = CJSON::decode($response);
		}
		
		/* echo '<pre>';
		print_r($result);
		if ($result['status']=='success'){
			$return = true;
		}else{
			$return = false;
		}  */
		
		return $result;
	}
	
	
	
	
}