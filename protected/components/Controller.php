<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
    
    public function init()
	{
		$this->initAjaxCsrfToken();
		parent::init();
	}
    
    protected function initAjaxCsrfToken(){
      	Yii::app()->clientScript->registerScript('AjaxCsrfToken', ' $.ajaxSetup({
           data: {"' . Yii::app()->request->csrfTokenName . '": "' . Yii::app()->request->csrfToken . '"},
           cache:false
      	});', CClientScript::POS_BEGIN);
    }
	
	public function getToken(){
		$data = array(
			"grant_type" => "client_credentials",
		);
		
		$token = curl_init(); 
		curl_setopt($token, CURLOPT_URL, "https://api.mainapi.net/token"); 
		curl_setopt($token, CURLOPT_POST, 1 ); 
		curl_setopt($token, CURLOPT_POSTFIELDS, http_build_query($data)); 
		curl_setopt($token, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($token, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($token, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($token, CURLOPT_HTTPHEADER, array('Authorization: Basic dFJEbmhxMk9WOThmcmZSVlA0TmZ2YXZaVWVZYTpJa2JZX2NBeEMwWUU4Q1Jsd2NVNjNRRTBfZk1h','Content-Type: application/x-www-form-urlencoded'));
		$postToken = curl_exec($token); 
		curl_close($token);
		$responseToken = json_decode($postToken, true);
		$token = $responseToken['token_type'].' '.$responseToken['access_token']; 
		return $token;
	}	
	
	protected function sendOtp($token, $no_hp, $id_user){
		$notp = Logic::setOtp();
		
		$wording = OtpWording::model()->find();
		$isi = str_replace('{otp}', $notp, $wording->wording_otp);
		$data = array(
			"msisdn" => $no_hp,
			"content" => $isi,
		);
		
		$otp = curl_init(); 
		curl_setopt($otp, CURLOPT_URL, "https://api.mainapi.net/smsnotification/1.0.0/messages"); 
		curl_setopt($otp, CURLOPT_POST, 1 ); 
		curl_setopt($otp, CURLOPT_POSTFIELDS, http_build_query($data)); 
		curl_setopt($otp, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($otp, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($otp, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($otp, CURLOPT_HTTPHEADER, array('Authorization: '.$token.'','Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));
		// curl_setopt($otp, CURLOPT_HTTPHEADER, array('Authorization: Bearer f70cbe312db1af949dad79e89a220453','Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));
		$postToken = curl_exec($otp); 
		curl_close($otp);
		$responseToken = json_decode($postToken, true);
		
		$userotp = OtpUser::model()->findByAttributes(array('id_user'=>$id_user));
		if(empty($userotp)) $userotp = new OtpUser;
		$userotp->id_user = $id_user;
		$userotp->otp = $notp;
		$userotp->durasi_otp = 10;
		$userotp->created_date = date('Y-m-d H:i:s');
		$userotp->otp_active = 1;
		if($userotp->save()){
			return true;
		}
	}
	
	protected function beforeAction($action){
		$controller = $action->controller->id;
		$actioncontroller = $action->controller->action->id;
		
		if($controller != 'site' && $actioncontroller != 'verifikasi') {
			$cekotp = CekOtp::model()->find();
			if($cekotp->is_otp != 2){
				$otp = OtpUser::model()->findByAttributes(array('id_user'=>Yii::app()->user->id));
				
				$id = Yii::app()->user->isGuest;
				if($otp->otp_active == 1 && !$id){
					$user = UserEnterprise::model()->findByPk(Yii::app()->user->id);
					
					$this->sendOtp($this->getToken(), $user->no_hp, $user->id);
					$this->redirect(array('site/verifikasi', array('id'=>'verifikasiotp', 'login'=>'belumlogin')));
					
				}
			}
		}
		
		return true;
	}
	
}