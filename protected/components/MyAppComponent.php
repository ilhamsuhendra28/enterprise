<?php

class MyAppComponent {
    public static function getTextTypeCompaign($type)
    {
        $data = array(
			'email'=>'Email',
			'sms'=>'SMS',
			'wa'=>'Whatsapp',
		);
		
		if (isset($data[$type])){
			return $data[$type];
		}else{
			return '-';
		}
    }
	
	 public static function getListTemplateEmail(){
		 $data = array();
		 $CampaignTemplate = CampaignTemplate::model()->findAllByAttributes(array('campaign_template_group'=>'mailchimp'));
		 foreach($CampaignTemplate as $template){
			 $data[$template->campaign_template_id]= $template->campaign_template_name;
		 }
		 
		 return $data;
	 }
	 
	 public static function getListTemplateEmailBee(){
		 $data = array();
		 $CampaignTemplate = CampaignTemplate::model()->findAllByAttributes(array('campaign_template_group'=>'bee'));
		 foreach($CampaignTemplate as $template){
			 $data[$template->id]= $template->campaign_template_name;
		 }
		 
		 return $data;
	 }
	
	
	 public static function loadImage($data){
			$return ='';
			foreach ($data as $image){
				 $imageHtml ='<img src="'.Yii::app()->baseUrl.'/'.$image->image.'" class="file-preview-image kv-preview-data" title="" alt="" style="width:30%;">';
				 $return .=$imageHtml;
			}
			return $return;
		}
	
	 public static function getUrlImageInApp(){
		if(MyAppComponent::checkIfLocalhost()){
			//$urlImage = Yii::app()->baseUrl.'/'.$image->image;
			$urlImage = Yii::app()->baseUrl.'/../app/';
		}else{
			$urlImage = 'https://app.bagidata.com/';
		}
		
		return $urlImage;
	 }
	 public static function loadImageFromApp($data){
			$return ='';
			$urlImage = MyAppComponent::getUrlImageInApp();
			foreach ($data as $image){
				 $imageHtml ='<img src="'.$urlImage.$image->image.'" class="file-preview-image kv-preview-data" title="" alt="" style="width:30%;">';
				 $return .=$imageHtml;
			}
			return $return;
	}
		
	public static function getStatusByBegdaEndda($begda,$endda){
			
		$date_now = date('Y-m-d');
		$date_now=date('Y-m-d', strtotime($date_now));;
		//echo $date_now; // echos today! 
		$begda_date = date('Y-m-d', strtotime($begda));
		$endda_date = date('Y-m-d', strtotime($endda));

		if (($date_now >= $begda_date) && ($date_now <= $endda_date))
		{
		  //echo "is between";
		  return true;
		}
		else
		{
		  //echo "NO GO!";  
		  return false;
		}
	}
	
	public static function getMonthData(){
			
			return array(
				'01'=>'January',
				'02'=>'February',
				'03'=>'March',
				'04'=>'April',
				'05'=>'May',
				'06'=>'June',
				'07'=>'July',
				'08'=>'August',
				'09'=>'September',
				'10'=>'October',
				'11'=>'November',
				'12'=>'December',
			);
		}
		
	public static function getArrayDataDayName(){
		
		$array = array(
			'1'=>'Monday',
			'2'=>'Tuesday',
			'3'=>'Wednesday',
			'4'=>'Thursday',
			'5'=>'Friday',
			'6'=>'Saturday',
			'7'=>'Sunday',
		);
		return $array;
	}
	
	/* public static function getlistDateNumber($year,$month){
			
			$a_date = $year.'-'.$month."-01";
			$total =  date("t", strtotime($a_date));
			//echo $total;
			$arrayReturn= array();
			$arrayData= array();
			for ($i = 1; $i <= $total; $i++) {
				//echo $i;
				$num_padded = sprintf("%02d", $i);
				$arrayData[$num_padded] = $num_padded;
			}
			$arrayReturn['start']  = '01';
			$arrayReturn['end']  = $total;
			$arrayReturn['data'] = $arrayData;
			return $arrayReturn;
		} */
		
	public static function getlistDateNumber(){
		
			$arrayData= array();
			for ($i = 1; $i <= 31; $i++) {
				//echo $i;
				$num_padded = sprintf("%02d", $i);
				$arrayData[$num_padded] = $num_padded;
			}
			return $arrayData;
	}
	
	
	public static function checkIfLocalhost(){
		$whitelist = array('127.0.0.1', "::1");

		if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
			$data = true;
		}else{
			$data = false;
		}
		
		return $data;
		//return false; // cek doank
	}
	
	
	public static function getStatusApproval($status){
		
		$arrayStatus = array('0'=>'In Progress','1'=>'Approved', '2'=>'Rejected');
		if (isset($arrayStatus[$status])){
			return $arrayStatus[$status];
		}else{
			return '-';
		}
	}
	
	public static function getArrayDataCampaignType(){
		
		$arrayStatus = array('0'=>'Once','1'=>'Every Month', '2'=>'Every Week', '3'=>'Every Day');
		return $arrayStatus;
	}
	
	public static function getArrayDataWeekDay(){
		
		$array = array(
			'1'=>'2018-01-01',
			'2'=>'2018-01-02',
			'3'=>'2018-01-03',
			'4'=>'2018-01-04',
			'5'=>'2018-01-05',
			'6'=>'2018-01-06',
			'7'=>'2018-01-07',
		);
		return $array;
	}
	
	public static function getCampaignTYpe($type){
		
		$arrayStatus = self::getArrayDataCampaignType();
		if (isset($arrayStatus[$type])){
			return $arrayStatus[$type];
		}else{
			return '-';
		}
	}

    
}