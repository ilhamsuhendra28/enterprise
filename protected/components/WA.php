<?php

class WA {
    public static function getAdminPhone()
    {
        $data = Admin::model()->findAll(array('select'=>'phone','condition'=>'active = 1 AND phone IS NOT NULL'));
		return $data;
    }
	
	public static function sendNotificationToAdmin($message)
    {
		
        $adminPhone = self::getAdminPhone();
		foreach ($adminPhone as $key => $data){
			self::sendWAData($data->phone,$message);
		}
    }
	
	
	public function sendWAData($no_hp,$message){	
		
		$nomor_awal_no_hp = substr($no_hp,0,1);
		//echo $nomor_awal_no_hp;
		if ($nomor_awal_no_hp == '0'){
			$no_hp ='62'.substr($no_hp,1);
		}
		
		$receiver = $no_hp;
		
		/* $token ='1b39ca944fdf7674ad9037d5e71da1565b0236e9d8a11';
		$from = '6281383930606'; */
		
		$token ='b42a3ddee4810ee259f924b0bf62f3b15b446a8760fce';
		$from = '628157060604';
		$destination = $receiver; 
		$message = $message;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, "https://www.waboxapp.com/api/send/chat"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$token."&uid=".$from."&to=".$destination."&text=".$message); 
		//curl_setopt($ch, CURLOPT_POSTFIELDS, "token=1b39ca944fdf7674ad9037d5e71da1565b0236e9d8a11&uid=".$from."&to=".$destination."&custom_uid=msg-5331&text=Hello+world%21"); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25); 

		$response = curl_exec($ch); 
		$info = curl_getinfo($ch); 
		curl_close ($ch);
		
		$result = CJSON::decode($response);
		
		if (isset($result['success'])){
			$return = true;
			echo '----- SUCCESS<br>';
		}else{
			$return = false;
			echo '----- ERROR '.$result['error'].'<br>';
		}
		
		/* echo '<pre>';
		print_r($result);
		exit;
		*/
		
		return $return;
	}
	
	
}