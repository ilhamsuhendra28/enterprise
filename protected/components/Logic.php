<?php

class Logic {
     public static function randomChar($length = 6, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        if($length > 0)
        {
            $len_chars = (strlen($chars) - 1);
            $the_chars = $chars{rand(0, $len_chars)};
            for ($i = 1; $i < $length; $i = strlen($the_chars))
            {
                $r = $chars{rand(0, $len_chars)};
                if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
            }

            return $the_chars;
        }
    }
    
	public static function getEncrypt($params){
		$security = Yii::app()->getSecurityManager();
		$_params = base64_encode(json_encode($params));
		$encryptedProperty = $security->encrypt($_params, Yii::app()->getSecurityManager()->encryptionKey);
		$utf8Property = utf8_encode($encryptedProperty);
		$params = array(
			'text'=>$utf8Property,
			'key'=>rand()
		);
		
		return $params;
	}	
	
	public static function getDecrypt($text){
		$security = Yii::app()->getSecurityManager();
        $hasil = $security->decrypt(utf8_decode($text), Yii::app()->getSecurityManager()->encryptionKey);
        return json_decode(base64_decode($hasil));
	}	
	
	public static function notifRegister($email){
		$params = $email;

		$activation_url = Yii::app()->createAbsoluteUrl('site/konfirmasi',array('params'=>$email, 'random'=>Logic::randomChar()));
		$message = Yii::app()->controller->renderPartial('temp_confirm', array('activation_url' => $activation_url), array(true));

		Yii::import('ext.yii-mail.YiiMailMessage');
		$mailer = new YiiMailMessage;
		$mailer->setBody($message,'text/html');
		$mailer->subject = 'Notifikasi Registrasi';
		$mailer->addTo($email);
		$mailer->setFrom('hello@bagidata.com', 'Bagi Data');
		try {
			Yii::app()->mail->send($mailer);
			return 'success';
		} catch (Exception $e) {
			return 'gagal';
		}
	}	
	
	public static function sendPassword($email){
		$user = User::model()->findByAttributes(array('email'=>$email));

		$activation_url = Yii::app()->createAbsoluteUrl('site/changepassword',array('params'=>$email, 'random'=>Logic::randomChar()));
		$message = Yii::app()->controller->renderPartial('temp_forgot', array('activation_url' => $activation_url), array(true));

		Yii::import('ext.yii-mail.YiiMailMessage');
		$mailer = new YiiMailMessage;
		$mailer->setBody($message,'text/html');
		$mailer->subject = 'Notifikasi Forgot Password';
		$mailer->addTo($email);
		$mailer->setFrom('hello@bagidata.com', 'Bagi Data');
		try {
			Yii::app()->mail->send($mailer);
			return 'success';
		} catch (Exception $e) {
			return 'gagal';
		}
	}

	public static function sendFeedback($user_id, $email, $message){
		$personal = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$user_id, 'id_personal_information'=>2));
		// var_dump(nl2br($message));exit;
		Yii::import('ext.yii-mail.YiiMailMessage');
		$mailer = new YiiMailMessage;
		$mailer->setBody(nl2br($message),'text/html');
		$mailer->subject = 'Halo, User '.$personal->value.' Mengirimkan Feedback';
		$mailer->addTo('hellow@bagidata.com');
		$mailer->setFrom($email, $email);
		try {
			Yii::app()->mail->send($mailer);
			return 'success';
		} catch (Exception $e) {
			return 'gagal';
		}
	}	
	
	public static function setOtp($length = 6, $chars = '1234567890')
    {
        if($length > 0)
        {
            $len_chars = (strlen($chars) - 1);
            $the_chars = $chars{rand(0, $len_chars)};
            for ($i = 1; $i < $length; $i = strlen($the_chars))
            {
                $r = $chars{rand(0, $len_chars)};
                if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
            }

            return $the_chars;
        }
    }
	
	
	public static function convertBulan($month){
		if($month == '01'){
			$bulan = 'Januari';
		}else if($month == '02'){
			$bulan = 'Febuari';
		}else if($month == '03'){
			$bulan = 'Maret';
		}else if($month == '04'){
			$bulan = 'April';
		}else if($month == '05'){
			$bulan = 'Mei';
		}else if($month == '06'){
			$bulan = 'Juni';
		}else if($month == '07'){
			$bulan = 'Juli';
		}else if($month == '08'){
			$bulan = 'Agustus';
		}else if($month == '09'){
			$bulan = 'September';
		}else if($month == '10'){
			$bulan = 'Oktober';
		}else if($month == '11'){
			$bulan = 'November';
		}else if($month == '12'){
			$bulan = 'Desember';
		}	
		
		return $bulan;
	}	
	
	public static function getBulan(){
		$bulan = array();
		$bulan['01'] = 'Januari';
		$bulan['02'] = 'Febuari';
		$bulan['03'] = 'Maret';
		$bulan['04'] = 'April';
		$bulan['05'] = 'Mei';
		$bulan['06'] = 'Juni';
		$bulan['07'] = 'Juli';
		$bulan['08'] = 'Agustus';
		$bulan['09'] = 'September';
		$bulan['10'] = 'Oktober';
		$bulan['11'] = 'November';
		$bulan['12'] = 'Desember';
		
		return $bulan;
	}	
	
	public static function getQueryTahun(){
			$sqltahun = 'SELECT a.tahun FROM(
			SELECT YEAR(created_date) tahun FROM receipt_retail_barang
			UNION ALL
			SELECT YEAR(created_date) tahun FROM receipt_pk_pesawat
			UNION ALL
			SELECT YEAR(created_date) tahun FROM receipt_pk_ka
		) a
		GROUP BY a.tahun';
		
		$cmdtahun = Yii::app()->db->createCommand($sqltahun);
		return $cmdtahun->queryAll();	
	}	
	
	public static function getQueryKategori(){
		$sqlkategori = 'SELECT a.brand, a.kategori FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori 
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			) a
		GROUP BY a.kategori
		ORDER BY a.kategori ASC';
		
		$cmdkategori = Yii::app()->db->createCommand($sqlkategori);
		return $cmdkategori->queryAll();
	}
	
	public static function getQueryGender($tahun, $bulan, $kategori, $brand){
		$arrv = explode(',', $brand);
		foreach ($arrv as $idx=>$value) {
			$bbrand[] = '"'.$value.'"';
			$nama_brand[$value]['label'] = '"'.$value.'"';
		}
		
		$brand = implode(',', $bbrand);
		
		$sqlgender = 'SELECT a.brand, a.kategori, 
		round(a.laki/(a.laki + a.perempuan) * 100 ) laki, 
		round(a.perempuan/(a.laki + a.perempuan)* 100) perempuan,
		a.tahun, a.bulan
		FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori,
			SUM(CASE WHEN e.value = 1 THEN 1 ELSE 0 END) laki,
			SUM(CASE WHEN e.value = 2 THEN 1 ELSE 0 END) perempuan,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			JOIN receipt_retail c ON c.id_retail = a.id_receipt_retail
			JOIN user_upload_data d ON d.id = c.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 4 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori,
			SUM(CASE WHEN e.value = 1 THEN 1 ELSE 0 END) laki,
			SUM(CASE WHEN e.value = 2 THEN 1 ELSE 0 END) perempuan,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 4 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_maskapai
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori, 
			SUM(CASE WHEN e.value = 1 THEN 1 ELSE 0 END) laki,
			SUM(CASE WHEN e.value = 2 THEN 1 ELSE 0 END) perempuan,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 4 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_ka
		) a
		WHERE a.tahun = :tahun AND a.bulan = :bulan AND a.kategori = :kategori AND a.brand IN ('.$brand.')';
		
		$cmdgender = Yii::app()->db->createCommand($sqlgender);
		$cmdgender->bindParam(":tahun", $tahun , PDO::PARAM_STR);
		$cmdgender->bindParam(":bulan", $bulan , PDO::PARAM_STR);
		$cmdgender->bindParam(":kategori", $kategori , PDO::PARAM_STR);
		$data = $cmdgender->queryAll();
		
		foreach($data as $gdx=>$grow){
			$nama_brand[$grow['brand']]['label'] = '"'.$grow['brand'].'"';
			$nama_brand[$grow['brand']]['valuelaki'] = $grow['laki'];
			$nama_brand[$grow['brand']]['valueperempuan'] = $grow['perempuan'];
		}	
		
		
		foreach($nama_brand as $abrand){
			$label[$abrand['label']] = $abrand['label'];
			$laki[$abrand['label']] = $abrand['valuelaki'];
			$perempuan[$abrand['label']] = $abrand['valueperempuan'];
		}	
		
		$label_brand_male = 'Laki - Laki';
		$label_brand_female = 'Perempuan';
		
		$gimplode = array();
		$gimplode['brand'] = implode(',', $label);
		$gimplode['label_brand_male'] = $label_brand_male;
		$gimplode['persen_brand_laki'] = implode(',', $laki);
		$gimplode['label_brand_female'] = $label_brand_female;
		$gimplode['persen_brand_perempuan'] = implode(',', $perempuan);
		
		return $gimplode;
	}	
	
	public static function getQueryMarried($tahun, $bulan, $kategori, $brand){
		$arrv = explode(',', $brand);
		foreach ($arrv as $value) {
			$bbrand[] = '"'.$value.'"';
			$nama_brand[$value]['label'] = '"'.$value.'"';
		}
		
		$brand = implode(',', $bbrand);
		
		$sqlmarried = 'SELECT a.brand, a.kategori, 
		round(a.lajang/(a.lajang + a.menikah + a.pernah_menikah) * 100) lajang, 
		round(a.menikah/(a.lajang + a.menikah + a.pernah_menikah) * 100) menikah,
		round(a.pernah_menikah/(a.lajang + a.menikah + a.pernah_menikah) * 100) pernah_menikah,
		a.tahun, a.bulan
		FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori,
			SUM(CASE WHEN e.value = 13 THEN 1 ELSE 0 END) lajang,
			SUM(CASE WHEN e.value = 14 THEN 1 ELSE 0 END) menikah,
			SUM(CASE WHEN e.value = 17 THEN 1 ELSE 0 END) pernah_menikah,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			JOIN receipt_retail c ON c.id_retail = a.id_receipt_retail
			JOIN user_upload_data d ON d.id = c.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 7 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori,
			SUM(CASE WHEN e.value = 13 THEN 1 ELSE 0 END) lajang,
			SUM(CASE WHEN e.value = 14 THEN 1 ELSE 0 END) menikah,
			SUM(CASE WHEN e.value = 17 THEN 1 ELSE 0 END) pernah_menikah,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 7 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_maskapai
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori, 
			SUM(CASE WHEN e.value = 13 THEN 1 ELSE 0 END) lajang,
			SUM(CASE WHEN e.value = 14 THEN 1 ELSE 0 END) menikah,
			SUM(CASE WHEN e.value = 17 THEN 1 ELSE 0 END) pernah_menikah,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 7 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_ka
		) a
		WHERE a.tahun = :tahun AND a.bulan = :bulan AND a.kategori = :kategori AND a.brand IN ('.$brand.')';
		
		$cmdmarried = Yii::app()->db->createCommand($sqlmarried);
		$cmdmarried->bindParam(":tahun", $tahun , PDO::PARAM_STR);
		$cmdmarried->bindParam(":bulan", $bulan , PDO::PARAM_STR);
		$cmdmarried->bindParam(":kategori", $kategori , PDO::PARAM_STR);
		$data = $cmdmarried->queryAll();
		
		foreach($data as $gdx=>$grow){
			$nama_brand[$grow['brand']]['label']= '"'.$grow['brand'].'"';
			$nama_brand[$grow['brand']]['valuelajang'] = $grow['lajang'];
			$nama_brand[$grow['brand']]['valuemenikah'] = $grow['menikah'];
			$nama_brand[$grow['brand']]['valuepernahmenikah'] = $grow['pernah_menikah'];
		}	
		
		foreach($nama_brand as $abrand){
			$label[$abrand['label']] = $abrand['label'];
			$lajang[$abrand['label']] = $abrand['valuelajang'];
			$menikah[$abrand['label']] = $abrand['valuemenikah'];
			$pernah_menikah[$abrand['label']] = $abrand['valuepernahmenikah'];
		}	
		
		$label_brand_lajang = 'Lajang';
		$label_brand_menikah = 'Menikah';
		$label_brand_pernah_menikah = 'Pernah Menikah';
		
		$gimplode = array();
		$gimplode['brand'] = implode(',', $label);
		$gimplode['label_brand_lajang'] = $label_brand_lajang;
		$gimplode['persen_brand_lajang'] = implode(',', $lajang);
		$gimplode['label_brand_menikah'] = $label_brand_menikah;
		$gimplode['persen_brand_menikah'] = implode(',', $menikah);
		$gimplode['label_brand_pernah_menikah'] = $label_brand_pernah_menikah;
		$gimplode['persen_brand_pernah_menikah'] = implode(',', $pernah_menikah);
		
		return $gimplode;
	}
	
	public static function getQueryAge($tahun, $bulan, $kategori, $brand){
		$arrv = explode(',', $brand);
		foreach ($arrv as $value) {
			$bbrand[] = '"'.$value.'"';
			$nama_brand[$value]['label'] = '"'.$value.'"';
		}
		
		$brand = implode(',', $bbrand);
		
		$sqlage = 'SELECT a.brand, a.kategori, 
		round(a.rangeumur1/(a.rangeumur1 + a.rangeumur2 + a.rangeumur3 + a.rangeumur4 + a.rangeumur5) * 100) rangeumur1,
		round(a.rangeumur2/(a.rangeumur1 + a.rangeumur2 + a.rangeumur3 + a.rangeumur4 + a.rangeumur5) * 100) rangeumur2,
		round(a.rangeumur3/(a.rangeumur1 + a.rangeumur2 + a.rangeumur3 + a.rangeumur4 + a.rangeumur5) * 100) rangeumur3,
		round(a.rangeumur4/(a.rangeumur1 + a.rangeumur2 + a.rangeumur3 + a.rangeumur4 + a.rangeumur5) * 100) rangeumur4,
		round(a.rangeumur5/(a.rangeumur1 + a.rangeumur2 + a.rangeumur3 + a.rangeumur4 + a.rangeumur5) * 100) rangeumur5,
		a.tahun, a.bulan
		FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) < "15" OR e.value IS NULL THEN 1 ELSE "0" END) rangeumur1,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "15" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "24" THEN 1 ELSE "0" END) rangeumur2,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "25" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "34" THEN 1 ELSE "0" END) rangeumur3,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "35" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "44" THEN 1 ELSE "0" END) rangeumur4,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) > "44" THEN 1 ELSE "0" END) rangeumur5,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			JOIN receipt_retail c ON c.id_retail = a.id_receipt_retail
			JOIN user_upload_data d ON d.id = c.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 16 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) < "15" OR e.value IS NULL THEN 1 ELSE "0" END) rangeumur1,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "15" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "24" THEN 1 ELSE "0" END) rangeumur2,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "25" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "34" THEN 1 ELSE "0" END) rangeumur3,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "35" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "44" THEN 1 ELSE "0" END) rangeumur4,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) > "44" THEN 1 ELSE "0" END) rangeumur5,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 16 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_maskapai
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori, 
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) < "15" OR e.value IS NULL THEN 1 ELSE "0" END) rangeumur1,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "15" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "24" THEN 1 ELSE "0" END) rangeumur2,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "25" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "34" THEN 1 ELSE "0" END) rangeumur3,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) >= "35" AND YEAR(CURDATE()) - SUBSTR(e.value, -4) <= "44" THEN 1 ELSE "0" END) rangeumur4,
			SUM(CASE WHEN YEAR(CURDATE()) - SUBSTR(e.value, -4) > "44" THEN 1 ELSE "0" END) rangeumur5,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 16 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_ka
		) a
		WHERE a.tahun = :tahun AND a.bulan = :bulan AND a.kategori = :kategori AND a.brand IN ('.$brand.')';
		
		$cmdage = Yii::app()->db->createCommand($sqlage);
		$cmdage->bindParam(":tahun", $tahun , PDO::PARAM_STR);
		$cmdage->bindParam(":bulan", $bulan , PDO::PARAM_STR);
		$cmdage->bindParam(":kategori", $kategori , PDO::PARAM_STR);
		$data = $cmdage->queryAll();
		
		foreach($data as $gdx=>$grow){
			$nama_brand[$grow['brand']]['label']= '"'.$grow['brand'].'"';
			$nama_brand[$grow['brand']]['valueumur1'] = $grow['rangeumur1'];
			$nama_brand[$grow['brand']]['valueumur2'] = $grow['rangeumur2'];
			$nama_brand[$grow['brand']]['valueumur3'] = $grow['rangeumur3'];
			$nama_brand[$grow['brand']]['valueumur4'] = $grow['rangeumur4'];
			$nama_brand[$grow['brand']]['valueumur5'] = $grow['rangeumur5'];
		}	
		
		foreach($nama_brand as $abrand){
			$label[$abrand['label']] = $abrand['label'];
			$umur1[$abrand['label']] = $abrand['valueumur1'];
			$umur2[$abrand['label']] = $abrand['valueumur2'];
			$umur3[$abrand['label']] = $abrand['valueumur3'];
			$umur4[$abrand['label']] = $abrand['valueumur4'];
			$umur5[$abrand['label']] = $abrand['valueumur5'];
		}	
		
		$label_brand_range_umur_1 = '15 and below';
		$label_brand_range_umur_2 = '15 - 24';
		$label_brand_range_umur_3 = '25 - 34';
		$label_brand_range_umur_4 = '35 - 44';
		$label_brand_range_umur_5 = '44 and above';
		
		$gimplode['brand'] = implode(',', $label);
		$gimplode['label_brand_range_umur_1'] = $label_brand_range_umur_1;
		$gimplode['persen_brand_range_umur_1'] = implode(',', $umur1);
		$gimplode['label_brand_range_umur_2'] = $label_brand_range_umur_2;
		$gimplode['persen_brand_range_umur_2'] = implode(',', $umur2);
		$gimplode['label_brand_range_umur_3'] = $label_brand_range_umur_3;
		$gimplode['persen_brand_range_umur_3'] = implode(',', $umur3);
		$gimplode['label_brand_range_umur_4'] = $label_brand_range_umur_4;
		$gimplode['persen_brand_range_umur_4'] = implode(',', $umur4);
		$gimplode['label_brand_range_umur_5'] = $label_brand_range_umur_5;
		$gimplode['persen_brand_range_umur_5'] = implode(',', $umur5);
	
		return $gimplode;
	}
	
	public static function getQueryReligion($tahun, $bulan, $kategori, $brand){
		$arrv = explode(',', $brand);
		foreach ($arrv as $value) {
			$bbrand[] = '"'.$value.'"';
			$nama_brand[$value]['label'] = '"'.$value.'"';
		}
		
		$brand = implode(',', $bbrand);
		
		$sqlreligion = 'SELECT a.brand, a.kategori, 
		round(a.islam/(a.islam+ a.kristen + a.katolik+ a.hindu + a.budha) * 100) islam,
		round(a.kristen/(a.islam+ a.kristen + a.katolik+ a.hindu + a.budha) * 100) kristen,
		round(a.katolik/(a.islam+ a.kristen + a.katolik+ a.hindu + a.budha) * 100) katolik,
		round(a.hindu/(a.islam+ a.kristen + a.katolik+ a.hindu + a.budha) * 100) hindu,
		round(a.budha/(a.islam+ a.kristen + a.katolik+ a.hindu + a.budha) * 100) budha,
		a.tahun, a.bulan
		FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori,
			SUM(CASE WHEN e.value = 8 THEN 1 ELSE 0 END) islam,
			SUM(CASE WHEN e.value = 9 THEN 1 ELSE 0 END) kristen,
			SUM(CASE WHEN e.value = 10 THEN 1 ELSE 0 END) katolik,
			SUM(CASE WHEN e.value = 11 THEN 1 ELSE 0 END) hindu,
			SUM(CASE WHEN e.value = 12 THEN 1 ELSE 0 END) budha,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			JOIN receipt_retail c ON c.id_retail = a.id_receipt_retail
			JOIN user_upload_data d ON d.id = c.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 6 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori,
			SUM(CASE WHEN e.value = 8 THEN 1 ELSE 0 END) islam,
			SUM(CASE WHEN e.value = 9 THEN 1 ELSE 0 END) kristen,
			SUM(CASE WHEN e.value = 10 THEN 1 ELSE 0 END) katolik,
			SUM(CASE WHEN e.value = 11 THEN 1 ELSE 0 END) hindu,
			SUM(CASE WHEN e.value = 12 THEN 1 ELSE 0 END) budha,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 6 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_maskapai
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori, 
			SUM(CASE WHEN e.value = 8 THEN 1 ELSE 0 END) islam,
			SUM(CASE WHEN e.value = 9 THEN 1 ELSE 0 END) kristen,
			SUM(CASE WHEN e.value = 10 THEN 1 ELSE 0 END) katolik,
			SUM(CASE WHEN e.value = 11 THEN 1 ELSE 0 END) hindu,
			SUM(CASE WHEN e.value = 12 THEN 1 ELSE 0 END) budha,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 6 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_ka
		) a
		WHERE a.tahun = :tahun AND a.bulan = :bulan AND a.kategori = :kategori AND a.brand IN ('.$brand.')';
		
		$cmdreligion = Yii::app()->db->createCommand($sqlreligion);
		$cmdreligion->bindParam(":tahun", $tahun , PDO::PARAM_STR);
		$cmdreligion->bindParam(":bulan", $bulan , PDO::PARAM_STR);
		$cmdreligion->bindParam(":kategori", $kategori , PDO::PARAM_STR);
		$data = $cmdreligion->queryAll();
		
		foreach($data as $gdx=>$grow){
			$nama_brand[$grow['brand']]['label']= '"'.$grow['brand'].'"';
			$nama_brand[$grow['brand']]['valueagama1'] = $grow['islam'];
			$nama_brand[$grow['brand']]['valueagama2'] = $grow['kristen'];
			$nama_brand[$grow['brand']]['valueagama3'] = $grow['katolik'];
			$nama_brand[$grow['brand']]['valueagama4'] = $grow['hindu'];
			$nama_brand[$grow['brand']]['valueagama5'] = $grow['budha'];
		}	
		
		foreach($nama_brand as $abrand){
			$label[$abrand['label']] = $abrand['label'];
			$agama1[$abrand['label']] = $abrand['valueagama1'];
			$agama2[$abrand['label']] = $abrand['valueagama2'];
			$agama3[$abrand['label']] = $abrand['valueagama3'];
			$agama4[$abrand['label']] = $abrand['valueagama4'];
			$agama5[$abrand['label']] = $abrand['valueagama5'];
		}
		
		$label_brand_religion_1 = 'Islam';
		$label_brand_religion_2 = 'Kristen';
		$label_brand_religion_3 = 'Katolik';
		$label_brand_religion_4 = 'Hindu';
		$label_brand_religion_5 = 'Budha';
		
		$gimplode['brand'] = implode(',', $label);
		$gimplode['label_brand_religion_1'] = $label_brand_religion_1;
		$gimplode['persen_brand_religion_1'] = implode(',', $agama1);
		$gimplode['label_brand_religion_2'] = $label_brand_religion_2;
		$gimplode['persen_brand_religion_2'] = implode(',', $agama2);
		$gimplode['label_brand_religion_3'] = $label_brand_religion_3;
		$gimplode['persen_brand_religion_3'] = implode(',', $agama3);
		$gimplode['label_brand_religion_4'] = $label_brand_religion_4;
		$gimplode['persen_brand_religion_4'] = implode(',', $agama4);
		$gimplode['label_brand_religion_5'] = $label_brand_religion_5;
		$gimplode['persen_brand_religion_5'] = implode(',', $agama5);

		return $gimplode;
	}
	
	public static function getQuerySalary($tahun, $bulan, $kategori, $brand){
		$arrv = explode(',', $brand);
		foreach ($arrv as $value) {
			$bbrand[] = '"'.$value.'"';
			$nama_brand[$value]['label'] = '"'.$value.'"';
		}
		
		$brand = implode(',', $bbrand);
		
		$sqlsalary = 'SELECT a.brand, a.kategori, 
		round(a.salary1/(a.salary1+ a.salary2 + a.salary3+ a.salary4 + a.salary5 + a.salary6) * 100) salary1,
		round(a.salary2/(a.salary1+ a.salary2 + a.salary3+ a.salary4 + a.salary5 + a.salary6) * 100) salary2,
		round(a.salary3/(a.salary1+ a.salary2 + a.salary3+ a.salary4 + a.salary5 + a.salary6) * 100) salary3,
		round(a.salary4/(a.salary1+ a.salary2 + a.salary3+ a.salary4 + a.salary5 + a.salary6) * 100) salary4,
		round(a.salary5/(a.salary1+ a.salary2 + a.salary3+ a.salary4 + a.salary5 + a.salary6) * 100) salary5,
		round(a.salary6/(a.salary1+ a.salary2 + a.salary3+ a.salary4 + a.salary5 + a.salary6) * 100) salary6,
		a.tahun, a.bulan
		FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori,
			SUM(CASE WHEN e.value <= 1000000 OR e.value IS NULL THEN 1 ELSE 0 END) salary1,
			SUM(CASE WHEN e.value > 1000000 AND e.value <= 3000000  THEN 1 ELSE 0 END) salary2,
			SUM(CASE WHEN e.value >3000000 AND e.value <= 5000000 THEN 1 ELSE 0 END) salary3,
			SUM(CASE WHEN e.value > 5000000 AND e.value <= 8000000 THEN 1 ELSE 0 END) salary4,
			SUM(CASE WHEN e.value > 8000000 AND e.value <= 15000000 THEN 1 ELSE 0 END) salary5,
			SUM(CASE WHEN e.value > 15000000 THEN 1 ELSE 0 END) salary6,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			JOIN receipt_retail c ON c.id_retail = a.id_receipt_retail
			JOIN user_upload_data d ON d.id = c.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 10 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori,
			SUM(CASE WHEN e.value <= 1000000 OR e.value IS NULL THEN 1 ELSE 0 END) salary1,
			SUM(CASE WHEN e.value > 1000000 AND e.value <= 3000000  THEN 1 ELSE 0 END) salary2,
			SUM(CASE WHEN e.value >3000000 AND e.value <= 5000000 THEN 1 ELSE 0 END) salary3,
			SUM(CASE WHEN e.value > 5000000 AND e.value <= 8000000 THEN 1 ELSE 0 END) salary4,
			SUM(CASE WHEN e.value > 8000000 AND e.value <= 15000000 THEN 1 ELSE 0 END) salary5,
			SUM(CASE WHEN e.value > 15000000 THEN 1 ELSE 0 END) salary6,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 10 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_maskapai
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori, 
			SUM(CASE WHEN e.value <= 1000000 OR e.value IS NULL THEN 1 ELSE 0 END) salary1,
			SUM(CASE WHEN e.value > 1000000 AND e.value <= 3000000  THEN 1 ELSE 0 END) salary2,
			SUM(CASE WHEN e.value >3000000 AND e.value <= 5000000 THEN 1 ELSE 0 END) salary3,
			SUM(CASE WHEN e.value > 5000000 AND e.value <= 8000000 THEN 1 ELSE 0 END) salary4,
			SUM(CASE WHEN e.value > 8000000 AND e.value <= 15000000 THEN 1 ELSE 0 END) salary5,
			SUM(CASE WHEN e.value > 15000000 THEN 1 ELSE 0 END) salary6,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan
			FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 10 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_ka
		) a
		WHERE a.tahun = :tahun AND a.bulan = :bulan AND a.kategori = :kategori AND a.brand IN ('.$brand.')';
		
		$cmdsalary = Yii::app()->db->createCommand($sqlsalary);
		$cmdsalary->bindParam(":tahun", $tahun , PDO::PARAM_STR);
		$cmdsalary->bindParam(":bulan", $bulan , PDO::PARAM_STR);
		$cmdsalary->bindParam(":kategori", $kategori , PDO::PARAM_STR);
		$data = $cmdsalary->queryAll();
		
		foreach($data as $gdx=>$grow){
			$nama_brand[$grow['brand']]['label']= '"'.$grow['brand'].'"';
			$nama_brand[$grow['brand']]['valuesalary1'] = $grow['salary1'];
			$nama_brand[$grow['brand']]['valuesalary2'] = $grow['salary2'];
			$nama_brand[$grow['brand']]['valuesalary3'] = $grow['salary3'];
			$nama_brand[$grow['brand']]['valuesalary4'] = $grow['salary4'];
			$nama_brand[$grow['brand']]['valuesalary5'] = $grow['salary5'];
			$nama_brand[$grow['brand']]['valuesalary6'] = $grow['salary6'];
		}	
		
		foreach($nama_brand as $abrand){
			$label[$abrand['label']] = $abrand['label'];
			$salary1[$abrand['label']] = $abrand['valuesalary1'];
			$salary2[$abrand['label']] = $abrand['valuesalary2'];
			$salary3[$abrand['label']] = $abrand['valuesalary3'];
			$salary4[$abrand['label']] = $abrand['valuesalary4'];
			$salary5[$abrand['label']] = $abrand['valuesalary5'];
			$salary6[$abrand['label']] = $abrand['valuesalary6'];
		}
		
		$label_brand_salary_1 = '1000000 and below';
		$label_brand_salary_2 = '> 1000000 - 3000000';
		$label_brand_salary_3 = '> 3000000 - 5000000';
		$label_brand_salary_4 = '> 5000000 - 8000000';
		$label_brand_salary_5 = '> 8000000 - 15000000';
		$label_brand_salary_6 = '> 15000000';
		
		$gimplode = array();
		$gimplode['brand'] = implode(',', $label);
		$gimplode['label_brand_salary_1'] = $label_brand_salary_1;
		$gimplode['persen_brand_salary_1'] = implode(',', $salary1);
		$gimplode['label_brand_salary_2'] = $label_brand_salary_2;
		$gimplode['persen_brand_salary_2'] = implode(',', $salary2);
		$gimplode['label_brand_salary_3'] = $label_brand_salary_3;
		$gimplode['persen_brand_salary_3'] = implode(',', $salary3);
		$gimplode['label_brand_salary_4'] = $label_brand_salary_4;
		$gimplode['persen_brand_salary_4'] = implode(',', $salary4);
		$gimplode['label_brand_salary_5'] = $label_brand_salary_5;
		$gimplode['persen_brand_salary_5'] = implode(',', $salary5);
		$gimplode['label_brand_salary_6'] = $label_brand_salary_6;
		$gimplode['persen_brand_salary_6'] = implode(',', $salary6);

		return $gimplode;
	}
	
	public static function getQueryKota($tahun, $bulan, $kategori, $brand){
		$sqlkota = 'SELECT a.brand, a.kategori, a.tahun, a.bulan, CASE WHEN a.value IS NULL THEN "NO DATA CITY" ELSE a.value END kota, a.totaluser
		FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan, e.value, count(a.nama_barang) totaluser
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			JOIN receipt_retail c ON c.id_retail = a.id_receipt_retail
			JOIN user_upload_data d ON d.id = c.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 17 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_barang, e.value
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori,
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan, e.value, count(a.nama_maskapai) totaluser
			FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 17 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_maskapai, e.value
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori, 
			YEAR(a.created_date) tahun, MONTH(a.created_date) bulan, e.value, count(a.nama_ka) totaluser
			FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			JOIN user_upload_data d ON d.id = b.id_user_upload_data
			LEFT JOIN user_personal_information e ON e.id_user = d.id_user AND e.id_personal_information = 17 AND e.begda <= NOW() AND e.endda >= NOW()
			GROUP BY a.nama_ka, e.value
		) a
		WHERE a.tahun = :tahun AND a.bulan = :bulan AND a.kategori = :kategori AND a.brand = :brand
		ORDER BY a.totaluser DESC
		LIMIT 5
		';
		
		$cmdkota = Yii::app()->db->createCommand($sqlkota);
		$cmdkota->bindParam(":tahun", $tahun , PDO::PARAM_STR);
		$cmdkota->bindParam(":bulan", $bulan , PDO::PARAM_STR);
		$cmdkota->bindParam(":kategori", $kategori , PDO::PARAM_STR);
		$cmdkota->bindParam(":brand", $brand , PDO::PARAM_STR);
		$data = $cmdkota->queryAll();
		
		$labelbrand['label'] = '"'.$brand.'"';
		
		foreach($data as $gdx=>$grow){
			$nama_brand[$gdx]['kota'] = $grow['kota'];			
			$nama_brand[$gdx]['totaluser'] = $grow['totaluser'];			
		}	
		
		foreach($data as $gdx=>$grow){
			$alluser['alluser'] += $grow['totaluser'];
			$labelbrand['label']= '"'.$grow['brand'].'"';
		}	
		
		foreach($nama_brand as $adx=>$abrand){
			$kota[$abrand['kota']]['city'] = $abrand['kota'];
			$percentage[$abrand['kota']] = round($abrand['totaluser'] / $alluser['alluser'] * 100);
			if($adx == 0){
				$kota[$abrand['kota']]['color'] = '#db4234';
			}else if($adx == 1){
				$kota[$abrand['kota']]['color'] = '#edac1c';
			}else if($adx == 2){
				$kota[$abrand['kota']]['color'] = '#6eca28';
			}else if($adx == 3){
				$kota[$abrand['kota']]['color'] = '#9968a3';	
			}else if($adx == 4){
				$kota[$abrand['kota']]['color'] = '#f5951f';
			}
		}
		
		$gimplode = array();
		$gimplode['brand'] = $labelbrand;
		$gimplode['label'] = $kota;
		$gimplode['color'] = $color;
		$gimplode['percentage'] = implode(',', $percentage);
		
		return $gimplode;
	}
	
	public static function getQueryTampilBrand($kategori, $brand){
		$arrv = explode(',', $brand);
		foreach ($arrv as $value) {
			$bbrand[] = '"'.$value.'"';
		}
		
		$brand = implode(',', $bbrand);
		
		$sqlfilter = 'SELECT a.brand, a.kategori FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			GROUP BY a.nama_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori
			FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			GROUP BY a.nama_maskapai
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type kategori
			FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			GROUP BY a.nama_ka
		) a
		WHERE a.kategori = :kategori AND a.brand IN ('.$brand.')';
		
		$cmdfilter = Yii::app()->db->createCommand($sqlfilter);
		$cmdfilter->bindParam(":kategori", $kategori , PDO::PARAM_STR);
		$data = $cmdfilter->queryAll();
		
		$array = array();
		foreach($data as $row){
			$array[$row['brand']] = $row['brand'];
		}	
		
		$tampil = implode(',', $array);
		return $tampil;
	}	
	
	
}

?>