<?php

class KirimEmailMailchimp {
    public static function getConfig()
    {
		
		$apikey = '1534c2cd2d551d7f70b56c002ab6d6b3-us18';
		$auth = base64_encode( 'user:'.$apikey );
		
		return $auth ;
    }
	
	
	public static function getDefaultIDCampaignForTemplate()
    {
		
		$campaign_id = 'd406a673a0';
		
		return $campaign_id ;
    }
	
	
	public static function getAllList()
    {
		$auth = self::getConfig();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/lists');
		//curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/lists/7f42bce919');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
		
		return self::returnResult($curl);
		
    }
	
	public static function getCampaignById($id)
    {
		$auth = self::getConfig();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/campaigns/'.$id);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
		
		return self::returnResult($curl);
		
    }
	
	public static function getCampaignContentById($id)
    {
		$auth = self::getConfig();
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/campaigns/'.$id.'/content');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
		
		return self::returnResult($curl);
		
    }
	
	
	public static function createList($newList,$from_name,$from_email,$subject)
    {
		$auth = self::getConfig();
		$data = array(
			'name'        => $newList,
			'contact' 	  => Array(
								'company' => 'Bagi Data',
								'address1' => 'Jl. Kebon Sirih No. 12, Kebon Sirih, Menteng, Jakarta Pusat',
								'address2' => '',
								'city' => 'Jakarta Pusat',
								'state' => '',
								'zip' => '11430',
								'country' => 'ID',
								'phone' => '',
							),
			'permission_reminder'  => 'Anda Menerima Email ini Karena anda berada di program Bagi data',
			'campaign_defaults' => Array(
						'from_name' => $from_name,
						'from_email' => $from_email,
						'subject' =>$subject, 
						'language' => 'en',
					),
			'email_type_option'=>true,
			
			);
		
		$json_data = json_encode($data);
		
	
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/lists');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
		
		return self::returnResult($curl);
		
    }
	
	public static function createSubscriberToList($listID,$email)
    {
		$auth = self::getConfig();
		$data = array(
			'members' 	  => array(
							
							Array(
								'email_address' => $email,
								'status' => 'subscribed',
							), 
							
			
			),
			'update_existing'=>true,
			);
		
		$json_data = json_encode($data);
		
	
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/lists/'.$listID);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data); 
		
		return self::returnResult($curl);
    }
	
	
	public static function createTemplate($name,$content)
    {
		$auth = self::getConfig();
		
		$data = array(
			'name'        => $name,
			'html'		=>$content,
			);
		
		$json_data = json_encode($data);
		
	
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/templates');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data); 
		
		return self::returnResult($curl);
    }
	
	
	public static function createBroadcast($listID,$from_email,$from_name,$title,$subject,$content,$templateID)
    {
		
		$auth = self::getConfig();
		$data = array(
			'type'        => 'regular',
			'recipients' 	  => Array(
								'list_id' => $listID,
								
							),
			'settings' => Array(
						'subject_line' => $subject,
						'preview_text' => $subject,
						'title' => $title,
						'from_name' => $from_name,
						'reply_to' => $from_email,
						'template_id'=>$templateID,
					
					),
			
			);
		
		$json_data = json_encode($data);
		
	
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/campaigns');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data); 
		
		
		return self::returnResult($curl);
		
	
    }
	
	
	public static function sendChecklist($broadcastID)
    {
		
		$auth = self::getConfig();
		$campaignID = $broadcastID;
		
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/campaigns/'.$campaignID.'/send-checklist');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		
		
		return self::returnResult($curl);
		
	
    }
	
	public static function sendBroadcast($broadcastID)
    {
		
		$auth = self::getConfig();
		$campaignID = $broadcastID;
		
		$data = array(
			);
		
		$json_data = json_encode($data);
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, 'https://us18.api.mailchimp.com/3.0/campaigns/'.$campaignID.'/actions/send');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			'Authorization: Basic '.$auth));
			
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data); 
		
		
		return self::returnResult($curl);
		
	
    }
	
	
	public static function returnResult($curl)
    {
		
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		$result = array();
		if ($err) {
		  //echo "cURL Error #:" . $err;
		  $result['status'] = 'error';
		  $result['message'] = $err;
		  
		} else {
		  $result['status'] = 'success';
		  $result['data'] = CJSON::decode($response);
		}
		
		/* echo '<pre>';
		print_r($result['data']);
		
		if ($result['status']=='error' || (isset($result['data']['type']) && $result['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){
			$return = false;
		}else{
			$return = true;
		}  */
		
		
		return $result;
	}
	
	
	
	
}