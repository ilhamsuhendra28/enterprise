<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	 private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$cuser = new CDbCriteria;
		$cuser->condition = 'no_hp = :param1 OR email = :param1';
		$cuser->params = array(':param1'=>$this->username);
		$users = UserEnterprise::model()->find($cuser);
		if(($users !== null && $this->password == 'bagidata') || ($this->password == Logic::getDecrypt($users->password))){
			$this->_id = $users->id;
			$this->username = $users->full_name;
            $this->errorCode = self::ERROR_NONE;
		}	

        return !$this->errorCode;
	}
	
	public function getId() {
        return $this->_id;
    }
}