<?php
class Finpay {
    private $merchant_id = 'BD909';
    private $merchant_password = '07BD90918';
    private $url = 'https://sandbox.finpay.co.id/servicescode/api/apiFinpay.php';
    public $param;
    public $paymentCode;
    public $statusCode;
    public $statusDesc;
    private $json;
    private $requested = 0;
    private $success = 0;
    
    function __construct($param) {
        $this->param = $param;
    }
    
    private function getRequestJson()
    {
        $this->param['mer_signature'] = $this->merSignature($param);
        $this->json = CJSON::encode($this->param);
        return $this->json;
    }
    
    
    private function merSignature()
    {
        $sparator = '%';
        $source = strtoupper(implode($sparator, $this->param)).$sparator.$this->merchant_password;
        return hash('sha256',$source);        
    }
    
    public function getJsonText()
    {
        $this->getRequestJson();
        return $this->json;
    }
    
    public function requestPaymentCode()
    {
        $ch = curl_init( $this->url );
        # Setup request to send json via POST.
        $payload = $this->getRequestJson();
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            $this->statusCode = '0';
            $this->statusDesc = 'Error Curl : '.$err;
            $this->paymentCode = null;
		} else {
            $response = CJSON::decode($result);
            $this->statusCode = $response['status_code'];
            $this->statusDesc = $response['status_desc'];
            $this->paymentCode = $response['payment_code'];
            $this->success = 1;
		}
        $this->requested++;
        return $this->paymentCode;
    }
    
    
}

