<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'BagiData',

	// preloading 'log' component
	'preload'=>array('log'),
	'theme'=>'ent',
	'defaultController'=>'site/login',
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.YiiMailer.*',
		'ext.yiiext.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'bagidata',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'securityManager'=>array(
            'cryptAlgorithm' => 'rijndael-256',
            'encryptionKey' => 'abcdefghijklmnop',
        ),
		 'mail'=>array(
		 	'class'=>'ext.yii-mail.YiiMail',
		 	'transportType' => 'php',
		 	'transportType'=>'smtp',
		 	'transportOptions'=>array(
		 		'host'=>'mailer.bagidata.com',
				'username'=>'noreplay@mailer.bagidata.com',
				'password'=>'MB3BVaxHa6',			
		 	),			
		 	'logging'=>true,
		 	'dryRun'=>false
		 ),
		 'shoppingCart' =>
			array(
			'class' => 'ext.yiiext.EShoppingCart',
		 ),
		 
		 'mailBlast'=>array(
		 	'class'=>'ext.YiiMailer.YiiMailer',
		 	'transportType' => 'php',
		 	'transportType'=>'smtp',
		 	'transportOptions'=>array(
		 		'host'=>'mailer.bagidata.com',
				'username'=>'noreplay@mailer.bagidata.com',
				'password'=>'MB3BVaxHa6',			
			),			
		 	'logging'=>true,
		 	'dryRun'=>false
		 ),
		 
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>array('/site/login'),
		),

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'showScriptName'=>false,
			'urlFormat'=>'path',
			'rules'=>array(
				array(
					'class' => 'application.components.GlobalEncryptDecrypt',
				),
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'request' => array(
			//'class' => 'application.components.HttpRequest',
			'class'=>'application.components.MyHttpRequestWithDisableValidation',
			'noCsrfValidationRoutes'=>array('cron/paymentResponseCart',),
			'enableCsrfValidation' => true,
		    'enableCookieValidation'=>true,
		),
		
		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
//				array(
//                    'class'=>'CWebLogRoute',
//                    'levels'=>'trace, info, error, warning',
//                ),
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
