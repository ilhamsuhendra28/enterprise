<?php
/* @var $this CampaignController */
/* @var $data Campaign */
?>
<?php 

$no = $widget->dataProvider->getPagination()->currentPage * $widget->dataProvider->getPagination()->pageSize + $index + 1;

$url = Yii::app()->createUrl('campaign/view',array('id'=> $data['id'],'type' => $type));
?>
<div class="col m12 con-check no-p" data-url="<?php echo $url?>">
	<div class="col m7 no-p">
		<div class="con-num-count valign-wrapper left">
			<div class="center-cont">
				<div class="num"><?php echo $no ?></div>
				<div class="check hidden"><i class="fa fa-check"></i></div>
			</div>
		</div>
		<div class="con left">
			<h5 class="title no-m">
				<?php 
					echo $data['title'] ;
				?>
			</h5>
			<div class="left">
				<p style="padding-right: 14px;">
					<small class="super-small">Subject</small> <br>
					<small>
						
						<?php 
							//echo $data['subject'] ;
							$subject = $data['subject'];
							if(strlen($subject) > 30){
								echo substr($subject,0,30).'...';
							}else{
								echo $subject;
							}
						?>
					</small>
				</p>
			</div>
			<div class="left">
				<p style="padding-right: 14px;">
					<small class="super-small">Sent Type</small> <br>
					<small>
						<?php 
							$type = MyAppComponent::getCampaignTYpe($data['sent_type']);
							echo $type;
						?>
					</small>
				</p>
			</div>
			
			<div class="left">
				<p style="padding-right: 14px;">
					<small class="super-small">Created Date</small> <br>
					<small><?php echo $data['created_date'] ?></small>
				</p>
			</div>
			<?php /*
			<div class="left">
				<p style="padding-right: 14px;">
					<small class="super-small">Post Date</small> <br>
					<small><?php echo $data['post_date'] ?></small>
				</p>
			</div>
			<div class="left" style="padding-left: 40px;">
				<p>
					<small class="super-small">Post Time</small> <br>
					<small><?php echo $data['post_time'] ?></small>
				</p>
			</div>
			<br><br>
			*/ ?>
			<?php /*
			<div class="left">
				<p>
					<small class="super-small">status</small> <br>
					<?php if ($data['status'] ==1) { ?>
						<small class="success small-btn">Active</small>
					<?php } else { ?>
						<small class="red small-btn">Not Active</small>
					<?php } ?>
				</p>
			</div>
			*/ ?>
		</div>
	</div>
	<div class="col m5">
		
		<div class="col m4">
		<div class="con-loc  valign-wrapper">
			<div class="center-cont">
				<h6 class="color-grey no-m">Status</h6>
				<div class="countage color-blue">
					<b>
						<?php 
							echo $data->statusCampaign;
						?>
					</b>
				</div>
			</div>
		</div>
		</div>
		
		<div class="col m4">
		<div class="con-loc  valign-wrapper">
			<div class="center-cont">
				<h6 class="color-grey no-m">Total Sent</h6>
				<div class="countage color-purple"><b><?php echo $data['total_sent']?></b></div>
			</div>
		</div>
		</div>
		
		
		<div class="col m4">
		<div class="con-loc  valign-wrapper">
			<div class="center-cont">
				<h6 class="color-grey no-m">Status Approval</h6>
				<div class="countage color-black">
					<b>
						<?php 
							//echo $data['approval'];
							echo MyAppComponent::getStatusApproval($data['approval']);
						?>
					</b>
				</div>
			</div>
		</div>
		</div>
		
		
		<?php /*
		<div class="col m3">
		<div class="con-loc  valign-wrapper">
			<div class="center-cont">
				<h6 class="color-grey no-m">Total Views</h6>
				<div class="countage color-purple"><b>2300</b></div>
			</div>
		</div>
		</div>
		<div class="col m4">
		<div class="con-loc  valign-wrapper">
			<div class="center-cont">
				<h6 class="color-grey no-m">Total Clicks</h6>
				<div class="countage color-blue"><b>1102</b></div>
			</div>
		</div>
		</div>
		<div class="col m5">
		<div class="con-loc  valign-wrapper">
			<div class="center-cont">
				<h6 class="color-grey no-m">Total Buyer</h6>
				<div class="countage color-orange"><b>1051</b></div>
			</div>
		</div>
		</div>
		*/ ?>
	</div>
</div>
			
<?php /*			
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_blast')); ?>:</b>
	<?php echo CHtml::encode($data->email_blast); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_blast')); ?>:</b>
	<?php echo CHtml::encode($data->sms_blast); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_date')); ?>:</b>
	<?php echo CHtml::encode($data->post_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_date); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('post_time')); ?>:</b>
	<?php echo CHtml::encode($data->post_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	

</div>
*/ ?>