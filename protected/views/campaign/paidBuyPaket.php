<title>Enterprise | Campaign Paket Cart</title>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>



<div class="row row-seg">

	<div class="col m12">
		<h5>Campaign Paket Cart </h5>	
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
		
	</div>
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content" style="min-height:400px">
				<h5>Order Info </h5>
				<?php 
					$orderInfo = CJSON::decode($model->json);
					//print_r($orderInfo);
				?>
				
				
				<?php if ($model->masterPaket->is_free == 0){ ?>
					<div class="col m6">Info</div><div class="col m6">: <?= $orderInfo['add_info1'];?></div>
					<div class="col m6">amount</div><div class="col m6">: <?= $orderInfo['amount'];?></div>
					<div class="col m6">cust Name</div><div class="col m6">: <?= $orderInfo['cust_name'];?></div>
					<div class="col m6">cust email</div><div class="col m6">: <?= $orderInfo['cust_email'];?></div>
					<div class="col m6">cust msisdn</div><div class="col m6">: <?= $orderInfo['cust_msisdn'];?></div>
					<div class="col m6">transaction invoice</div><div class="col m6">: <?= $orderInfo['invoice'];?></div>
					<div class="col m6">transaction date</div><div class="col m6">: <?= $orderInfo['trans_date'];?></div>
				<?php }else { ?>
					<div class="col m6">Info</div><div class="col m6">: <?= $model->masterPaket->nama_paket;?></div>
				<?php } ?>
				<div class="col m6">transaction code</div><div class="col m6">: <?= $model['kode_transaksi'];?></div>
				<div class="col m6" style="margin-bottom:50px">Status</div><div class="col m6" style="margin-bottom:50px">: <?= $model->statusString;?></div>
				
				<?php if ($model->masterPaket->is_free == 0){ ?>
					<ol >
						<b>How to pay </b>
							  <li>Login to your Internet Banking (App &amp; Web)</li>
							  <li>Select “Pay” menu and select “Multi Payment”</li>
							  <li>Select the account you want to use then the service provider selected “FinPay”</li>
							  <li>In the Pay Code column enter the account virtual number (VA) sent via Email / SMS</li>
							  <li>Click “Continue”</li>
							  <li>Put a checklist next to the Currency column</li>
							  <li>Enter ‘Mandiri PIN’ for Confirmation (APPLI Method 1)</li>
							  <li>Click Submit</li>
					</ol>
				<?php } ?>
			
				
				<?php
					echo'<br>';
					echo '<br>';
					echo CHtml::link('<i class="icon-trash icon-white"></i> '.Yii::t('strings','Kembali Ke List Paket'),array('campaign/detailPaket'),array('class'=>'btn btn-danger','id'=>'paket-list'));
					echo '<br>';
					echo '<br>';
				?>
				
				
				<?php /*
				<form action="<?php echo Yii::app()->createAbsoluteUrl('cron/paymentResponseCart') ?>" method="POST">
				<input type ="hidden" name="payment_code" value="<?php echo $model['kode_transaksi'] ?>">
				<button class="btn btn-danger" type ="submit" value="ok">Dummy Paid</button>
				</form>
				*/ ?>
							
							
							
				
					
			</div>
		</div>
	</div>
	</div>
</div>
	
	
