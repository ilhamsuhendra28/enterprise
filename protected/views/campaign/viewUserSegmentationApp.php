<?php
/* @var $this DummyController */
/* @var $model Dummy */

/* $this->breadcrumbs=array(
	'Dummies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Dummy', 'url'=>array('index')),
	array('label'=>'Create Dummy', 'url'=>array('create')),
	array('label'=>'Update Dummy', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Dummy', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dummy', 'url'=>array('admin')),
); */
?>

<link href="<?php echo Yii::app()->request->baseUrl.'/css/widget/gridview/styles.css'?>" media="all" rel="stylesheet" type="text/css"/>

<title>Detail Segmentation</title>
<?php $segmen = $model ?>

<div class="row row-seg">
	<div class="col m12">
		<h5>Detail Segmentation</h5>	
	</div>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="left">
					<h6 class="text-bold"><b><?= $segmen->title;?></b></h6>
				</div>
				<br><br>
				<div class="row">					
					<div class="col m8">
						<p class="color-grey p-b-10"><?= $segmen->description;?></p>
					</div>
                    <div class="col m8">
                        <h6 class="text-bold"><b>Demographic Segmentation</b></h6>
                        <div class="col m6">Gender</div><div class="col m6">: <?= $segmen->gender->text->value == '' ? 'All' : $segmen->gender->text->value;?></div>
                        <div class="col m6">Age</div><div class="col m6">: <?= $segmen->age->value;?> Tahun</div>
                        <div class="col m6">Mariage</div><div class="col m6">: <?= $segmen->mariage->text->value == '' ? 'All' : $segmen->mariage->text->value;?></div>
                        <div class="col m6">Religion</div><div class="col m6">: <?= $segmen->religion->textRelig;?></div>
                        <div class="col m6">Salary</div><div class="col m6">: <?= $segmen->salary->value;?> Juta</div>
                    </div>
                    <div class="col m4">
                        <h6 class="text-bold"><b>Behavioural Segmentation</b></h6>
                    </div>
					<div class="col m4">
                        <p class="color-grey p-b-10"><?= str_replace('|',', ',ltrim($segmen->purchase->value, '|')) == '' ? '-' : str_replace('|',', ',ltrim($segmen->purchase->value, '|'));?></p>
					</div>
                    <div class="col m4">
                        <h6 class="text-bold"><b>Clustered User</b></h6>
                    </div>
                    <div class="col m4">
                        <h6 class="text-bold"><b><?= $segmen->countUser;?></b></h6>
					</div>
				</div>
				
				
				<?php /*
				<div class="row">
					<p class="color-grey">
						User Data Segmentation
					</p>
					<hr class="hr-grey">
					
					
					<p class="color-grey">
						Phone Number ( Allowed )
					</p>
					 <div id="dummy-grid" class="grid-view">
					 <table class="materialize-items border-right bordered striped condensed responsive-table">
						<thead>
							<tr>
								<th>No</th>
								<th>Phone Number</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 1;
								foreach($phoneNumbers as $phone){
									//echo '- '.$phone.'<br>';
									echo '<tr>';
										echo'<td>'.$no.'</td>';
										echo'<td>'.$phone.'</td>';
									echo '</tr>';
									$no++;
								}
							?>
							
						</tbody>
					</table>
					</div>

		
					<p class="color-grey">
						Email ( Allowed )
					</p>
					<div id="dummy-grid" class="grid-view">
					 <table class="materialize-items border-right bordered striped condensed responsive-table">
						<thead>
							<tr>
								<th>No</th>
								<th>Email</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$no = 1;
								foreach($emails as $email){
									//echo '- '.$email.'<br>';
									echo '<tr>';
										echo'<td>'.$no.'</td>';
										echo'<td>'.$email.'</td>';
									echo '</tr>';
									$no++;
								}
							?>
							
						</tbody>
					</table>
					</div>
				</div>
				
				*/ ?>
				
				
			</div>
		</div>
	</div>	
</div>
