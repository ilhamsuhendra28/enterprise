<?php
	$text = MyAppComponent::getTextTypeCompaign($type);
?>
<title>Enterprise | Campaign Overview ( <?php echo $text ?> )</title>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>

<?php
	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			
			$.fn.yiiListView.update('list-campaign', { 
				data: $(this).serialize()
			});
			
			return false;
		});
	");
?>



<div class="row row-seg">
	<div class="col m12">
		<h5>Campaign Overview ( <?php echo $text ?> )</h5>	
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
		
	</div>
	
	<div class ="row">
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Sent (<?php echo $year ?>)</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php echo $totalSent ?></b><br>
							<?php /*<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Kuota <?php echo $type ?></h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php 
								if($kuotaPaket){
									$field_sisa = 'total_sisa_'.$type;
									echo $kuotaPaket [$field_sisa];
								}else{
									echo 0; 
								}
							?>
							</b><br>
							<?php /*<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<?php echo CHtml::link('Detail',array('campaign/detailPaket'),array('class'=>'waves-effect waves-dark btn')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	
	<?php /*
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Views</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Click</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-blue"><b><i class="fa fa-arrow-up"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-blue.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Buyer</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-yellow"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-yellow.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	*/ ?>
	
	
	<?php /*
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/growth.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-purple">+27%</b><br>
							<small>Growth</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/click.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-blue">3521</b><br>
							<small>Click</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/love.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-yellow">321987</b><br>
							<small>Total Impression</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/price.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-green">Rp. 125.421.000</b><br>
							<small>Coast</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	*/ ?>
	
	<div class ="row">
	<div class="col m8">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Overview (<?php echo $year ?>)</h5>
				<canvas id="campaignChart"></canvas>
			</div>
		</div>
	</div>
	</div>
	
	<div class="col m8">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Overview Per Date (<span id="yearid"><?php echo $year ?></span>)</h5>
				
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'grafik-campaign-perdate',
					'htmlOptions'=>array('enctype'=>'multipart/form-data'),
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					/*'enableAjaxValidation'=>false,*/
					/* 'clientOptions'=>array('validateOnSubmit'=>true),
					'enableClientValidation'=>true, */
				)); ?>
		
				<div class="col m4">
				<?php
					echo CHtml::label('Month');
				?>
				</div>
				<div class="col m2">
				<?php
					echo CHtml::label('Start Date');
				?>
				</div>
				<div class="col m2">
				<?php
					echo CHtml::label('End Date');
				?>
				</div>
				<div class="col m4">
					-
				</div>
				
				
				<div class="col m4">
				<?php 
					echo CHtml::hiddenField('Grafik[year]',$year,array());
					echo CHtml::dropdownList('Grafik[month]',$month,MyAppComponent::getMonthData(),array('id'=>'grafik-month'));
				?>
				</div>
				<div class="col m2">
				<?php 
					//echo CHtml::textField('Grafik[start_date]',$start_date,array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker'));
					$dataDateNumner = MyAppComponent::getlistDateNumber();
					//echo CHtml::dropdownList('Grafik[start_date]',$dataDateNumner['start'],$dataDateNumner['data'],array());
					echo CHtml::dropdownList('Grafik[start_date]','01',$dataDateNumner,array());
				?>
				</div>
				<div class="col m2">
				<?php 
					//echo CHtml::textField('Grafik[end_date]',$end_date,array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker'));
					echo CHtml::dropdownList('Grafik[end_date]','31',$dataDateNumner,array());
				?>				
				</div>
				
				<div class="col m4">
					<button type="submit" class="waves-effect waves-dark btn  btn-segment" id="view-grafik-perdate">View</button>
				</div>
				
				<?php $this->endWidget(); ?>
				
				
				<canvas id="campaignChartByDate"></canvas>
			</div>
		</div>
	</div>
	
	
	<?php /*
	<div class="col m8">
		<div class="row">
			<div class="col s12">
			  <ul class="tabs">
				<li class="tab col s3"><a class="active" href="#test1">Free Smart Pro...</a></li>
				<li class="tab col s3"><a href="#test2">Test 2</a></li>
				<li class="tab col s3 disabled"><a href="#test3">+</a></li>
			  </ul>
			</div>
			<div id="test1" class="col s12">
				<canvas id="mybarChart"></canvas>
			</div>
			<div id="test2" class="col s12">Test 2</div>
		  </div>
	</div>
	*/ ?>
	
	<?php /*
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Cost Preview</h6>
				<div class="con-donat" style="text-align: center;">
					<div class="persen"><p>20% <br><small class="color-grey">Effectivenes</small></p></div>
				  <canvas id="c2" height="220"></canvas>
				</div>
			</div>
		</div>
	</div>
	*/ ?>
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					<p class="color-grey">
						All Campaign ( <?php echo $text ?> )
					</p>
					<hr class="hr-grey">

					
					<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
					<?php echo CHtml::link('New Campaign ( '.$text .' )',array('campaign/create','type'=>$type),array('class'=>'waves-effect waves-dark btn')); ?>
					<div class="search-form" style="margin-top:20px;display:none">
						<?php $this->renderPartial('_search',array(
							'model'=>$model,
						)); ?>
					</div>
					
					
					
					<?php $this->widget('zii.widgets.CListView', array(
						'id'=>'list-campaign', 
						'dataProvider'=>$dataProvider,
						'itemView'=>'_view_campaign',
						'viewData' => array('type' => $type),  
						/* 'pager'=>array(
							'cssFile'=>false
						), */
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => '',
							'lastPageLabel' => '',
							
						),
					)); ?>

				</div>
			</div>
		</div>
	</div>
	</div>
	
</div>

<?php

//-- Total Sent --//
$arrayDataOverViewTotalSent= array();
foreach($overview as $total_sent){
	//$arrayDataOverViewTotalSent[$total_sent->month_number] = $total_sent->countTotalSent; // diganti
	$monthNumber =$total_sent['month_number'];
	$arrayDataOverViewTotalSent[$monthNumber] = $total_sent['countTotalSent'];
}
//print_r($arrayDataOverViewTotalSent);

$dataOverViewTotalSent ='';
$dataOverViewTotalSent .='[';
for ($i = 1; $i <= 12; $i++) {
    if (isset($arrayDataOverViewTotalSent[$i])){
		$dataOverViewTotalSent .=$arrayDataOverViewTotalSent[$i].',';
	}else{
		$dataOverViewTotalSent .='0,';
	}
}
$dataOverViewTotalSent .=']';
//echo $dataOverViewTotalSent;
//-- end Total Sent --//


?>
<script>
if ($('#campaignChart').length ){ 		  
		var ctx = document.getElementById("campaignChart");
		var campaignChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
				datasets: [
				{
					label: "Total Sent",
					backgroundColor: "rgba(255, 255, 255, 0)",
					borderColor: "#c74793",
					pointBorderColor: "#c74793",
					pointBackgroundColor: "#c74793",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "rgba(151,187,205,1)",
					pointBorderWidth: 1,
					//data: [82, 13, 65, 21, 49, 10, 15]
					data : <?php echo $dataOverViewTotalSent ?>
				}]
			},

			options: {
				legend:{
					labels:{
						FontSize : '5px'
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}],
					xAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Post Date ( per Month )'
						}
					}]
				}
			}
		});
	} 
</script>


<?php
//-- total Sent//
$arrayoverviewTotalSentByDateLabel = array();
$arrayoverviewTotalSentByDateValue = array();
foreach ($overviewDataTotalSentByDate as $key => $data){
	$arrayoverviewTotalSentByDateLabel[]=substr($key,-2);
}
//print_r($arrayoverviewTotalSentByDateLabel);
foreach ($overviewDataTotalSentByDate as $key => $data){
	$arrayoverviewTotalSentByDateValue[]=$data;
}
//print_r($arrayoverviewTotalSentByDateValue);
$labelsTotalSentByDate = CJSON::encode($arrayoverviewTotalSentByDateLabel);
//echo $labelsTotalSentByDate;
$valuesTotalSentByDate = CJSON::encode($arrayoverviewTotalSentByDateValue);


?>




<script>
if ($('#campaignChartByDate').length ){ 		  
		var ctx = document.getElementById("campaignChartByDate");
		var campaignChartByDate = new Chart(ctx, {
			type: 'line',
			data: {
				labels: <?php echo $labelsTotalSentByDate ?>,
				datasets: [
				{
					label: "Total Sent",
					backgroundColor: "rgba(255, 255, 255, 0)",
					borderColor: "#c74793",
					pointBorderColor: "#c74793",
					pointBackgroundColor: "#c74793",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "rgba(151,187,205,1)",
					pointBorderWidth: 1,
					//data: [82, 13, 65, 21, 49, 10, 15]
					data : <?php echo $valuesTotalSentByDate ?>
				}]
			},

			options: {
				legend:{
					labels:{
						FontSize : '5px'
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}],
					xAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Post Date'
						}
					}]
				}
			}
		});
	} 
	
	
	
	$("#grafik-campaign-perdate").submit(function(){
		//alert("Submitted");
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: "<?php echo Yii::app()->createAbsoluteUrl('campaign/cOverview',array('type'=>$type))?>" ,
			type: 'POST',
			dataType:"JSON",
			data: formData,
			//async: false,
			beforeSend :function(){
				// Should you want to have a loading widget onClick
				// http://www.yiiframework.com/extension/loading/
				// Loading.show();
				/* swal({
				  title: "Please Wait",
				  text: "Reverse Document..",
				  type: "info",
				  allowOutsideClick: false,
				  showCancelButton: false, // There won't be any cancel button
				  showConfirmButton: false, // There won't be any confirm button
				  onOpen: function () {
					swal.showLoading();
				  }, 
				  allowEscapeKey:false,
				}); */
				$('#view-grafik-perdate').prop('disabled', true);
				$("#view-grafik-perdate").html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
				
			},
			complete  : function(){
				// hide the loading widget when complete
				// Loading.hide();
				//swal.close();
				$('#view-grafik-perdate').prop('disabled', false);
				$("#view-grafik-perdate").html('View');
			},
			success: function (data) {
				//alert(data);
				console.log(data);
				
				//total Sent
				var LabelTotalSent = data.totalSent.label;
				//console.log(LabelTotalSent);
				var valueTotalSent = data.totalSent.value;
				//console.log(valueTotalSent);
				
				campaignChartByDate.data.labels = LabelTotalSent;
				campaignChartByDate.data.datasets[0].data = valueTotalSent;
				//end total Sent
				

				campaignChartByDate.update();  
			},
			error: function(xhr, status, error) {
				// this will display the error callback in the modal.
				alert( xhr.status + " " +xhr.statusText + " " + xhr.responseText);
				/* swal({
				  title: xhr.status,
				  text: xhr.statusText + " " + xhr.responseText,
				  type: "error",
				});  */
			},
			cache: false,
			contentType: false,
			processData: false 
		});
		
		return false;
	});
	
</script>