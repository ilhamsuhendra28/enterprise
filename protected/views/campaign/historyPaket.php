<title>Enterprise | History Paket </title>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>



<div class="row row-seg">
	<div class="col m12">
		<h5>History Paket </h5>	
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
		
	</div>
	
	
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>History Paket </h5>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'list-paket',
						'dataProvider'=>$userPaket,
						//'filter'=>$paket,
						//'cssFile' => Yii::app()->request->baseUrl.'/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'enableSorting'=>false,
						'columns'=>array(
							array(
							'header'=>'No',   
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							array(
								'header'=>'Subscribe ID',   
								'value'=>function($data){
										return $data->id;
									},
							),
							'nama_paket',
							array(
								'header'=>'Kuota Email',   
								'value'=>function($data){
										return $data->kuota_email;
									},
							),
							array(
								'header'=>'Kuota SMS',   
								'value'=>function($data){
										return $data->kuota_sms;
									},
							),
							array(
								'header'=>'Kuota WA',   
								'value'=>function($data){
										return $data->kuota_wa;
									},
							),
							//'masa_aktif',
							array(
								'name'=>'masa_aktif',   
								'value'=>function($data){
										return $data->masa_aktif. ' Hari';
									},
							),
							'tgl_aktif',
							'tgl_kadaluarsa',
							/* array(
								'name'=>'status',   
								'value'=>function($data){
										return $data->status;
									},
							), */
							
							array(
								'header'=>'Type',   
								'type'=>'raw',
								'value'=>function($data){
										
										if($data->is_free == 1){
											return 'Free ';
										}else{
											return 'Regular';
										}	 
									},
							),
							array(
								'name'=>'harga',   
								'value'=>function($data){
										return $data->harga;
									},
							),
							
							/* array(
								'class'=>'CButtonColumn',
								'htmlOptions'=>array('style'=>'min-width:100px;padding:0px 5px 0px 5px'),
								'buttons'=>array(
									'view' => array(
										 'label'=>'<i class="Small material-icons">visibility</i>',
										 'imageUrl'=>false,
										),
									'update' => array(
										 'label'=>'<i class="Small material-icons">clear_all</i>',
										 'imageUrl'=>false,
										),
									'delete' => array(
										 'label'=>'<i class="Small material-icons">delete</i>',
										 'imageUrl'=>false,
										),
								),
							), */
						),
					)); ?>
					
					<?php
						$dataAllProvider = $paket->searchHistoryPaket();
						$dataAllProvider->setPagination(false);
						$dataAll = $dataAllProvider->getData();
						$total = 0 ;
						foreach ($dataAll as $datas){
							$total = $total + $datas['harga'];
						}
						
						echo '<b> Total yang tekah dihabiskan untuk Paket Campaign senilai : '.$total.'</b>'; 
					?>
					
			</div>
		</div>
	</div>
	</div>
	
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>History Paket Blast</h5>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'list-paket-blast',
						'dataProvider'=>$historyData,
						//'filter'=>$history,
						//'cssFile' => Yii::app()->request->baseUrl.'/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'enableSorting'=>false,
						'columns'=>array(
							array(
							'header'=>'No',   
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							array(
								'header'=>'Subscribe ID',   
								'value'=>function($data){
										return $data->subscribed_id;
									},
							),
							
							array(
								'header'=>'Nama Paket',   
								'value'=>function($data){
										return $data->nama_paket;
									},
							),
							
							array(
								'header'=>'Total Blast Campaign',   
								'value'=>function($data){
										return $data->total_blast_campaign;
									},
							),
							
							array(
								'header'=>'',  
								'type'=>'raw',
								'value'=>function($data){
										return '<a href="'.Yii::app()->createAbsoluteUrl('campaign/historyPaketDetail',['id'=>$data->subscribed_id]).'"><button  style="margin-top:0px" class="waves-effect waves-dark btn btn-segment-dash">Detail</button></a>';
									},
							),
							
							
							/* array(
								'class'=>'CButtonColumn',
								'htmlOptions'=>array('style'=>'min-width:100px;padding:0px 5px 0px 5px'),
								'buttons'=>array(
									'view' => array(
										 'label'=>'<i class="Small material-icons">visibility</i>',
										 'imageUrl'=>false,
										),
									'update' => array(
										 'label'=>'<i class="Small material-icons">clear_all</i>',
										 'imageUrl'=>false,
										),
									'delete' => array(
										 'label'=>'<i class="Small material-icons">delete</i>',
										 'imageUrl'=>false,
										),
								),
							), */
						),
					)); ?>
					
					
					
			</div>
		</div>
	</div>
	</div>
	
	
	
	
</div>
