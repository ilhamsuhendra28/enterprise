<?php
	$text = MyAppComponent::getTextTypeCompaign($type);
?>
<title>Enterprise | <?php echo ($model->isNewRecord) ? 'New Campaign' : 'Edit Campaign' ?> ( <?php echo $text ?> ) </title>
<style>
textarea {
height: auto !important;
width: 90% !important;
margin: 0 3px 0 3px;
}
</style>

<?php /*<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/css/bootstrap.min.css" rel="stylesheet">*/ ?>
<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<?php /*<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>*/ ?>
<?php /*<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>*/ ?>
<?php /*<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>*/ ?>
<?php /*<script src="../js/plugins/sortable.js" type="text/javascript"></script>*/ ?>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<?php /*<script src="../js/locales/fr.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="../js/locales/es.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/explorer-fa/theme.js" type="text/javascript"></script>*/ ?>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/fa/theme.js" type="text/javascript"></script>
<?php /*<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>*/ ?>

<?php
/* if ($type=='email'){
	if ($model->isNewRecord){
		$campaign_id = KirimEmailMailchimp::getDefaultIDCampaignForTemplate();
		$campaignDefault = KirimEmailMailchimp::getCampaignContentById($campaign_id);
		if ($createTemplate['status']=='error' || (isset($createTemplate['data']['type']) && $createTemplate['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){ // jika error // jika error
			$htmlCampaignDefault ='';
		}else{
			$htmlCampaignDefault = $campaignDefault['data']['archive_html'];
		}
		//print_r($htmlCampaignDefault);
		$htmlCampaignDefault = str_replace("<!doctype html>","",$htmlCampaignDefault);
		$model->description = $htmlCampaignDefault;
	}
} */
?>


<div class="row row-seg">
	<div class="col m12" align="center">
		<h5>Campaign ( <?php echo $text ?> ) </h5>	
	</div>
</div>

<div class="row"> 
	<div class="col s2"></div>
	<div class="col s8">
	
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'custom-segmentation-form',
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			/*'enableAjaxValidation'=>false,*/
			/* 'clientOptions'=>array('validateOnSubmit'=>true),
			'enableClientValidation'=>true, */
		)); ?>
	
		
		<?php  if($form->errorSummary($model)) : ?>
		<div class = "animated bounce">
			<div class="card-panel red lighten-2" style="color:#444">
			<span class="white-text">
				<?php echo $form->errorSummary($model); ?>
			</div>
			
		</div>
		<?php  endif; ?>
		
		
		
		<ul class="stepper horizontal">
			<li class="step active">
				<div class="step-title waves-effect">
					<div class="step-f-title">Campaign Title</div>
				</div>
				<div class="step-content">
					<div class="row">
						<p class="title-stepper"><?php echo ($model->isNewRecord) ? 'New Campaign' : 'Edit Campaign' ?> Set Up ( <?php echo $text ?> )</p>
						<div class="col s2"></div>
						<div class="col s8">
							<div class="row">
								<div class="input-field input-seg">
									<?php echo $form->labelEx($model,'title'); ?>
									<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Campaign Title','class'=>'validate','required'=>true)); ?>
									<?php echo $form->error($model,'title'); ?>
								
								</div>
							</div>
							<div class="row">
								<div class="input-field input-seg">
									<?php echo $form->labelEx($model,'subject'); ?>
									<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Campaign Subject','class'=>'validate','required'=>true)); ?>
									<?php echo $form->error($model,'subject'); ?>
								
								</div>
							</div>
							<div class="row">
								<center>
								<label>Campaign Mode</label>
								</center>
							</div>
							<div class="row">
							
								<div class="col s4 c-campaign">
								</div>
								<div class="col s4 c-campaign">
									<?php
										if ($type=='email'){
											echo '<img src="'.Yii::app()->theme->baseUrl.'/assets/images/mail.png" class="center-align"><br>';
											echo $form->labelEx($model,'Email Blasting').'<br>';
										}else if ($type=='sms'){
											echo '<img src="'.Yii::app()->theme->baseUrl.'/assets/images/sms.png" class="center-align"><br>';
											echo $form->labelEx($model,'SMS Blasting').'<br>';
										}if ($type=='wa'){
											echo '<img src="'.Yii::app()->theme->baseUrl.'/assets/images/phone.png" class="center-align"><br>';
											echo $form->labelEx($model,'Whatsapp Blasting').'<br>';
										}else{
										}
									?>
								</div>
								<div class="col s4 c-campaign">
								</div>
								<?php /*
								<div class="col s6 c-campaign">
									<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/mail.png" class="center-align"><br>
									<?php echo $form->labelEx($model,'Email Blasting'); ?><br>
									<label><?php echo $form->checkBox($model,'email_blast',array('value'=>1, 'uncheckValue'=>0,'class'=>'filled-in'));?><span></span></label>
									<?php echo $form->error($model,'email_blast'); ?>
		
								</div>
								<div class="col s6 c-campaign">
									<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/sms.png"><br>
									<?php echo $form->labelEx($model,'SMS Blasting'); ?><br>
									<label><?php echo $form->checkBox($model,'sms_blast',array('value'=>1, 'uncheckValue'=>0,'class'=>'filled-in'));?><span></span></label>
									<?php echo $form->error($model,'sms_blast'); ?>
								</div>
								*/ ?>
								
								<?php /*
								<div class="col s4 c-campaign">
									<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/phone.png"><br>
									<label>Mobile App</label><br>
									<label><input type="checkbox" class="filled-in"/><span></span></label>
								</div>
								*/ ?>
							</div>
						</div>
						<div class="col s2"></div>
						<div class="row">
							<div class="col s5"></div>
							<div class="col s2">
								<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
							</div>
							<div class="col s5"></div>
						</div>
					</div>
				</div>
			</li>
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">User Segment</div>
				</div>
				<div class="step-content">
					<div class="row row-select">
						<p class="title-stepper">Demographic Segmentation</p>
						<div class="row">
						
						<?php 
							$no = 0;
							$arrayCheckedSegmentation = array();
							foreach ($campaignDetail as $detail){
								$arrayCheckedSegmentation[]=$detail->id_user_segmentation;
							}
							//print_r($arrayCheckedSegmentation);
							foreach ($dataUserSegmentation as $segment ) { 
								$no++;
								
								if ($model->isNewRecord){
									$con_check_active_class ='';
									$num_active_class ='';
									$check_class ='hidden';
									$checked = '';
									$required = 'required';
								}else{
									
									if (in_array($segment['id'], $arrayCheckedSegmentation)){
										$con_check_active_class ='aktif';
										$num_active_class ='hidden';
										$check_class ='';
										$checked = 'checked="checked"';
										$required = '';
									}else{
										$con_check_active_class ='';
										$num_active_class ='';
										$check_class ='hidden';
										$checked = '';
										$required = '';
									}
								}
						?>
								<div class="col m12 con-check no-p <?php echo $con_check_active_class ?>">
										<div class="col m7 no-p">
											<div class="con-num-count valign-wrapper left">
												<div class="center-cont">
													<div class="num <?php echo $num_active_class ?>"><?php echo $no ?></div>
													<div class="check <?php echo $check_class ?>"><i class="fa fa-check"></i></div>
													<input class="checkboxlist validate" name ="user_segment[]" value =<?php echo $segment['id'] ?> type="checkbox"  <?php echo $required ?>  <?php echo $checked ?>/>
												</div>
											</div>
											
											
											<div class="con left">
												<h5 class="title no-m"><?= $segment['title'];?></h5>
												<div class="left">
													<p style="padding-right: 14px;">
														<small class="super-small">Session start</small> <br>
														<small><?= date('Y-m-d', strtotime($segment['created_date']));?></small>
													</p>
												</div>
												<div class="left" style="padding-left: 40px;">
													<p>
														<small class="super-small">status</small> <br>
														<small class="<?= $segment['status'] == 1 ? 'success':'red';?> small-btn"><?= $segment['status'] == 1 ? 'Active':'Not Active';?></small>
													</p>
												</div>
												<br><br>
											</div>
											
											<?php /*
											<div class="con left">
												<h5 class="title no-m"><?php echo $segment['title'] ?></h5>
												<div class="left">
													<p style="padding-right: 14px;">
														<small class="super-small">Session start</small> <br>
														<small><?php echo $segment['created_date'] ?></small>
													</p>
												</div>
												<br><br>
												<div class="left">
													<p>
														<small class="super-small">Data source</small> <br>
														<ul class="sosmed super-small">
															<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
															<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
															<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
														</ul>
													</p>
												</div>
												<br><br>
												<div class="left">
													<p>
														<small class="super-small">status</small> <br>
														<?php if ($segment['status'] ==1) { ?>
															<small class="success small-btn">Active</small>
														<?php } else { ?>
															<small class="red small-btn">Not Active</small>
														<?php } ?>
														
														<br>
														<?php if ($segment['is_custom'] ==1) { ?>
															<small class="super-small">Custom Segmentation</small>
														<?php } else { ?>
															<small class="super-small">Clustering User Data</small>
														<?php } ?>
														
														
													</p>
												</div>
											</div>
											
											*/ ?>
										</div>
										<div class="col m5">
										
											<?php if ($segment['is_custom']==1) { ?>
												<div class="col m3">
													<div class="con-loc  valign-wrapper">
														<div class="center-cont">
															<h6 class="color-grey no-m">Total User Data</h6>
															<?php 
																$total = CustomSegmentationDetail::model()->countByAttributes(array('id_custom_segmentation'=>$segment['id']));
															?>
															<div class="countage color-green"><b><?php echo $total ?></b></div>
														</div>
													</div>
												</div>
											<?php } else{ ?>
												<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
														<div class="center-cont">
															<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
															<h5 class="percent-sel"><?= $females = $segment->females;?>%</h5>
														</div>
												</div></div>
												<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
														<div class="center-cont">
															<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
															<h5 class="percent-sel"><?= 100 - $females;?>%</h5>
														</div>
												</div></div>
											<?php } ?>
											
											<?php /*
											<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
													<div class="center-cont">
														<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
														<h5 class="percent-sel">53%</h5>
													</div>
											</div></div>
											<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
													<div class="center-cont">
														<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
														<h5 class="percent-sel">47%</h5>
													</div>
											</div></div>
											*/ ?>
										</div>
								</div>
						<?php } ?>
						
						
						<?php /*
							<div class="col m12 con-check no-p">
								<div class="col m7 no-p">
									<div class="con-num-count valign-wrapper left">
										<div class="center-cont">
											<div class="num">1</div>
											<div class="check hidden"><i class="fa fa-check"></i></div>
											<input class="checkboxlist" name ="user_segment[]" value =1 type="checkbox"/>
										</div>
									</div>
									<div class="con left">
										<h5 class="title no-m">USER SEGMENT TITLE</h5>
										<div class="left">
											<p style="padding-right: 14px;">
												<small class="super-small">Session start</small> <br>
												<small>26/3/2018</small>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">Data source</small> <br>
												<ul class="sosmed super-small">
													<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
													<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
													<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
												</ul>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">status</small> <br>
												<small class="success small-btn">Active</small>
											</p>
										</div>
									</div>
								</div>
								<div class="col m5">
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">53%</h5>
											</div>
									</div></div>
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">47%</h5>
											</div>
									</div></div>
								</div>
							</div>
							
							<div class="col m12 con-check no-p">
								<div class="col m7 no-p">
									<div class="con-num-count valign-wrapper left">
										<div class="center-cont">
											<div class="num">2</div>
											<div class="check hidden"><i class="fa fa-check"></i></div>
											<input class="checkboxlist" name ="user_segment[]" value =2 type="checkbox"/>
										</div>
									</div>
									<div class="con left">
										<h5 class="title no-m">USER SEGMENT TITLE 2</h5>
										<div class="left">
											<p style="padding-right: 14px;">
												<small class="super-small">Session start</small> <br>
												<small>26/3/2018</small>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">Data source</small> <br>
												<ul class="sosmed super-small">
													<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
													<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
													<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
												</ul>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">status</small> <br>
												<small class="success small-btn">Active</small>
											</p>
										</div>
									</div>
								</div>
								<div class="col m5">
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">53%</h5>
											</div>
									</div></div>
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">47%</h5>
											</div>
									</div></div>
								</div>
							</div>
						*/ ?>
							
							
						</div>
						<div class="col s5"> </div>
						<div class="col s2">
							<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
						</div>
						<div class="col s5"></div>
					</div>            
				</div>
			</li>
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">Strategy</div>
				</div>
				<div class="step-content">
					<div class="row">   
						<p class="title-stepper">App Promotion Schedule & Content</p>
						
						<!--
						<div class="row">
							<div class="input-field input-seg col s4">
								<?php echo $form->textField($model,'post_date',array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker validate','required'=>true)); ?>
								<?php echo $form->labelEx($model,'post_date'); ?>
								<?php echo $form->error($model,'post_date'); ?>
							</div>
							<?php /*
							<div class="input-field input-seg col s4">
								<?php echo $form->textField($model,'end_date',array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker validate','required'=>true)); ?>
								<?php echo $form->labelEx($model,'end_date'); ?>
								<?php echo $form->error($model,'end_date'); ?>
							</div>
							*/ ?>
							<div class="input-field input-seg col s4">
								<?php echo $form->textField($model,'post_time',array('placeholder'=>'HH:MM','id'=>'hour','class'=>'timepicker validate','required'=>true)); ?>
								<?php echo $form->labelEx($model,'post_time'); ?>
								<?php echo $form->error($model,'post_time'); ?>
							</div>
						</div>
						-->
						
						<div class="row">
						
							<div class="input-field input-seg col s4">
								<?php echo $form->dropDownList($model,'sent_type',MyAppComponent::getArrayDataCampaignType(),array('onChange'=>'changeField(this)', 'class'=>'validate','required'=>true)); ?>
								<?php echo $form->labelEx($model,'sent_type'); ?>
								<?php echo $form->error($model,'sent_type'); ?>
							</div>
							
							<div class="input-field input-seg col s4" id="post_date_div">
								<?php echo $form->textField($model,'post_date',array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker validate')); ?>
								<?php echo $form->labelEx($model,'post_date'); ?>
								<?php echo $form->error($model,'post_date'); ?>
							</div>
							
							<div class="input-field input-seg col s4" id="dayNumber_div">
								<?php echo $form->dropDownList($model,'dayNumber',MyAppComponent::getlistDateNumber(),array('class'=>'validate')); ?>
								<?php echo $form->labelEx($model,'dayNumber'); ?>
								<?php echo $form->error($model,'dayNumber'); ?>
							</div>
							
							<div class="input-field input-seg col s4" id="dayName_div">
								<?php echo $form->dropDownList($model,'dayName',MyAppComponent::getArrayDataDayName(),array('class'=>'validate')); ?>
								<?php echo $form->labelEx($model,'dayName'); ?>
								<?php echo $form->error($model,'dayName'); ?>
							</div>
							
							
							<div class="input-field input-seg col s4" id="begda_div">
								<?php echo $form->textField($model,'begda',array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker validate')); ?>
								<?php echo $form->labelEx($model,'begda'); ?>
								<?php echo $form->error($model,'begda'); ?>
							</div>
							
							<div class="input-field input-seg col s4" id="endda_div">
								<?php echo $form->textField($model,'endda',array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker validate')); ?>
								<?php echo $form->labelEx($model,'endda'); ?>
								<?php echo $form->error($model,'endda'); ?>
							</div>
							
							
							
							<div class="input-field input-seg col s4">
								<?php echo $form->textField($model,'post_time',array('placeholder'=>'HH:MM','id'=>'hour','class'=>'timepicker validate','required'=>true)); ?>
								<?php echo $form->labelEx($model,'post_time'); ?>
								<?php echo $form->error($model,'post_time'); ?>
							</div>
							
						</div>
						
						<?php if ($type=='wa'){ ?>
							<div class="row">
								<label>Campaign Image</label>
							</div>
							<div class="row">
							
								 <div class="file-loading">
									<?php /*<input id="file-eng" name="file-eng[]" type="file" multiple>*/ ?>
									<?php echo $form->fileField($model,'images[]',array('id'=>'file-eng','multiple'=>true)); ?>
								</div>
			
								<?php /*
								<div class="col s3">
									<div class="img-camp valign-wrapper center-align"><i class="material-icons">add_circle</i></div>
								</div>
								<div class="col s3">
									<div class="img-camp"></div>
								</div>
								<div class="col s3">
									<div class="img-camp"></div>
								</div>
								<div class="col s3">
									<div class="img-camp"></div>
								</div>
								*/ ?>
								
							</div>
						<?php } ?>
						
						
						
						<?php // ini karena template mailchimp jadi di hide 
						/*
						<div class="row">
						
							<div class="input-field input-seg col s4">
								<?php echo $form->dropDownList($model,'campaign_template_id',MyAppComponent::getListTemplateEmail(),array('prompt'=>'None','onChange'=>'changeTemplate(this)', 'class'=>'validate')); ?>
								<?php echo $form->labelEx($model,'campaign_template_id'); ?>
								<?php echo $form->error($model,'campaign_template_id'); ?>
							</div>
						</div>
						*/ ?>
						
						<?php if ($type !='email'){ //karena description nya pake templating bee plugin, maka di sini description tidak di munculkan untuk type email ?>
						<?php echo $form->labelEx($model,'description'); ?>
						<div class="row">
							
							<?php 
								if ($type=='email'){
									$classtextarea = 'cktemplate';
								}else{
									$classtextarea = '';
								}
								echo $form->textArea($model,'description',array('class'=>'validate '.$classtextarea,'rows'=>9, 'cols'=>50,'required'=>true));
							?>
							
						</div>
						<?php } ?>
					</div>  
					<div class="col s4"> </div>
					<div class="col s4">
						<?php 
								if ($type=='email'){
									$buttonText = 'Set Content';
								}else{
									$buttonText = 'Save';
								}
						?>
						<button type="submit" class="waves-effect waves-dark btn  btn-segment"><?php echo $buttonText?></button>
					</div>
					<div class="col s4"></div>
				</div>
			</li>
		</ul>  
		<?php $this->endWidget(); ?>
		<div class="col s2"></div>                  
	</div>
</div>

<script>
$('#file-eng').fileinput({
       theme: 'fa',
	    //'theme': 'explorer-fa',
        //language: 'fr',
        uploadUrl: '',
	    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
	    showUpload: false,
	    showCaption: false,
	    showRemove: false,
	    //maxFileSize: 1000,
        //maxFilesNum: 10,
        allowedFileExtensions: ['jpg', 'png', 'gif'],
		initialPreviewAsData: true,
		overwriteInitial: false,
		
		<?php if (!$model->isNewRecord){ // jika edit ?>
			initialPreview: [
				<?php foreach ($campaignImage as $image ) { ?>
					"<?php echo Yii::app()->baseUrl.'/'.$image->image; ?>",
				<?php } ?>
			],
			initialPreviewConfig: [
				<?php foreach ($campaignImage as $image ) { ?>
				{
					//caption: "image", 
					//size: 329892, 
					width: "50px", 
					url: '<?php echo Yii::app()->createUrl('campaign/deleteImage')?>', 
					key: <?php echo $image->id?>,
					// extra: function() { 
						// return {
							// id: $('#id').val()
						// };
					// }, 
				},
				<?php } ?>
			]  
		<?php } ?>
    }); 
	
</script>

<script>
function changeField(elem){
	//alert(elem.value);
	dynamicField(elem.value);
}

function dynamicField(valueData){
	if(valueData == 0){
		$('#dayNumber_div').hide();
		$('#dayName_div').hide();
		$('#begda_div').hide();
		$('#endda_div').hide();
		$('#post_date_div').show();
	}
	else if(valueData == 1){
		$('#dayNumber_div').show();
		$('#dayName_div').hide();
		$('#begda_div').show();
		$('#endda_div').show();
		$('#post_date_div').hide();
	}
	else if(valueData == 2){
		$('#dayNumber_div').hide();
		$('#dayName_div').show();
		$('#begda_div').show();
		$('#endda_div').show();
		$('#post_date_div').hide();
	}
	else if(valueData == 3){
		$('#dayNumber_div').hide();
		$('#dayName_div').hide();
		$('#begda_div').show();
		$('#endda_div').show();
		$('#post_date_div').hide();
	}
}
</script>
<?php if ($model->isNewRecord){ 
	$valueData = 0;
}else{
	$valueData = $model->sent_type;
}
?>

<script>
$( document ).ready(function() {
    //console.log( "ready!" );
	var valueDataDefault = '<?php echo $valueData ?>'; 
	dynamicField(valueDataDefault);
});
</script>


<script>

function changeTemplate(elem){
	//alert(elem.value);
	var valueData = elem.value;
	if (valueData==''){
		tinyMCE.activeEditor.setContent('');
	}else{
		
		$.ajax({
				url: "<?php echo Yii::app()->createAbsoluteUrl('/campaign/getCampaignTemplate', array())?>?id="+valueData ,
				type: 'GET',
				dataType:"JSON",
				//async: false,
				beforeSend :function(){
					// Should you want to have a loading widget onClick
					// http://www.yiiframework.com/extension/loading/
					// Loading.show();
					swal({
					  title: "Please Wait",
					  html: " Processing Data.... <br> <i class='fa fa-spinner fa-3x fa-spin'></i>",
					  type: "info",
					  
					  allowOutsideClick: false,
					  showCancelButton: false, // There won't be any cancel button
					  showConfirmButton: false, // There won't be any confirm button
					  allowEscapeKey:false,
					}); 
				},
				complete  : function(){
					// hide the loading widget when complete
					// Loading.hide();
					//swal.close();
				},
				success: function (data) {
					console.log(data);
					 if (data.status == "SUCCESS"){
						swal.close();
						tinyMCE.activeEditor.setContent(data.text);
					 }else{
						swal({
							  title: 'Gagal',
							  text: data.message,
							  confirmButtonText: "OK", 
							});
					 }
					

				},
				error: function(xhr, status, error) {
					// this will display the error callback in the modal.
					//alert( xhr.status + " " +xhr.statusText + " " + xhr.responseText);
					
						
						swal({
						  title: "Gagal", 
						  text: xhr.responseText,  
						  confirmButtonText: "OK", 
						});
					
				},
				/* cache: false,
				contentType: false,
				processData: false */
			});
			
				return false;
				
				
	}
	
}

/* $( "#target" ).click(function() {
  alert( "Handler for .click() called." );
  var s = '<p><b>afsa</b></p>';
  tinyMCE.activeEditor.setContent(s);
  return false;
}); */
</script>