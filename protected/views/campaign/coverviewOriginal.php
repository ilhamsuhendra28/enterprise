<title>Enterprise | Campaign Overview</title>

<div class="row row-seg">
	<div class="col m12">
		<h5>Campaign Overview</h5>	
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/growth.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-purple">+27%</b><br>
							<small>Growth</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/click.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-blue">3521</b><br>
							<small>Click</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/love.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-yellow">321987</b><br>
							<small>Total Impression</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/price.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-green">Rp. 125.421.000</b><br>
							<small>Coast</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m8">
		<div class="row">
			<div class="col s12">
			  <ul class="tabs">
				<li class="tab col s3"><a class="active" href="#test1">Free Smart Pro...</a></li>
				<li class="tab col s3"><a href="#test2">Test 2</a></li>
				<li class="tab col s3 disabled"><a href="#test3">+</a></li>
			  </ul>
			</div>
			<div id="test1" class="col s12">
				<canvas id="mybarChart"></canvas>
			</div>
			<div id="test2" class="col s12">Test 2</div>
		  </div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Cost Preview</h6>
				<div class="con-donat" style="text-align: center;">
					<div class="persen"><p>20% <br><small class="color-grey">Effectivenes</small></p></div>
				  <canvas id="c2" height="220"></canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					<p class="color-grey">
						All Campaign
					</p>
					<hr class="hr-grey">
					<div class="col m12 con-check no-p">
						<div class="col m7 no-p">
							<div class="con-num-count valign-wrapper left">
								<div class="center-cont">
									<div class="num">1</div>
									<div class="check hidden"><i class="fa fa-check"></i></div>
								</div>
							</div>
							<div class="con left">
								<h5 class="title no-m">Free Smart Promotion BagiData</h5>
								<div class="left">
									<p style="padding-right: 14px;">
										<small class="super-small">Session start</small> <br>
										<small>26/3/2018</small>
									</p>
								</div>
								<div class="left" style="padding-left: 40px;">
									<p>
										<small class="super-small">End</small> <br>
										<small>26/3/2018</small>
									</p>
								</div>
								<br><br>
								<div class="left">
									<p>
										<small class="super-small">status</small> <br>
										<small class="success small-btn">Active</small>
									</p>
								</div>
							</div>
						</div>
						<div class="col m5">
							<div class="col m3"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Total Views</h6>
									<div class="countage color-purple"><b>2300</b></div>
								</div>
							</div></div>
							<div class="col m4"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Total Clicks</h6>
									<div class="countage color-blue"><b>1102</b></div>
								</div>
							</div></div>
							<div class="col m5"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Total Buyer</h6>
									<div class="countage color-orange"><b>1051</b></div>
								</div>
							</div></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>