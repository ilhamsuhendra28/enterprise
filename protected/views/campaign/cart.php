<title>Enterprise | Campaign Paket Cart</title>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>



<div class="row row-seg">

	<div class="col m12">
		<h5>Campaign Paket Cart </h5>	
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
		
	</div>
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Cart </h5>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'cart-grid',
						'summaryText'=>'',
						'emptyText'=>'Tidak Ada Data Paket',
						'dataProvider'=>$dataProvider,
						'ajaxUpdate'=>false,
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'enableSorting'=>false,
						'columns'=>array(
							array(
								'header'=>'No',   
								'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							array('header'=>'Paket', 'name'=>'name'),
							array('header'=>'Harga', 'value'=>'Yii::app()->numberFormatter->formatCurrency($data->price,"Rp. ")'),
							
							array(
								'header'=>'Jumlah',
								'htmlOptions'=>array('width'=>'100px'),
								'type'=>'raw',
								'value'=>'CHtml::dropDownlist($data->id,"",  array(1 => 1, 2 => 2, 3 => 3, 4 => 4 , 5 => 5) ,array("class"=>"span6", "options" => array($data->Quantity=>array("selected"=>true)),
								"id" => $data->id, "style"=>"margin-top:3px;margin-bottom:3px;",  "onChange" => "javascript:updatecart($data->id,this.value)"))',
							),
								   
							array('header'=>'Subtotal', 'value'=>'Yii::app()->numberFormatter->formatCurrency($data->SumPrice,"Rp. ")'),
							
							array(
							'class'=>'CButtonColumn',
							'template'=>'{delete}',
							 'buttons'=>array(
											'delete' => array(
												 'label'=>'Hapus ',
												 //'imageUrl'=>false,
												 'url'=>'Yii::app()->createUrl("/campaign/deletecart", array("id"=>$data->id))',
																  ),
										   
											 ),
							),
						),
					)); ?>
					
					
					
					
					
					<?php
					$empty = Yii::app()->shoppingCart->isEmpty(1);
					if ( $empty !=1){


						// rincian keranjang belanja
						echo"<b>Rincian </b> "."<br>";

						echo '<ul>';
							echo '<li>';
								echo Yii::app()->shoppingCart->getCount()." Jenis Paket"; 
							echo '</li>';
							echo '<li>';
								echo "Jumlah Paket di keranjang ". Yii::app()->shoppingCart->getItemsCount()." "."<br>";
							echo '</li>';
						echo '</ul>';	


						echo "<h5>"."Total Bayar ".Yii::app()->numberFormatter->formatCurrency(Yii::app()->shoppingCart->getCost(),"Rp. ")."</h5>"; 
						echo"<br>";
						echo"<br>";


						echo CHtml::link('<i class="icon-trash icon-white"></i> '.Yii::t('strings','Kosongkan keranjang Belanja'),array('campaign/clearcart'),array('class'=>'btn btn-danger','id'=>'clear-cart'));
					

						// tindakan option keranjang belanja
						echo"<div align = 'right'>";

							
							echo CHtml::link('<i class="icon-trash icon-white"></i> '.Yii::t('strings','Kembali Ke List Paket'),array('campaign/detailPaket'),array('class'=>'btn btn-danger','id'=>'paket-list'));
							echo ' ';
							echo CHtml::link('<i class="icon-trash icon-white"></i> '.Yii::t('strings','Bayar'),array('campaign/buyPaketInCart'),array('class'=>'btn btn-danger','id'=>'buyPaketInCart'));
															
						echo"</div>";
					
					}else{
						echo CHtml::link('<i class="icon-trash icon-white"></i> '.Yii::t('strings','Kembali Ke List Paket'),array('campaign/detailPaket'),array('class'=>'btn btn-danger','id'=>'paket-list'));
							
					}
					?>
					
					
			</div>
		</div>
	</div>
	</div>
</div>
	
	
<script>

function updatecart(id,jumlah)
	{
		//alert(id);
		//alert(jumlah);
		
		jQuery.ajax({
			'type':'get',
			'success':allFine,
			'error': function(XMLHttpRequest, textStatus, errorThrown) {
						//alert(XMLHttpRequest.responseText);
					},
			'beforeSend':function() {        
				   //$("#loadingcart").addClass("loadingzz");
				},
				'complete':function() {
				  //$("#loadingcart").removeClass("loadingzz");
				},
			'url':'<?php echo Yii::app()->request->baseUrl; ?>/campaign/updatecart?id='+id+'&jumlah='+jumlah,
			'cache':false,
		});
		return false;
		

	}

	
	function allFine(data) {
			// refresh your grid
			//alert(data);
			$.fn.yiiGridView.update('cart-grid');
	}

		
		
$( "#clear-cart" ).click(function() {
  var a_href = $(this).attr('href');
  if (confirm("Kamu Yakin menghapus semua isi dalam Cart ?")){
        //alert( a_href );
		return true;
  }else{
	  return false;
  }
	
	
});



$( "#buyPaketInCart" ).click(function() {
  var a_href = $(this).attr('href');
  if (confirm("Are You Sure To Paid your Order ?")){
        //alert( a_href );
		return true;
  }else{
	  return false;
  }
	
	
});

</script>
	