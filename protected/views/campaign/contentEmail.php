<?php
	$text = MyAppComponent::getTextTypeCompaign($type);
?>
<title>Enterprise | <?php echo 'Campaign Content' ?> ( <?php echo $text ?> ) </title>
 <style type="text/css">
    /*
    #bee-plugin-container {
      position: absolute;
      top: 5px;
      bottom: 30px;
      left: 5px;
      right: 5px;
    }

    #integrator-bottom-bar {
      position: absolute;
      height: 25px;
      bottom: 0px;
      left: 5px;
      right: 0px;
    }
	*/
  </style>
  




<div class="row row-seg">
	<div class="col m12" align="center">
		<h5>Campaign Content ( <?php echo $text ?> ) </h5>	
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
	</div>
</div>

<div class="row" style="margin:20px"> 
		<div class="row">
			<div class="col m4"></div>  
			<div class="col m4">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'custom-segmentation-form',
					'htmlOptions'=>array('enctype'=>'multipart/form-data'),
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					/*'enableAjaxValidation'=>false,*/
					/* 'clientOptions'=>array('validateOnSubmit'=>true),
					'enableClientValidation'=>true, */
				)); ?>
				<div class="input-field input-seg col s12">
					<?php 
						if ($idTemplate !=''){
							$model->campaign_template_id = $idTemplate;
						}
					?>
					<?php echo $form->dropDownList($model,'campaign_template_id',MyAppComponent::getListTemplateEmailBee(),array('onChange'=>'changeTemplate(this)', 'class'=>'validate')); ?>
					<?php echo $form->labelEx($model,'campaign_template_id'); ?>
					<?php echo $form->error($model,'campaign_template_id'); ?>
				</div>
				<?php $this->endWidget(); ?>
			</div>  
			<div class="col m4"></div>  
		</div>
		
	  <div id="bee-plugin-container"></div>
	  
	  <div id="integrator-bottom-bar" style="margin:12px">
		<!-- You can change the download function to get the JSON and use this input to load it -->
		Select template to load:
		<input id="choose-template" type="file" />
		
		<!--
		You need to provide a send function to use this input field 
		Send test e-mail to:
		<input id="integrator-test-emails" type="text" />
		-->
	  </div>
</div>



<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bee/Blob.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bee/fileSaver.js"></script>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bee/BeePlugin.js"></script>
<?php 

$urlGetContentJson =  Yii::app()->createAbsoluteUrl('/campaign/getContentEmail', array('id'=>$model->id,'type'=>$type,'idTemplate'=>$idTemplate,));
$urlSavedContent =  Yii::app()->createAbsoluteUrl('/campaign/redirectSavedContentEmail', array('id'=>$model->id,'type'=>$type));

?>

<script type="text/javascript">
function changeTemplate(elem){
	//alert(elem.value);
	var valueData = elem.value;
	$.ajax({
				url: "<?php echo Yii::app()->createAbsoluteUrl('/campaign/changeTemplate', array('id'=>$model->id,'type'=>$type))?>",
				type: 'POST',
				dataType:"JSON",
				//async: false,
				data : {idTemplate : valueData},
				beforeSend :function(){
					// Should you want to have a loading widget onClick
					// http://www.yiiframework.com/extension/loading/
					// Loading.show();
					swal({
					  title: "Please Wait",
					  html: " Processing Data.... <br> <i class='fa fa-spinner fa-3x fa-spin'></i>",
					  type: "info",
					  
					  allowOutsideClick: false,
					  showCancelButton: false, // There won't be any cancel button
					  showConfirmButton: false, // There won't be any confirm button
					  allowEscapeKey:false,
					}); 
				},
				complete  : function(){
					// hide the loading widget when complete
					// Loading.hide();
					//swal.close();
				},
				success: function (data) {
					//console.log(data);
				
					 if (data.status == "SUCCESS"){
						swal.close();
						window.location = data.urlNext;
					 }else{
						swal({
							  title: 'Gagal',
							  text: data.message,
							  confirmButtonText: "OK", 
							});
					 } 
					

				},
				error: function(xhr, status, error) {
					// this will display the error callback in the modal.
					//alert( xhr.status + " " +xhr.statusText + " " + xhr.responseText);
					
						
						swal({
						  title: "Gagal", 
						  text: xhr.responseText,  
						  confirmButtonText: "OK", 
						});
					
				},
				/* cache: false,
				contentType: false,
				processData: false */
			});
}









function saveToDB(jsonFile,htmlFile){
	var template_id = $('#Campaign_campaign_template_id').val();
	$.ajax({
				url: "<?php echo Yii::app()->createAbsoluteUrl('/campaign/saveContentEmail', array('id'=>$model->id,'type'=>$type))?>",
				type: 'POST',
				dataType:"JSON",
				//async: false,
				data : {json : jsonFile, template : htmlFile, idTemplate : template_id},
				beforeSend :function(){
					// Should you want to have a loading widget onClick
					// http://www.yiiframework.com/extension/loading/
					// Loading.show();
					swal({
					  title: "Please Wait",
					  html: " Saving Data.... <br> <i class='fa fa-spinner fa-3x fa-spin'></i>",
					  type: "info",
					  
					  allowOutsideClick: false,
					  showCancelButton: false, // There won't be any cancel button
					  showConfirmButton: false, // There won't be any confirm button
					  allowEscapeKey:false,
					}); 
				},
				complete  : function(){
					// hide the loading widget when complete
					// Loading.hide();
					//swal.close();
				},
				success: function (data) {
					//console.log(data);
				
					 if (data.status == "SUCCESS"){
						swal.close();
						window.location = "<?php echo $urlSavedContent ?>";
					 }else{
						swal({
							  title: 'Gagal',
							  text: data.message,
							  confirmButtonText: "OK", 
							});
					 } 
					

				},
				error: function(xhr, status, error) {
					// this will display the error callback in the modal.
					//alert( xhr.status + " " +xhr.statusText + " " + xhr.responseText);
					
						
						swal({
						  title: "Gagal", 
						  text: xhr.responseText,  
						  confirmButtonText: "OK", 
						});
					
				},
				/* cache: false,
				contentType: false,
				processData: false */
			});
}


  var request = function (method, url, data, type, callback) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function () {
      if (req.readyState === 4 && req.status === 200) {
        var response = JSON.parse(req.responseText);
        callback(response);
      } else if (req.readyState === 4 && req.status !== 200) {
        console.error('Access denied, invalid credentials. Please check you entered a valid client_id and client_secret.');
		swal({
		  title: 'error',
		  text: 'Access denied, invalid credentials. Please check you entered a valid client_id and client_secret. (Default Content Tidak ada dalam sistem)',
		  confirmButtonText: "OK", 
		});
      }
    };

    req.open(method, url, true);
    if (data && type) {
      if (type === 'multipart/form-data') {
        var formData = new FormData();
        for (var key in data) {
          formData.append(key, data[key]);
        }
        data = formData;
      }
      else {
        req.setRequestHeader('Content-type', type);
      }
    }

    req.send(data);
  };

  var save = function (filename, content) {
    saveAs(
      new Blob([content], { type: 'text/plain;charset=utf-8' }),
      filename
    );
  };

  var specialLinks = [{
    type: 'unsubscribe',
    label: 'SpecialLink.Unsubscribe',
    link: 'http://[unsubscribe]/'
  }, {
    type: 'subscribe',
    label: 'SpecialLink.Subscribe',
    link: 'http://[subscribe]/'
  }];

  var mergeTags = [{
    name: 'tag 1',
    value: '[tag1]'
  }, {
    name: 'tag 2',
    value: '[tag2]'
  }];

  var mergeContents = [{
    name: 'content 1',
    value: '[content1]'
  }, {
    name: 'content 2',
    value: '[content1]'
  }];

  function userInput(message, sample) {
    return function handler(resolve, reject) {
      var data = prompt(message, JSON.stringify(sample))
      return (data == null || data == '')
        ? reject()
        : resolve(JSON.parse(data))
    }
  }

  var beeConfig = {
    uid: 'user_enterprise_<?php echo Yii::app()->user->id?>',
    container: 'bee-plugin-container',
    autosave: 30,
    language: 'en-US',
    specialLinks: specialLinks,
    mergeTags: mergeTags,
    mergeContents: mergeContents,
    contentDialog: {
      specialLinks: {
        label: 'Add a custom Special Link',
        handler: userInput('Enter the deep link:', {
          type: 'custom',
          label: 'external special link',
          link: 'http://www.example.com'
        })
      },
      mergeTags: {
        label: 'Add custom tag 2',
        handler: userInput('Enter the merge tag:', {
          name: 'name',
          value: '[name]'
        })
      },
      mergeContents: {
        label: 'Choose a custom merge content',
        handler: userInput('Enter the merge content:', {
          name: 'my custom content',
          value: '{my-custom-content}'
        })
      },
      rowDisplayConditions: {
        label: 'Open builder',
        handler: userInput('Enter the row display condition:', {
          type: 'People',
          label: 'Person is a developer',
          description: 'Check if a person is a developer',
          before: "{if job == 'developer'}",
          after: '{endif}'
        })
      },
    },
    onSave: function (jsonFile, htmlFile) {
      //save('newsletter.html', htmlFile);
	  //window.location = "http://localhost/bee_plugin/html_js/asff";
	  saveToDB(jsonFile,htmlFile);
	  
    },
    onSaveAsTemplate: function (jsonFile) { // + thumbnail? 
      save('<?php echo $model->title?> template.json', jsonFile);
    },
    onAutoSave: function (jsonFile) { // + thumbnail? 
      console.log(new Date().toISOString() + ' autosaving...');
      window.localStorage.setItem('newsletter.autosave', jsonFile);
    },
    onSend: function (htmlFile) {
      //write your send test function here
	  //alert('OK');
    },
	
    onError: function (errorMessage) {
      console.log('onError ', errorMessage);
	  swal({
		  title: 'error',
		  text: errorMessage,
		  confirmButtonText: "OK", 
		});
    }
  };

  var bee = null;

  var loadTemplate = function (e) {
    var templateFile = e.target.files[0];
    var reader = new FileReader();

    reader.onload = function () {
      var templateString = reader.result;
      var template = JSON.parse(templateString);
      bee.load(template);
    };

    reader.readAsText(templateFile);
  };

  document.getElementById('choose-template').addEventListener('change', loadTemplate, false);

  request(
    'POST',
    'https://auth.getbee.io/apiauth',
    'grant_type=password&client_id=303097f0-4a9f-423b-99c7-f253f59390fb&client_secret=FKEDJbdGmnd1WYEE0Mn4295kJTL34EXH5UE4AnJlQtQV1J73VK3',
    'application/x-www-form-urlencoded',
    function (token) {
      BeePlugin.create(token, beeConfig, function (beePluginInstance) {
        bee = beePluginInstance;
        request(
          'GET',
          //'https://rsrc.getbee.io/api/templates/m-bee',
		  //'http://localhost/bee_plugin/html_js/template_default.json',
		  '<?php echo $urlGetContentJson ?>',
          null,
          null,
          function (template) {
            bee.start(template);
          });
      });
    });

</script>