<?php
/* @var $this CampaignController */
/* @var $data Campaign */
?>
<?php 
$con_check_active_class ='';
$num_active_class ='';
$check_class ='hidden';			
$checked = '';
$segment = $data->userSegment;

$no = $widget->dataProvider->getPagination()->currentPage * $widget->dataProvider->getPagination()->pageSize + $index + 1;
$url = Yii::app()->createUrl('campaign/viewUserSegmentation',array('id'=> $segment['id']));

						
?>

<div class="col m12 con-check no-p <?php echo $con_check_active_class ?>" data-url="<?php echo $url?>" >
	<div class="col m7 no-p">
		<div class="con-num-count valign-wrapper left">
			<div class="center-cont">
				<div class="num <?php echo $num_active_class ?>"><?php echo $no ?></div>
				<div class="check <?php echo $check_class ?>"><i class="fa fa-check"></i></div>
				<input class="checkboxlist" name ="user_segment[]" value =<?php echo $segment['id'] ?> type="checkbox" <?php echo $checked ?>/>
			</div>
		</div>
		
		<div class="con left">
			<h5 class="title no-m"><?= $segment['title'];?></h5>
			<div class="left">
				<p style="padding-right: 14px;">
					<small class="super-small">Session start</small> <br>
					<small><?= date('Y-m-d', strtotime($segment['created_date']));?></small>
				</p>
			</div>
			<div class="left" style="padding-left: 40px;">
				<p>
					<small class="super-small">status</small> <br>
					<small class="<?= $segment['status'] == 1 ? 'success':'red';?> small-btn"><?= $segment['status'] == 1 ? 'Active':'Not Active';?></small>
				</p>
			</div>
			<br><br>
		</div>
				
		<?php /*
		<div class="con left">
			<h5 class="title no-m"><?php echo $segment['title'] ?></h5>
			<div class="left">
				<p style="padding-right: 14px;">
					<small class="super-small">Session start</small> <br>
					<small><?php echo $segment['created_date'] ?></small>
				</p>
			</div>
			<br><br>
			<div class="left">
				<p>
					<small class="super-small">Data source</small> <br>
					<ul class="sosmed super-small">
						<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
						<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
						<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
					</ul>
				</p>
			</div>
			<br><br>
			<div class="left">
				<p>
					<small class="super-small">status</small> <br>
					<?php if ($segment['status'] ==1) { ?>
						<small class="success small-btn">Active</small>
					<?php } else { ?>
						<small class="red small-btn">In Active</small>
					<?php } ?>
					
					<br>
					<?php if ($segment['is_custom'] ==1) { ?>
						<small class="super-small">Custom Segmentation</small>
					<?php } else { ?>
						<small class="super-small">Clustering User Data</small>
					<?php } ?>
					
					
				</p>
			</div>
		</div>
		*/ ?>
	</div>
	<div class="col m5">
		<?php if ($segment['is_custom']==1) { ?>
			<div class="col m3">
				<div class="con-loc  valign-wrapper">
					<div class="center-cont">
						<h6 class="color-grey no-m">Total User Data</h6>
						<?php 
							$total = CustomSegmentationDetail::model()->countByAttributes(array('id_custom_segmentation'=>$segment['id']));
						?>
						<div class="countage color-green"><b><?php echo $total ?></b></div>
					</div>
				</div>
			</div>
		<?php } else{ ?>
			<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
					<div class="center-cont">
						<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
						<h5 class="percent-sel"><?= $females = $segment->females;?>%</h5>
					</div>
			</div></div>
			<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
					<div class="center-cont">
						<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
						<h5 class="percent-sel"><?= 100 - $females;?>%</h5>
					</div>
			</div></div>
		<?php } ?>
		<?php /*
		<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
				<div class="center-cont">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
					<h5 class="percent-sel">53%</h5>
				</div>
		</div></div>
		<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
				<div class="center-cont">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
					<h5 class="percent-sel">47%</h5>
				</div>
		</div></div>
		*/ ?>
	</div>
</div>