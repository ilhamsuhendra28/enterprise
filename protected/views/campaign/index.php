<?php
	$text = MyAppComponent::getTextTypeCompaign($type);
?>
<title>Enterprise | Campaign ( <?php echo $text ?> )</title>

<div class="container">
		<?php  if(Yii::app()->user->hasFlash('error')): ?>
			<div class = "animated bounce">
				<div class="card-panel red lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('error') ; ?>	
				</span>
				</div>
				
			</div>
		<?php  endif; ?>
		
	<div class="row row-empty">
		<div class="col s5">
			<div class="vector-empty">
				<div class="vector-image-left">
					<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/campaign-vector.svg">
				</div>
			</div>
		</div>
		<div class="col s7 col-empty">
			<div class="text-empty">
				<p class="text-not">Hmm, It seems to be empty</p>
				<p class="text-not-b">Why don't see it now?</p>
				<a href="<?php echo Yii::app()->createUrl('campaign/create',array('type'=>$type)); ?>"><button class="waves-effect waves-dark btn btn-segment-dash">New Campaign ( <?php echo $text ?> )</button></a>
			</div>
		</div>
	</div>
</div>