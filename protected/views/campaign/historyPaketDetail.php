<title>Enterprise | History Paket Detail </title>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>



<div class="row row-seg">
	<div class="col m12">
		<h5>History Paket Detail</h5>	
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
		
	</div>
	
	
	
	
	
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>History Paket Blast Detail </h5>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'list-paket-blast',
						'dataProvider'=>$historyData,
						//'filter'=>$history,
						//'cssFile' => Yii::app()->request->baseUrl.'/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'enableSorting'=>false,
						'columns'=>array(
							array(
							'header'=>'No',   
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							array(
								'header'=>'Subscribe ID',   
								'value'=>function($data){
										return $data->subscribed_id;
									},
							),
							
							array(
								'header'=>'Nama Paket',   
								'value'=>function($data){
										return $data->nama_paket;
									},
							),
							
							array(
								'header'=>'Campaign',   
								'type'=>'raw',
								'value'=>function($data){
										//return $data->title;
										return '<a href="'.Yii::app()->createAbsoluteUrl('campaign/view',['id'=>$data->id_campaign]).'">'.$data->title.'</a>';
									},
							),
							
							array(
								'header'=>'Total Blast ',   
								'value'=>function($data){
										return $data->total;
									},
							),
							
							
							
							/* array(
								'class'=>'CButtonColumn',
								'htmlOptions'=>array('style'=>'min-width:100px;padding:0px 5px 0px 5px'),
								'buttons'=>array(
									'view' => array(
										 'label'=>'<i class="Small material-icons">visibility</i>',
										 'imageUrl'=>false,
										),
									'update' => array(
										 'label'=>'<i class="Small material-icons">clear_all</i>',
										 'imageUrl'=>false,
										),
									'delete' => array(
										 'label'=>'<i class="Small material-icons">delete</i>',
										 'imageUrl'=>false,
										),
								),
							), */
						),
					)); ?>
					
					
					
			</div>
		</div>
	</div>
	</div>
	
	
	
	
</div>
