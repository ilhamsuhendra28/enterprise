<?php
	$text = MyAppComponent::getTextTypeCompaign($type);
?>

<title>Enterprise | Campaign Detail ( <?php echo $text ?> )</title>
<?php
/* @var $this DummyController */
/* @var $model Dummy */

/* $this->breadcrumbs=array(
	'Dummies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Dummy', 'url'=>array('index')),
	array('label'=>'Create Dummy', 'url'=>array('create')),
	array('label'=>'Update Dummy', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Dummy', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dummy', 'url'=>array('admin')),
); */
?>

<script>
function getDocHeight(doc){
           	     doc = doc || document;
           	     // stackoverflow.com/questions/1145850/
           	     var body = doc.body, html = doc.documentElement;
           	     var height = Math.max( body.scrollHeight, body.offsetHeight,
           	     html.clientHeight, html.scrollHeight, html.offsetHeight );
           	     return height;
            }

function setIframeHeight(id) {
           	     var ifrm = document.getElementById(id);
           	     var doc = ifrm.contentDocument? ifrm.contentDocument:
           	     ifrm.contentWindow.document;
           	     ifrm.style.visibility = 'hidden';
           	     ifrm.style.height = "10px"; // reset to minimal height ...
           	     // IE opt. for bing/msn needs a bit added or scrollbar appears
           	     ifrm.style.height = getDocHeight( doc ) + 4 + "px";
           	     ifrm.style.visibility = 'visible';
            }

</script>
<style>
.cursornya{
	cursor:pointer;
}
</style>


<div class="row row-seg">

<div class="left">
	<h5>Detail Campaign</h5>	
</div>
<div class="right">
	<?php if ($model->approval == 0) { ?>
		<?php if ( $model->statusCampaign=='Draft'){ ?>
			<a href="<?= Yii::app()->createAbsoluteUrl('campaign/update',array('id'=>$model->id,'type'=>$type));?>"><button class="waves-effect waves-dark btn btn-segment-dash">Edit Campaign ( <?php echo $text ?> )</button></a>
		<?php } ?>
	<?php } ?>
</div>


<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					<?php /*
					<p class="color-grey">
						<h4>View Campaign #<?php echo $model->id; ?></h4>
						<?php echo CHtml::link('Edit Campaign',array('campaign/update','id'=>$model->id),array('class'=>'waves-effect waves-dark btn')); ?>
					</p>
					*/ ?>
					
					<?php  if(Yii::app()->user->hasFlash('success')): ?>
						<script>
						$( document ).ready(function() {
							//console.log( "ready!" );
							var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
							M.toast({html: toastHTML});
						});
						</script>
						<?php /*
						<div class = "animated bounce">
							<div class="card-panel green lighten-2" style="color:#444">
							<span class="white-text">
								<?php  echo Yii::app()->user->getFlash('success') ; ?>	</span>
							</div>
						</div>
						*/ ?>
					<?php  endif; ?>
					<?php /*<hr class="hr-grey">*/ ?>
					
					
					<div class="row">
						<div class="col m4"></div>
						<div class="col m4">
						<div class="card z-depth-1">
								<div class="card-content">
									<h6 class="color-grey">Kuota <?php echo $type ?></h6>
									<br>
									<div class="row no-m">
										<div class="col m8 no-p">
											<div class="counter-champaign smart-count">
												<b><?php 
													if($kuotaPaket){
														$field_sisa = 'total_sisa_'.$type;
														echo $kuotaPaket [$field_sisa];
													}else{
														echo 0; 
													}
												?>
												</b><br>
												<?php /*<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
											</div>
										</div>
										<div class="col m4 no-p">
											<?php echo CHtml::link('Detail',array('campaign/detailPaket'),array('class'=>'waves-effect waves-dark btn')); ?>
										</div>
									</div>
								</div>
						</div>
						</div>
						<div class="col m4"></div>
					</div>

					<div>
						<h6 align="center" class="text-bold"><b><?= $model->title;?></b></h6>
					</div>
					<br><br>
					
					
					<div class="row">	
						<div class="col m2"></div>
						<div class="col m8">
							<iframe id="frame_content_description" src="<?php echo Yii::app()->controller->createAbsoluteUrl('/campaign/viewcontent',array('id'=>$model->id))?>;" width="100%" height="400px" frameBorder="0" onload="setIframeHeight(this.id);" ></iframe>
						</div>
						<div class="col m2"></div>
					</div>
							
					<div class="row">					
						<div class="col m8">
							<?php /*
							<p class="color-grey p-b-10"><?= $model->description;?></p>
							*/ ?>
						</div>
						<div class="col m8">
							<h6 class="text-bold"><b>Detail Data</b></h6>
							<div class="col m6">Subject</div><div class="col m6">: <?= $model->subject;?></div>
							<div class="col m6">Created Date</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->created_date));?></div>
							
							
							
							<div class="col m6">Sent Type</div><div class="col m6">: <?php echo  MyAppComponent::getCampaignTYpe($model->sent_type); ?></div>
							<?php if ($model->sent_type == 0) { // sekali ?>
								<div class="col m6">Post Date</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->post_date));?></div>
							<?php } else if ($model->sent_type == 1) { // perbulan ?>
								<div class="col m6">Send Every Date </div><div class="col m6">: 
								<?php 
									$arrayPostDate = explode('-',$model->post_date);
									echo $arrayPostDate[2];
								?>
								</div>
							<?php } else if ($model->sent_type == 2) { // perminggu ?>
								<div class="col m6">Send Every Day </div><div class="col m6">: 
								<?php 
									$date = $model->post_date;
									$weekday = date('l', strtotime($date)); 
									echo $weekday; 
								?>
								</div>
							<?php } else if ($model->sent_type == 3) { // perhari ?>
								<?php //--// ?>
							<?php } ?> 
							
							
							<?php if ($model->sent_type != 0) { // jika bukan sekali ?>
								<div class="col m6">Begin date</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->begda));?></div>
								<div class="col m6">End date</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->endda));?></div>
							<?php } ?>
							
							
							
							<div class="col m6">Post Time</div><div class="col m6">: <?= $model->post_time;?></div>
							<div class="col m6">Total Sent</div><div class="col m6">: <?= $model->total_sent;?></div>
							<div class="col m6">Status Approval</div><div class="col m6">: <?php echo MyAppComponent::getStatusApproval($model->approval); ?></div>
							<div class="col m6">Approval Message</div><div class="col m6">:  <?php echo $model->approval_message; ?></div>
							<div class="col m6">Status</div><div class="col m6">: <?php echo $model->statusCampaign ?></div>
		
		
							
							
							<?php if ($type=='wa'){ ?>
								<div class="col m6">Image</div><div class="col m6"> <?php echo MyAppComponent::loadImage($campaignImage) ?></div>
							<?php } ?>
							
							
							<?php 
								if ( $model->approval==1){
									if ( $model->statusCampaign=='Draft'){
										echo CHtml::link('Send Campaign',array('campaign/sendCampaign','id'=>$model->id,'type'=>$type),array('class'=>'btn setOnGoing')); 
									}
									if ( $model->statusCampaign=='On-Going'){
										echo CHtml::link('Hold Campaign (Draft)',array('campaign/holdCampaign','id'=>$model->id,'type'=>$type),array('class'=>'btn setDraft')); 
									}
									if ( $model->statusCampaign=='Sent'){
										echo CHtml::link('Replicate Campaign',array('campaign/replicate','id'=>$model->id,'type'=>$type),array('class'=>'btn replicate')); 
									}
								}
							?>
					

						</div>
					</div>
					
					<?php /* $this->widget('zii.widgets.CDetailView', array(
					'data'=>$model,
					'htmlOptions'=>array('class'=>'detail-view bordered'),
					'cssFile' => Yii::app()->request->baseUrl.'/css/widget/detailview/styles.css',
					'attributes'=>array(
						//'id',
						'title',
						'description',
						'email_blast',
						'sms_blast',
						'post_date',
						'end_date',
						'post_time',
						'status',
						array(
							'label'=>'Image',
							'type'=>'raw',
							'value'=>MyAppComponent::loadImage($campaignImage),
						),
					),
				));  */?>

				</div>
				
				
				
				<div class="row">
					<p class="color-grey">
						Detail Of Sent Campaign 
					</p>
					<hr class="hr-grey">
					
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'detail_campaign_sent-grid',
						'dataProvider'=>$sentDataProvider,
						//'filter'=>$sentData,
						//'cssFile' => Yii::app()->request->baseUrl.'/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'columns'=>array(
							array(
							'header'=>'No',   
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							'sent_date_time',
							'total_sent',
							array(
								'type'=>'raw',
								'value'=>function($data){
									if ($data->campaign_guid !=''){
										//return '<a href ="'.Yii::app()->createAbsoluteUrl('/campaign/traceSent',['id'=>$data->campaign_guid]).'" target="_blank">Trace Sent Data</a>';
										return '<a class="cursornya" onClick=openTrace("'.$data->campaign_guid.'") >Trace Sent Data</a>';	
									}
								}
							),
							
							/* array(
								'class'=>'CButtonColumn',
								'htmlOptions'=>array('style'=>'min-width:100px;padding:0px 5px 0px 5px'),
								'buttons'=>array(
									'view' => array(
										 'label'=>'<i class="Small material-icons">visibility</i>',
										 'imageUrl'=>false,
										),
									'update' => array(
										 'label'=>'<i class="Small material-icons">clear_all</i>',
										 'imageUrl'=>false,
										),
									'delete' => array(
										 'label'=>'<i class="Small material-icons">delete</i>',
										 'imageUrl'=>false,
										),
								),
							), */
						),
					)); ?>
				</div>
				
				
				
				
				

				<div class="row">
					<p class="color-grey">
						User Segmentation
					</p>
					<hr class="hr-grey">
					<?php $this->widget('zii.widgets.CListView', array(
						'id'=>'list-user_segmentation', 
						'dataProvider'=>$dataProvider,
						'itemView'=>'_view_user_segmentation',
						/* 'pager'=>array(
							'cssFile'=>false
						), */
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => '',
							'lastPageLabel' => '',
							
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col m12">
        <div class="col m12 prompt">
            <div class="left">
                <button type="button" onclick="prompt();" class="waves-effect waves-dark btn btn-segment-dash">Delete</button>
            </div>
        </div>
        <div class="col m12 prompt hide">Are you sure ?</div>
        <div class="col m2 prompt hide">
            <a href="<?= Yii::app()->createAbsoluteUrl('campaign/delete',['id'=>$model->id,'type'=>$type]);?>"><button class="waves-effect waves-dark btn btn-segment-dash">Yes</button></a>
        </div>
        <div class="col m3 prompt hide">
            <button onclick="prompt();" type="button" class="waves-effect waves-dark btn btn-segment-dash">Cancel</button>
        </div>
    </div>
</div>

<script>
function openTrace(id){
	//alert(id);
	swal({
	  title: "Info", 
	  html: "<div id ='contentSwal'><b>Loading..,</b></div>",  
	  confirmButtonText: "<u>OK</u>", 
	});
	
	$.ajax({
			url: '<?php echo Yii::app()->createAbsoluteUrl('/campaign/traceSent') ?>?id='+id ,
			type: 'GET',
			//dataType:"JSON",
			//data: formData,
			//async: false,
			beforeSend :function(){
				
				//$('#contentSwal').html('<b>Loading...</b>');
			},
			complete  : function(){
				
			},
			success: function (data) {
				$('#contentSwal').html(data);
			},
			error: function(xhr, status, error) {
				// this will display the error callback in the modal.
				alert( xhr.status + " " +xhr.statusText + " " + xhr.responseText);
				
			},
			cache: false,
			/* contentType: false,
			processData: false */
		});
		
}
</script>

<script>

$( ".setOnGoing" ).click(function() {
  var a_href = $(this).attr('href');
  if (confirm("Are You Sure To Send This Campaign ?")){
        //alert( a_href );
		swal({
		  title: "Please Wait",
		  html: " Processing Data.... <br> <i class='fa fa-spinner fa-3x fa-spin'></i>",
		  type: "info",
		  
		  allowOutsideClick: false,
		  showCancelButton: false, // There won't be any cancel button
		  showConfirmButton: false, // There won't be any confirm button
		  allowEscapeKey:false,
		}); 
		return true;
  }else{
	  return false;
  }
	
	
});

$( ".setDraft" ).click(function() {
  var a_href = $(this).attr('href');
  if (confirm("Are You Sure To Hold This Campaign ?")){
        //alert( a_href );
		swal({
		  title: "Please Wait",
		  html: " Processing Data.... <br> <i class='fa fa-spinner fa-3x fa-spin'></i>",
		  type: "info",
		  
		  allowOutsideClick: false,
		  showCancelButton: false, // There won't be any cancel button
		  showConfirmButton: false, // There won't be any confirm button
		  allowEscapeKey:false,
		}); 
		return true;
  }else{
	  return false;
  }
	
	
});


$( ".replicate" ).click(function() {
  var a_href = $(this).attr('href');
  if (confirm("Are You Sure To Replicate This Campaign ?")){
        //alert( a_href );
		swal({
		  title: "Please Wait",
		  html: " Processing Data.... <br> <i class='fa fa-spinner fa-3x fa-spin'></i>",
		  type: "info",
		  
		  allowOutsideClick: false,
		  showCancelButton: false, // There won't be any cancel button
		  showConfirmButton: false, // There won't be any confirm button
		  allowEscapeKey:false,
		}); 
		return true;
  }else{
	  return false;
  }
	
	
});

</script>


  
<?php
//var_dump($segmen->listEmail,$segmen->listPhone,$segmen->listEmailAllowed,$segmen->listPhoneAllowed,$listWA);
$script = "function goToPage(link){"
    . "location.href = link;"
    . "}"
    . "function prompt(){"
    . "$('.prompt').each(function(){"
    . "if ($(this).hasClass('hide')){"
    . "$(this).removeClass('hide');"
    . "} else {"
    . "$(this).addClass('hide');"
    . "}"
    . "})"
    . "}";
Yii::app()->clientScript->registerScript('openPage',$script, CClientScript::POS_END);
?>