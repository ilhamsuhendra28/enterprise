<?php
/* @var $this CompaignController */
/* @var $model Compaign */
/* @var $form CActiveForm */
?>

<div class="row">

	<div class="col s2"></div>
	<div class="col s8">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
			
			<?php /*
			<div class="row">
				<?php echo $form->label($model,'id'); ?>
				<?php echo $form->textField($model,'id'); ?>
			</div>
			*/ ?>

			<div class="row">
				<?php echo $form->label($model,'title'); ?>
				<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
			</div>

			<?php /*
			<div class="row">
				<?php echo $form->label($model,'description'); ?>
				<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'email_blast'); ?>
				<?php echo $form->textField($model,'email_blast'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'sms_blast'); ?>
				<?php echo $form->textField($model,'sms_blast'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'post_date'); ?>
				<?php echo $form->textField($model,'post_date'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'end_date'); ?>
				<?php echo $form->textField($model,'end_date'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'post_time'); ?>
				<?php echo $form->textField($model,'post_time'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'created_date'); ?>
				<?php echo $form->textField($model,'created_date'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'updated_date'); ?>
				<?php echo $form->textField($model,'updated_date'); ?>
			</div>

			<div class="row">
				<?php echo $form->label($model,'status'); ?>
				<?php echo $form->textField($model,'status'); ?>
			</div>
			*/ ?>

			<div class="row buttons">
				<button type="submit" class="waves-effect waves-dark btn  btn-segment">Search</button>
			</div>

		<?php $this->endWidget(); ?>
	</div>
	<div class="col s2"></div>

</div><!-- search-form -->