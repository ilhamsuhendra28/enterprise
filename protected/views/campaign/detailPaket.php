<title>Enterprise | Campaign Paket </title>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>



<div class="row row-seg">
	<div class="col m12">
		<h5>Campaign Paket </h5>	
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
		
	</div>
	
	<div class ="row">
	
	
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Kuota Email</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php 
								if($kuotaPaket){
									$field_sisa = 'total_sisa_email';
									echo $kuotaPaket [$field_sisa];
								}else{
									echo 0; 
								}
							?>
							</b><br>
							<?php /*<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Kuota SMS</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php 
								if($kuotaPaket){
									$field_sisa = 'total_sisa_sms';
									echo $kuotaPaket [$field_sisa];
								}else{
									echo 0; 
								}
							?>
							</b><br>
							<?php /*<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Kuota WhatsApp</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php 
								if($kuotaPaket){
									$field_sisa = 'total_sisa_wa';
									echo $kuotaPaket [$field_sisa];
								}else{
									echo 0; 
								}
							?>
							</b><br>
							<?php /*<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	</div>
	
	
	<?php /*
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Views</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Click</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-blue"><b><i class="fa fa-arrow-up"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-blue.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Buyer</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-yellow"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-yellow.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	*/ ?>
	
	
	<?php /*
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/growth.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-purple">+27%</b><br>
							<small>Growth</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/click.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-blue">3521</b><br>
							<small>Click</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/love.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-yellow">321987</b><br>
							<small>Total Impression</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="col m2 no-p">
						<div class="logo-champaign valign-wrapper">
							<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/price.jpg" class="responsive-img" alt="">
						</div>
					</div>
					<div class="col m7 no-p">
						<div class="counter-champaign p-l-10">
							<b class="color-green">Rp. 125.421.000</b><br>
							<small>Coast</small>
						</div>
					</div>
					<div class="col m3 no-p" style="text-align: right;">
						<i style="text-align: right;margin-top: 10px;" class="fa fa-ellipsis-h color-grey"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	*/ ?>
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Paket Aktif </h5>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'aktif-paket',
						'dataProvider'=>$userPaketAktif,
						//'filter'=>$paketAktif,
						//'cssFile' => Yii::app()->request->baseUrl.'/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'enableSorting'=>false,
						'columns'=>array(
							array(
							'header'=>'No',   
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							array(
								'header'=>'Subscribe ID',   
								'value'=>function($data){
										return $data->id;
									},
							),
							'nama_paket',
							array(
								'header'=>'Kuota Email',   
								'value'=>function($data){
										return $data->sisa_email;
									},
							),
							array(
								'header'=>'Kuota SMS',   
								'value'=>function($data){
										return $data->sisa_sms;
									},
							),
							array(
								'header'=>'Kuota WA',   
								'value'=>function($data){
										return $data->sisa_wa;
									},
							),
							//'masa_aktif',
							array(
								'name'=>'masa_aktif',   
								'value'=>function($data){
										return $data->masa_aktif. ' Hari';
									},
							),
							'tgl_aktif',
							'tgl_kadaluarsa',
							array(
								'header'=>'Type',   
								'type'=>'raw',
								'value'=>function($data){
										
										if($data->is_free == 1){
											return 'Free ';
										}else{
											return 'Regular';
										}	 
									},
							),
							/* array(
								'class'=>'CButtonColumn',
								'htmlOptions'=>array('style'=>'min-width:100px;padding:0px 5px 0px 5px'),
								'buttons'=>array(
									'view' => array(
										 'label'=>'<i class="Small material-icons">visibility</i>',
										 'imageUrl'=>false,
										),
									'update' => array(
										 'label'=>'<i class="Small material-icons">clear_all</i>',
										 'imageUrl'=>false,
										),
									'delete' => array(
										 'label'=>'<i class="Small material-icons">delete</i>',
										 'imageUrl'=>false,
										),
								),
							), */
						),
					)); ?>
					
					
					<?php
						echo 'See Detail Of <a href="'.Yii::app()->createAbsoluteUrl('campaign/historyPaket',[]).'"><button  style="margin-top:0px" class="waves-effect waves-dark btn btn-segment-dash">History Paket</button></a>';
					?>
			</div>
		</div>
	</div>
	</div>
	
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Paket In Process </h5>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'in-process-paket',
						'dataProvider'=>$userPaketInProcess,
						//'filter'=>$paketInProcess,
						//'cssFile' => Yii::app()->request->baseUrl.'/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'enableSorting'=>false,
						'columns'=>array(
							array(
							'header'=>'No',   
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							array(
								'header'=>'Subscribe ID',   
								'value'=>function($data){
										return $data->id;
									},
							),
							'nama_paket',
							array(
								'header'=>'Paket Email',   
								'value'=>function($data){
										return $data->kuota_email;
									},
							),
							array(
								'header'=>'Paket SMS',   
								'value'=>function($data){
										return $data->kuota_sms;
									},
							),
							array(
								'header'=>'Paket WA',   
								'value'=>function($data){
										return $data->kuota_wa;
									},
							),
							//'masa_aktif',
							array(
								'name'=>'masa_aktif',   
								'value'=>function($data){
										return $data->masa_aktif. ' Hari';
									},
							),
							array(
								'header'=>'Waktu Transaksi',   
								'value'=>function($data){
										return $data->created_date;
									},
							),
							'kode_transaksi',
							
							array(
								'header'=>'Status', 
								'value'=>function($data){
									return $data->statusString;
								},
							),
							
							// ini hanya untuk developer
							/* array(
								'header'=>'Bayar',  
								'type'=>'raw',
								'value'=>function($data){
										return '<a href="'.Yii::app()->createAbsoluteUrl('cron/paymentResponse',['id'=>$data->id,'id_pc'=>$data->id_paket_campaign]).'"><button  style="margin-top:0px" class="waves-effect waves-dark btn btn-segment-dash">Bayar</button></a>';
									},
							), */
							//
							
							array(
								'header'=>'View',  
								'type'=>'raw',
								'value'=>function($data){
										return '<a href="'.Yii::app()->createAbsoluteUrl('campaign/paidBuyPaket',['id'=>$data->id]).'"><button  style="margin-top:0px" class="waves-effect waves-dark btn btn-segment-dash">View</button></a>';
									},
							),
							
							
							/* 'tgl_aktif',
							'tgl_kadaluarsa', */
							/* array(
								'class'=>'CButtonColumn',
								'htmlOptions'=>array('style'=>'min-width:100px;padding:0px 5px 0px 5px'),
								'buttons'=>array(
									'view' => array(
										 'label'=>'<i class="Small material-icons">visibility</i>',
										 'imageUrl'=>false,
										),
									'update' => array(
										 'label'=>'<i class="Small material-icons">clear_all</i>',
										 'imageUrl'=>false,
										),
									'delete' => array(
										 'label'=>'<i class="Small material-icons">delete</i>',
										 'imageUrl'=>false,
										),
								),
							), */
						),
					)); ?>
			</div>
		</div>
	</div>
	</div>
	
	
	
	<div class ="row">
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Beli Paket Campaign </h5>
				<div id="message-cart"></div>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'list-paket',
						'dataProvider'=>$listPaketData,
						//'filter'=>$listPaket,
						//'cssFile' => Yii::app()->request->baseUrl.'/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							//'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => 'First',
							'lastPageLabel' => 'Last',
							
						),
						'enableSorting'=>false,
						'columns'=>array(
							array(
							'header'=>'No',   
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							'nama_paket',
							array(
								'header'=>'Kuota Email',   
								'value'=>function($data){
										return $data->kuota_email;
									},
							),
							array(
								'header'=>'Kuota SMS',   
								'value'=>function($data){
										return $data->kuota_sms;
									},
							),
							array(
								'header'=>'Kuota WA',   
								'value'=>function($data){
										return $data->kuota_wa;
									},
							),
							//'masa_aktif',
							array(
								'name'=>'masa_aktif',   
								'value'=>function($data){
										return $data->masa_aktif. ' Hari';
									},
							),
							array(
								'header'=>'Harga',   
								'value'=>function($data){
										return $data->harga;
									},
							),
							
							array(
								'header'=>'Type',   
								'type'=>'raw',
								'value'=>function($data){
										if($data->is_free == 1){
											return 'Free <br>Available Use For User : '.$data->available_use_per_user;
										}else{
											return 'Regular';
										}	
									},
							),
							
							/* array(
								'header'=>'Beli',  
								'type'=>'raw',
								'value'=>function($data){
										return '<a href="'.Yii::app()->createAbsoluteUrl('campaign/buyPaket',['id'=>$data->id]).'" class="buyPaket"><button style="margin-top:0px" class="waves-effect waves-dark btn btn-segment-dash">Beli</button></a>';
									},
							), */
							
							array(
								'header'=>'add to Cart',  
								'type'=>'raw',
								'value'=>function($data){
										if($data->is_free == 1){
											$available = $data->available_use_per_user;
											$cekCount = UserPaketCampaign::model()->countByAttributes(array('id_paket_campaign'=>$data->id,'id_user'=>Yii::app()->user->id));
											if($cekCount < $available){
												return '<a href="'.Yii::app()->createAbsoluteUrl('campaign/claimPaket',['id'=>$data->id]).'" class="claimPaket"><button style="margin-top:0px" class="waves-effect waves-dark btn ">Claim</button></a>';
											}else{
												return '<a ><button style="margin-top:0px" class="waves-effect waves-dark btn disabled">Claimed</button></a>';
											}
											
										}else{
											
											if(!Yii::app()->shoppingCart->itemAt($data->id)){
												return '<a href="'.Yii::app()->createAbsoluteUrl('campaign/cartcreate',['id'=>$data->id]).'" class="addToCart"><button style="margin-top:0px" class="waves-effect waves-dark btn btn-segment-dash">add to Cart</button></a>';
											}else{
												return CHtml::link('view in Cart', array('campaign/viewcart'), array('class'=>'btn'));
											}
										}
									},
							),
						),
					)); ?>
					
					<?php
						echo CHtml::link('<i class="icon-trash icon-white"></i> '.Yii::t('strings','View Cart'),array('campaign/viewcart'),array('class'=>'btn btn-danger','id'=>'view-cart'));
					
					?>
					
			</div>
		</div>
	</div>
	</div>
	
</div>

<script>


$( ".buyPaket" ).click(function() {
  var a_href = $(this).attr('href');
  if (confirm("Are You Sure To Buy This Paket ?")){
        //alert( a_href );
		return true;
  }else{
	  return false;
  }
	
	
});

$( ".claimPaket" ).click(function() {
  var a_href = $(this).attr('href');
  if (confirm("Are You Sure To Claim This Paket ?")){
        //alert( a_href );
		return true;
  }else{
	  return false;
  }
	
	
});




$( ".addToCart" ).click(function() {
  var a_href = $(this).attr('href');
  
  
  jQuery.ajax({
		'type':'get',
		'success':function(data) { 
			//alert(data);
			$('#message-cart').html(data);
			$.fn.yiiGridView.update('list-paket');
			
		},
		'error': function(XMLHttpRequest, textStatus, errorThrown) {
					alert(XMLHttpRequest.responseText);
				},
		'beforeSend':function() {        
	           //$("#loadingcart").addClass("loadingzz");
	        },
			'complete':function() {
	          //$("#loadingcart").removeClass("loadingzz");
	        },
		'url':a_href,
		'cache':false,
	});
		
	return false;
});

function loadingdown(id, data) {
	
	$( ".buyPaket" ).click(function() {
	  var a_href = $(this).attr('href');
	  if (confirm("Are You Sure To Buy This Paket ?")){
			//alert( a_href );
			return true;
	  }else{
		  return false;
	  }
	});
	
	
	$( ".claimPaket" ).click(function() {
	  var a_href = $(this).attr('href');
	  if (confirm("Are You Sure To Claim This Paket ?")){
			//alert( a_href );
			return true;
	  }else{
		  return false;
	  }
		
		
	});
	
	$( ".addToCart" ).click(function() {
  var a_href = $(this).attr('href');
  
  
  jQuery.ajax({
		'type':'get',
		'success':function(data) { 
			//alert(data);
			$('#message-cart').html(data);
			$.fn.yiiGridView.update('list-paket');
			
		},
		'error': function(XMLHttpRequest, textStatus, errorThrown) {
					alert(XMLHttpRequest.responseText);
				},
		'beforeSend':function() {        
	           //$("#loadingcart").addClass("loadingzz");
	        },
			'complete':function() {
	          //$("#loadingcart").removeClass("loadingzz");
	        },
		'url':a_href,
		'cache':false,
	});
		
	return false;
});
	
}


</script>
	