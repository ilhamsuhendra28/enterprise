<?php
/* @var $this DummyController */
/* @var $model Dummy */

/* $this->breadcrumbs=array(
	'Dummies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Dummy', 'url'=>array('index')),
	array('label'=>'Create Dummy', 'url'=>array('create')),
	array('label'=>'Update Dummy', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Dummy', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dummy', 'url'=>array('admin')),
); */
?>


<div class="row row-seg">

<div class="col m12">
	<h5>Detail Segmentation</h5>	
</div>
		
		
<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					<p class="color-grey">
						<?php /*<h4>View User Segmentation #<?php echo $model->id; ?></h4>*/?>
						<?php //echo CHtml::link('Edit User Segmentation',array('clusteringCustom/update','id'=>$model->id),array('class'=>'waves-effect waves-dark btn')); ?>
					</p>
					
					<?php  if(Yii::app()->user->hasFlash('success')): ?>
						<script>
						$( document ).ready(function() {
							//console.log( "ready!" );
							var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
							M.toast({html: toastHTML});
						});
						</script>
						<?php /*
						<div class = "animated bounce">
							<div class="card-panel green lighten-2" style="color:#444">
							<span class="white-text">
								<?php  echo Yii::app()->user->getFlash('success') ; ?>	</span>
							</div>
						</div>
						*/ ?>
					<?php  endif; ?>
					
					<div class="left">
						<h6 class="text-bold"><b><?= $model->title;?></b></h6>
					</div>
					
					<div class="right">
						<?php //echo CHtml::link('Edit Segmentation',array('clusteringCustom/update','id'=>$model->id),array('class'=>'waves-effect waves-dark btn btn-segment-dash')); ?>
					</div>
					<br><br>
					<div class="row">					
						<div class="col m8">
							<p class="color-grey p-b-10"><?= $model->description;?></p>
						</div>
						<div class="col m8">
							<h6 class="text-bold"><b>Detail Data</b></h6>
							<div class="col m6">Status</div><div class="col m6">: <small class="<?= $model->status == 1 ? 'success':'red';?> small-btn"><?= $model->status == 1 ? 'Active':'Not Active';?></small></div>
							<div class="col m6">Session Start</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->created_date));?></div>
						</div>
					</div>
				
					<?php /*
					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions'=>array('class'=>'detail-view bordered'),
						'cssFile' => Yii::app()->request->baseUrl.'/css/widget/detailview/styles.css',
						'attributes'=>array(
							//'id',
							'title',
							'description',
							'status',
							'is_custom',
						),
					)); ?>
					*/ ?>

				</div>
				
				<div class="row">
					<p class="color-grey">
						User Data Segmentation
					</p>
					<hr class="hr-grey">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'dummy-grid',
						'dataProvider'=>$dataProvider,
						//'filter'=>$model,
						'cssFile' => Yii::app()->request->baseUrl.'/css/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => '',
							'lastPageLabel' => '',
							
						),
						'columns'=>array(
							//'id',
							'nama',
							'no_hp',
							'email',
						),
					)); ?>
					
				</div>
					
			</div>
		</div>
	</div>
</div>
	