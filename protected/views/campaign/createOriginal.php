<title>Enterprise | New Campaign</title>

<div class="row"> 
	<div class="col s3"></div>
	<div class="col s6">
		<ul class="stepper horizontal">
			<li class="step active">
				<div class="step-title waves-effect">
					<div class="step-f-title">Campaign Title</div>
				</div>
				<div class="step-content">
					<div class="row">
						<p class="title-stepper">New Campaign Set Up</p>
						<div class="col s2"></div>
						<div class="col s8">
							<div class="row">
								<div class="input-field input-seg">
									<input id="email" name="title" placeholder="Your Campaign Title" type="text" class="validate" required>
									<label for="first_name">Title</label>
								</div>
							</div>
							<div class="row">
								<label>Campaign Mode</label>
							</div>
							<div class="row">
								<div class="col s4 c-campaign">
									<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/mail.png" class="center-align"><br>
									<label>Email Blasting</label><br>
									<label><input type="checkbox" class="filled-in" checked="checked" /><span></span></label>
								</div>
								<div class="col s4 c-campaign">
									<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/sms.png"><br>
									<label>SMS Blasting</label><br>
									<label><input type="checkbox" class="filled-in"/><span></span></label>
								</div>
								<div class="col s4 c-campaign">
									<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/phone.png"><br>
									<label>Mobile App</label><br>
									<label><input type="checkbox" class="filled-in"/><span></span></label>
								</div>
							</div>
						</div>
						<div class="col s2"></div>
						<div class="row">
							<div class="col s5"></div>
							<div class="col s2">
								<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
							</div>
							<div class="col s5"></div>
						</div>
					</div>
				</div>
			</li>
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">User Segment</div>
				</div>
				<div class="step-content">
					<div class="row row-select">
						<p class="title-stepper">Demographic Segmentation</p>
						<div class="row">
							<div class="col m12 con-check no-p">
								<div class="col m7 no-p">
									<div class="con-num-count valign-wrapper left">
										<div class="center-cont">
											<div class="num">1</div>
											<div class="check hidden"><i class="fa fa-check"></i></div>
										</div>
									</div>
									<div class="con left">
										<h5 class="title no-m">USER SEGMENT TITLE</h5>
										<div class="left">
											<p style="padding-right: 14px;">
												<small class="super-small">Session start</small> <br>
												<small>26/3/2018</small>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">Data source</small> <br>
												<ul class="sosmed super-small">
													<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
													<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
													<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
												</ul>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">status</small> <br>
												<small class="success small-btn">Active</small>
											</p>
										</div>
									</div>
								</div>
								<div class="col m5">
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">53%</h5>
											</div>
									</div></div>
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">47%</h5>
											</div>
									</div></div>
								</div>
							</div>
						</div>
						<div class="col s5"> </div>
						<div class="col s2">
							<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
						</div>
						<div class="col s5"></div>
					</div>            
				</div>
			</li>
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">Strategy</div>
				</div>
				<div class="step-content">
					<div class="row">   
						<p class="title-stepper">App Promotion Schedule & Content</p>
						<div class="row">
							<div class="input-field input-seg col s4">
								<input id="date_one" placeholder="MM/DD/YYYY" type="text" class="datepicker" required>
								<label for="date_one">Post Date</label>
							</div>
							<div class="input-field input-seg col s4">
								<input id="date_two" placeholder="MM/DD/YYYY" type="text" class="datepicker" required>
								<label for="date_two">End Date</label>
							</div>
							<div class="input-field input-seg col s4">
								<input id="hour" type="text" placeholder="HH:MM" class="timepicker">
								<label for="hour">Post Time</label>
							</div>
						</div>
						<div class="row">
							<label>Campaign Image</label>
						</div>
						<div class="row">
							<div class="col s3">
								<div class="img-camp valign-wrapper center-align"><i class="material-icons">add_circle</i></div>
							</div>
							<div class="col s3">
								<div class="img-camp"></div>
							</div>
							<div class="col s3">
								<div class="img-camp"></div>
							</div>
							<div class="col s3">
								<div class="img-camp"></div>
							</div>
						</div>
						<div class="row">
							<textarea></textarea>
						</div>
					</div>  
					<div class="col s5"> </div>
					<div class="col s2">
						<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
					</div>
					<div class="col s5"></div>
				</div>
			</li>
		</ul>  
		<div class="col s3"></div>                  
	</div>
</div>