<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'action'=>array('site/login'),
		'htmlOptions'=>array(
			'onsubmit'=>'return false', // Disable Normal Submit
		),
		'clientOptions'=>array(
			'validateOnType'=>true,
			'validateOnSubmit'=>true,
			'afterValidate'=>'js:function(form, data, hasError){
				if(!hasError){
					login();
				}	
			}'
		)
	)); ?>
	
	<div class="col s6 col-bg">
		<div class="col-bg"></div>
	</div>
	<div class="col s6 col-form">
		<div class="col-depan">
			<div class="row">
				<div class="col s12 col-logo">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/logo.png" class="responsive-img">
				</div>
				<div class="col s12 col-capt">
					<p>Welcome back! Please login to your account.</p>
				</div>
				<div class="col s12 input-field">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username', array('class'=>'validate')); ?>
					<?php echo $form->error($model,'username'); ?>
				</div>
				<div class="col s12 input-field">
					<?php echo $form->labelEx($model,'password'); ?>
					<?php echo $form->passwordField($model,'password', array('class'=>'validate')); ?>
					<?php echo $form->error($model,'password'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col s6">
					<label>
						<?php echo $form->checkBox($model,'rememberMe', array('class'=>'filled-in')); ?>
						<span>Remember me</span>
					</label>
				</div>
				<div class="col s6">
					<span class="f-pass"><a href="<?php echo Yii::app()->createUrl('site/forgot'); ?>">Forgot Password</a></span>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<div class="errorep" style="display:none;"></div>
				</div>
			</div>
			<div class="row">
				<div class="col s6">
					<button type="submit" class="waves-effect waves-light btn btn-log">Login</button>
				</div>
				<div class="col s6">
					<a class="waves-effect waves-light btn btn-logs" href="<?php echo Yii::app()->createUrl('site/signup'); ?>">Sign Up</a>
				</div>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
localStorage.clear();
function login(){
	$("#main-loader").show();
	var data=$("#login-form").serialize();
	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: '<?php echo Yii::app()->createAbsoluteUrl("site/login"); ?>',
		data:data,
		
		beforeSend: function() {
			$('.errorep').hide();
		},
		
		success:function(data){
			if(data.hasil == 'success'){
				$("#main-loader").hide();
				window.location = data.url;
			}else{
				$("#main-loader").hide();
				$('.errorep').html(data.showerror);
				$('.errorep').show();
			}	
		},
  });
}	
</script>