<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'change-form',
		'enableClientValidation'=>true,
		'action'=>array('site/changepassword', ['params'=>$params]),
		'htmlOptions'=>array(
			'onsubmit'=>'return false', // Disable Normal Submit
		),
		'clientOptions'=>array(
			'validateOnType'=>true,
			'validateOnSubmit'=>true,
			'afterValidate'=>'js:function(form, data, hasError){
				if(!hasError){
					change();
				}	
			}'
		)
	)); ?>
	<div class="col s6 col-bg">
		<div class="col-bg"></div>
	</div>
	<div class="col s6 col-form">
		<div class="col-depan">
			<div class="row">
				<div class="col s12 col-logo">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/logo.png" class="responsive-img">
				</div>
				<div class="col s12 col-capt">
					<p>Change your password here</p>
				</div>
				<div class="col s12 input-field">
					<?php echo $form->passwordField($change,'new_password', array('class'=>'validate', 'placeholder'=>'Set Strong Password')); ?>
					<?php echo $form->error($change,'new_password'); ?>
					<label for="new_password">Password</label>
				</div>                    
				<div class="col s12 input-field">
					<?php echo $form->passwordField($change,'repeat_password', array('class'=>'validate', 'placeholder'=>'Set Strong Password')); ?>
					<?php echo $form->error($change,'repeat_password'); ?>
					<label for="repeat_password">Confirmation Password</label>
				</div>    
				<div class="col s12 input-field">
					<button type="submit" class="waves-effect waves-light btn btn-log">Reset Password</button>
				</div>
				<div class="col s12 input-field">
					<a class="login-txt" href="<?php echo Yii::app()->getBaseUrl(true); ?>">
						Not First Time in Here? <b>Log In</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>


<script>
function change(){
	var data=$("#change-form").serialize();
	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: '<?php echo Yii::app()->createAbsoluteUrl("site/changepassword", array('params'=>$params)); ?>',
		data:data,
		
		beforeSend: function() {
			$("#main-loader").show();
		},
		
		success:function(data){
			if(data.hasil == 'success'){
				$("#main-loader").hide();
				swal({
					title : "Password telah berhasil di reset, anda akan di arahkan ke halaman login. Terimakasih",
					showCancelButton: false,
					showConfirmButton: false,
					allowOutsideClick: false,
					padding: 10,
				}).catch(swal.noop);
					
				setTimeout(
					function(){	
						window.location="<?php echo Yii::app()->createAbsoluteUrl("site/login");?>";
					},5000
				);
			}
		},
  });
}
</script>