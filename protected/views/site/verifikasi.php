<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cekotp-form',
)); ?>
<div class="col s6 col-bg">
	<div class="col-bg"></div>
</div>
<div class="col s6 col-form">
	<div class="col-depan">
		<div class="row">
			<div class="col s12 col-logo">
				<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/logo.png" class="responsive-img">
			</div>
			<div class="col s12 col-capt">
				<p>A text message with a 6-digit verification code was just sent to ••••-••••-•• <?php echo substr($user->no_hp, -2); ?></p>
				<div id="time-ass" class="time-ass" style="text-align:center;width:100%;"></div>
				<div class="timeup" style="display:none;"></div>
			</div>
			<div class="col s12 input-field input-f">
				<input id="otp" name="otp" type="text" class="validate" maxlength="6">
				<label for="otp">Masukkan Kode</label>
				<div class="errorep" style="display:none;"></div>
			</div>
			</div>
			<div class="row">
				<div class="col s6">
					<button type="submit" class="waves-effect waves-light btn btn-log" onclick="resendotp()">Kirim Ulang</button>
				</div>
				<div class="col s6">
					<button type="submit" class="waves-effect waves-light btn btn-log" onclick="submitotp()">Submit</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
				
<script>
	var myCounter;
	var waktuStorage = localStorage.getItem("detikotp[" +<?php echo $user->id; ?> + "]");
	if (waktuStorage == null) {
		var waktu = <?php echo $otp->durasi_otp * 60; ?>;
	}else{
		var waktu = waktuStorage;
	}	
	
	var detik = waktu;
	var count = detik;
	var myCounter = setInterval(timer, 1000);    
	
	function format_output(time_left) {
		var hours, minutes, seconds;
		seconds = time_left % 60;
		minutes = Math.floor(time_left / 60) % 60;

		seconds = add_leading_zero( seconds );
		minutes = add_leading_zero( minutes );

		return minutes + ':' + seconds;
	}

	function add_leading_zero(n) {
		if(n.toString().length < 2) {
			return '0' + n;
		} else {
			return n;
		}
	}
	
	function timer()
	{
		count = count - 1;
		document.getElementById("time-ass").innerHTML ='<i class="fa fa-clock-o"></i> ' + format_output(count) + "";
		time_left = count;
		
		if (count <= 0)
		{
			clearInterval(myCounter);
			$('#time-ass').css('color', '#ff2d2d');
			// $('.timeup').show();
			// $('.timeup').html('Waktu telah habis silahkan ajukan ulang kode verifikasi terbaru');
			return;
		} else {
			if (localStorage) {
				$('#time-ass').css('color', '#000');
				$('.timeup').hide();
				localStorage.setItem("detikotp[" +<?php echo $user->id; ?> + "]", time_left);
			}
		}
	}
	
	function submitotp(){
		event.preventDefault();
		$("#main-loader").show();
		var data=$("#cekotp-form").serialize();
		
		jQuery.ajax({
			type: 'POST',
			dataType: 'json',
			url: '<?php echo Yii::app()->createAbsoluteUrl("site/cekotp"); ?>id/<?php echo $user->id ?>/time/'+count,
			data:data,
			
			beforeSend: function() {
				$('.errorep').hide();
			},
		 
			success:function(data){
				if(data.hasil == 'invalid'){
					$("#main-loader").hide();
					$('.errorep').html(data.terms);
					$('.errorep').show();
				}else{		
					$('.errorep').hide();
					$("#main-loader").hide();
					swal({
						title : "Kode yang anda masukkan benar, mohon tunggu sebentar, anda akan diarahkan ke halaman selanjutnya",
						showCancelButton: false,
						showConfirmButton: false,
						allowOutsideClick: false,
						padding: 10,
					}).catch(swal.noop);
						
					setTimeout(
						function(){	
							window.location="<?php echo Yii::app()->createAbsoluteUrl('home/index');?>";
						},3000
					);
				}
			},
		});
	}	
	
	function resendotp(){
		event.preventDefault();
		$("#main-loader").show();
		var data=$("#cekotp-form").serialize();
		
		jQuery.ajax({
			type: 'POST',
			dataType: 'json',
			url: '<?php echo Yii::app()->createAbsoluteUrl("site/resendotp", array('id'=>$user->id)); ?>',
			data:data,
			
			beforeSend: function() {
				$('.errorep').hide();
			},
		 
			success:function(data){
				if(data.hasil == 'success'){	
					clearInterval(myCounter);
					localStorage.clear();
					
					$('.errorep').hide();
					$("#main-loader").hide();
					swal({
						title : "Kode Verifikasi terbaru telah di kirimkan ke no handphone tujuan",
						showCancelButton: false,
						showConfirmButton: false,
						allowOutsideClick: false,
						padding: 10,
					}).catch(swal.noop);
						
					setTimeout(
						function(){	
							window.location="<?php echo Yii::app()->createAbsoluteUrl('site/verifikasi', array('id'=>'verifikasiotp'));?>";
						},3000
					);
				}
			},
		});
	}	
</script>	