<div class="form">	
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'action'=>array('site/signup'),
		'htmlOptions'=>array(
			'onsubmit'=>'return false', // Disable Normal Submit
		),
		'clientOptions'=>array(
			'validateOnType'=>true,
			'validateOnSubmit'=>true,
			'afterValidate'=>'js:function(form, data, hasError){
				if(!hasError){
					signup();
				}	
			}'
		)
	)); ?>
	<div class="col s6 col-bg">
		<div class="col-bg"></div>
	</div>
	<div class="col s6 col-form">
		<div class="col-depan">
			<div class="row">
				<div class="col s12 col-logo">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/logo.png" class="responsive-img">
				</div>
				<div class="col s12 col-capt">
					<p>Please complete to create your account.</p>
				</div>
				<div class="col s12 input-field input-f">
					<?php echo $form->textField($model,'full_name', array('class'=>'validate')); ?>
					<?php echo $form->labelEx($model,'full_name'); ?>
					<?php echo $form->error($model,'full_name'); ?>
				</div>
				<div class="col s12 input-field input-f">
					<?php echo $form->textField($model,'no_hp', array('class'=>'validate')); ?>
					<?php echo $form->labelEx($model,'no_hp'); ?>
					<?php echo $form->error($model,'no_hp'); ?>
				</div>
				<div class="col s12 input-field input-f">
					<?php echo $form->textField($model,'email', array('class'=>'validate')); ?>
					<?php echo $form->labelEx($model,'email'); ?>
					<?php echo $form->error($model,'email'); ?>
					<div class="emailexist" style="display:none;"></div>
				</div>
				<div class="col s12 input-field input-f">
					<?php echo $form->passwordField($model,'new_password', array('class'=>'validate')); ?>
					<?php echo $form->labelEx($model,'new_password'); ?>
					<?php echo $form->error($model,'new_password'); ?>
				</div>
				<div class="col s12 input-field input-f">
					<?php echo $form->passwordField($model,'repeat_password', array('class'=>'validate')); ?>
					<?php echo $form->labelEx($model,'repeat_password'); ?>
					<?php echo $form->error($model,'repeat_password'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<label>
						<input id="agree" name="agree" type="checkbox" class="filled-in"/>
						<span>I agree with terms and conditions</span>
					</label>
					<div class="agree" style="display:none;"></div>
				</div>
			</div>
			<div class="row">
				<div class="col s6">
				</div>
				<div class="col s6">
					<button type="submit" class="waves-effect waves-light btn btn-log">Sign Up</button>
				</div>
				<div class="col s12">
					<p class="text-sign"><a href="<?php echo Yii::app()->getBaseUrl(true); ?>">Already have an account? Sign in.</a></p>
				</div>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
function signup(){
	$("#main-loader").show();
	var data=$("#login-form").serialize();
	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: '<?php echo Yii::app()->createAbsoluteUrl("site/signup"); ?>',
		data:data,
		
		beforeSend: function() {
			$('.agree').hide();
			$('.emailexist').hide();
		},
	 
		success:function(data){
			if(data.hasil == 'emailexist'){
				$("#main-loader").hide();
				$('.emailexist').html(data.terms);
				$('.emailexist').show();
				$('.agree').hide();
			}else{	
				$("#main-loader").hide();
				$('.emailexist').hide();
				if(data.hasil == 'disagree'){
					$('.agree').html(data.terms);
					$('.agree').show();
				}else{
					$('.agree').hide();						
					if(data.hasil == 'success'){
						$('.emailexist').hide();
						swal({
							title : "Registrasi telah berhasil, mohon tunggu sebentar, anda akan diarahkan ke halaman login. Terimakasih",
							showCancelButton: false,
							showConfirmButton: false,
							allowOutsideClick: false,
							padding: 10,
						}).catch(swal.noop);
							
						setTimeout(
							function(){	
								window.location="<?php echo Yii::app()->getBaseUrl(true);?>";
							},5000
						);
					}else{
						$('.agree').show();
						$('.agree').html(data.terms);
					}	
				}	
			}
		},
  });
}
</script>