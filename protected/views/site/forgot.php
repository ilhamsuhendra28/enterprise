<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'forgot-form',
	)); ?>
	<div class="col s6 col-bg">
		<div class="col-bg"></div>
	</div>
	<div class="col s6 col-form">
		<div class="col-depan">
			<div class="row">
				<div class="col s12 col-logo">
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/logo.png" class="responsive-img">
				</div>
				<div class="col s12 col-capt">
					<p>Please input your email here</p>
				</div>	
				<div class="col s12 input-field">
					<?php echo $form->textField($forgot,'email', array('class'=>'validate', 'placeholder'=>'Type Your Email Address'));  ?>
					<div class="emailexist" style="display:none;"></div>
					<label for="email">Enter Your Account Email</label>
				</div>

				<div class="col s12 input-field">
					<button type="submit" onclick="forgot()" class="waves-effect waves-light btn btn-log">Send Forget Password</button>
				</div>
				<div class="col s12 input-field">
					<a class="login-txt" href="<?php echo Yii::app()->getBaseUrl(true); ?>">
						Not First Time in Here? <b>Log In</b>
					</a>
				</div>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>	

<script>
	function forgot(){
		event.preventDefault();
		$("#main-loader").show();
		var data=$("#forgot-form").serialize();
		jQuery.ajax({
			type: 'POST',
			dataType: 'json',
			url: '<?php echo Yii::app()->createAbsoluteUrl("site/forgotsend"); ?>',
			data:data,
			
			beforeSend: function() {
				$('.emailexist').hide();
			},
			
			success:function(data){
				if(data.hasil == 'success'){
					$("#main-loader").hide();
					$('.emailexist').hide();
						swal({
							title : "Reset password telah di kirim ke email anda, silahkan di periksa. Terimakasih",
							showCancelButton: false,
							showConfirmButton: false,
							allowOutsideClick: false,
							padding: 10,
						}).catch(swal.noop);
					setTimeout(
						function(){	
							window.location="<?php echo Yii::app()->createAbsoluteUrl("site/forgot");?>";
						},5000
					);
				}else{
					$("#main-loader").hide();
					$('.emailexist').html(data.terms);
					$('.emailexist').show();
				}	
			},
	  });
	}
</script>