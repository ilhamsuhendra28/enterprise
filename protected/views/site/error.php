<!DOCTYPE html>
<html>
    <head>

        <!--Import CSS-->
        <title>Bagidata | Indonesia Data Exchange</title>

        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bwdstyle.css?version=6"  media="screen,projection"/>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="icon" type="image/png" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.png">
		
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/lightbox-master/css/lightbox.css" rel="stylesheet" type="text/css">
		
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/ckeditor/config.js"></script>
		<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117843851-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-117843851-1');
		</script>

		<style type="text/css">
			.error-page {
			    background-color: #222e62 !important;
			    text-align: center;
			    padding-top: 50px;
			    padding-bottom: 30px;
			    color: white !important;

			    height: 100%; 

			    /* Center and scale the image nicely */
			    background-position: center;
			    background-repeat: no-repeat;
			    background-size: cover;
			}	
		</style>

    </head>
	<body class="error-page">

		<div class="error-page">
			<div class="container">
				<img style="width: 50%;padding-top: 2em" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/error.png" class="responsive-img logo">
				<div class="row">
					<div class="col s8 offset-s2">
						<p style="font-size: 2em;font-weight: bold;margin: 0.5em 0 0.5em 0">Error <?php echo $code; ?></p>
						<?php echo CHtml::encode($message); ?>
					</div>
				</div>
			</div>
		</div>
		
		<!-- loader start -->
		<style type="text/css">
		#main-loader:before {
		  content: '';
		  display: block;
		  position: fixed;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		  background-color: rgba(255, 255, 255, 0.5);
		}
		#main-loader {
		  position: fixed;
		  z-index: 99999;
		  height: 2em;
		  width: 2em;
		  overflow: show;
		  margin: auto;
		  top: 0;
		  left: 0;
		  bottom: 0;
		  right: 0;
		  display : none;
		}
		</style>

		<div id="main-loader">
			<div class="preloader-wrapper small active">
			  <div class="spinner-layer spinner-blue">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>

			  <div class="spinner-layer spinner-red">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>

			  <div class="spinner-layer spinner-yellow">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>

			  <div class="spinner-layer spinner-green">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>
			</div>
		</div>
		<!-- loader end -->
        <!--JavaScript at end of body for optimized loading-->
        <?php /*<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-3.2.1.min.js"></script>*/ ?>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/materialize.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/main.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$("body").addClass("overflow-hide");
				$('.carousel.carousel-slider').carousel({fullWidth: true});

				$("a:not([href*=#])").click(function() {
					var attr = $(this).attr('href');
					if (typeof attr !== typeof undefined && attr !== false && !$(this).hasClass("no-loader")){
						$('#main-loader').show();
					} 
				});

				$(":submit").click(function() {
					$('#main-loader').show();
				});
			});
		</script>
		
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/lightbox-master/js/lightbox.js"></script>
    </body>
</html>