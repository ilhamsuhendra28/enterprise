<title>Enterprise | App Promotion</title>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>
<?php
	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			
			$.fn.yiiListView.update('list-promotion', { 
				data: $(this).serialize()
			});
			
			return false;
		});
	");
?>


<div class="row row-seg">
	<div class="col m12">
		<h5>Free Smart Promotion</h5>	
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		
		<?php  endif; ?>
		
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Views (<?php echo $year ?>)</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php echo $totalViewsData ?></b><br>
							<?php /*<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Click (<?php echo $year ?>)</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php echo $totalClicksData ?></b><br>
							<?php /*<small class="color-blue"><b><i class="fa fa-arrow-up"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-blue.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Buyer (<?php echo $year ?>)</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b><?php echo $totalBuyerData ?></b><br>
							<?php /*<small class="color-yellow"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>*/ ?>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-yellow.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m8">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Overview (<?php echo $year ?>)</h5>
				<canvas id="promoChart"></canvas>
			</div>
		</div>
	</div>
	<?php /*
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6><b>Response Details</b></h6>
				<center>
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/indocuy.jpg" class="responsive-img" alt="">
				</center>
				<br><br>
				<p class="no-m"><i class="fas fa-circle color-grey"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-green"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-orange"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-purple"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-blue"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-orange"></i> Sumatra <small class="right">29.134</small></p>
			</div>
		</div>
	</div>	
	*/ ?>
	
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					<p class="color-grey">
						All Promotions
					</p>
					<hr class="hr-grey">

					
					<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
					<?php echo CHtml::link('New Promotion',array('appPromotion/create'),array('class'=>'waves-effect waves-dark btn')); ?>
					<div class="search-form" style="margin-top:20px;display:none">
						<?php $this->renderPartial('_search',array(
							'model'=>$model,
						)); ?>
					</div>
					
					
					
					<?php $this->widget('zii.widgets.CListView', array(
						'id'=>'list-promotion', 
						'dataProvider'=>$dataProvider,
						'itemView'=>'_view_promotion',
						/* 'pager'=>array(
							'cssFile'=>false
						), */
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => '',
							'lastPageLabel' => '',
							
						),
					)); ?>

				</div>
			</div>
		</div>
	</div>
	
</div>

<?php
//-- Total Views --//
$arrayDataOverViewsData= array();
foreach($overviewViewsData as $total_view){
	$month_number = $total_view['month_number'];
	$arrayDataOverViewsData[$month_number] = $total_view['countTotalViews'];
}
/* print_r($arrayDataOverViewsData);
exit; */

$dataOverViewTotalView ='';
$dataOverViewTotalView .='[';
for ($i = 1; $i <= 12; $i++) {
    if (isset($arrayDataOverViewsData[$i])){
		$dataOverViewTotalView .=$arrayDataOverViewsData[$i].',';
	}else{
		$dataOverViewTotalView .='0,';
	}
}
$dataOverViewTotalView .=']';
//echo $dataOverViewTotalView;
//-- end Total Views --//

//-- Total Clicks --//
$arrayDataOverClicksData= array();
foreach($overviewClicksData as $total_clicks){
	$month_number = $total_clicks['month_number'];
	$arrayDataOverClicksData[$month_number] = $total_clicks['countTotalClicks'];
}
/* print_r($arrayDataOverClicksData);
exit; */ 
$dataOverViewTotalClicks ='';
$dataOverViewTotalClicks .='[';
for ($i = 1; $i <= 12; $i++) {
    if (isset($arrayDataOverClicksData[$i])){
		$dataOverViewTotalClicks .=$arrayDataOverClicksData[$i].',';
	}else{
		$dataOverViewTotalClicks .='0,';
	}
}
$dataOverViewTotalClicks .=']';
//echo $dataOverViewTotalClicks;

//-- end Total Clicks --//


//-- Total Buyer --//
$arrayDataOverBuyerData= array();
foreach($overviewBuyerData as $total_buyer){
	$month_number = $total_buyer['month_number'];
	$arrayDataOverBuyerData[$month_number] = $total_buyer['countTotalBuyer'];
}
/* print_r($arrayDataOverBuyerData);
exit; */
$dataOverViewTotalBuyer ='';
$dataOverViewTotalBuyer .='[';
for ($i = 1; $i <= 12; $i++) {
    if (isset($arrayDataOverBuyerData[$i])){
		$dataOverViewTotalBuyer .=$arrayDataOverBuyerData[$i].',';
	}else{
		$dataOverViewTotalBuyer .='0,';
	}
}
$dataOverViewTotalBuyer .=']';
//echo $dataOverViewTotalBuyer;

//-- end Total Buyer --//



?>
<script>
if ($('#promoChart').length ){ 		  
		var ctx = document.getElementById("promoChart");
		var promoChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
				datasets: [
					{
						label: "Total Views",
						backgroundColor: "rgba(255, 255, 255, 0)",
						borderColor: "#c74793",
						pointBorderColor: "#c74793",
						pointBackgroundColor: "#c74793",
						pointHoverBackgroundColor: "#fff",
						pointHoverBorderColor: "rgba(151,187,205,1)",
						pointBorderWidth: 1,
						//data: [82, 13, 65, 21, 49, 10, 15,65, 21, 49, 10, 15]
						data: <?php echo $dataOverViewTotalView ?>,
						
					},
					{
							label: "Total Clicks",
							backgroundColor: "rgba(255, 255, 255, 0)",
							borderColor: "#3aa8b8",
							pointBorderColor: "#3aa8b8",
							pointBackgroundColor: "#3aa8b8",
							pointHoverBackgroundColor: "#fff",
							pointHoverBorderColor: "rgba(151,187,205,1)",
							pointBorderWidth: 1,
							data: <?php echo $dataOverViewTotalClicks ?>,
					},
					
					{
							label: "Total Buyer",
							backgroundColor: "rgba(255, 255, 255, 0)",
							borderColor: "#ffb60a",
							pointBorderColor: "#ffb60a",
							pointBackgroundColor: "#ffb60a",
							pointHoverBackgroundColor: "#fff",
							pointHoverBorderColor: "rgba(220,220,220,1)",
							pointBorderWidth: 1,
							data: <?php echo $dataOverViewTotalBuyer ?>,
						}
				]
			},

			options: {
						legend:{
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true
								}
							}]
						}
					}
		});
	} 
</script>


















<?php /*

<div class="row row-seg">
	<div class="col m6">
	<canvas id="myChartTest" width="400" height="250"></canvas>
	<input type="button" value="Add Data" onclick="adddata()">
	
	<input type="button" value="Add Data" onclick="getDataTest()">
	
	</div>
</div>
<script>
var canvas = document.getElementById('myChartTest');
var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "My First dataset",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 5,
            pointHitRadius: 10,
            data: [65, 59, 80, 0, 56, 55, 40],
        }
    ]
};

function adddata(){
	
   
  // di hapus satu per satu 
  //myLineChart.data.labels.pop();
  //myLineChart.data.datasets[0].data.pop();
  
  // di push (tambah_baru)
  //myLineChart.data.labels.push('1','2');
  //myLineChart.data.datasets[0].data.push(80,70);
  
  var x = [1,2,3];
  var y = [1,1,1];
  myLineChart.data.labels = x;
  myLineChart.data.datasets[0].data = y;
  
  
  // ini contoh manual
  //myLineChart.data.datasets[0].data[7] = 60;
  //myLineChart.data.labels[7] = "Newly Added";
  
  myLineChart.update();
}

var option = {
	showLines: true
};
var myLineChart = Chart.Line(canvas,{
	data:data,
  options:option
});


function getDataTest(){
	
	//var formData = new FormData($(this)[0]);

		$.ajax({
			url: "<?php echo Yii::app()->createAbsoluteUrl('appPromotion/getDataTest')?>" ,
			type: 'GET',
			dataType:"JSON",
			//data: formData,
			//async: false,
			beforeSend :function(){
				// Should you want to have a loading widget onClick
				// http://www.yiiframework.com/extension/loading/
				// Loading.show();
				// swal({
				  // title: "Please Wait",
				  // text: "Reverse Document..",
				  // type: "info",
				  // allowOutsideClick: false,
				  // showCancelButton: false, // There won't be any cancel button
				  // showConfirmButton: false, // There won't be any confirm button
				  // onOpen: function () {
					// swal.showLoading();
				  // }, 
				  // allowEscapeKey:false,
				// }); 
			},
			complete  : function(){
				// hide the loading widget when complete
				// Loading.hide();
				//swal.close();
			},
			success: function (data) {
				//alert(data);
				console.log(data);
				
				  var x = data;
				  var y = [1,4,2];
				  myLineChart.data.labels = x;
				  myLineChart.data.datasets[0].data = y;
				  
				  myLineChart.update();
			},
			error: function(xhr, status, error) {
				// this will display the error callback in the modal.
				alert( xhr.status + " " +xhr.statusText + " " + xhr.responseText);
				 // swal({
				  // title: xhr.status,
				  // text: xhr.statusText + " " + xhr.responseText,
				  // type: "error",
				// }); 
			},
			cache: false,
			// contentType: false,
			// processData: false 
		});
	
	
		return false;
		
}
</script>
*/ ?>





