<title>Enterprise | <?php echo ($model->isNewRecord) ? 'New ' : 'Edit ' ?> App promotion</title>

<?php /*<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/css/bootstrap.min.css" rel="stylesheet">*/ ?>
<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<?php /*<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>*/ ?>
<?php /*<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>*/ ?>
<?php /*<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>*/ ?>
<?php /*<script src="../js/plugins/sortable.js" type="text/javascript"></script>*/ ?>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<?php /*<script src="../js/locales/fr.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="../js/locales/es.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/explorer-fa/theme.js" type="text/javascript"></script>*/ ?>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/fa/theme.js" type="text/javascript"></script>
<?php /*<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>*/ ?>

<div class="row"> 
	<div class="col s3"></div>
	<div class="col s6">
	
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'custom-segmentation-form',
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			/*'enableAjaxValidation'=>false,*/
			/* 'clientOptions'=>array('validateOnSubmit'=>true),
			'enableClientValidation'=>true, */
		)); ?>
	
		
		<?php  if($form->errorSummary($model)) : ?>
		<div class = "animated bounce">
			<div class="card-panel red lighten-2" style="color:#444">
			<span class="white-text">
				<?php echo $form->errorSummary($model); ?>
			</div>
			
		</div>
		<?php  endif; ?>
		
		<ul class="stepper horizontal">
			<li class="step active">
				<div class="step-title waves-effect">
					<div class="step-f-title">Promotion Title</div>
				</div>
				<div class="step-content">
					<div class="row">
						<p class="title-stepper"><?php echo ($model->isNewRecord) ? 'New ' : 'Edit ' ?> Promotion Set Up</p>
						
						<div class="col s12">
						
							<div class="row">
								<div class="input-field input-seg">
									<?php echo $form->labelEx($model,'title'); ?>
									<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Promotion Title','class'=>'validate','required'=>true)); ?>
									<?php echo $form->error($model,'title'); ?>
								
								</div>
							</div>
							<div class="row">
								<center>
								<label>Campaign Mode</label>
								</center>
							</div>
							<div class="row">
							
								<div class="col s4 c-campaign">
								</div>
								<div class="col s4 c-campaign">
									<?php
										echo '<img src="'.Yii::app()->theme->baseUrl.'/assets/images/phone.png" class="center-align"><br>';
										echo $form->labelEx($model,'App Promotion').'<br>';

									?>
								</div>
								<div class="col s4 c-campaign">
								</div>
							</div>
							
							<?php /*
							<div class="row">
								<div class="input-field input-seg">
									<?php echo $form->labelEx($model,'title'); ?>
									<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Promotion Title','class'=>'validate','required'=>true)); ?>
									<?php echo $form->error($model,'title'); ?>
								</div>
							</div>	
							
							*/ ?>
							
						</div>
						<div class="row">
							<div class="col s5"></div>
							<div class="col s2">
								<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
							</div>
							<div class="col s5"></div>
						</div>
					</div>
				</div>
			</li>
			
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">User Segment</div>
				</div>
				<div class="step-content">
					<div class="row row-select">
						<p class="title-stepper">Demographic Segmentation</p>
						<div class="row">
						
						<?php 
							$no = 0;
							$arrayCheckedSegmentation = array();
							foreach ($promoDetail as $detail){
								$arrayCheckedSegmentation[]=$detail->id_user_segmentation;
							}
							//print_r($arrayCheckedSegmentation);
							foreach ($dataUserSegmentation as $segment ) { 
								$no++;
								
								if ($model->isNewRecord){
									$con_check_active_class ='';
									$num_active_class ='';
									$check_class ='hidden';
									$checked = '';
									$required = 'required';
								}else{
									
									if (in_array($segment['id'], $arrayCheckedSegmentation)){
										$con_check_active_class ='aktif';
										$num_active_class ='hidden';
										$check_class ='';
										$checked = 'checked="checked"';
										$required = '';
									}else{
										$con_check_active_class ='';
										$num_active_class ='';
										$check_class ='hidden';
										$checked = '';
										$required = '';
									}
								}
						?>
								<div class="col m12 con-check no-p <?php echo $con_check_active_class ?>">
										<div class="col m7 no-p">
											<div class="con-num-count valign-wrapper left">
												<div class="center-cont">
													<div class="num <?php echo $num_active_class ?>"><?php echo $no ?></div>
													<div class="check <?php echo $check_class ?>"><i class="fa fa-check"></i></div>
													<input class="checkboxlist validate" name ="user_segment[]" value =<?php echo $segment['id'] ?> type="checkbox" <?php echo $required ?> <?php echo $checked ?>/>
												</div>
											</div>
											
											
											<div class="con left">
												<h5 class="title no-m"><?= $segment['title'];?></h5>
												<div class="left">
													<p style="padding-right: 14px;">
														<small class="super-small">Session start</small> <br>
														<small><?= date('Y-m-d', strtotime($segment['created_date']));?></small>
													</p>
												</div>
												<div class="left" style="padding-left: 40px;">
													<p>
														<small class="super-small">status</small> <br>
														<small class="<?= $segment['status'] == 1 ? 'success':'red';?> small-btn"><?= $segment['status'] == 1 ? 'Active':'Not Active';?></small>
													</p>
												</div>
												<br><br>
											</div>
											
											<?php /*
											<div class="con left">
												<h5 class="title no-m"><?php echo $segment['title'] ?></h5>
												<div class="left">
													<p style="padding-right: 14px;">
														<small class="super-small">Session start</small> <br>
														<small><?php echo $segment['created_date'] ?></small>
													</p>
												</div>
												<br><br>
												<div class="left">
													<p>
														<small class="super-small">Data source</small> <br>
														<ul class="sosmed super-small">
															<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
															<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
															<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
														</ul>
													</p>
												</div>
												<br><br>
												<div class="left">
													<p>
														<small class="super-small">status</small> <br>
														<?php if ($segment['status'] ==1) { ?>
															<small class="success small-btn">Active</small>
														<?php } else { ?>
															<small class="red small-btn">Not Active</small>
														<?php } ?>
														
														<br>
														<?php if ($segment['is_custom'] ==1) { ?>
															<small class="super-small">Custom Segmentation</small>
														<?php } else { ?>
															<small class="super-small">Clustering User Data</small>
														<?php } ?>
														
														
													</p>
												</div>
											</div>
											
											*/ ?>
										</div>
										<div class="col m5">
										
											<?php if ($segment['is_custom']==1) { ?>
												<div class="col m3">
													<div class="con-loc  valign-wrapper">
														<div class="center-cont">
															<h6 class="color-grey no-m">Total User Data</h6>
															<?php 
																$total = CustomSegmentationDetail::model()->countByAttributes(array('id_custom_segmentation'=>$segment['id']));
															?>
															<div class="countage color-green"><b><?php echo $total ?></b></div>
														</div>
													</div>
												</div>
											<?php } else{ ?>
												<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
														<div class="center-cont">
															<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
															<h5 class="percent-sel"><?= $females = $segment->females;?>%</h5>
														</div>
												</div></div>
												<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
														<div class="center-cont">
															<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
															<h5 class="percent-sel"><?= 100 - $females;?>%</h5>
														</div>
												</div></div>
											<?php } ?>
											
											<?php /*
											<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
													<div class="center-cont">
														<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
														<h5 class="percent-sel">53%</h5>
													</div>
											</div></div>
											<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
													<div class="center-cont">
														<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
														<h5 class="percent-sel">47%</h5>
													</div>
											</div></div>
											*/ ?>
										</div>
								</div>
						<?php } ?>
						
						
						<?php /*
							<div class="col m12 con-check no-p">
								<div class="col m7 no-p">
									<div class="con-num-count valign-wrapper left">
										<div class="center-cont">
											<div class="num">1</div>
											<div class="check hidden"><i class="fa fa-check"></i></div>
											<input class="checkboxlist" name ="user_segment[]" value =1 type="checkbox"/>
										</div>
									</div>
									<div class="con left">
										<h5 class="title no-m">USER SEGMENT TITLE</h5>
										<div class="left">
											<p style="padding-right: 14px;">
												<small class="super-small">Session start</small> <br>
												<small>26/3/2018</small>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">Data source</small> <br>
												<ul class="sosmed super-small">
													<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
													<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
													<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
												</ul>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">status</small> <br>
												<small class="success small-btn">Active</small>
											</p>
										</div>
									</div>
								</div>
								<div class="col m5">
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">53%</h5>
											</div>
									</div></div>
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">47%</h5>
											</div>
									</div></div>
								</div>
							</div>
							
							<div class="col m12 con-check no-p">
								<div class="col m7 no-p">
									<div class="con-num-count valign-wrapper left">
										<div class="center-cont">
											<div class="num">2</div>
											<div class="check hidden"><i class="fa fa-check"></i></div>
											<input class="checkboxlist" name ="user_segment[]" value =2 type="checkbox"/>
										</div>
									</div>
									<div class="con left">
										<h5 class="title no-m">USER SEGMENT TITLE 2</h5>
										<div class="left">
											<p style="padding-right: 14px;">
												<small class="super-small">Session start</small> <br>
												<small>26/3/2018</small>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">Data source</small> <br>
												<ul class="sosmed super-small">
													<li><a title="Instagram" href="#"><i class="fab fa-instagram"></i></a></li>
													<li><a title="Facebook" href="#"><i class="fab fa-facebook"></i></a></li>
													<li><a title="Twitter" href="#"><i class="fab fa-twitter"></i></a></li>
												</ul>
											</p>
										</div>
										<br><br>
										<div class="left">
											<p>
												<small class="super-small">status</small> <br>
												<small class="success small-btn">Active</small>
											</p>
										</div>
									</div>
								</div>
								<div class="col m5">
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">53%</h5>
											</div>
									</div></div>
									<div class="col m6"><div class="con-loc valign-wrapper"  style="height:160px;">
											<div class="center-cont">
												<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" class="responsive-img img-select">
												<h5 class="percent-sel">47%</h5>
											</div>
									</div></div>
								</div>
							</div>
						*/ ?>
							
							
						</div>
						<div class="col s5"> </div>
						<div class="col s2">
							<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
						</div>
						<div class="col s5"></div>
					</div>            
				</div>
			</li>
			
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">Strategy</div>
				</div>
				<div class="step-content">
					<div class="row">   
						<p class="title-stepper">App Promotion Schedule & Content</p>
						<div class="row" style="margin-left:20px">
							
							<div class="row">
									<p>
									<label>Promotion Mode</label>
									</p>
							</div>
							<div class="row">
							<?php if ($model->isNewRecord && $model->promo_mode==null) { 
									$pointCheck ='checked';
									$cashCheck ='';
									$pointStyle ='display:inline';
									$cashStyle ='display:none';
								}else{
									if ($model->point > 0){
										$pointCheck ='checked';
										$cashCheck ='';
										$pointStyle ='display:inline';
										$cashStyle ='display:none';
									}else{
										$pointCheck ='';
										$cashCheck ='checked';
										$pointStyle ='display:none';
										$cashStyle ='display:inline';
									}
								}
								
							?>
							
								
									<p>
									  <label>
										<input name="Promo[promo_mode]" type="radio" class="promo_mode_on" value="point" <?php echo $pointCheck ?> />
										<span>Point</span>
									  </label>
									  <?php echo $form->textField($model,'point',array('placeholder'=>'Input Point','class'=>'validate','required'=>true,'style'=>$pointStyle)); ?>
									</p>
									<p>
									  <label>
										<input name="Promo[promo_mode]" type="radio" class="promo_mode_on"  value="cash" <?php echo $cashCheck ?> />
										<span>Cash</span>
									  </label>
									  <?php echo $form->textField($model,'cash',array('placeholder'=>'Input Cash','class'=>'validate','style'=>$cashStyle)); ?>
									</p>
								
							</div>
							
							
							<div class="row">
								<p>
								<label>Promotion Category</label>
								</p>
							
								<?php
									$PromoCategory = PromoCategory::model()->findAll(array('order' => 'id ASC'));
									$listPromoCategory = CHtml::listData($PromoCategory, 'id', 'category');
								?>
								<?php echo $form->dropDownList($model,'id_category',$listPromoCategory,array('prompt'=>'NONE', 'class'=>'validate')); ?>
								<?php //echo $form->labelEx($model,'id_category'); ?>
								<?php echo $form->error($model,'id_category'); ?>
							</div>
							
							<div class="row">
								<div class="input-field input-seg col s4">
									<?php echo $form->textField($model,'begda',array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker validate','required'=>true)); ?>
									<?php echo $form->labelEx($model,'begda'); ?>
									<?php echo $form->error($model,'begda'); ?>
								</div>
								<div class="input-field input-seg col s4">
									<?php echo $form->textField($model,'endda',array('placeholder'=>'YYYY-MM-DD','class'=>'datepicker validate','required'=>true)); ?>
									<?php echo $form->labelEx($model,'endda'); ?>
									<?php echo $form->error($model,'endda'); ?>
								</div>
								<div class="input-field input-seg col s4">
									<?php echo $form->labelEx($model,'max_buy'); ?>
									<?php echo $form->textField($model,'max_buy',array('placeholder'=>'Max User Can Buy','class'=>'validate','required'=>true)); ?>	
									<?php echo $form->error($model,'max_buy'); ?>
								</div>
							</div>
							
						<div class="row s12">
							<label>Promotion Image</label>
						</div>
						
						<div class="row s12">
							<div class="file-loading">
								<?php /*<input id="file-eng" name="file-eng[]" type="file" multiple>*/ ?>
								<?php echo $form->fileField($model,'images[]',array('id'=>'file-eng','multiple'=>true)); ?>
							</div>
							<?php /*
							<div class="col s3">
								<div class="img-camp valign-wrapper center-align"><i class="material-icons">add_circle</i></div>
							</div>
							<div class="col s3">
								<div class="img-camp"></div>
							</div>
							<div class="col s3">
								<div class="img-camp"></div>
							</div>
							<div class="col s3">
								<div class="img-camp"></div>
							</div>
							*/ ?>
						</div>
							
						<div class="row">
							<?php echo $form->textArea($model,'content',array('class'=>'validate ck','rows'=>9, 'cols'=>50,'required'=>true));?>
						</div>
					</div>  
					<div class="col s5"> </div>
					<div class="col s2">
						<button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
					</div>
					<div class="col s5"></div>
				</div>
			</li>
			
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">Code</div>
				</div>
				<div class="step-content">
					<div class="row">   
						<p class="title-stepper">App Promotion Code</p>
						
						<?php /*
						<div class="row s12" style="margin-left:20px">
							<label>Promotion Code</label>
						</div>
						*/ ?>
						<div class="col s12">
							<div class="row col s12">	
								<?php 
									if(isset($_POST['Promo']['promo_code'])){
										$model->promo_code = $_POST['Promo']['promo_code'] ; 
									}else{
										$model->promo_code = $promo_code ; 
									}
								?>
								<?php echo $form->labelEx($model,'promo_code'); ?>
								<?php //echo $form->textField($model,'promo_code',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Promotion Code','class'=>'validate','required'=>true)); ?>
								<?php echo $form->textArea($model,'promo_code',array('class'=>'validate ck','rows'=>9, 'cols'=>50,'required'=>true));?>
								<?php echo $form->error($model,'promo_code'); ?>
							</div>
							<div class="row col s12">
								<?php 
									if(isset($_POST['Promo']['jumlah'])){
										$model->jumlah = $_POST['Promo']['jumlah'] ; 
									}else{
										$model->jumlah = $totalPromoKode ; 
									}
								?>
								<?php echo $form->labelEx($model,'jumlah'); ?>
								<?php echo $form->textField($model,'jumlah',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Promotion Code','class'=>'validate','required'=>true)); ?>
								<?php echo $form->error($model,'jumlah'); ?>
							</div>
						</div>
						
				
					</div>  
					<div class="col s5"> </div>
					<div class="col s2">
						<button type="submit" class="waves-effect waves-dark btn  btn-segment">Save</button>
					</div>
					<div class="col s5"></div>
				</div>
			</li>
		</ul>  
		
		<?php $this->endWidget(); ?>
		<div class="col s3"></div>                  
	</div>
</div>

<script>
<?php if ($model->isNewRecord && $model->promo_mode==null) { ?>

$( document ).ready(function() {
   $( ".promo_mode_on" ).change(function() {
	  //alert( $(this).val() );
	  hideShowPromoMode($(this).val(),true);
	});
});
<?php } else{ ?>

$( document ).ready(function() {
    //console.log( "ready!" );
	//var valueData = $( ".promo_mode_on" ).val();
	var valueData = $('.promo_mode_on:checked').val();
	//alert(valueData);
	hideShowPromoMode(valueData,false);
	
	$( ".promo_mode_on" ).change(function() {
	  //alert( $(this).val() );
	  hideShowPromoMode($(this).val(),true);
	});
}); 

<?php } ?>

function hideShowPromoMode(valueData,emptyData){
	
	 if (emptyData){
	  $('#Promo_point').toggle();
	  $('#Promo_cash').toggle();
	 }
	 
		  
	  if (valueData == 'cash'){
		   $('#Promo_cash').attr('required',true);
		   $('#Promo_point').attr('required',false);
	  }else{
		   $('#Promo_cash').attr('required',false);
		   $('#Promo_point').attr('required',true);
	  }
	  
	  // di hide aja jadi val nya gak hilang
	  /* if (emptyData){
		$('#Promo_cash').val('');
		$('#Promo_point').val('');
	  } */
}
</script>

<script>
$('#file-eng').fileinput({
       theme: 'fa',
	    //'theme': 'explorer-fa',
        //language: 'fr',
        uploadUrl: '',
	    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
	    showUpload: false,
	    showCaption: false,
	    showRemove: false,
	    //maxFileSize: 1000,
        //maxFilesNum: 10,
        allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg'],
		initialPreviewAsData: true,
		overwriteInitial: false,
		
		<?php if (!$model->isNewRecord){ // jika edit ?>
			initialPreview: [
				<?php foreach ($promoImage as $image ) { ?>
					"<?php echo MyAppComponent::getUrlImageInApp().$image->image; ?>",
				<?php } ?>
			],
			initialPreviewConfig: [
				<?php foreach ($promoImage as $image ) { ?>
				{
					//caption: "image", 
					//size: 329892, 
					width: "50px", 
					url: '<?php echo Yii::app()->createUrl('appPromotion/deleteImage')?>', 
					key: <?php echo $image->id?>,
					// extra: function() { 
						// return {
							// id: $('#id').val()
						// };
					// }, 
				},
				<?php } ?>
			]  
		<?php } ?>
    }); 
	
</script>