<title>Enterprise | App Promotion</title>

<div class="row row-seg">
	<div class="col m12">
		<h5>Free Smart Promotion</h5>	
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Views</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-purple"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-pink.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Click</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-blue"><b><i class="fa fa-arrow-up"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-blue.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6 class="color-grey">Total Buyer</h6>
				<br>
				<div class="row no-m">
					<div class="col m8 no-p">
						<div class="counter-champaign smart-count">
							<b>2300</b><br>
							<small class="color-yellow"><b><i class="fa fa-arrow-down"></i> 13.8%</b></small>
						</div>
					</div>
					<div class="col m4 no-p">
						<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/chart-yellow.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m8">
		<div class="card z-depth-1">
			<div class="card-content">
				<h5>Overview</h5>
				<canvas id="mybarChart"></canvas>
			</div>
		</div>
	</div>
	<div class="col m4">
		<div class="card z-depth-1">
			<div class="card-content">
				<h6><b>Response Details</b></h6>
				<center>
					<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/indocuy.jpg" class="responsive-img" alt="">
				</center>
				<br><br>
				<p class="no-m"><i class="fas fa-circle color-grey"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-green"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-orange"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-purple"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-blue"></i> Sumatra <small class="right">29.134</small></p>
				<p class="no-m"><i class="fas fa-circle color-orange"></i> Sumatra <small class="right">29.134</small></p>
			</div>
		</div>
	</div>	
