<title>Enterprise | Promo Detail </title>

<div class="row row-seg">

<div class="left">
	<h5>Detail Promo</h5>	
</div>
<div class="right">
	<a href="<?= Yii::app()->createAbsoluteUrl('appPromotion/update',array('id'=>$model->id));?>"><button class="waves-effect waves-dark btn btn-segment-dash">Edit Promo </button></a>
</div>


<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					
					
					<?php  if(Yii::app()->user->hasFlash('success')): ?>
						<script>
						$( document ).ready(function() {
							//console.log( "ready!" );
							var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
							M.toast({html: toastHTML});
						});
						</script>
						<?php /*
						<div class = "animated bounce">
							<div class="card-panel green lighten-2" style="color:#444">
							<span class="white-text">
								<?php  echo Yii::app()->user->getFlash('success') ; ?>	</span>
							</div>
						</div>
						*/ ?>
					<?php  endif; ?>
					<?php /*<hr class="hr-grey">*/ ?>

					<div class="left">
						<h6 class="text-bold"><b><?= $model->title;?></b></h6>
					</div>
					<br><br>
					
					<div class="row">					
						<div class="col m8">
							<p class="color-grey p-b-10"><?= $model->content;?></p>
						</div>
						<div class="col m8">
							<h6 class="text-bold"><b>Detail Data</b></h6>
							<div class="col m6">Session Start</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->begda));?></div>
							<div class="col m6">End</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->endda));?></div>
							<div class="col m6">Category</div><div class="col m6">: <?php echo ($model->category) ? $model->category->category : 'NONE';?></div>
							<div class="col m6">Promo Code</div><div class="col m6"> <?= $promo_code;?></div>
							<div class="col m6">Jumlah</div><div class="col m6">: <?= $totalPromoKode;?></div>
							<div class="col m6">Total Views</div><div class="col m6">: <?= $model->total_views;?></div>
							<div class="col m6">Total Clicks</div><div class="col m6">: <?= $model->total_clicks;?></div>
							<div class="col m6">Total Buyer</div><div class="col m6">: <?= $model->total_buys;?></div>
							<?php $statusActive = MyAppComponent::getStatusByBegdaEndda($model->begda,$model->endda);?>
							<div class="col m6">Status</div><div class="col m6">: <small class="<?= $statusActive == true ? 'success':'red';?> small-btn"><?= $statusActive == true ? 'Active':'Not Active';?></small></div>
							<div class="col m6">Status Approval</div><div class="col m6">: <?php echo MyAppComponent::getStatusApproval($model->approval); ?></div>
							<div class="col m6">Approval Message</div><div class="col m6">:  <?php echo $model->approval_message; ?></div>
							
							<div class="col m6">Image</div><div class="col m6"> <?php echo MyAppComponent::loadImageFromApp($promoImage) ?></div>

						</div>
					</div>
					
				</div>
				
				<div class="row">
					<p class="color-grey">
						User Segmentation
					</p>
					<hr class="hr-grey">
					<?php $this->widget('zii.widgets.CListView', array(
						'id'=>'list-user_segmentation', 
						'dataProvider'=>$dataProvider,
						'itemView'=>'/campaign/_view_user_segmentation',
						// 'pager'=>array(
							// 'cssFile'=>false
						// ), 
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => '',
							'lastPageLabel' => '',
							
						),
					));  ?>
				</div>
			</div>
		</div>
	</div>
</div>
