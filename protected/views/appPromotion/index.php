<title>Enterprise | App Promotion</title>

<div class="container">
	<?php  if(Yii::app()->user->hasFlash('error')): ?>
			<div class = "animated bounce">
				<div class="card-panel red lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('error') ; ?>	
				</span>
				</div>
				
			</div>
		<?php  endif; ?>
	<div class="row row-empty">
		<div class="col s5">&nbsp;</div>
		<div class="col s7 col-empty">
			<div class="text-empty">
				<p class="text-not">Hmm, It seems to be empty</p>
				<p class="text-not-b">Why don't see it now?</p>
				<a href="<?php echo Yii::app()->createUrl('appPromotion/create'); ?>"><button class="waves-effect waves-dark btn btn-segment-dash">New Promo</button></a>
			</div>
		</div>
	</div>
</div>