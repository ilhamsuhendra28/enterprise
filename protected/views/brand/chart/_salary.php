<?php
	if(!empty($tampilsalary)){
		$totalbrandr = explode(',', $tampilsalary['brand']);
		if(count($totalbrandr) > 7){
			echo '<div class="col m12">';
		}else{
			echo '<div class="col m6">';
		}
?>
		
			<canvas id="salary"></canvas>
			<script>
			if ($('#salary').length ){ 		  
				var ctx1 = document.getElementById("salary");
				var mybarChart1 = new Chart(ctx1, {
					type: 'horizontalBar',
					responsive : true,
					 showTooltips: false,
					onAnimationComplete: function () {

						var ctx1 = this.chart.ctx1;
						ctx1.font = this.scale.font;
						ctx1.fillStyle = this.scale.textColor
						ctx1.textAlign = "center";
						ctx1.textBaseline = "bottom";

						this.datasets.forEach(function (dataset) {
							dataset.points.forEach(function (points) {
								ctx1.fillText(points.value, points.x, points.y - 10);
							});
						})
					},
					data: {
						labels: [<?php echo $tampilsalary['brand']; ?>],
						datasets: [
							{
								label: '<?php echo $tampilsalary['label_brand_salary_1'] ?>',
								backgroundColor: "#db4234",
								data: [<?php echo $tampilsalary['persen_brand_salary_1']; ?>],
							}, {
								label: '<?php echo $tampilsalary['label_brand_salary_2'] ?>',
								backgroundColor: "#edac1c",
								data: [<?php echo $tampilsalary['persen_brand_salary_2']; ?>],
							}, {
								label: '<?php echo $tampilsalary['label_brand_salary_3'] ?>',
								data: [<?php echo $tampilsalary['persen_brand_salary_3']; ?>],
								backgroundColor: "#6eca28",
							}, {
								label: '<?php echo $tampilsalary['label_brand_salary_4'] ?>',
								data: [<?php echo $tampilsalary['persen_brand_salary_4']; ?>],
								backgroundColor: "#24b6dc",
							}, {
								label: '<?php echo $tampilsalary['label_brand_salary_5'] ?>',
								data: [<?php echo $tampilsalary['persen_brand_salary_5']; ?>],
								backgroundColor: "#9968a3",
							}, {
								label: '<?php echo $tampilsalary['label_brand_salary_6'] ?>',
								data: [<?php echo $tampilsalary['persen_brand_salary_6']; ?>],
								backgroundColor: "#f5951f",
							}
						]
					},
	
					options: {
						title: {
							display: true,
							text: '<?php echo strtoupper('BRANDS SALARY CATEGORY '.$_POST['kategori'].' DI BULAN '.Logic::convertBulan($_POST['bulan']).' TAHUN '.$_POST['tahun']) ?>',
							FontSize : '5px'
						},
						legend:{
							position: 'top',
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							xAxes: [{
								 stacked: true,
								ticks: {
									beginAtZero: true,
									min: 0,
									max: 100,
									callback: function(value) {
										return value + "%"
									}
								},
								scaleLabel: {
								   display: true,
								   labelString: "Percentage"
							   },
							}],
							yAxes: [{
								stacked: true
							}]
						},
						tooltips: {
							mode: 'index',
							intersect: false,
						},
						plugins: {
							datalabels: {
								color: 'white',
								display: function(context) {
									return context.dataset.data[context.dataIndex] > 15;
								},
								font: {
									weight: 'bold'
								},
								formatter: Math.round
							}
						},
					}
				});
			}
			</script>
		</div>
<?php 
	}else{
?>
	<div class="col m12" style="text-align:center;">
		<h5>Data Tidak Ditemukan</h5>	
	</div>
<?php } ?>