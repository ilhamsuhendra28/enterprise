<?php
	if(!empty($tampilmarried)){
		$totalbrandm = explode(',', $tampilmarried['brand']);
		if(count($totalbrandm) > 7){
			echo '<div class="col m12">';
		}else{
			echo '<div class="col m6">';
		}
?>
		
			<canvas id="married"></canvas>
			<script>
			if ($('#married').length ){ 		  
				var ctx1 = document.getElementById("married");
				var mybarChart1 = new Chart(ctx1, {
					type: 'horizontalBar',
					responsive : true,
					 showTooltips: false,
					onAnimationComplete: function () {

						var ctx1 = this.chart.ctx1;
						ctx1.font = this.scale.font;
						ctx1.fillStyle = this.scale.textColor
						ctx1.textAlign = "center";
						ctx1.textBaseline = "bottom";

						this.datasets.forEach(function (dataset) {
							dataset.points.forEach(function (points) {
								ctx1.fillText(points.value, points.x, points.y - 10);
							});
						})
					},
					data: {
						labels: [<?php echo $tampilmarried['brand']; ?>],
						datasets: [
							{
								label: '<?php echo $tampilmarried['label_brand_lajang'] ?>',
								backgroundColor: "#24b6dc",
								data: [<?php echo $tampilmarried['persen_brand_lajang']; ?>],
							}, {
								label: '<?php echo $tampilmarried['label_brand_menikah'] ?>',
								backgroundColor: "#9968a3",
								data: [<?php echo $tampilmarried['persen_brand_menikah']; ?>],
							}, {
								label: '<?php echo $tampilmarried['label_brand_pernah_menikah'] ?>',
								data: [<?php echo $tampilmarried['persen_brand_pernah_menikah']; ?>],
								backgroundColor: "#db4234",
							}
						]
					},
	
					options: {
						title: {
							display: true,
							text: '<?php echo strtoupper('BRANDS MARITAL CATEGORY '.$_POST['kategori'].' DI BULAN '.Logic::convertBulan($_POST['bulan']).' TAHUN '.$_POST['tahun']) ?>',
							FontSize : '5px'
						},
						legend:{
							position: 'top',
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							xAxes: [{
								 stacked: true,
								ticks: {
									beginAtZero: true,
									min: 0,
									max: 100,
									callback: function(value) {
										return value + "%"
									}
								},
								scaleLabel: {
								   display: true,
								   labelString: "Percentage"
							   },
							}],
							yAxes: [{
								stacked: true
							}]
						},
						tooltips: {
							mode: 'index',
							intersect: false,
						},
						plugins: {
							datalabels: {
								color: 'white',
								display: function(context) {
									return context.dataset.data[context.dataIndex] > 15;
								},
								font: {
									weight: 'bold'
								},
								formatter: Math.round
							}
						},
					}
				});
			}
			</script>
		</div>
<?php 
	}else{
?>
	<div class="col m12" style="text-align:center;">
		<h5>Data Tidak Ditemukan</h5>	
	</div>
<?php } ?>