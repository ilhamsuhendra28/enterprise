<?php 
	$arrv = explode(',', $pbrand);
	foreach ($arrv as $idx=>$value) {
		$tampilkota = Logic::getQueryKota($ptahun, $pbulan, $pkategori, $value); 
		// var_dump($tampilkota);exit;
?>
	<?php
		echo '<div class="col m6">';	
	?>
			
				<canvas id="city_<?php echo $idx; ?>"></canvas>
				<script>
				if ($('#city_'+<?php echo $idx; ?>).length ){ 		  
					var ctx1 = document.getElementById("city_"+<?php echo $idx; ?>);
					var mybarChart1 = new Chart(ctx1, {
						type: 'horizontalBar',
						responsive : true,
						 showTooltips: false,
						onAnimationComplete: function () {

							var ctx1 = this.chart.ctx1;
							ctx1.font = this.scale.font;
							ctx1.fillStyle = this.scale.textColor
							ctx1.textAlign = "center";
							ctx1.textBaseline = "bottom";

							this.datasets.forEach(function (dataset) {
								dataset.points.forEach(function (points) {
									ctx1.fillText(points.value, points.x, points.y - 10);
								});
							})
						},
						data: {
							labels: [<?php echo $tampilkota['brand']['label']; ?>],
							datasets: [
								<?php foreach($tampilkota['label'] as $lrow){ ?>
									{
										label: '<?php echo $lrow['city']; ?>',
										backgroundColor: "<?php echo $lrow['color']; ?>",
										data: [<?php echo $tampilkota['percentage']; ?>],
									}, 
								<?php } ?>
							]
						},
		
						options: {
							title: {
								display: true,
								text: '<?php echo strtoupper('BRANDS CITY CATEGORY '.$_POST['kategori'].' DI BULAN '.Logic::convertBulan($_POST['bulan']).' TAHUN '.$_POST['tahun']) ?>',
								FontSize : '5px'
							},
							legend:{
								position: 'top',
								labels:{
									FontSize : '5px'
								}
							},
							scales: {
								xAxes: [{
									 stacked: true,
									ticks: {
										beginAtZero: true,
										min: 0,
										max: 100,
										callback: function(value) {
											return value + "%"
										}
									},
									scaleLabel: {
									   display: true,
									   labelString: "Percentage"
								   },
								}],
								yAxes: [{
									stacked: true
								}]
							},
							tooltips: {
								mode: 'index',
								intersect: false,
							},
							plugins: {
								datalabels: {
									color: 'white',
									display: function(context) {
										return context.dataset.data[context.dataIndex] > 15;
									},
									font: {
										weight: 'bold'
									},
									formatter: Math.round
								}
							},
						}
					});
				}
				</script>
			</div>
<?php		
	}
?>