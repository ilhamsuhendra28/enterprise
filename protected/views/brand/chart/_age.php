<?php
	if(!empty($tampilage)){
		$totalbranda = explode(',', $tampilage['brand']);
		if(count($totalbranda) > 7){
			echo '<div class="col m12">';
		}else{
			echo '<div class="col m6">';
		}
?>
		
			<canvas id="age"></canvas>
			<script>
			if ($('#age').length ){ 		  
				var ctx1 = document.getElementById("age");
				var mybarChart1 = new Chart(ctx1, {
					type: 'horizontalBar',
					responsive : true,
					 showTooltips: false,
					onAnimationComplete: function () {

						var ctx1 = this.chart.ctx1;
						ctx1.font = this.scale.font;
						ctx1.fillStyle = this.scale.textColor
						ctx1.textAlign = "center";
						ctx1.textBaseline = "bottom";

						this.datasets.forEach(function (dataset) {
							dataset.points.forEach(function (points) {
								ctx1.fillText(points.value, points.x, points.y - 10);
							});
						})
					},
					data: {
						labels: [<?php echo $tampilage['brand']; ?>],
						datasets: [
							{
								label: '<?php echo $tampilage['label_brand_range_umur_1'] ?>',
								backgroundColor: "#db4234",
								data: [<?php echo $tampilage['persen_brand_range_umur_1']; ?>],
							}, {
								label: '<?php echo $tampilage['label_brand_range_umur_2'] ?>',
								backgroundColor: "#edac1c",
								data: [<?php echo $tampilage['persen_brand_range_umur_2']; ?>],
							}, {
								label: '<?php echo $tampilage['label_brand_range_umur_3'] ?>',
								data: [<?php echo $tampilage['persen_brand_range_umur_3']; ?>],
								backgroundColor: "#6eca28",
							}, {
								label: '<?php echo $tampilage['label_brand_range_umur_4'] ?>',
								data: [<?php echo $tampilage['persen_brand_range_umur_4']; ?>],
								backgroundColor: "#24b6dc",
							}, {
								label: '<?php echo $tampilage['label_brand_range_umur_5'] ?>',
								data: [<?php echo $tampilage['persen_brand_range_umur_5']; ?>],
								backgroundColor: "#9968a3",
							}
						]
					},
	
					options: {
						title: {
							display: true,
							text: '<?php echo strtoupper('BRANDS AGE CATEGORY '.$_POST['kategori'].' DI BULAN '.Logic::convertBulan($_POST['bulan']).' TAHUN '.$_POST['tahun']) ?>',
							FontSize : '5px'
						},
						legend:{
							position: 'top',
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							xAxes: [{
								 stacked: true,
								ticks: {
									beginAtZero: true,
									min: 0,
									max: 100,
									callback: function(value) {
										return value + "%"
									}
								},
								scaleLabel: {
								   display: true,
								   labelString: "Percentage"
							   },
							}],
							yAxes: [{
								stacked: true
							}]
						},
						tooltips: {
							mode: 'index',
							intersect: false,
						},
						plugins: {
							datalabels: {
								color: 'white',
								display: function(context) {
									return context.dataset.data[context.dataIndex] > 15;
								},
								font: {
									weight: 'bold'
								},
								formatter: Math.round
							}
						},
					}
				});
			}
			</script>
		</div>
<?php 
	}else{
?>
	<div class="col m12" style="text-align:center;">
		<h5>Data Tidak Ditemukan</h5>	
	</div>
<?php } ?>