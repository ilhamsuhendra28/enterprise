<?php
	if(!empty($tampilgender)){
		$totalbrandg = explode(',', $tampilgender['brand']);
		if(count($totalbrandg) > 7){
			echo '<div class="col m12">';
		}else{
			echo '<div class="col m6">';
		}
?>
		
			<canvas id="gender"></canvas>
			<script>
			if ($('#gender').length ){ 		  
				var ctx1 = document.getElementById("gender");
				var mybarChart1 = new Chart(ctx1, {
					type: 'horizontalBar',
					responsive : true,
					 showTooltips: false,
					onAnimationComplete: function () {

						var ctx1 = this.chart.ctx1;
						ctx1.font = this.scale.font;
						ctx1.fillStyle = this.scale.textColor
						ctx1.textAlign = "center";
						ctx1.textBaseline = "bottom";

						this.datasets.forEach(function (dataset) {
							dataset.points.forEach(function (points) {
								ctx1.fillText(points.value, points.x, points.y - 10);
							});
						})
					},
					data: {
						labels: [<?php echo $tampilgender['brand']; ?>],
						datasets: [
							{
								label: '<?php echo $tampilgender['label_brand_male']; ?>',
								backgroundColor: "#ecad10",
								data: [<?php echo $tampilgender['persen_brand_laki']; ?>],
							}, {
								label: '<?php echo $tampilgender['label_brand_female']; ?>',
								backgroundColor: "#4e7a99",
								data: [<?php echo $tampilgender['persen_brand_perempuan']; ?>],
							}
						]
					},
	
					options: {
						title: {
							display: true,
							text: '<?php echo strtoupper('BRANDS GENDER CATEGORY '.$_POST['kategori'].' DI BULAN '.Logic::convertBulan($_POST['bulan']).' TAHUN '.$_POST['tahun']); ?>',
							FontSize : '5px'
						},
						legend:{
							position: 'top',
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							xAxes: [{
								 stacked: true,
								ticks: {
									beginAtZero: true,
									min: 0,
									max: 100,
									callback: function(value) {
										return value + "%"
									}
								},
								scaleLabel: {
								   display: true,
								   labelString: "Percentage"
							   },
							}],
							yAxes: [{
								stacked: true
							}]
						},
						tooltips: {
							mode: 'index',
							intersect: false,
						},
						plugins: {
							datalabels: {
								color: 'white',
								display: function(context) {
									return context.dataset.data[context.dataIndex] > 15;
								},
								font: {
									weight: 'bold'
								},
								formatter: Math.round
							}
						},
					}
				});
			}
			</script>
		</div>
<?php 
	}else{
?>
	<div class="col m12" style="text-align:center;">
		<h5>Data Tidak Ditemukan</h5>	
	</div>
<?php } ?>