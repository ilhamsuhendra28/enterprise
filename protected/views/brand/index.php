<title>Enterprise | Brand</title>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/tag-input/src/jquery.tagsinput.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/tag-input/src/jquery.tagsinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/select2/select2.css" />
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/select2/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/select2/select2.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/chartjs-plugin-datalabels.min.js"></script>

<?php 
	if(empty($_POST['tahun'])){
		$dtahun = date('Y');
	}else{
		$dtahun = $_POST['tahun'];
	}
	
	if(empty($_POST['bulan'])){
		$dbulan = date('m');
	}else{
		$dbulan = $_POST['bulan'];
	}
	
	if(!empty($_POST['kategori'])){
		$dkategori = $_POST['kategori'];
	}
?>

<div class="row row-seg">
	<div class="col m12">
		<h5>Brand</h5>	
	</div>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<div class="form">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'brand-form',
							'action'=>Yii::app()->createUrl($this->route),
							'method'=>'post', 
						)); ?>
							
							<div class="col m4 input-field input-f">
								<?php echo CHtml::dropDownList('tahun', $dtahun, CHtml::listData($tahun, 'tahun', 'tahun'), array('class'=>'validate'));?>
								<label for="tahun">Tahun</label>
							</div>	
								
							<div class="col m4 input-field input-f">
								<?php echo CHtml::dropDownList('bulan', $dbulan, Logic::getBulan(), array('class'=>'validate'));?>
								<label for="bulan">Bulan</label>
								
							</div>
							
							<div class="col m4 input-field input-f">
								<?php echo CHtml::dropDownList('kategori', $dkategori, CHtml::listData($kategori, 'kategori', 'kategori'), array('onchange'=>'getKategori($(this).val())'));?>
								<label for="kategori">Kategori</label>
							</div>	
							
							<div class="col m12 input-f">
								<label for="brand">Brand</label>
								<input type="hidden" class="bigdrop validate" id="e6" style="width:100%;"  name="brand" value="<?php echo $pbrand; ?>" />
								<div id="niks"></div>
								<div class="select2-container select2-container-multi bigdrop validate" id="s2id_e6" style="width:100%;">
									<ul class="select2-choices">
										<?php 
											if($show){
												$arrv = explode(',', $pbrand);
												foreach ($arrv as $vdx=>$value) {
										?>			
													<li class="select2-search-choice" id="hapus_<?php echo $vdx; ?>">    
														<div><?php echo $value; ?></div>  
														<a href="#" onclick="dclose('<?php echo $vdx; ?>','<?php echo $value; ?>', '<?php echo $pbrand; ?>')" class="select2-search-choice-close" tabindex="-1"></a>
													</li>
										<?php	
												}	
											}
										?>
									</ul>
								</div>
							</div>
				
							<div class="col m12">
								<button type="submit" class="waves-effect waves-dark btn btn-segment-dash">Cari Brand</button>
								<a class="waves-effect waves-dark btn btn-segment-dash" href="<?php echo Yii::app()->createUrl('brand/index'); ?>">Reset Brand</a>
							</div>
						<?php $this->endWidget(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
	if ($show) {
	?>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<?php $this->renderPartial('chart/_gender', array('tampilgender'=>$tampilgender)); ?>
					<?php $this->renderPartial('chart/_married', array('tampilmarried'=>$tampilmarried)); ?>
					<?php $this->renderPartial('chart/_age', array('tampilage'=>$tampilage)); ?>
					<?php $this->renderPartial('chart/_religion', array('tampilreligion'=>$tampilreligion)); ?>
					<?php $this->renderPartial('chart/_salary', array('tampilsalary'=>$tampilsalary)); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row no-m">
					<?php $this->renderPartial('chart/_city', array(
						'ptahun'=>$ptahun,
						'pbulan'=>$pbulan,
						'pkategori'=>$pkategori,
						'pbrand'=>$pbrand
					)); ?>
				</div>
			</div>
		</div>
	</div>
	<?php 
	}
	?>
</div>

<script>
jq214 = jQuery.noConflict(true);

var encodeHtmlEntity = function(str) {
  var buf = [];
  for (var i=str.length-1;i>=0;i--) {
    buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
  }
  return buf.join('');
};

function dclose(index, brand, pbrand){
	branduri = encodeURIComponent(brand);
	pbranduri = encodeURIComponent(pbrand);
	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: '<?php echo Yii::app()->createAbsoluteUrl("brand/eliminate"); ?>',
		data:{brand:branduri, pbrand:pbranduri},
		
		beforeSend: function() {
			$("#main-loader").show();
		},
		
		success:function(data){
			$("#main-loader").hide();
			$("#hapus_"+index).remove();
			$("#e6").val(data.hasil);
		},
	});
}

function repoFormatResult(repo) {        
	var markup = '<div class="row">' +
			'<div class="col m12">' +
			'<div class="row">' +
			'<div class="col m12">' + repo.text + '</div>' +
			'</div>';
			
	markup += '</div></div>';
	
	return markup;
}

function repoFormatSelection(repo) {
	return repo.text;
}
 
	
function getKategori(obj){
	jq214(document).ready(function () {
		jq214("#e6").select2({
			multiple: true,
			searchInputPlaceholder: 'Input Brands',
			minimumInputLength: 1,
			ajax: {
				url: "<?php echo Yii::app()->controller->createUrl("brand/compare"); ?>",
				dataType: 'json',
				quietMillis: 250,
				data: function (valbrand, page) {
					return {
						valbrand: valbrand, 
						kategori : obj,
					};
				},   
				results: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama_brand,
								id: item.nama_brand
							}
						})
					};
				},
				cache: true
			},
			formatResult: repoFormatResult, 
			formatSelection: repoFormatSelection,
			dropdownCssClass: "bigdrop", 
			escapeMarkup: function (m) {
				return m;
			} 
		});
	});
}

jq214(document).ready(function () {
	jq214("#e6").select2({
		multiple: true,
		searchInputPlaceholder: 'Input Brands',
		minimumInputLength: 1,
		ajax: {
			url: "<?php echo Yii::app()->controller->createUrl("brand/compare"); ?>",
			dataType: 'json',
			quietMillis: 250,
			data: function (valbrand, page) {
				return {
					valbrand: valbrand, 
					kategori : $('select#kategori').val(),
				};
			},   
			results: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama_brand,
							id: item.nama_brand
						}
					})
				};
			},
			cache: true
		},
		formatResult: repoFormatResult, 
		formatSelection: repoFormatSelection,
		dropdownCssClass: "bigdrop", 
		escapeMarkup: function (m) {
			return m;
		} 
	});
});

</script>