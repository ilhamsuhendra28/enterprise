<?php
/* @var $this CampaignController */
/* @var $data Campaign */
?>
<?php 
$con_check_active_class ='';
$num_active_class ='';
$check_class ='hidden';			
$checked = '';
$value = $data;

$no = $widget->dataProvider->getPagination()->currentPage * $widget->dataProvider->getPagination()->pageSize + $index + 1;
$url = Yii::app()->createUrl('clusteringCustom/viewUserSegmentation',array('id'=> $value['id']));

						
?>					
	
<div class="col m12 con-check no-p <?php echo $con_check_active_class ?>" data-url="<?php echo $url?>" >
	<div class="col m7 no-p">
		<div class="con-num-count valign-wrapper left">
			<div class="center-cont">
				<div class="num <?php echo $num_active_class ?>"><?php echo $no ?></div>
				<div class="check <?php echo $check_class ?>"><i class="fa fa-check"></i></div>
				<input class="checkboxlist" name ="user_segment[]" value =<?php echo $value['id'] ?> type="checkbox" <?php echo $checked ?>/>
			</div>
		</div>
		<div class="con left">
			<h5 class="title no-m"><?= $value->title;?></h5>
			<div class="left">
				<p style="padding-right: 14px;">
					<small class="super-small">Session start</small> <br>
					<small><?= date('Y-m-d', strtotime($value->created_date));?></small>
				</p>
			</div>
			<div class="left" style="padding-left: 40px;">
				<p>
					<small class="super-small">status</small> <br>
					<small class="<?= $value->status == 1 ? 'success':'red';?> small-btn"><?= $value->status == 1 ? 'Active':'Not Active';?></small>
				</p>
			</div>
			<br><br>
			<!-- <div class="left">
				<p>
					<small class="super-small">Data source</small> <br>
					<ul class="sosmed super-small">
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-instagram"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-facebook"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-google-wallet"></i></a></li>
					</ul>
				</p>
			</div>-->
			<?php /*
			<div class="left" style="padding-left: 10px">
				<p>
					<small class="super-small">Gender</small> <br>
					<div class="flex">
						<img style="max-height: 15px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" alt="">
						<small class="color-grey"><?= $females = $value->females;?>%</small>
						&nbsp;&nbsp;
						<img style="max-height: 15px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" alt="">
						<small class="color-grey"><?= 100 - $females;?>%</small>
					</div>
				</p>
			</div>
			*/ ?>
		</div>
	</div>
	<div class="col m5">
		<div class="col m3">
			<div class="con-loc  valign-wrapper">
				<div class="center-cont">
					<h6 class="color-grey no-m">Total User Data</h6>
					<?php 
						$total = CustomSegmentationDetail::model()->countByAttributes(array('id_custom_segmentation'=>$value->id));
					?>
					<div class="countage color-green"><b><?php echo $total ?></b></div>
				</div>
			</div>
		</div>
		<?php /*
		<div class="col m4">
			<div class="con-loc  valign-wrapper">
				<div class="center-cont">
					<h6 class="color-grey no-m">Top Income</h6>
					<div class="countage color-blue"><b>3.1M - 5M</b></div>
				</div>
			</div>
		</div>
		*/ ?>
	</div>
</div>
