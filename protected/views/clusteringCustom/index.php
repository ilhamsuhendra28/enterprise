<title>Enterprise | User Segment Upload Data</title>

<div class="container">
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		<?php  endif; ?>
		
	<div class="row row-empty">
		<div class="col s5">&nbsp;</div>
		<div class="col s7 col-empty">
			<div class="text-empty">
				<p class="text-not">Oops, No user segment to find</p>
				<p class="text-not-b">Let's make one</p>
				<a href="<?php echo Yii::app()->createUrl('clusteringCustom/create'); ?>"><button class="waves-effect waves-dark btn btn-segment-dash">New Segment</button></a>
			</div>
		</div>
	</div>
</div>