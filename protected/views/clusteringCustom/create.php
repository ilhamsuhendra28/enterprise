<title>Enterprise | <?php echo ($model->isNewRecord) ? 'New Segment ' : 'Edit Segment' ?> Upload Data</title>
<?php $script = '';?>
<div class="row"> 
	<div class="col s2"></div>
	<div class="col s8">
	
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'custom-segmentation-form',
		'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		/*'enableAjaxValidation'=>false,*/
		/* 'clientOptions'=>array('validateOnSubmit'=>true),
		'enableClientValidation'=>true, */
	)); ?>
	
		
		<?php  if($form->errorSummary($model)) : ?>
		<div class = "animated bounce">
			<div class="card-panel red lighten-2" style="color:#444">
			<span class="white-text">
				<?php echo $form->errorSummary($model); ?>
			</div>
			
		</div>
		<?php  endif; ?>
		
	

		<ul class="stepper horizontal">
                <li class="step active">
                    <div class="step-title waves-effect">
                        <div class="step-f-title">Segmentation Title</div>
                    </div>
                    <div class="step-content">
                        <div class="row">
                            <p class="title-stepper"><?php echo ($model->isNewRecord) ? 'New ' : 'Edit ' ?> User Segment</p>
                            <div class="col s2"></div>
                            <div class="col s8">
                            <div class="input-field input-seg">
                                <?php echo $form->labelEx($model,'title'); ?>
								<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'placeholder'=>'Segmentation Title','class'=>'validate','required'=>true)); ?>
								<?php echo $form->error($model,'title'); ?>
                            </div>
                            <div class="input-field input-seg">								
								<?php echo $form->labelEx($model,'description'); ?>
								<?php echo $form->textField($model,'description',array('maxlength'=>255,'placeholder'=>'Segmentation Description','class'=>'validate','required'=>true)); ?>
								<?php echo $form->error($model,'description'); ?>
		
                            </div>
                            </div>
                            <div class="col s2"></div>
                            <div class="row">
                                <div class="col s5"></div>
                                <div class="col s2">
                                    <button class="waves-effect waves-dark btn next-step btn-segment">Next</button>
                                </div>
                                <div class="col s5"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="step">
                    <div class="step-title waves-effect">
                        <div class="step-f-title">File Data</div>
                    </div>
                    <div class="step-content">
                         <div class="row">
                            <p class="title-stepper">File Data User Segmentation</p>
                            <div class="col s2"></div>
                            <div class="col s8">
                            <div class="input-field input-seg">
							
									Silahkan download terlebih dahulu template file untuk data upload 
									<a href="<?php echo Yii::app()->baseUrl; ?>/download/files/excel/template/user_segmentation/template_upload_data_segmentation_1_0_0.xlsx" target ="_blank"> disini </a>
									 <br>
									 <?php
										if(!$model->isNewRecord){
											echo 'Data yang anda Upload Akan Mereplace Data Sebelumnya';
										}
									 ?>
								 <div class="file-field input-field">
								  <div class="btn">
									<span>File</span>
									<?php echo $form->fileField($model,'file',array()); ?>
								  </div>
								  <div class="file-path-wrapper">
									<?php echo CHtml::textField('file-path','',array('placeholder'=>'Upload Data Segmentation','class'=>'file-path validate','required'=>true)); ?>
								  </div>
								</div>
	
                               
                            </div>
                            
                            </div>
                            <div class="col s2"></div>
                            <div class="row">
                                <div class="col s5"></div>
                                <div class="col s2">
                                    <button type="submit" class="waves-effect waves-dark btn  btn-segment">Save</button>
                                </div>
                                <div class="col s5"></div>
                            </div>
                        </div>            
                    </div>
                </li>
            </ul>  
        
		<?php $this->endWidget(); ?>
		
	 <div class="col s2"></div>                  
	</div>
</div>
