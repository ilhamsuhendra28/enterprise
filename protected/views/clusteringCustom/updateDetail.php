<title>Enterprise | <?php echo ($model->isNewRecord) ? 'New Detail Segment ' : 'Edit Detail Segment' ?> Upload Data</title>
<?php $script = '';?>
<div class="row"> 
	<div class="col s2"></div>
	<div class="col s8">
	
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'custom-segmentation-form',
		'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		/*'enableAjaxValidation'=>false,*/
		/* 'clientOptions'=>array('validateOnSubmit'=>true),
		'enableClientValidation'=>true, */
	)); ?>
	
		
		<?php  if($form->errorSummary($model)) : ?>
		<div class = "animated bounce">
			<div class="card-panel red lighten-2" style="color:#444">
			<span class="white-text">
				<?php echo $form->errorSummary($model); ?>
			</div>
			
		</div>
		<?php  endif; ?>
		
	

		<ul class="stepper horizontal">
                <li class="step active">
					<?php /*
                    <div class="step-title waves-effect">
                        <div class="step-f-title">Detail Segmentation</div>
                    </div>
					*/ ?>
                    <div class="step-content">
                        <div class="row">
                            <p class="title-stepper"><?php echo ($model->isNewRecord) ? 'New ' : 'Edit ' ?> User Detail Segment</p>
                            <div class="col s2"></div>
                            <div class="col s8">
                            <div class="input-field input-seg">
                                <?php echo $form->labelEx($model,'nama'); ?>
								<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>255,'placeholder'=>'Segmentation Title','class'=>'validate','required'=>true)); ?>
								<?php echo $form->error($model,'nama'); ?>
                            </div>
                            <div class="input-field input-seg">								
								<?php echo $form->labelEx($model,'no_hp'); ?>
								<?php echo $form->textField($model,'no_hp',array('maxlength'=>255,'placeholder'=>'Segmentation Description','class'=>'validate','required'=>true)); ?>
								<?php echo $form->error($model,'no_hp'); ?>
		
                            </div>
							<div class="input-field input-seg">								
								<?php echo $form->labelEx($model,'email'); ?>
								<?php echo $form->textField($model,'email',array('maxlength'=>255,'placeholder'=>'Segmentation Description','class'=>'validate','required'=>true)); ?>
								<?php echo $form->error($model,'email'); ?>
		
                            </div>
                            </div>
                            <div class="col s2"></div>
                            <div class="row">
                                <div class="col s5"></div>
                                <div class="col s2">
                                                                        <button type="submit" class="waves-effect waves-dark btn  btn-segment">Save</button>
                                </div>
                                <div class="col s5"></div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>  
        
		<?php $this->endWidget(); ?>
		
	 <div class="col s2"></div>                  
	</div>
</div>
