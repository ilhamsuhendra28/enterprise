<title>Enterprise | User Overview Custom</title>

<?php
	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			
			$.fn.yiiListView.update('list-segmentation', { 
				data: $(this).serialize()
			});
			
			return false;
		});
	");
?>

<div class="row row-seg">
	<div class="col m12">
		
		<div class="left">
            <h5>User Segment Custom</h5>	
        </div>
        <div class="right">
            <a href="<?= Yii::app()->createAbsoluteUrl('clusteringCustom/create');?>"><button class="waves-effect waves-dark btn btn-segment-dash">New Segment</button></a>
        </div>
		
		<?php  if(Yii::app()->user->hasFlash('success')): ?>
		
			<script>
			$( document ).ready(function() {
				//console.log( "ready!" );
				var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
				M.toast({html: toastHTML});
			});
			</script>
			<?php /*
			<div class = "animated bounce">
				<div class="card-panel green lighten-2" style="color:#444">
				<span class="white-text">
					<?php  echo Yii::app()->user->getFlash('success') ; ?>	
				</span>
				</div>
				
			</div>
			*/ ?>
		<?php  endif; ?>
		
		
	</div>
	
	<?php /*
	<div class="col m6">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="left">
					<h6 class="text-bold"><b>Overview</b></h6>
				</div>
				<div class="right">
					<ul class="sosmed">
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-instagram"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-facebook"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-google-wallet"></i></a></li>
					</ul>
				</div>
				<br><br>
				<div class="row">
					<div class="col m6 no-p">
						<p class="color-grey p-b-10">Gender</p>
					</div>
					<div class="col m6">
						<p class="color-grey p-b-10">Age Range</p>
					</div>
					<div class="col m6 no-p">
						<div class="col m6 no-p">
							<div class="flex">
								<div class="col m3 no-p">
									<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" alt="">
								</div>
								<div class="col m9 m-t-auto">
									<small class="color-grey">Female 53%</small>
									<h6 class="no-m counter"><b>3229</b></h6>
								</div>
							</div>
						</div>
						<div class="col m6 no-p">
							<div class="flex">
								<div class="col m3 no-p">
									<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" alt="">
								</div>
								<div class="col m9 m-t-auto">
									<small class="color-grey">Male 53%</small>
									<h6 class="no-m counter"><b>3229</b></h6>
								</div>
							</div>
						</div>
						<div class="col m12 no-p">
							<canvas id="mybarChart1"></canvas>
						</div>
					</div>
					<div class="col m6">
						<small class="color-grey">15></small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">15></small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">15></small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">15></small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">15></small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: 70%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m6">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="left">
					<h6 class="text-bold"><b>User Segmentation by Island</b></h6>
				</div>
				<br><br>
				<div class="row">
					<div class="col m12"><p class="color-grey p-b-10">&nbsp;</p></div>
					<div class="col m7">
						<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/indo.jpg" class="responsive-img" alt="">
					</div>
					<div class="col m5">
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate purple-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate red-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate orange-bg" style="width: 70%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	*/ ?>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					<p class="color-grey">
						All Segmentation
					</p>
					<hr class="hr-grey">
					
					
					<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
					<?php //echo CHtml::link('New User Segmentation',array('clusteringCustom/create'),array('class'=>'waves-effect waves-dark btn')); ?>
					<div class="search-form" style="margin-top:20px;display:none">
						<?php $this->renderPartial('_search',array(
							'model'=>$model,
						)); ?>
					</div>
					
					
					
					<?php $this->widget('zii.widgets.CListView', array(
						'id'=>'list-segmentation', 
						'dataProvider'=>$dataProvider,
						'itemView'=>'_view_user_segmentation',
						/* 'pager'=>array(
							'cssFile'=>false
						), */
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => '',
							'lastPageLabel' => '',
							
						),
					)); ?>
					
					<?php /*
					<div class="col m12 con-check no-p">
						<div class="col m7 no-p">
							<div class="con-num-count valign-wrapper left">
								<div class="center-cont">
									<div class="num">1</div>
									<div class="check hidden"><i class="fa fa-check"></i></div>
								</div>
							</div>
							<div class="con left">
								<h5 class="title no-m">USER SEGMENT TITLE</h5>
								<div class="left">
									<p style="padding-right: 14px;">
										<small class="super-small">Session start</small> <br>
										<small>26/3/2018</small>
									</p>
								</div>
								<div class="left" style="padding-left: 40px;">
									<p>
										<small class="super-small">status</small> <br>
										<small class="success small-btn">Active</small>
									</p>
								</div>
								<br><br>
								<div class="left">
									<p>
										<small class="super-small">Data source</small> <br>
										<ul class="sosmed super-small">
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-instagram"></i></a></li>
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-facebook"></i></a></li>
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-google-wallet"></i></a></li>
										</ul>
									</p>
								</div>
								<div class="left" style="padding-left: 10px">
									<p>
										<small class="super-small">Gender</small> <br>
										<div class="flex">
											<img style="max-height: 15px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" alt="">
											<small class="color-grey">53%</small>
											&nbsp;&nbsp;
											<img style="max-height: 15px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" alt="">
											<small class="color-grey">53%</small>
										</div>
									</p>
								</div>
							</div>
						</div>
						<div class="col m5">
							<div class="col m3"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Top Age</h6>
									<div class="countage color-green"><b>20-29</b></div>
								</div>
							</div></div>
							<div class="col m4"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Top Income</h6>
									<div class="countage color-blue"><b>3.1M - 5M</b></div>
								</div>
							</div></div>
							<div class="col m5"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Top Island</h6>
									<div class="countage color-orange"><b>Kalimantan</b></div>
								</div>
							</div></div>
						</div>
					</div>
					
					*/ ?>
					
				</div>
			</div>
		</div>
	</div>
</div>