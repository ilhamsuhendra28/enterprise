<div class="row row-seg">

<div class="col m12">
	<h5>Detail Segmentation</h5>	
</div>
					
<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					
					<?php  if(Yii::app()->user->hasFlash('success')): ?>
						<script>
						$( document ).ready(function() {
							//console.log( "ready!" );
							var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
							M.toast({html: toastHTML});
						});
						</script>
						<?php /*
						<div class = "animated bounce">
							<div class="card-panel green lighten-2" style="color:#444">
							<span class="white-text">
								<?php  echo Yii::app()->user->getFlash('success') ; ?>	</span>
							</div>
						</div>
						*/ ?>
					<?php  endif; ?>
					<div class="left">
						<h6 class="text-bold"><b><?= $model->title;?></b></h6>
					</div>
					
					<div class="right">
						<?php echo CHtml::link('Edit Segmentation',array('clusteringCustom/update','id'=>$model->id),array('class'=>'waves-effect waves-dark btn btn-segment-dash')); ?>
					</div>
					<br><br>
					<div class="row">					
						<div class="col m8">
							<p class="color-grey p-b-10"><?= $model->description;?></p>
						</div>
						<div class="col m8">
							<h6 class="text-bold"><b>Detail Data</b></h6>
							<div class="col m6">Status</div><div class="col m6">: <small class="<?= $model->status == 1 ? 'success':'red';?> small-btn"><?= $model->status == 1 ? 'Active':'Not Active';?></small></div>
							<div class="col m6">Session Start</div><div class="col m6">: <?= date('Y-m-d', strtotime($model->created_date));?></div>
						</div>
					</div>
				
					<?php /*
					<?php $this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'htmlOptions'=>array('class'=>'detail-view bordered'),
						'cssFile' => Yii::app()->request->baseUrl.'/css/widget/detailview/styles.css',
						'attributes'=>array(
							//'id',
							'title',
							'description',
							'status',
							'is_custom',
						),
					)); ?>
					*/ ?>

				</div>
				
				<div class="row">
					<p class="color-grey">
						User Data Segmentation
					</p>
					<?php echo CHtml::link('New Detail Segmentation',array('clusteringCustom/createDetail','id_segmentation'=>$model->id),array('class'=>'waves-effect waves-dark btn')); ?>
					
					<hr class="hr-grey">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'dummy-grid',
						'dataProvider'=>$dataProvider,
						//'filter'=>$model,
						'cssFile' => Yii::app()->request->baseUrl.'/css/widget/gridview/styles.css',
						//'cssFile' => FALSE,
						//'htmlOptions'=>array('class'=>'striped'),
						//'loadingCssClass'=>'',
						//'beforeAjaxUpdate' => 'loadingup', 
						//'afterAjaxUpdate' => 'loadingdown', 
						'itemsCssClass' => 'materialize-items border-right bordered striped condensed responsive-table',
						'pager' => array(
							'header' => '',
							'hiddenPageCssClass' => 'disabled',
							'htmlOptions'=>array('class'=>'pagination'),
							'selectedPageCssClass'=>'page active',
							'internalPageCssClass'=>'page waves-effect',
							'previousPageCssClass'=>'next waves-effect',
							'nextPageCssClass'=>'previous waves-effect',
							
							//'maxButtonCount' => 3,
							'cssFile' => false,
							//'cssFile' => Yii::app()->request->baseUrl.'/widget/pagination/pager.css',
							//'prevPageLabel' => '<i class="icon-chevron-left"></i>',
							//'nextPageLabel' => '<i class="icon-chevron-right"></i>',
							//'firstPageLabel' => 'First',
							//'lastPageLabel' => 'Last',
							'prevPageLabel' => '<i class="material-icons">chevron_left</i>',
							'nextPageLabel' => '<i class="material-icons">chevron_right</i>',
							'firstPageLabel' => '',
							'lastPageLabel' => '',
							
						),
						'columns'=>array(
							//'id',
							array(
								'header'=>'No',    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							),
							'nama',
							'no_hp',
							'email',
							
							array(
							'class'=>'CButtonColumn',
							'template'=>'{update}{delete}',
							'htmlOptions'=>array('width'=>'150px'),
							'buttons'=>array(
										'update'=>
										array(
											
											 'label'=>'<i class ="fa fa-pencil"></i>',
											'imageUrl'=>false,
											'url'=>function($data){
													return Yii::app()->createUrl('clusteringCustom/updateDetail',array("id"=>$data->id));
												},
											'options'=>array(
												'class'=>'waves-effect waves-light btn-small',
												'rel'=>'',
											),
										),


										 'delete'=>
										array(

											 'label'=>'<i class ="fa fa-trash"></i>',
											 'imageUrl'=>false,
											 'url'=>function($data){
													return Yii::app()->createUrl('clusteringCustom/deleteDetail',array("id"=>$data->id));
												}, 
											 'options'=>array(
													'class'=>'btn waves-effect waves-light btn-small red lighten-2',
													'rel'=>'',
												),
										   
										),
										),
							),
		
						),
					)); ?>
					
				</div>
					
			</div>
		</div>
	</div>
	
	<div class="col m12">
        <div class="col m12 prompt">
            <div class="left">
                <button type="button" onclick="prompt();" class="waves-effect waves-dark btn btn-segment-dash">Delete</button>
            </div>
        </div>
        <div class="col m12 prompt hide">Are you sure ?</div>
        <div class="col m2 prompt hide">
            <a href="<?= Yii::app()->createAbsoluteUrl('clusteringCustom/delete',['id'=>$model->id]);?>"><button class="waves-effect waves-dark btn btn-segment-dash">Yes</button></a>
        </div>
        <div class="col m3 prompt hide">
            <button onclick="prompt();" type="button" class="waves-effect waves-dark btn btn-segment-dash">Cancel</button>
        </div>
    </div>
	
</div>

<?php
//var_dump($segmen->listEmail,$segmen->listPhone,$segmen->listEmailAllowed,$segmen->listPhoneAllowed,$listWA);
$script = "function goToPage(link){"
    . "location.href = link;"
    . "}"
    . "function prompt(){"
    . "$('.prompt').each(function(){"
    . "if ($(this).hasClass('hide')){"
    . "$(this).removeClass('hide');"
    . "} else {"
    . "$(this).addClass('hide');"
    . "}"
    . "})"
    . "}";
Yii::app()->clientScript->registerScript('openPage',$script, CClientScript::POS_END);
?>
	