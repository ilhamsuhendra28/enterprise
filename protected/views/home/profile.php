<title>Enterprise | Profile Account </title>
<style>
textarea {
height: auto !important;
width: 90% !important;
margin: 0 3px 0 3px;
}
</style>

<?php /*<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/css/bootstrap.min.css" rel="stylesheet">*/ ?>
<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<?php /*<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>*/ ?>
<?php /*<link href="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>*/ ?>
<?php /*<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>*/ ?>
<?php /*<script src="../js/plugins/sortable.js" type="text/javascript"></script>*/ ?>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
<?php /*<script src="../js/locales/fr.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="../js/locales/es.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/explorer-fa/theme.js" type="text/javascript"></script>*/ ?>
<script src="<?php echo Yii::app()->baseUrl; ?>/plugin/bootstrap-fileinput/themes/fa/theme.js" type="text/javascript"></script>
<?php /*<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>*/ ?>
<?php /*<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>*/ ?>

<div class="row row-seg">
	<div class="col m12" align="center">
		<h5>Profile Account </h5>	
	</div>
</div>


<?php  if(Yii::app()->user->hasFlash('success')): ?>
	<script>
	$( document ).ready(function() {
		//console.log( "ready!" );
		var toastHTML = '<?php  echo Yii::app()->user->getFlash('success') ; ?>';
		M.toast({html: toastHTML});
	});
	</script>
	<?php /*
	<div class = "animated bounce">
		<div class="card-panel green lighten-2" style="color:#444">
		<span class="white-text">
			<?php  echo Yii::app()->user->getFlash('success') ; ?>	</span>
		</div>
	</div>
	*/ ?>
<?php  endif; ?>
<?php /*<hr class="hr-grey">*/ ?>

<div class="row"> 
	<div class="col s3"></div>
	<div class="col s6">
	
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'custom-segmentation-form',
			'htmlOptions'=>array('enctype'=>'multipart/form-data'),
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			/*'enableAjaxValidation'=>false,*/
			/* 'clientOptions'=>array('validateOnSubmit'=>true),
			'enableClientValidation'=>true, */
		)); ?>
	
		
		<?php  if($form->errorSummary($email)) : ?>
		<div class = "animated bounce">
			<div class="card-panel red lighten-2" style="color:#444">
			<span class="white-text">
				<?php echo $form->errorSummary($email); ?>
			</div>
			
		</div>
		<?php  endif; ?>
		
		
		
		<ul class="stepper horizontal">
			<li class="step active">
				<div class="step-title waves-effect">
					<div class="step-f-title">Email Account</div>
				</div>
				<div class="step-content">
					<div class="row">
						<div class="col s2"></div>
						<div class="col s8">
							<div class="row">
								<div class="input-field input-seg">
									<?php echo $form->labelEx($email,'email'); ?>
									<?php echo $form->textField($email,'email',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Email ','class'=>'validate','required'=>true,'readonly'=>'readonly')); ?>
									<?php echo $form->error($email,'email'); ?>
								
								</div>
							</div>
							<div class="row">
								<div class="input-field input-seg">
									<?php echo $form->labelEx($email,'email_name'); ?>
									<?php echo $form->textField($email,'email_name',array('size'=>60,'maxlength'=>255,'placeholder'=>'Your Email Name','class'=>'validate','required'=>true)); ?>
									<?php echo $form->error($email,'email_name'); ?>
								
								</div>
							</div>
							
							<div class="col s5"> </div>
							<div class="col s2">
								<button type="submit" class="waves-effect waves-dark btn  btn-segment">Save</button>
							</div>
							<div class="col s5"></div>
							
			</li>
			<?php /*
			<li class="step">
				<div class="step-title waves-effect">
					<div class="step-f-title">Strategy</div>
				</div>
				<div class="step-content">
					<div class="row">   
						
					</div>  
					<div class="col s5"> </div>
					<div class="col s2">
						<button type="submit" class="waves-effect waves-dark btn  btn-segment">Save</button>
					</div>
					<div class="col s5"></div>
				</div>
			</li>
			*/ ?>
		</ul>  
		<?php $this->endWidget(); ?>
		<div class="col s3"></div>                  
	</div>
</div>
