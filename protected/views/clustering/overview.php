<title>Overview</title>

<div class="row row-seg">
	<div class="col m12">
        <div class="left">
            <h5>User Segment</h5>	
        </div>
        <div class="right">
            <a href="<?= Yii::app()->createAbsoluteUrl('clustering/create');?>"><button class="waves-effect waves-dark btn btn-segment-dash">New Segment</button></a>
        </div>
	</div>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="left">
					<h6 class="text-bold"><b>Overview</b></h6>
				</div>
				<div class="right">
<!--					<ul class="sosmed">
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-instagram"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-facebook"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-twitter"></i></a></li>
						<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-google-wallet"></i></a></li>
					</ul>-->
				</div>
				<br><br>
				<div class="row">
					<div class="col m4 no-p">
						<p class="color-grey p-b-10">Gender</p>
					</div>
					<div class="col m2">
						<p class="color-grey p-b-10">Age Range</p>
					</div>
                    <div class="col m2">
						<p class="color-grey p-b-10">Religion</p>
					</div>
                    <div class="col m2">
						<p class="color-grey p-b-10">Children</p>
					</div>
                    <div class="col m2">
						<p class="color-grey p-b-10">City</p>
					</div>
					<div class="col m4 no-p">
						<div class="col m6 no-p">
							<div class="flex">
								<div class="col m3 no-p">
									<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" alt="">
								</div>
								<div class="col m9 m-t-auto">
									<small class="color-grey">Female <?= $persenFemale = ceil($overview['female']/($overview['female']+$overview['male'])*100);?>%</small>
									<h6 class="no-m counter"><b><?= $overview['female'];?></b></h6>
								</div>
							</div>
						</div>
						<div class="col m6 no-p">
							<div class="flex">
								<div class="col m3 no-p">
									<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" alt="">
								</div>
								<div class="col m9 m-t-auto">
									<small class="color-grey">Male <?= 100-$persenFemale;?>%</small>
									<h6 class="no-m counter"><b><?= $overview['male'];?></b></h6>
								</div>
							</div>
						</div>
						<div class="col m12 no-p">
							<canvas id="salaryChart"></canvas>
						</div>
					</div>
					<div class="col m2">
						<small class="color-grey"><15</small>
						<div class="progress white-bg">
                            <div class="determinate green-bg" style="width: <?= $overview['classUser']['<15']/ array_sum($overview['classUser'])*100;?>%"></div>
						</div>
						<small class="color-grey">15-24</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: <?= $overview['classUser']['15-24']/ array_sum($overview['classUser'])*100;?>%"></div>
						</div>
						<small class="color-grey">25-34</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: <?= $overview['classUser']['25-34']/ array_sum($overview['classUser'])*100;?>%"></div>
						</div>
						<small class="color-grey">35-44</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: <?= $overview['classUser']['35-44']/ array_sum($overview['classUser'])*100;?>%"></div>
						</div>
						<small class="color-grey">>44</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: <?= $overview['classUser']['>44']/ array_sum($overview['classUser'])*100;?>%"></div>
						</div>
					</div>
                    <div class="col m2">
						<small class="color-grey">Islam</small>
						<div class="progress white-bg">
                            <div class="determinate green-bg" style="width: <?= $overview['classRelig']['islam']/ array_sum($overview['classRelig'])*100;?>%"></div>
						</div>
						<small class="color-grey">Kristen</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: <?= $overview['classRelig']['kristen']/ array_sum($overview['classRelig'])*100;?>%"></div>
						</div>
						<small class="color-grey">Katolik</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: <?= $overview['classRelig']['katolik']/ array_sum($overview['classRelig'])*100;?>%"></div>
						</div>
						<small class="color-grey">Hindu</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: <?= $overview['classRelig']['hindu']/ array_sum($overview['classRelig'])*100;?>%"></div>
						</div>
						<small class="color-grey">Budha</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: <?= $overview['classRelig']['budha']/ array_sum($overview['classRelig'])*100;?>%"></div>
						</div>
					</div>
                    <div class="col m2">
						<small class="color-grey">0/1</small>
						<div class="progress white-bg">
                            <div class="determinate green-bg" style="width: <?= $overview['classChild']['0/1']/ array_sum($overview['classChild'])*100;?>%"></div>
						</div>
						<small class="color-grey">2</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: <?= $overview['classChild']['2']/ array_sum($overview['classChild'])*100;?>%"></div>
						</div>
						<small class="color-grey">3</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: <?= $overview['classChild']['3']/ array_sum($overview['classChild'])*100;?>%"></div>
						</div>
						<small class="color-grey">4</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: <?= $overview['classChild']['4']/ array_sum($overview['classChild'])*100;?>%"></div>
						</div>
						<small class="color-grey">5/6</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: <?= $overview['classChild']['5/6']/ array_sum($overview['classChild'])*100;?>%"></div>
						</div>
                        <small class="color-grey">>6</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: <?= $overview['classChild']['>6']/ array_sum($overview['classChild'])*100;?>%"></div>
						</div>
					</div>
                    <div class="col m2">
                        <?php
                        $sum = 0;
                        foreach ($overview['classCity'] as $keyC => $valueC) {
                            $sum += $valueC['jml'];
                        }
                        for($i=0;$i<5;$i++){
                        ?>
                        <small class="color-grey"><?= $overview['classCity'][$i]['kota'];?></small>
						<div class="progress white-bg">
							<div class="determinate <?= $i % 2 == 1 ? 'green':'yellow';?>-bg" style="width: <?= $overview['classCity'][$i]['jml']/ $sum*100;?>%"></div>
						</div>
                        <?php
                        }
                        ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m6">
<!--		<div class="card z-depth-1">
			<div class="card-content">
				<div class="left">
					<h6 class="text-bold"><b>User Segmentation by Island</b></h6>
				</div>
				<br><br>
				<div class="row">
					<div class="col m12"><p class="color-grey p-b-10">&nbsp;</p></div>
					<div class="col m7">
						<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/indo.jpg" class="responsive-img" alt="">
					</div>
					<div class="col m5">
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate purple-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate green-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate red-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate yellow-bg" style="width: 70%"></div>
						</div>
						<small class="color-grey">Lorem Ipsum dolor sit amet</small>
						<div class="progress white-bg">
							<div class="determinate orange-bg" style="width: 70%"></div>
						</div>
					</div>
				</div>
			</div>
		</div>-->
	</div>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="row">
					<p class="color-grey">
						All Segmentation
					</p>
					<hr class="hr-grey">
                    <?php
                    $i = 1;
                    foreach ($listSegmentation as $key => $value) {
                    ?>
                    <div class="col m12 con-check no-p" onclick="goToPage('<?= Yii::app()->createAbsoluteUrl('clustering/view',['id'=>$value->id]);?>')">
						<div class="col m6 no-p">
							<div class="con-num-count valign-wrapper left">
								<div class="center-cont">
									<div class="num"><?= $i++;?></div>
									<div class="check hidden"><i class="fa fa-check"></i></div>
								</div>
							</div>
							<div class="con left">
								<h5 class="title no-m"><?= $value->title;?></h5>
								<div class="left">
									<p style="padding-right: 14px;">
										<small class="super-small">Created Date</small> <br>
                                        <small><?= date('Y-m-d', strtotime($value->created_date));?></small>
									</p>
								</div>
								<div class="left" style="padding-left: 40px;">
									<p>
										<small class="super-small">status</small> <br>
										<small class="<?= $value->status == 1 ? 'success':'red';?> small-btn"><?= $value->status == 1 ? 'Active':'Not Active';?></small>
									</p>
								</div>
								<br><br>
<!--								<div class="left">
									<p>
										<small class="super-small">Data source</small> <br>
										<ul class="sosmed super-small">
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-instagram"></i></a></li>
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-facebook"></i></a></li>
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a title="kasi judul cuy biar gk bingung" href="#"><i class="fab fa-google-wallet"></i></a></li>
										</ul>
									</p>
								</div>-->
							</div>
						</div>
						<div class="col m6">
							<div class="col m5">
								<div class="con-loc  valign-wrapper">
									<div class="center-cont">
										<h6 class="color-grey no-m">Gender</h6>
										<div class="flex">
											<img style="max-height: 50px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/wedok.jpg" alt="">
											<font class="color-grey"><?= $females = $value->females;?>%</font>
											&nbsp;&nbsp;
											<img style="max-height: 50px;" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/lanang.jpg" alt="">
											<font class="color-grey"><?= 100 - $females;?>%</font>
										</div>
									</div>
								</div>
							</div>
							<div class="col m3"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Age</h6>
									<div class="countage color-green"><b><?= $value->age->value;?></b></div>
								</div>
							</div></div>
							<div class="col m4"><div class="con-loc  valign-wrapper">
								<div class="center-cont">
									<h6 class="color-grey no-m">Income</h6>
									<div class="countage color-blue"><b><?= $value->salary->value;?>M</b></div>
								</div>
							</div></div>
						</div>
					</div>
                    <?php
                    }
                    ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$script = "function goToPage(link){"
    . "location.href = link;"
    . "}";
Yii::app()->clientScript->registerScript('openPage',$script, CClientScript::POS_END);

$listSalaryMale = ''; $listSalaryFemale = '';
foreach ($overview['classSalar']['male'] as $key => $value) {
    $listSalaryMale .= $value.',';
}
foreach ($overview['classSalar']['female'] as $key => $value) {
    $listSalaryFemale .= $value.',';
}

$scriptChart = "var ctx1 = document.getElementById('salaryChart');
				var salaryChart = new Chart(ctx1, {
					type: 'bar',
					data: {
						labels: [";
foreach ($overview['classSalar']['male'] as $key => $value) {
    $scriptChart .= "'".$key."',";
}
        $scriptChart .= "],
						datasets: [{
							label: 'Male',
							backgroundColor: '#5ee2a0',
							data: [".$listSalaryMale."]
						}, {
							label: 'Female',
							backgroundColor: '#ffb60a',
							data: [".$listSalaryFemale."]
						}]
					},

					options: {
						legend:{
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true
								}
							}]
						}
					}
				});";
Yii::app()->clientScript->registerScript('chartScript',$scriptChart, CClientScript::POS_END);
?>