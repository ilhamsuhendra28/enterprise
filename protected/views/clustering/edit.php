<title>Edit Segmentation</title>
<?php $script = '';?>
<div class="row"> 
	<div class="col s2"></div>
	<div class="col s8">
        <?php 
        $form=$this->beginWidget('CActiveForm', array(
            'id'=>'segmentation-form',
            'action'=>array('clustering/saveEdit',['id'=>$segmen->id]),
            'htmlOptions'=>array(
                'class'=>''
            ),
        )); 
        ?>
		<ul class="stepper horizontal">
            <?php
            foreach ($listGrup as $key => $value) {
            ?>
            <li class="step <?= $key < 1 ? 'active':''?>">
				<div class="step-title waves-effect">
					<div class="step-f-title"><?= $value->nama_group;?></div>
				</div>
				<div class="step-content">
					<div class="row">
						<p class="title-stepper"><?= $value->title;?></p>
						<div class="row">		
                        <?php
                        foreach ($value->details as $key2 => $value2) {
                            switch ($value2->tipe_field) {
                                case 'text': //===============================================================================
                                    ?>
                                    <div class="col s<?= $value2->col_size;?>">
                                        <div class="input-field input-seg">
                                            <input class="<?= $value2->is_count == 1 ? 'is-hitung':'';?>" name="<?= $value2->id.','.$value2->fieldid;?>" id="<?= $value2->id.','.$value2->fieldid;?>" name="title" type="text" class="validate" value="<?= $value2->value->value;?>" required>
                                            <label for="<?=$value2->fieldid;?>"><?= $value2->nama_field;?></label>
                                        </div>
                                    </div>
                                    <?php
                                    break;
                                case 'select': //===============================================================================
                                    ?>
                                    <div class="input-field col s<?= $value2->col_size;?>">
                                            <select class="<?= $value2->is_count == 1 ? 'is-hitung':'';?>" name="<?= $value2->id.','.$value2->fieldid;?>" id="<?= $value2->id.','.$value2->fieldid;?>">
                                                <option value="">All</option>
                                                <?php
                                                $listOptions = MasterValue::model()->findAll(
                                                    [
                                                        'condition' => 'masterid = :fieldid',
                                                        'params' => [':fieldid' => $value2->fieldid],
                                                        'order' => 't.order ASC'
                                                    ]
                                                );
                                                foreach ($listOptions as $key3 => $value3) {
                                                    if ($value2->value->value == $value3->id){
                                                        $selected = 'selected = "selected"';
                                                    } else {
                                                        $selected = '';
                                                    }
                                                    echo '<option '.$selected.' value="'.$value3->id.'">'.$value3->value.'</option>';
                                                }
                                                ?>
                                            </select>
                                            <label for="<?=$value2->fieldid;?>"><?= $value2->nama_field;?></label>
                                    </div>
                                    <?php
                                    break;
                                case 'slider': //===============================================================================
                                    $_start = explode('-', $value2->value->value);
                                    $script .= " var ".$value2->fieldid."Slider = document.getElementById('".$value2->fieldid."-slider');
                                                noUiSlider.create(".$value2->fieldid."Slider, {
                                                start: [".$_start[0].", ".$_start[1]."],
                                                tooltips: [wNumb({ decimals: 0 }), wNumb({ decimals: 0 })],
                                                connect: true,
                                                step: 1,
                                                orientation: 'horizontal', 
                                                range: {
                                                'min': ".$value2->min.",
                                                'max': ".$value2->max."
                                                },
                                                format: wNumb({
                                                decimals: 0
                                                })
                                                });
                                                ".$value2->fieldid."Slider.noUiSlider.on('update', function( values, handle ) {
                                                $('#".$value2->fieldid."-input-slider').val(values[0]+'-'+values[1]);
                                                });
                                                ".$value2->fieldid."Slider.noUiSlider.on('change', function() {
                                                ajaxCount();
                                                });
                                                ";
                                    ?>
                                    <div class="col s<?= $value2->col_size;?>">
                                        <label><?=$value2->nama_field;?></label>
                                        <div id="<?=$value2->fieldid;?>-slider" class="slider-age"></div>
                                        <input class="<?= $value2->is_count == 1 ? 'is-hitung':'';?>" type="hidden" name="<?= $value2->id.','.$value2->fieldid;?>" id="<?=$value2->fieldid;?>-input-slider">
                                    </div>
                                    <?php
                                    break;
                                case 'check': //===============================================================================
                                    $listOptions = MasterValue::model()->findAll(
                                        [
                                            'condition' => 'masterid = :fieldid',
                                            'params' => [':fieldid' => $value2->fieldid],
                                            'order' => 't.order ASC'
                                        ]
                                    );
                                    echo '<div class="col s6 religion-col">';
                                    echo '<div class="col s12">
                                            <label>'.$value2->nama_field.'</label>
                                          </div>';
                                    $checked0 = array_key_exists(0, CJSON::decode($value2->value->value)) ? 'checked="checked"':'';
                                    echo '<div class="col s4">
                                            <label>
                                                <input class="is-hitung" name="'.$value2->id.',relig[0]" type="checkbox" '.$checked0.' class="filled-in"/>
                                                <span>All</span>
                                            </label>
                                          </div>';
                                    foreach ($listOptions as $key3 => $value3) {
                                        $checked0 = array_key_exists($value3->id, CJSON::decode($value2->value->value)) ? 'checked="checked"':'';
                                    ?>
                                        <div class="col s4">
                                            <label>
                                                <input class="<?= $value2->is_count == 1 ? 'is-hitung':'';?>" <?= $checked0;?> name="<?= $value2->id.','.$value2->fieldid.'['.$value3->id.']';?>" id="<?=$value2->fieldid.'_'.$value3->id.'_check';?>" type="checkbox" class="filled-in"/>
                                                <span><?=$value3->value;?></span>
                                            </label>
                                        </div>
                                    <?php
                                    }
                                    echo '</div>';
                                    break;
                                case 'tag': //=============================================================================================
                                    $_tag = explode('|', ltrim($value2->value->value,'|'));
                                    $dataT = '';
                                    foreach ($_tag as $keyT => $valueT) {
                                        if (!empty($valueT)){
                                            $dataT .= '{tag:"'.$valueT.'"},';
                                        }
                                    }
                                    $script .= "$('.".$value2->fieldid."-tag').chips({
                                                    data:[".$dataT."],
                                                    autocompleteOptions: {
                                                      data: {";
//                                    foreach ($listKategoriBarang as $keyKB => $valueKB) {
//                                        $script .= "'".$valueKB->kategori_barang."': null,";
//                                    }
                                    foreach ($listTambahan as $keyKB => $valueKB) {
                                        $script .= "\"".$valueKB."\": null,";
                                    }
                                            
                                            $script .= "},
                                                      limit: Infinity,
                                                      minLength: 1,
                                                      placeholder: 'Enter a tag',
                                                    },
                                                    onChipAdd: function(){updateTag(); ajaxCount();},
                                                    onChipDelete: function(){updateTag(); ajaxCount();},
                                                  });
                                                  function updateTag(){
                                                    var instance = M.Chips.getInstance($('.".$value2->fieldid."-tag'));
                                                    chipsVal = instance.chipsData;
                                                    strChipsVal = '';
                                                    $.each(chipsVal, function(key,value){
                                                        strChipsVal += '|'+value.tag;
                                                    });
                                                    $('#".$value2->id.'_'.$value2->fieldid."').val(strChipsVal);
                                                  }
                                                      
                                                  ";
                                    ?>
                                    <div class="col s2"></div>
                                    <div class="col s8">
                                        <div class="chips <?= $value2->fieldid.'-tag';?> input-field"></div>
                                        <input type="hidden" name="<?= $value2->id.','.$value2->fieldid;?>" id="<?= $value2->id.'_'.$value2->fieldid;?>" value="<?= $value2->value->value;?>"/>
                                    </div>
                                    <div class="col s2"></div>
                                    <?php
                                    break;
                                case 'tag2': //=============================================================================================
                                    $_tag = explode('|', ltrim($value2->value->value,'|'));
                                    $dataT = '';
                                    foreach ($_tag as $keyT => $valueT) {
                                        if (!empty($valueT)){
                                            $dataT .= '{tag:"'.$valueT.'"},';
                                        }
                                    }
                                    
                                    $script .= "$('.".$value2->fieldid."-tag').chips({
                                                    data:[".$dataT."],
                                                    autocompleteOptions: {
                                                      data: {";
                                    foreach ($listKota as $keyK => $valueK) {
                                        $script .= "'".$valueK->nama_kota."': null,";
                                    }                                    
                                            
                                            $script .= "},
                                                      limit: Infinity,
                                                      minLength: 1,
                                                      placeholder: 'Enter a tag',
                                                    },
                                                    onChipAdd: function(){updateTag2(); ajaxCount();},
                                                    onChipDelete: function(){updateTag2(); ajaxCount();},
                                                  });
                                                  function updateTag2(){
                                                    var instance = M.Chips.getInstance($('.".$value2->fieldid."-tag'));
                                                    chipsVal = instance.chipsData;
                                                    strChipsVal = '';
                                                    $.each(chipsVal, function(key,value){
                                                        strChipsVal += '|'+value.tag;
                                                    });
                                                    $('#".$value2->id.'_'.$value2->fieldid."').val(strChipsVal);
                                                  }
                                                      
                                                  ";
                                    ?>
                                    <div class="col s12">
                                        <div class="chips <?= $value2->fieldid.'-tag';?> input-field"></div>
                                        <label for="<?=$value2->fieldid;?>"><?= $value2->nama_field;?></label>
                                        <input type="hidden" name="<?= $value2->id.','.$value2->fieldid;?>" id="<?= $value2->id.'_'.$value2->fieldid;?>" value="<?= $value2->value->value;?>"/>
                                    </div>
                                    <?php
                                    break;
                                default: //===============================================================================
                                    ?>
                                    <div class="col s<?= $value2->col_size;?>">
                                        <div class="input-field input-seg">
                                            <input class="<?= $value2->is_count == 1 ? 'is-hitung':'';?>" name="<?= $value2->id.','.$value2->fieldid;?>" id="<?= $value2->id.','.$value2->fieldid;?>" name="title" type="text" class="validate" required>
                                            <label for="<?=$value2->fieldid;?>"><?= $value2->nama_field;?></label>
                                        </div>
                                    </div>
                                    <?php
                                    break;
                            }
                        }
                        ?>
                        </div>
                        <div class="row">
                            <div class="col s5"></div>
                            <div class="col s2"><h4 class="result-place"><?= $countUser;?></h4></div>
                            <div class="col s5"></div>
                        </div>
						<div class="row">
							<div class="col s5"></div>
							<div class="col s2">
                                <button type="<?= $key == count($listGrup)-1 ? 'submit':'button';?>" class="waves-effect waves-dark btn <?= $key == count($listGrup)-1 ? '':'next-step';?> btn-segment"><?= $key == count($listGrup)-1 ? 'Save':'Next';?></button>
							</div>
							<div class="col s5"></div>
						</div>
					</div>
				</div>
			</li>
            <?php
            }
            ?>
			
		</ul>  
        <?php $this->endWidget(); ?>
	 <div class="col s2"></div>                  
	</div>
</div>
<?php

$script .= " 
function readSlider(){
alert(salarSlider.noUiSlider.get());
}
    ";

Yii::app()->clientScript->registerScript('slider',$script, CClientScript::POS_END);

$script2 = "$(document).on('change','.is-hitung', function(){
        ajaxCount();
    }); ";
$script2 .= "function ajaxCount(){      
        $.ajax({
            url: '".Yii::app()->createAbsoluteUrl('clustering/countUser')."',
            data: \$('#segmentation-form').serialize(),
            type: 'POST',
            success: function(result) {
                $('.result-place').html(result);
            },
            error: function() {
                $('.result-place').html('nan');
            }
        });
    } ";
    
Yii::app()->clientScript->registerScript('ajaxCount',$script2, CClientScript::POS_END);
?>
