<title>Enterprise | User Segment</title>

<div class="container">
	<div class="row row-empty">
		<div class="col s5">
			<div class="vector-empty">
				<div class="vector-image-left">
					<img class="responsive-img" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/user-segment-vector.svg">
				</div>
			</div>
		</div>
		<div class="col s7 col-empty">
			<div class="text-empty">
				<p class="text-not">Oops, No user segment to find</p>
				<p class="text-not-b">Let's make one</p>
				<a href="<?php echo Yii::app()->createUrl('clustering/create'); ?>"><button class="waves-effect waves-dark btn btn-segment-dash">New Segment</button></a>
			</div>
		</div>
	</div>
</div>