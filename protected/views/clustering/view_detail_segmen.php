<title>Detail Segmentation</title>

<div class="row row-seg">
	<div class="col m8">
		<h5>Detail Segmentation</h5>	
	</div>
    <div class="col m4">
        <div class="right">
            <a href="<?= Yii::app()->createAbsoluteUrl('clustering/edit',['id'=>$segmen->id]);?>"><button class="waves-effect waves-dark btn btn-segment-dash">Edit</button></a>
        </div>
    </div>
	<div class="col m12">
		<div class="card z-depth-1">
			<div class="card-content">
				<div class="left">
					<h6 class="text-bold"><b><?= $segmen->title;?></b></h6>
				</div>
				<br><br>
				<div class="row">					
					<div class="col m8">
						<p class="color-grey p-b-10"><?= $segmen->description;?></p>
					</div>
                    <div class="col m8">
                        <h6 class="text-bold"><b>Demographic Segmentation</b></h6>
                        <div class="col m6">Gender</div><div class="col m6">: <?= $segmen->gender->text->value == '' ? 'All' : $segmen->gender->text->value;?></div>
                        <div class="col m6">Age</div><div class="col m6">: <?= $segmen->age->value;?> Tahun</div>
                        <div class="col m6">Mariage</div><div class="col m6">: <?= $segmen->mariage->text->value == '' ? 'All' : $segmen->mariage->text->value;?></div>
                        <div class="col m6">Religion</div><div class="col m6">: <?= $segmen->religion->textRelig == '' ? 'All' : $segmen->religion->textRelig;?></div>
                        <div class="col m6">Salary</div><div class="col m6">: <?= $segmen->salary->value;?> Juta</div>
                        <div class="col m6">City</div><div class="col m6">: <?= str_replace('|',', ',ltrim($segmen->city->value, '|')) == '' ? '-' : str_replace('|',', ',ltrim($segmen->city->value, '|'));?></div>
                    </div>
                    <div class="col m4">
                        <h6 class="text-bold"><b>Behavioural Segmentation</b></h6>
                    </div>
					<div class="col m4">
                        <p class="color-grey p-b-10"><?= str_replace('|',', ',ltrim($segmen->purchase->value, '|')) == '' ? '-' : str_replace('|',', ',ltrim($segmen->purchase->value, '|'));?></p>
					</div>
                    <div class="col m4">
                        <h6 class="text-bold"><b>Clustered User</b></h6>
                    </div>
                    <div class="col m4">
                        <h6 class="text-bold"><b><?= $segmen->countUser;?></b></h6>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="col m12">
        <div class="col m12 prompt">
            <div class="left">
                <button type="button" onclick="prompt();" class="waves-effect waves-dark btn btn-segment-dash">Delete</button>
            </div>
        </div>
        <div class="col m12 prompt hide">Are you sure ?</div>
        <div class="col m2 prompt hide">
            <a href="<?= Yii::app()->createAbsoluteUrl('clustering/delete',['id'=>$segmen->id]);?>"><button class="waves-effect waves-dark btn btn-segment-dash">Yes</button></a>
        </div>
        <div class="col m3 prompt hide">
            <button onclick="prompt();" type="button" class="waves-effect waves-dark btn btn-segment-dash">Cancel</button>
        </div>
    </div>
</div>

<?php
//var_dump($segmen->listEmail,$segmen->listPhone,$segmen->listEmailAllowed,$segmen->listPhoneAllowed,$listWA);
$script = "function goToPage(link){"
    . "location.href = link;"
    . "}"
    . "function prompt(){"
    . "$('.prompt').each(function(){"
    . "if ($(this).hasClass('hide')){"
    . "$(this).removeClass('hide');"
    . "} else {"
    . "$(this).addClass('hide');"
    . "}"
    . "})"
    . "}";
Yii::app()->clientScript->registerScript('openPage',$script, CClientScript::POS_END);
?>