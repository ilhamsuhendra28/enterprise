<?php

class CronController extends Controller
{
	public $layout=false;
	
	
	
	public function isDummyReceiver()
	{
		return false; // ini ubah ke false saat production
	}
	
	public function isDummyBlast()
	{
		return false; // ini ubah ke false saat production
	}
	
	public function getDummyReceiver()
	{
		return "ichsan.must10@gmail.com"; // email ichsanmust
	}
	
	public function getDummyReceiverPhone()
	{
		return "081220851035"; //nomor ichsanmust
	}
	
	public function getDummyReceiverPhoneWA()
	{
		return "6285869355501"; // nomor mas adi
	}
	
	
	
	

	
	
	public function actionIndex()
	{
		ini_set('memory_limit', '-1');
		set_time_limit(0);
		
		
		$this->blastCampaignOnce();
		$this->blastCampaignMonthly();
		$this->blastCampaignWeekly();
		$this->blastCampaignDayly();
	}
	
	
	
	
	
	//-- ONCE --//
	
	// pengelompokan list data receiver per segmentasi , supaya nantinya ga di panggil berkali kali 
	public function actionGetDataReceiverByUserSegment()
	{
		
		$sql = "
			SELECT 
				#a.id as id_campaign,
				a.title,
				#a.description, 
				b.id_user_segmentation as id_segmentation, 
				c.is_custom 
			FROM campaign a
			LEFT JOIN campaign_detail b ON a.id = b.id_campaign
			LEFT JOIN user_segmentation c ON b.id_user_segmentation = c.id
			WHERE 
			( NOW() >= ADDTIME(a.post_date, a.post_time) ) AND a.is_sent = 0 AND a.status = 1 AND a.approval = 1 AND a.sent_type = 0
			GROUP BY b.id_user_segmentation";
		$query = Yii::app()->db->createCommand($sql);
        $row = $query->queryAll();
		//print_r($row);
		$dataInIdSegmentation = array();
		foreach ($row as $data){
			$dataInIdSegmentation[]=$data ['id_segmentation'];
		}
		//print_r($dataInIdSegmentation);
		
		
		$dataReceiver = array();
		if (count($dataInIdSegmentation) > 0){
			$inData = implode(',',$dataInIdSegmentation);
			//echo $inData;
			
			$segmentation = UserSegmentation::model()->allSegment()->findAll(array(
				'condition' => 'id IN ('.$inData.')',
				//'params'=>array(':listID'=>$inData),
			));
			/* echo '<pre>';
			print_r($segmentation); */
			
			foreach ($segmentation as $segment){
				if ($segment->is_custom == 0){
					$phoneNumbers = $segment->listPhoneAllowed;
					$emails = $segment->listEmailAllowed;
					
					$dataReceiver[$segment->id]['emails'] = $emails;
					$dataReceiver[$segment->id]['phoneNumbers'] = $phoneNumbers;
				}else{
					$emailsAndphoneNumbers = $segment->detailsCustom;
					$dataReceiver[$segment->id]['emailsAndphoneNumbers'] = $emailsAndphoneNumbers;
				}
			}
		}
		
		//echo '<pre>';
		//print_r($dataReceiver);
		
		return $dataReceiver;
		
		
		
	}
	
	public function blastCampaignOnce(){
		
		$dataReceiver = $this->actionGetDataReceiverByUserSegment();
		$campaign = Campaign::model()->findAll(array(
			'alias' =>'c',
			'select'=>'c.*, e.email, e.email_name',
			'join'=>'LEFT JOIN user_enterprise_email e on c.id_user = e.id_user',
			'condition' => '( NOW() >= ADDTIME(post_date, post_time) ) AND is_sent = 0 AND status = 1 AND approval = 1 AND sent_type = 0',
		));
	
		/* echo '<pre>';
		print_r($dataReceiver);
		print_r($campaign);
		exit;  */
		
		echo ' - Campaign (Once) : <br>';
		if (!empty($campaign)) {
			foreach ($campaign as $data_campaign){
				if ($data_campaign->email_blast ==1){
					$this->sendEmail($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->sms_blast ==1){
					$this->sendSMS($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->wa_blast ==1){
					$this->sendWA($data_campaign,$dataReceiver);
				}  
			}
		}
	}
	//-- ONCE --//
	
	
	//-- MONTHLY //
	public function actionGetDataReceiverByUserSegmentMonthly()
	{
		
		$sql = "
			SELECT 
				#a.id as id_campaign,
				a.title,
				#a.description, 
				b.id_user_segmentation as id_segmentation, 
				c.is_custom 
			FROM campaign a
			LEFT JOIN campaign_detail b ON a.id = b.id_campaign
			LEFT JOIN user_segmentation c ON b.id_user_segmentation = c.id
			WHERE 
			
			day(a.post_date) = day(NOW())
			AND
			TIME(NOW()) >= a.post_time
			AND 
			NOW() BETWEEN a.begda AND a.endda
			AND a.status = 1 AND a.approval = 1 AND a.sent_type = 1

			GROUP BY b.id_user_segmentation";
		$query = Yii::app()->db->createCommand($sql);
        $row = $query->queryAll();
		//print_r($row);
		$dataInIdSegmentation = array();
		foreach ($row as $data){
			$dataInIdSegmentation[]=$data ['id_segmentation'];
		}
		//print_r($dataInIdSegmentation);
		
		
		$dataReceiver = array();
		if (count($dataInIdSegmentation) > 0){
			$inData = implode(',',$dataInIdSegmentation);
			//echo $inData;
			
			$segmentation = UserSegmentation::model()->allSegment()->findAll(array(
				'condition' => 'id IN ('.$inData.')',
				//'params'=>array(':listID'=>$inData),
			));
			/* echo '<pre>';
			print_r($segmentation); */
			
			foreach ($segmentation as $segment){
				if ($segment->is_custom == 0){
					$phoneNumbers = $segment->listPhoneAllowed;
					$emails = $segment->listEmailAllowed;
					
					$dataReceiver[$segment->id]['emails'] = $emails;
					$dataReceiver[$segment->id]['phoneNumbers'] = $phoneNumbers;
				}else{
					$emailsAndphoneNumbers = $segment->detailsCustom;
					$dataReceiver[$segment->id]['emailsAndphoneNumbers'] = $emailsAndphoneNumbers;
				}
			}
		}
		
		//echo '<pre>';
		//print_r($dataReceiver);
		
		return $dataReceiver;
		
		
		
	}
	
	public function blastCampaignMonthly(){
		
		$dataReceiver = $this->actionGetDataReceiverByUserSegmentMonthly();
		$campaign = Campaign::model()->findAll(array(
			'alias' =>'c',
			'select'=>'c.*, e.email, e.email_name',
			'join'=>'LEFT JOIN user_enterprise_email e on c.id_user = e.id_user',
			'condition' => '
							day(post_date) = day(NOW())
							AND
							TIME(NOW()) >= post_time
							AND 
							NOW() BETWEEN begda AND endda
							AND status = 1 AND approval = 1 AND sent_type = 1
						  ',
		));
	
		/* echo '<pre>';
		print_r($dataReceiver);
		print_r($campaign);
		exit;  */
		
		echo ' - Campaign (Monthly) : <br>';
		if (!empty($campaign)) {
			foreach ($campaign as $data_campaign){
				if ($data_campaign->email_blast ==1){
					$this->sendEmail($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->sms_blast ==1){
					$this->sendSMS($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->wa_blast ==1){
					$this->sendWA($data_campaign,$dataReceiver);
				}  
			}
		}
	}
	//-- MONTHLY --//
	
	
	//-- WEEKLY //
	public function actionGetDataReceiverByUserSegmentWeekly()
	{
		
		$sql = "
			SELECT 
				#a.id as id_campaign,
				a.title,
				#a.description, 
				b.id_user_segmentation as id_segmentation, 
				c.is_custom 
			FROM campaign a
			LEFT JOIN campaign_detail b ON a.id = b.id_campaign
			LEFT JOIN user_segmentation c ON b.id_user_segmentation = c.id
			WHERE 
			
			DAYNAME(now()) = DAYNAME(a.post_date)
			AND 
			TIME(NOW()) >= a.post_time
			AND 
			NOW() BETWEEN a.begda AND a.endda
			AND a.status = 1 AND a.approval = 1 AND a.sent_type = 2

			GROUP BY b.id_user_segmentation";
		$query = Yii::app()->db->createCommand($sql);
        $row = $query->queryAll();
		//print_r($row);
		$dataInIdSegmentation = array();
		foreach ($row as $data){
			$dataInIdSegmentation[]=$data ['id_segmentation'];
		}
		//print_r($dataInIdSegmentation);
		
		
		$dataReceiver = array();
		if (count($dataInIdSegmentation) > 0){
			$inData = implode(',',$dataInIdSegmentation);
			//echo $inData;
			
			$segmentation = UserSegmentation::model()->allSegment()->findAll(array(
				'condition' => 'id IN ('.$inData.')',
				//'params'=>array(':listID'=>$inData),
			));
			/* echo '<pre>';
			print_r($segmentation); */
			
			foreach ($segmentation as $segment){
				if ($segment->is_custom == 0){
					$phoneNumbers = $segment->listPhoneAllowed;
					$emails = $segment->listEmailAllowed;
					
					$dataReceiver[$segment->id]['emails'] = $emails;
					$dataReceiver[$segment->id]['phoneNumbers'] = $phoneNumbers;
				}else{
					$emailsAndphoneNumbers = $segment->detailsCustom;
					$dataReceiver[$segment->id]['emailsAndphoneNumbers'] = $emailsAndphoneNumbers;
				}
			}
		}
		
		//echo '<pre>';
		//print_r($dataReceiver);
		
		return $dataReceiver;
		
		
		
	}
	
	public function blastCampaignWeekly(){
		
		$dataReceiver = $this->actionGetDataReceiverByUserSegmentWeekly();
		$campaign = Campaign::model()->findAll(array(
			'alias' =>'c',
			'select'=>'c.*, e.email, e.email_name',
			'join'=>'LEFT JOIN user_enterprise_email e on c.id_user = e.id_user',
			'condition' => '
							DAYNAME(now()) = DAYNAME(post_date)
							AND 
							TIME(NOW()) >= post_time
							AND 
							NOW() BETWEEN begda AND endda
							AND status = 1 AND approval = 1 AND sent_type = 2
						  ',
		));
	
		/* echo '<pre>';
		print_r($dataReceiver);
		print_r($campaign);
		exit;  */
		
		echo ' - Campaign (Weekly) : <br>';
		if (!empty($campaign)) {
			foreach ($campaign as $data_campaign){
				if ($data_campaign->email_blast ==1){
					$this->sendEmail($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->sms_blast ==1){
					$this->sendSMS($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->wa_blast ==1){
					$this->sendWA($data_campaign,$dataReceiver);
				}  
			}
		}
	}
	//-- Weekly --//
	
	//-- DAYLY //
	public function actionGetDataReceiverByUserSegmentDayly()
	{
		
		$sql = "
			SELECT 
				#a.id as id_campaign,
				a.title,
				#a.description, 
				b.id_user_segmentation as id_segmentation, 
				c.is_custom 
			FROM campaign a
			LEFT JOIN campaign_detail b ON a.id = b.id_campaign
			LEFT JOIN user_segmentation c ON b.id_user_segmentation = c.id
			WHERE 
			
			TIME(NOW()) >= a.post_time
			AND 
			NOW() BETWEEN a.begda AND a.endda
			AND a.status = 1 AND a.approval = 1 AND a.sent_type = 3

			GROUP BY b.id_user_segmentation";
		$query = Yii::app()->db->createCommand($sql);
        $row = $query->queryAll();
		//print_r($row);
		$dataInIdSegmentation = array();
		foreach ($row as $data){
			$dataInIdSegmentation[]=$data ['id_segmentation'];
		}
		//print_r($dataInIdSegmentation);
		
		
		$dataReceiver = array();
		if (count($dataInIdSegmentation) > 0){
			$inData = implode(',',$dataInIdSegmentation);
			//echo $inData;
			
			$segmentation = UserSegmentation::model()->allSegment()->findAll(array(
				'condition' => 'id IN ('.$inData.')',
				//'params'=>array(':listID'=>$inData),
			));
			/* echo '<pre>';
			print_r($segmentation); */
			
			foreach ($segmentation as $segment){
				if ($segment->is_custom == 0){
					$phoneNumbers = $segment->listPhoneAllowed;
					$emails = $segment->listEmailAllowed;
					
					$dataReceiver[$segment->id]['emails'] = $emails;
					$dataReceiver[$segment->id]['phoneNumbers'] = $phoneNumbers;
				}else{
					$emailsAndphoneNumbers = $segment->detailsCustom;
					$dataReceiver[$segment->id]['emailsAndphoneNumbers'] = $emailsAndphoneNumbers;
				}
			}
		}
		
		//echo '<pre>';
		//print_r($dataReceiver);
		
		return $dataReceiver;
		
		
		
	}
	
	public function blastCampaignDayly(){
		
		$dataReceiver = $this->actionGetDataReceiverByUserSegmentDayly();
		$campaign = Campaign::model()->findAll(array(
			'alias' =>'c',
			'select'=>'c.*, e.email, e.email_name',
			'join'=>'LEFT JOIN user_enterprise_email e on c.id_user = e.id_user',
			'condition' => '
							TIME(NOW()) >= post_time
							AND 
							NOW() BETWEEN begda AND endda
							AND status = 1 AND approval = 1 AND sent_type = 3
						  ',
		));
	
		/* echo '<pre>';
		print_r($dataReceiver);
		print_r($campaign);
		exit;  */
		
		echo ' - Campaign (Dayly) : <br>';
		if (!empty($campaign)) {
			foreach ($campaign as $data_campaign){
				if ($data_campaign->email_blast ==1){
					$this->sendEmail($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->sms_blast ==1){
					$this->sendSMS($data_campaign,$dataReceiver);
				}
				else if ($data_campaign->wa_blast ==1){
					$this->sendWA($data_campaign,$dataReceiver);
				}  
			}
		}
	}
	//-- Dayly --//
	
	
	
	
	
	
	
	
	
	public function sendEmail($data_campaign,$dataReceiver){
		echo '<br> -- Send Email : <br>';
		echo '<b>--- id : '.$data_campaign->id.' | '.'title : '.$data_campaign->title. /* .' | '.'desc : '.$data_campaign->description*/  '</b><br>';
		$from_name = ($data_campaign->email_name !='') ? $data_campaign->email_name : 'Campaign Bagi Data';
		$from_email = 'hello@bagidata.com';
		// check apakah campaign ini sudah di blast pada hari ini, karena cron nya ini setiap 5 menit sekali
		$chekBlastEmail = $this->checkSentBlastOfThisDay($data_campaign->id);
		if ($chekBlastEmail){
			echo ' ----  Sudah Terkirim Pada '.$chekBlastEmail['sent_date_time'].'<br>';
			return true ;
		}
		
		if($this->isDummyBlast()){
			$listID = '29934'; // ini buat test
			echo ' ----  Sukses Create List API - '.$listID.'<br>';
		}else{
			// create list di API Kirim Email
			/* $newList = 'campaign_'.$data_campaign->id.'_'.date('Ymd_His');
			$createList = KirimEmail::createList($newList);
			if ($createList['status']=='error'){ // jika error
				echo ' ----  Gagal Create List API - '.$createList['message'].'<br>';
				return true ;
			}else{
				$listID = $createList['data']['id'];
				echo ' ----  Sukses Create List API - '.$listID.'<br>';
			}   */
			
			// create list di API MailChimp
			$newList = 'campaign_'.$data_campaign->id.'_'.date('Ymd_His');
			$newTemplate = 'template_campaign_'.$data_campaign->id.'_'.date('Ymd_His');
			$createList = KirimEmailMailchimp::createList($newList,$from_name,$from_email,$data_campaign->subject);
			if ($createList['status']=='error' || (isset($createList['data']['type']) && $createList['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){
				echo ' ----  Gagal Create List API - '.$createList['message'].'<br>';
				return true ;
			}else{
				$listID = $createList['data']['id'];
				echo ' ----  Sukses Create List API - '.$listID.'<br>';
			} 
		}
		
		
		
		
		
		
		$details = $data_campaign->details;
		//print_r($details);
		$sent = 0; // counting sent email untuk campaign ini
		
		$field_type = 'email';
		$alias = 'a';
		$field = 'sisa_'.$field_type;
		$fieldWithAlias = $alias.'.'.$field;
		$arrayCampaignIDSent = array ();
		
		foreach ($details as $detail){
			echo ' ---- Segmentation : <br>';
			$segmentation = $detail->userSegment;
			echo ' ---- id : '.$detail->id_user_segmentation.' | '.'title : '.$segmentation->title.'<br>';
			
			if ($segmentation->is_custom == 0){
				if (isset($dataReceiver[$segmentation->id])){
					$emails = $dataReceiver[$segmentation->id]['emails'];
				}else{
					$emails = array();
				}
				//print_r($emails);
				
				
				
				$i = 1;
				$jumlahEmailAll = count($emails);
				$jmlemail6=1;
				foreach ($emails as $email){
					echo ' ----- Kirim ke : '.$email.'<br>';
					
					if ($this->isDummyReceiver()){
						$to = $this->getDummyReceiver();  // ini receiver yang dummy, nanti coment aja
					}else{
						$to = $email; // ini receiver yang asli, nanti uncoment aja
					}
					$subject = $data_campaign->subject; 
					$message  = '<h3>'.$data_campaign->title.'</h3>';
					$message .= $data_campaign->description;
					//$fromEmail = ($data_campaign->email_name !='') ? $data_campaign->email_name : 'Campaign Bagi Data';
					$fromEmail = 'Campaign Bagi Data';
					
					// send email 
					/* $mailer = new YiiMailer();
					$mailer->setFrom('noreplay@bagidata.com', $fromEmail);
					$mailer->setTo($to);
					$mailer->setSubject($subject);
					$mailer->setBody($message,'text/html');
					//$mailer->send();
					if ($mailer->send()){
						$sent++;
						$this->actionAddWallet('email',$to,'Promosi Melalui Email',40);
					}
					//$mailer->SmtpClose();
					if($jmlemail6==5 || $i == $jumlahEmailAll){
					  $mailer->SmtpClose();
					  $jmlemail6=1;
					} */
					
					
					$UserPaketCampaign = UserPaketCampaign::model()->find(
						array(
							'alias'=>$alias,
							//'select'=>'a.id, a.id_user, a.id_paket_campaign, b.nama_paket,  '.$fieldWithAlias,
							'select'=>'a.id, a.id_user, b.nama_paket,  '.$fieldWithAlias,
							'join'=>'join paket_campaign b on a.id_paket_campaign = b.id',
							'condition'=> 'b.status = 1 AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa AND  a.id_user = :userid AND '.$fieldWithAlias.' > 0',
							'params'=>array(':userid'=>$data_campaign->id_user),
							'order'=>'a.created_date ASC',
						)
					);
					//print_r($UserPaketCampaign);
					//exit;
			
					if($UserPaketCampaign && $UserPaketCampaign->$field > 0){
					
						// create subscriber dan mapping ke list di API Kirim Email
						if ($this->kirimEmail($listID,$to)){
							$this->actionAddWallet('email',$to,'Promosi Melalui Email',40);
							$sent++;
							
							
							$UserPaketCampaign->$field = $UserPaketCampaign->$field - 1 ;
							$UserPaketCampaign->save(false);
							
							if (isset($arrayCampaignIDSent[$UserPaketCampaign->id])){
								$arrayCampaignIDSent[$UserPaketCampaign->id] = $arrayCampaignIDSent[$UserPaketCampaign->id] + 1;
							}else{
								$arrayCampaignIDSent[$UserPaketCampaign->id] = 1;
							}
							
						}
						
						echo '- Sisa Kuota '.$field_type.' ('.$UserPaketCampaign->nama_paket.', - subscribed_paket_id : '.$UserPaketCampaign->id.' ) : '.$UserPaketCampaign->$field.'<br>';
				
					
					}else{
						echo 'Paket Tidak Mencukupi ';
						echo '----- Sisa Kuota '.$field_type.' : 0 <br>';
					}
					
					
					
					
					
					$jmlemail6++;
					$i++;
				} 
				
				
			}else{
				if (isset($dataReceiver[$segmentation->id])){
					$emails = $dataReceiver[$segmentation->id]['emailsAndphoneNumbers'];
				}else{
					$emails = array();
				}
				//print_r($emails);
				
				$i = 1;
				$jumlahEmailAll = count($emails);
				$jmlemail6=1;
				foreach ($emails as $email){
					echo ' ----- Kirim ke : '.$email->email.'<br>';
					
					if ($this->isDummyReceiver()){
						$to = $this->getDummyReceiver(); // ini receiver yang dummy, nanti coment aja
					}else{
						$to = $email->email; // ini receiver yang asli, nanti uncoment aja
					}
					$subject = $data_campaign->subject; 
					$message  = '<h3>'.$data_campaign->title.'</h3>';
					$message .= $data_campaign->description;
					$fromEmail = ($data_campaign->email_name !='') ? $data_campaign->email_name : 'Campaign Bagi Data';
					
					// send email 
					/* $mailer = new YiiMailer();
					$mailer->setFrom('noreplay@bagidata.com', $fromEmail);
					$mailer->setTo($to);
					$mailer->setSubject($subject);
					$mailer->setBody($message,'text/html');
					//$mailer->send();
					if ($mailer->send()){
						$sent++;
					}
					//$mailer->SmtpClose();
					if($jmlemail6==5 || $i == $jumlahEmailAll){
					  $mailer->SmtpClose();
					  $jmlemail6=1;
					} */
					
					
					
					$UserPaketCampaign = UserPaketCampaign::model()->find(
						array(
							'alias'=>$alias,
							//'select'=>'a.id, a.id_user, a.id_paket_campaign, b.nama_paket,  '.$fieldWithAlias,
							'select'=>'a.id, a.id_user, b.nama_paket,  '.$fieldWithAlias,
							'join'=>'join paket_campaign b on a.id_paket_campaign = b.id',
							'condition'=> 'b.status = 1 AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa AND  a.id_user = :userid AND '.$fieldWithAlias.' > 0',
							'params'=>array(':userid'=>$data_campaign->id_user),
							'order'=>'a.created_date ASC',
						)
					);
			
					if($UserPaketCampaign && $UserPaketCampaign->$field > 0){
					
						// create subscriber dan mapping ke list di API Kirim Email
						if ($this->kirimEmail($listID,$to)){
							$sent++;
							
							
							$UserPaketCampaign->$field = $UserPaketCampaign->$field - 1 ;
							$UserPaketCampaign->save(false);
							
							if (isset($arrayCampaignIDSent[$UserPaketCampaign->id])){
								$arrayCampaignIDSent[$UserPaketCampaign->id] = $arrayCampaignIDSent[$UserPaketCampaign->id] + 1;
							}else{
								$arrayCampaignIDSent[$UserPaketCampaign->id] = 1;
							}
							
						}
						
						echo '- Sisa Kuota '.$field_type.' ('.$UserPaketCampaign->nama_paket.', - subscribed_paket_id : '.$UserPaketCampaign->id.' ) : '.$UserPaketCampaign->$field.'<br>';
				
					
					}else{
						echo 'Paket Tidak Mencukupi ';
						echo '----- Sisa Kuota '.$field_type.' : 0 <br>';
					}
					
					
					
					
					$jmlemail6++;
					$i++;
				} 
			}
			
		}
		
		// update data campaign
		if($sent > 0){
			//$fromEmail = ($data_campaign->email_name !='') ? $data_campaign->email_name : 'Campaign Bagi Data';
			//$sender = 'hello@bagidata.com'; // ini mekanisme nya pengen ke $fromEmail, tapi ini di penyedia API nya harus di daftarin dulu ya sender nya, sedangkan api untuk mendaftarkan sender nya ga ada
			$title = $data_campaign->title;
			$subject = $data_campaign->subject;
			$content = $data_campaign->description;
			
			// create template 
			$templateData = $this->emailTemplate($newTemplate,$content);
		
			if ($templateData){
				$templateID = $templateData['templateID'];
				// create broadcast campaign di API Kirim Email
				//$broadcastData = $this->emailBlast($listID,$sender,$title,$subject,$content);
				$broadcastData = $this->emailBlastMailChimp($listID,$from_email,$from_name,$title,$subject,$content,$templateID);
				if ($broadcastData){

					$data_campaign->total_sent = $data_campaign->total_sent + $sent;
					$data_campaign->is_sent = 1;
					$data_campaign->save(false); 
					
					// save campaign sent
					$this->saveCampaignSent($data_campaign->id,$broadcastData['broadcastID'],$sent);
					$this->savehistoryBlastPaket($arrayCampaignIDSent,$data_campaign->id);
					
					
				
				}
			}
		}
		echo '--- Jumlah Terkirim : '.$sent.'<br>';
		
		
	}
	
	
	
	public function sendSMS($data_campaign,$dataReceiver){
		echo '<br> -- Send SMS : <br>';
		echo '<b>--- id : '.$data_campaign->id.' | '.'title : '.$data_campaign->title. /* .' | '.'desc : '.$data_campaign->description*/  '</b><br>';
		
		// check apakah campaign ini sudah di blast pada hari ini, karena cron nya ini setiap 5 menit sekali
		$chekBlastEmail = $this->checkSentBlastOfThisDay($data_campaign->id);
		if ($chekBlastEmail){
			echo ' ----  Sudah Terkirim Pada '.$chekBlastEmail['sent_date_time'].'<br>';
			return true ;
		}
		
		
		$message = $data_campaign->title."\n";
		$message .= $data_campaign->description;
		
		$details = $data_campaign->details;
		//print_r($details);
		
		$sent = 0;
		$field_type = 'sms';
		$alias = 'a';
		$field = 'sisa_'.$field_type;
		$fieldWithAlias = $alias.'.'.$field;
		$arrayCampaignIDSent = array ();
		foreach ($details as $detail){
			echo ' ---- Segmentation : <br>';
			$segmentation = $detail->userSegment;
			echo ' ---- id : '.$detail->id_user_segmentation.' | '.'title : '.$segmentation->title.'<br>';
			
			if ($segmentation->is_custom == 0){
				if (isset($dataReceiver[$segmentation->id])){
					$phoneNumbers = $dataReceiver[$segmentation->id]['phoneNumbers'];
				}else{
					$phoneNumbers = array();
				}
				//print_r($phoneNumbers);
				foreach ($phoneNumbers as $phoneNumber){
					if($phoneNumber != '0'){
						echo ' ----- Kirim ke : '.$phoneNumber.'<br>';
						
						
						/* if($this->actionSendSmsData($phoneNumber,$message)){
							$sent ++;
							$this->actionAddWallet('phone',$phoneNumber,'Promosi Melalui SMS',50);
						} */
						
						$UserPaketCampaign = UserPaketCampaign::model()->find(
						array(
								'alias'=>$alias,
								//'select'=>'a.id, a.id_user, a.id_paket_campaign, b.nama_paket,  '.$fieldWithAlias,
								'select'=>'a.id, a.id_user, b.nama_paket,  '.$fieldWithAlias,
								'join'=>'join paket_campaign b on a.id_paket_campaign = b.id',
								'condition'=> 'b.status = 1 AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa AND  a.id_user = :userid AND '.$fieldWithAlias.' > 0',
								'params'=>array(':userid'=>$data_campaign->id_user),
								'order'=>'a.created_date ASC',
							)
						);
				
						if($UserPaketCampaign && $UserPaketCampaign->$field > 0){
						
							
							if($this->actionSendSmsData($phoneNumber,$message)){
								$sent ++;
								$this->actionAddWallet('phone',$phoneNumber,'Promosi Melalui SMS',50);
								
								
								$UserPaketCampaign->$field = $UserPaketCampaign->$field - 1 ;
								$UserPaketCampaign->save(false);
								
								if (isset($arrayCampaignIDSent[$UserPaketCampaign->id])){
									$arrayCampaignIDSent[$UserPaketCampaign->id] = $arrayCampaignIDSent[$UserPaketCampaign->id] + 1;
								}else{
									$arrayCampaignIDSent[$UserPaketCampaign->id] = 1;
								}
								
							}
							
							echo '- Sisa Kuota '.$field_type.' ('.$UserPaketCampaign->nama_paket.', - subscribed_paket_id : '.$UserPaketCampaign->id.' ) : '.$UserPaketCampaign->$field.'<br>';
					
						
						}else{
							echo 'Paket Tidak Mencukupi ';
							echo '----- Sisa Kuota '.$field_type.' : 0 <br>';
						}
					
					
					}
				} 
			}else{
				if (isset($dataReceiver[$segmentation->id])){
					$phoneNumbers = $dataReceiver[$segmentation->id]['emailsAndphoneNumbers'];
				}else{
					$phoneNumbers = array();
				}
				//print_r($phoneNumbers);
				foreach ($phoneNumbers as $phoneNumber){
					echo ' ----- Kirim ke : '.$phoneNumber->no_hp.'<br>';
					
					/* if($this->actionSendSmsData($phoneNumber->no_hp,$message)){
							$sent ++;
					} */
					
					$UserPaketCampaign = UserPaketCampaign::model()->find(
						array(
								'alias'=>$alias,
								//'select'=>'a.id, a.id_user, a.id_paket_campaign, b.nama_paket,  '.$fieldWithAlias,
								'select'=>'a.id, a.id_user, b.nama_paket,  '.$fieldWithAlias,
								'join'=>'join paket_campaign b on a.id_paket_campaign = b.id',
								'condition'=> 'b.status = 1 AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa AND  a.id_user = :userid AND '.$fieldWithAlias.' > 0',
								'params'=>array(':userid'=>$data_campaign->id_user),
								'order'=>'a.created_date ASC',
							)
						);
				
						if($UserPaketCampaign && $UserPaketCampaign->$field > 0){
						
							
							if($this->actionSendSmsData($phoneNumber->no_hp,$message)){
								$sent ++;
								
								
								$UserPaketCampaign->$field = $UserPaketCampaign->$field - 1 ;
								$UserPaketCampaign->save(false);
								
								if (isset($arrayCampaignIDSent[$UserPaketCampaign->id])){
									$arrayCampaignIDSent[$UserPaketCampaign->id] = $arrayCampaignIDSent[$UserPaketCampaign->id] + 1;
								}else{
									$arrayCampaignIDSent[$UserPaketCampaign->id] = 1;
								}
								
							}
							
							echo '- Sisa Kuota '.$field_type.' ('.$UserPaketCampaign->nama_paket.', - subscribed_paket_id : '.$UserPaketCampaign->id.' ) : '.$UserPaketCampaign->$field.'<br>';
					
						
						}else{
							echo 'Paket Tidak Mencukupi ';
							echo '----- Sisa Kuota '.$field_type.' : 0 <br>';
						}
					
					
				} 
			}
			 
		}
		
		// update data campaign
		if($sent > 0){
			$data_campaign->total_sent = $data_campaign->total_sent + $sent;
			$data_campaign->is_sent = 1;
			$data_campaign->save(false);
			
			// save campaign sent
			$this->saveCampaignSent($data_campaign->id,'',$sent);
			$this->savehistoryBlastPaket($arrayCampaignIDSent,$data_campaign->id);
		}
		echo '--- Jumlah Terkirim : '.$sent.'<br>';
		
	}
	
	public function sendWA($data_campaign,$dataReceiver){
		echo '<br> -- Send WA : <br>';
		echo '<b>--- id : '.$data_campaign->id.' | '.'title : '.$data_campaign->title. /* .' | '.'desc : '.$data_campaign->description*/  '</b><br>';
		
		// check apakah campaign ini sudah di blast pada hari ini, karena cron nya ini setiap 5 menit sekali
		$chekBlastEmail = $this->checkSentBlastOfThisDay($data_campaign->id);
		if ($chekBlastEmail){
			echo ' ----  Sudah Terkirim Pada '.$chekBlastEmail['sent_date_time'].'<br>';
			return true ;
		}
		
		$message = $data_campaign->title."\n";
		$message .= $data_campaign->description;
		
		$details = $data_campaign->details;
		//print_r($details);
		
		$sent = 0;
		$field_type = 'wa';
		$alias = 'a';
		$field = 'sisa_'.$field_type;
		$fieldWithAlias = $alias.'.'.$field;
		$arrayCampaignIDSent = array ();
		foreach ($details as $detail){
			echo ' ---- Segmentation : <br>';
			$segmentation = $detail->userSegment;
			echo ' ---- id : '.$detail->id_user_segmentation.' | '.'title : '.$segmentation->title.'<br>';
			
			if ($segmentation->is_custom == 0){
				if (isset($dataReceiver[$segmentation->id])){
					$phoneNumbers = $dataReceiver[$segmentation->id]['phoneNumbers'];
				}else{
					$phoneNumbers = array();
				}
				//print_r($phoneNumbers);
				foreach ($phoneNumbers as $phoneNumber){
					if($phoneNumber != '0'){
						echo ' ----- Kirim ke : '.$phoneNumber.'<br>';
						
						/* if($this->actionSendWAData($phoneNumber,$message)){
							$sent ++;
							$this->actionSendWADataImage($phoneNumber,$data_campaign->id);
							$this->actionAddWallet('phone',$phoneNumber,'Promosi Melalui Whatsapp',50);
						} */
						
						$UserPaketCampaign = UserPaketCampaign::model()->find(
						array(
								'alias'=>$alias,
								//'select'=>'a.id, a.id_user, a.id_paket_campaign, b.nama_paket,  '.$fieldWithAlias,
								'select'=>'a.id, a.id_user, b.nama_paket,  '.$fieldWithAlias,
								'join'=>'join paket_campaign b on a.id_paket_campaign = b.id',
								'condition'=> 'b.status = 1 AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa AND  a.id_user = :userid AND '.$fieldWithAlias.' > 0',
								'params'=>array(':userid'=>$data_campaign->id_user),
								'order'=>'a.created_date ASC',
							)
						);
				
						if($UserPaketCampaign && $UserPaketCampaign->$field > 0){
						
							
							if($this->actionSendWAData($phoneNumber,$message)){
								$sent ++;
								$this->actionSendWADataImage($phoneNumber,$data_campaign->id);
								$this->actionAddWallet('phone',$phoneNumber,'Promosi Melalui Whatsapp',50);
								
								
								$UserPaketCampaign->$field = $UserPaketCampaign->$field - 1 ;
								$UserPaketCampaign->save(false);
								
								if (isset($arrayCampaignIDSent[$UserPaketCampaign->id])){
									$arrayCampaignIDSent[$UserPaketCampaign->id] = $arrayCampaignIDSent[$UserPaketCampaign->id] + 1;
								}else{
									$arrayCampaignIDSent[$UserPaketCampaign->id] = 1;
								}
								
							}
							
							echo '- Sisa Kuota '.$field_type.' ('.$UserPaketCampaign->nama_paket.', - subscribed_paket_id : '.$UserPaketCampaign->id.' ) : '.$UserPaketCampaign->$field.'<br>';
					
						
						}else{
							echo 'Paket Tidak Mencukupi ';
							echo '----- Sisa Kuota '.$field_type.' : 0 <br>';
						}
						
						
					}
				} 
			}else{
				if (isset($dataReceiver[$segmentation->id])){
					$phoneNumbers = $dataReceiver[$segmentation->id]['emailsAndphoneNumbers'];
				}else{
					$phoneNumbers = array();
				}
				//print_r($phoneNumbers);
				foreach ($phoneNumbers as $phoneNumber){
					echo ' ----- Kirim ke : '.$phoneNumber->no_hp.'<br>';
					
					/* if($this->actionSendWAData($phoneNumber->no_hp,$message)){
							$sent ++;
							$this->actionSendWADataImage($phoneNumber->no_hp,$data_campaign->id);
					} */
					
					$UserPaketCampaign = UserPaketCampaign::model()->find(
						array(
								'alias'=>$alias,
								//'select'=>'a.id, a.id_user, a.id_paket_campaign, b.nama_paket,  '.$fieldWithAlias,
								'select'=>'a.id, a.id_user, b.nama_paket,  '.$fieldWithAlias,
								'join'=>'join paket_campaign b on a.id_paket_campaign = b.id',
								'condition'=> 'b.status = 1 AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa AND  a.id_user = :userid AND '.$fieldWithAlias.' > 0',
								'params'=>array(':userid'=>$data_campaign->id_user),
								'order'=>'a.created_date ASC',
							)
						);
				
						if($UserPaketCampaign && $UserPaketCampaign->$field > 0){
						
							
							if($this->actionSendWAData($phoneNumber->no_hp,$message)){
								$sent ++;
								$this->actionSendWADataImage($phoneNumber->no_hp,$data_campaign->id);
								
								
								$UserPaketCampaign->$field = $UserPaketCampaign->$field - 1 ;
								$UserPaketCampaign->save(false);
								
								if (isset($arrayCampaignIDSent[$UserPaketCampaign->id])){
									$arrayCampaignIDSent[$UserPaketCampaign->id] = $arrayCampaignIDSent[$UserPaketCampaign->id] + 1;
								}else{
									$arrayCampaignIDSent[$UserPaketCampaign->id] = 1;
								}
								
							}
							
							echo '- Sisa Kuota '.$field_type.' ('.$UserPaketCampaign->nama_paket.', - subscribed_paket_id : '.$UserPaketCampaign->id.' ) : '.$UserPaketCampaign->$field.'<br>';
					
						
						}else{
							echo 'Paket Tidak Mencukupi ';
							echo '----- Sisa Kuota '.$field_type.' : 0 <br>';
						}
					
				} 
			}
			 
		}
		
		// update data campaign
		if($sent > 0){
			$data_campaign->total_sent = $data_campaign->total_sent + $sent;
			$data_campaign->is_sent = 1;
			$data_campaign->save(false);
			
			// save campaign sent
			$this->saveCampaignSent($data_campaign->id,'',$sent);
			$this->savehistoryBlastPaket($arrayCampaignIDSent,$data_campaign->id);
		}
		echo '--- Jumlah Terkirim : '.$sent.'<br>';
		
	}
	
	
	public function actionSendSmsData($no_hp,$message){
		
		if($this->isDummyBlast()){
			echo '----- SUCCESS<br>';
			return true;
			exit;
		}
		
		
		$data = array(
			"grant_type" => "client_credentials",
		);
		
		$token = curl_init(); 
		curl_setopt($token, CURLOPT_URL, "https://api.mainapi.net/token"); 
		curl_setopt($token, CURLOPT_POST, 1 ); 
		curl_setopt($token, CURLOPT_POSTFIELDS, http_build_query($data)); 
		curl_setopt($token, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($token, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($token, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($token, CURLOPT_HTTPHEADER, array('Authorization: Basic dFJEbmhxMk9WOThmcmZSVlA0TmZ2YXZaVWVZYTpJa2JZX2NBeEMwWUU4Q1Jsd2NVNjNRRTBfZk1h','Content-Type: application/x-www-form-urlencoded'));
		$postToken = curl_exec($token); 
		curl_close($token);
		$responseToken = json_decode($postToken, true);
		$token = $responseToken['token_type'].' '.$responseToken['access_token']; 
		//return $token; 
		
		if ($this->isDummyReceiver()){
			$receiver = $this->getDummyReceiverPhone();
		}else{
			$receiver = $no_hp;
		}

		$data = array(
			"msisdn" => $receiver,
			"content" => $message,
		);
		$otp = curl_init(); 
		curl_setopt($otp, CURLOPT_URL, "https://api.mainapi.net/smsnotification/1.0.0/messages"); 
		curl_setopt($otp, CURLOPT_POST, 1 ); 
		curl_setopt($otp, CURLOPT_POSTFIELDS, http_build_query($data)); 
		curl_setopt($otp, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($otp, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($otp, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($otp, CURLOPT_HTTPHEADER, array('Authorization: '.$token.'','Content-Type: application/x-www-form-urlencoded', 'Accept: application/json')); // ini yang token nya statis untuk guest tapi di limit perhari nya, tapi gak tentu
		//curl_setopt($otp, CURLOPT_HTTPHEADER, array('Authorization: Bearer 9c49e1a75a7559b506b72631743bdd79','Content-Type: application/x-www-form-urlencoded', 'Accept: application/json')); // ini buat yang kita , paket sendiri bisa di lihat token nya di bagian Bearer
		$postToken = curl_exec($otp); 
		curl_close($otp);
		$responseToken = json_decode($postToken, true); 
		
		
		
		//echo '<pre>';
		//print_r($responseToken);
		//exit;
		
		if (isset($responseToken['status'])){
			$return = true;
			echo '----- '.$responseToken['status'].'<br>';
		}else{
			echo '----- ERROR'.' '.$responseToken['fault']['description'].'<br>';
			$return = false;
		}
		
		return $return;
		
	}
	
	
	public function actionSendWAData($no_hp,$message){	
		
		if($this->isDummyBlast()){
			echo '----- SUCCESS<br>';
			return true;
			exit;
		}
		
		$nomor_awal_no_hp = substr($no_hp,0,1);
		//echo $nomor_awal_no_hp;
		if ($nomor_awal_no_hp == '0'){
			$no_hp ='62'.substr($no_hp,1);
		}
		
		
		if ($this->isDummyReceiver()){
			$receiver = $this->getDummyReceiverPhoneWA();
		}else{
			$receiver = $no_hp;
		}
		
		
		/* $token ='1b39ca944fdf7674ad9037d5e71da1565b0236e9d8a11';
		$from = '6281383930606'; */
		$token ='b42a3ddee4810ee259f924b0bf62f3b15b446a8760fce';
		$from = '628157060604';
		$destination = $receiver; 
		$message = $message;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, "https://www.waboxapp.com/api/send/chat"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$token."&uid=".$from."&to=".$destination."&text=".$message); 
		//curl_setopt($ch, CURLOPT_POSTFIELDS, "token=1b39ca944fdf7674ad9037d5e71da1565b0236e9d8a11&uid=".$from."&to=".$destination."&custom_uid=msg-5331&text=Hello+world%21"); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25); 

		$response = curl_exec($ch); 
		$info = curl_getinfo($ch); 
		curl_close ($ch);
		
		$result = CJSON::decode($response);
		
		if (isset($result['success'])){
			$return = true;
			echo '----- SUCCESS<br>';
		}else{
			$return = false;
			echo '----- ERROR '.$result['error'].'<br>';
		}
		
		/* echo '<pre>';
		print_r($result);
		exit;
		*/
		
		
		// ini gak jadi
		/* $my_apikey = "4ZF5RFA4ZMJ47LT8WMNS"; 
		$destination = $receiver; 
		$message = $message; 
		$api_url = "http://panel.apiwha.com/send_message.php"; 
		$api_url .= "?apikey=". urlencode ($my_apikey); 
		$api_url .= "&number=". urlencode ($destination); 
		$api_url .= "&text=". urlencode ($message); 
		$my_result_object = json_decode(file_get_contents($api_url, false)); 
		if ($my_result_object->success == 1){
			//echo "<br>Result: ". $my_result_object->success; 
			$return = true;
			echo "<br>SUCCESS - "; 
			echo "Description: ". $my_result_object->description; 
		}else{
			echo "<br>ERROR - "; 
			echo "Description: ". $my_result_object->description; 
			$return = false;
		} */
		
		
		return $return;
	}
	
	
	public function actionSendWADataImage($no_hp,$id_campaign){	
	
		if($this->isDummyBlast()){
			echo '----- SUCCESS<br>';
			return true;
			exit;
		}
		
		$campaignImage = CampaignImage::model()->findAllByAttributes(array('id_campaign'=>$id_campaign));
		foreach ($campaignImage as $image){
			
				$baseUrl = 'https://enterprise.bagidata.com/';
				$urlImage = $baseUrl.$image->image;
				 
				 
				$nomor_awal_no_hp = substr($no_hp,0,1);
				//echo $nomor_awal_no_hp;
				if ($nomor_awal_no_hp == '0'){
					$no_hp ='62'.substr($no_hp,1);
				}
				
				
				if ($this->isDummyReceiver()){
					$receiver = $this->getDummyReceiverPhoneWA();
				}else{
					$receiver = $no_hp;
				}
				
				
				/* $token ='1b39ca944fdf7674ad9037d5e71da1565b0236e9d8a11';
				$from = '6281383930606'; */
				$token ='b42a3ddee4810ee259f924b0bf62f3b15b446a8760fce';
				$from = '628157060604';
				$destination = $receiver; 
				$url = $urlImage;
				//$url = 'https://enterprise.bagidata.com/upload/images/campaign/campaign_image_20180523135130_tiket_kereta_png.png'; // ini buat test aja
				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_URL, "https://www.waboxapp.com/api/send/image"); 
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
				curl_setopt($ch, CURLOPT_POST, 1); 
				curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$token."&uid=".$from."&to=".$destination."&url=".$url); 
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
				curl_setopt($ch, CURLOPT_MAXREDIRS, 5); 
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
				curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 25); 

				$response = curl_exec($ch); 
				$info = curl_getinfo($ch); 
				curl_close ($ch);
				
				$result = CJSON::decode($response);
				
				if (isset($result['success'])){
					$return = true;
					echo '----- SUCCESS (IMAGE)<br>';
				}else{
					$return = false;
					echo '----- ERROR (IMAGE)'.$result['error'].'<br>';
				}
				 
				 
		}
		
		return $return;
	}
	
	
	public function actionAddWallet($typearameter,$valueparameter,$statement,$amount){
			
			/* $typearameter = 'email';
			$valueparameter = 'adinugroho0002@gmail.com';
			$statement = 'Promosi Melalui Whatsapp';
			$amount = 50; */
			
			
			
			$sql = "
				SELECT id_user FROM `user_personal_information` 
				where 
				fieldid = :fieldid AND
				value = :value AND
				NOW() between begda AND endda
				";
			$query = Yii::app()->db->createCommand($sql);
			$query->params = array(
						':fieldid'=>$typearameter,
						':value'=>$valueparameter,
			);
			$row = $query->queryRow();
			/* echo '<pre>';
			print_r($row);
			exit; */
			
			if ($row){
				
				$idUser = $row['id_user'];
				$type = 'cash';
				$drcr = 'credit';
				
				$newWallet = new Wallet();
				$newWallet->id_user = $idUser;
				$newWallet->datetime = date('Y-m-d H:i:s');
				$newWallet->type = $type;
				$newWallet->amount = $amount;
				$newWallet->drcr = $drcr;
				$newWallet->statement = $statement;
				if ($newWallet->save()){
					$this->updateWallet($type, $newWallet->amount, $newWallet->id_user);
					echo '----- SUCCESS ADD WALLET '.$type.' '.$amount.'<br>';
				}
				
			}
	}
	
	private function updateWallet($type, $amount, $idUser)
    {
        $sql = 'UPDATE user SET '.$type.' = '.$type.' + '.$amount.' WHERE id = '.$idUser;
        $query = Yii::app()->db->createCommand($sql);
        $query->execute();            
    }
	
	
	
	public function kirimEmail($listID,$email){
		
		if($this->isDummyBlast()){
			return true ;
			exit;
		}
		
		// api kirim email
		/* $createSubscriberTiList = KirimEmail::createSubscriberToList($listID,$email);
		if ($createSubscriberTiList['status']=='error'){ // jika error
			//echo ' -----  Gagal Create Subscriber to List API - '.$createSubscriberTiList['message'].'<br>';
			return false;
		}else{
			//$subscriberID = $createSubscriberTiList['data']['id'];
			return true;
		} */
		
		// api kirim mail chimp
		$createSubscriberTiList = KirimEmailMailchimp::createSubscriberToList($listID,$email);
		if ($createSubscriberTiList['status']=='error' || (isset($createSubscriberTiList['data']['type']) && $createSubscriberTiList['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){ // jika error
			return false;
		}else{
			return true;
		}
		
		
	}
	
	public function emailTemplate($name,$content){
		
		if($this->isDummyBlast()){
			$return['templateID']=77789;
			return $return;
			exit;
		}
		
		$return = array();
		$createTemplate = KirimEmailMailchimp::createTemplate($name,$content);
		if ($createTemplate['status']=='error' || (isset($createTemplate['data']['type']) && $createTemplate['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){ // jika error // jika error
			//return false;
			$return = array();
		}else{
			$templateID = $createTemplate['data']['id'];
			//return true;
			$return['templateID']=$templateID;
		}
		
		return $return ;
		
	}
	
	/* public function emailBlast($listID,$sender,$title,$subject,$content){
		
		if($this->isDummyBlast()){
			$return['broadcastID']='dummy_blast';
			return $return;
			exit;
		}
		
		$return = array();
		$createBroadcast = KirimEmail::createBroadcast($listID,$sender,$title,$subject,$content);
		if ($createBroadcast['status']=='error'){ // jika error
			//echo ' -----  Gagal Create Broadcast to List API - '.$createBroadcast['message'].'<br>';
			//return false;
			$return = array();
		}else{
			$broadcastID = $createBroadcast['data']['campaign_guid'];
			//return true;
			$return['broadcastID']=$broadcastID;
		}
		
		return $return ;
		
	} */
	
	public function emailBlastMailChimp($listID,$from_email,$from_name,$title,$subject,$content,$templateID){
		
		if($this->isDummyBlast()){
			$return['broadcastID']='dummy_blast';
			return $return;
			exit;
		}
		
		$return = array();
		$createBroadcast = KirimEmailMailchimp::createBroadcast($listID,$from_email,$from_name,$title,$subject,$content,$templateID);
		if ($createBroadcast['status']=='error' || (isset($createBroadcast['data']['type']) && $createBroadcast['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){ // jika error // jika error // jika error
			//echo ' -----  Gagal Create Broadcast to List API - '.$createBroadcast['message'].'<br>';
			//return false;
			$return = array();
		}else{
			$broadcastID = $createBroadcast['data']['id'];
			//return true;
			$return['broadcastID']=$broadcastID;
			
			// send email
			$sentceklist = KirimEmailMailchimp::sendChecklist($broadcastID);
			/* echo '<pre>';
			print_r($sentceklist);
			echo '<pre>'; */
			
			$sent = KirimEmailMailchimp::sendBroadcast($broadcastID);
			
			/* echo '<pre>';
			print_r($sent);
			echo '<pre>'; */
		}
		
		return $return ;
		
	}
	
	
	
	public function saveCampaignSent($id_campaign,$broadcastID,$sent){
		
				// save data campaign
				$campaignSent = new CampaignSent;
				$campaignSent->id_campaign = $id_campaign;
				$campaignSent->sent_date_time = date("Y-m-d H:i:s");
				$campaignSent->campaign_guid = $broadcastID;
				$campaignSent->total_sent = $sent;
				$campaignSent->save();
				
	}
	
	
	public function checkSentBlastOfThisDay($id_campaign){
		
		
		$return = array();
		$campaignSentThisday = CampaignSent::model()->find(array(
			'select'=>'id, id_campaign, sent_date_time',
			'condition'=> 'id_campaign = :id_campaign and DATE(sent_date_time) = DATE(NOW())',
			'params'=>array('id_campaign'=>$id_campaign),
		));
		if ($campaignSentThisday){ 
			$return['sent_date_time']=$campaignSentThisday->sent_date_time;
		}else{
			$return = array();
		}
		
		return $return ;
		
	}
	
	public function savehistoryBlastPaket($arrayCampaignIDSent,$id_campaign){
			foreach ($arrayCampaignIDSent as $key => $value){
				if($value > 0){
					$history = new HistoryBlastPaket;
					$history->id_campaign = $id_campaign;
					$history->id_user_paket_campaign = $key; 
					$history->total = $value; 
					$history->created_date = date('Y-m-d H:i:s');
					$history->save(false);
				}
			}
	}
	
	public function actionPaymentResponse($id,$id_pc)
    {
		$id_paket_campaign = $id_pc;
		/* echo $id.'-'.$id_paket_campaign ;
		exit; */
		
		
		
		$PaketCampaign = PaketCampaign::model()->findByPk($id_paket_campaign);
		
		$status = 1;
		$masa_aktif = $PaketCampaign->masa_aktif;
		
		$dayDiff = '+'.$masa_aktif ;
		$dateNow = date('Y-m-d'); // ini dinamis
		$dateAfter = date('Y-m-d', strtotime($dayDiff.' days', strtotime($dateNow)));
		
		$tgl_aktif = $dateNow;
		$tgl_kadaluarsa = $dateAfter;
		
		
		

        $UserPaketCampaign = UserPaketCampaign::model()->findByPk($id);
        $UserPaketCampaign->status = $status;
		$UserPaketCampaign->tgl_aktif = $tgl_aktif;
		$UserPaketCampaign->tgl_kadaluarsa = $tgl_kadaluarsa;
		$UserPaketCampaign->sisa_email = $PaketCampaign->kuota_email;
		$UserPaketCampaign->sisa_sms = $PaketCampaign->kuota_sms;
		$UserPaketCampaign->sisa_wa = $PaketCampaign->kuota_wa;
		$UserPaketCampaign->updated_date = date("Y-m-d H:i:s");
        $UserPaketCampaign->save();
		
		echo 'Info Subcribe Paket Campaign For user : '.$UserPaketCampaign->id_user.'<br>';
		echo 'User Paket with Subscribe ID '.$id.' has been activated at '.$UserPaketCampaign->tgl_aktif;
    }
	
	
	public function actionPaymentResponseCart()
    {
		if(isset($_POST['payment_code'])){
			$payment_code = $_POST['payment_code'];
			//echo $payment_code;
			$result_desc = $_POST['result_desc'];
			
			
			$UserPaketCampaign = UserPaketCampaign::model()->findAll(array(
				'condition'=>'kode_transaksi = :kode_transaksi',
				'params'=>array(':kode_transaksi'=>$payment_code),
			));
			
			$dateTimeNow =  date("Y-m-d H:i:s");
			foreach($UserPaketCampaign as $userpaket){
				$PaketCampaign = PaketCampaign::model()->findByPk($userpaket->id_paket_campaign);
		
				if ($result_desc == 'Expired'){ // jika expired pembayarannya
					$status = 2;
				
					$userpaket->status = $status;
					$userpaket->updated_date =$dateTimeNow;
					$userpaket->save(false);
					
				}else{
					$status = 1;
					$masa_aktif = $PaketCampaign->masa_aktif;
					
					$dayDiff = '+'.$masa_aktif ;
					$dateNow = date('Y-m-d'); // ini dinamis
					$dateAfter = date('Y-m-d', strtotime($dayDiff.' days', strtotime($dateNow)));
					
					$tgl_aktif = $dateNow;
					$tgl_kadaluarsa = $dateAfter;
					

					
					$userpaket->status = $status;
					$userpaket->tgl_aktif = $tgl_aktif;
					$userpaket->tgl_kadaluarsa = $tgl_kadaluarsa;
					$userpaket->sisa_email = $PaketCampaign->kuota_email;
					$userpaket->sisa_sms = $PaketCampaign->kuota_sms;
					$userpaket->sisa_wa = $PaketCampaign->kuota_wa;
					$userpaket->updated_date =$dateTimeNow;
					$userpaket->save();
				}
			}
			
//			echo 'User Paket with Transaction Code '.$payment_code.' has been activated at '.$dateTimeNow;
            //echo '00';
			if ($result_desc == 'Expired'){
				echo '05';
			}else{
				echo '00';
			}
		}
		
	}
    
    /* public function sampleRequestFinpay()
    {        
        $merchant_id = 'BD909';
        $amount = 11000;
        $param = array();
        $param['add_info1'] = 'Test Saja'; 
        $param['amount'] = '12000'; 
        $param['cust_email'] = 'buatmainan123@gmail.com'; 
        $param['cust_id'] = '6'; 
        $param['cust_msisdn'] = '08212345678'; 
        $param['cust_name'] = 'buatmainan123'; 
        $param['invoice'] = 'INV-123346'; 
        $param['merchant_id'] = $merchant_id; 
        $param['return_url'] = 'http://localhost/admbagidata/faq/testReturn'; 
        $param['sof_id'] = 'finpay021'; 
        $param['sof_type'] = 'pay'; 
        $param['timeout'] = '100000'; 
        $param['trans_date'] = '20180719214900';         
//        $param['mer_signature'] = $this->merSignature($param);
//        echo CJSON::encode($param);exit;
        
        $finpay = new Finpay($param);
        echo 'Payment Code :' .$finpay->requestPaymentCode(); // function ini return payment code jika succeess dan return null jika gagal. 
        echo '<br> Status Code :' .$finpay->statusCode.' '.$finpay->statusDesc; // ini untuk menampilkan status code dan message response dari API.
        echo $finpay->getJsonText(); // mendapatkan json requestnya        
    } */
}