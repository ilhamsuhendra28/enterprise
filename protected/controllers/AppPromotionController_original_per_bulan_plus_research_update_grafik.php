<?php

class AppPromotionController extends Controller
{
	public $layout='../layouts/mainadmin';
	public $uploadDir = '../app/images/promo_photo/';
	//public $uploadDir = 'upload/images/promo_kode/';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'view', 'create', 'update', 'deleteImage', 'aPromotion','getDataTest'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function loadModel($id)
	{
		$model=Promo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	public function actionView($id){
		
			
		$model = $this->loadModel($id);
		
		$promoImage = PromoImage::model()->findAllByAttributes(array('id_promo'=>$model->id));
		
		$criteria=new CDbCriteria;
		$criteria->compare('id_promo',$model->id);
		$dataProvider=new CActiveDataProvider('PromoDetail', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'created_date DESC',
			),
			'pagination'=>array(
				'pageSize'=>10,
			)
		));
		
		$totalPromoKode = PromoKode::model()->countByAttributes(array('id_promo'=>$model->id));
		$dataPromoKode = PromoKode::model()->findByAttributes(array('id_promo'=>$model->id));
		if($dataPromoKode){
			$promo_code = $dataPromoKode->kode_promo;
		}else{
			$promo_code = '';
		}
		
		
		$this->render('view',array(
			'model'=>$model,
			'promoImage'=>$promoImage,
			'totalPromoKode'=>$totalPromoKode,
			'promo_code'=>$promo_code,
			'dataProvider'=>$dataProvider,
		));
	}	
	
	public function actionIndex(){
		
		$totalDataPromo = Promo::model()->countByAttributes(array('id_company'=>Yii::app()->user->id));
		if ($totalDataPromo > 0){
			$this->actionAPromotion();
		}else{
			$this->render('index');
		}
	}	
	
	public function actionCreate(){
		$model=new Promo;
		
		$totalPromoKode = PromoKode::model()->countByAttributes(array('id_promo'=>$model->id));
		$dataPromoKode = PromoKode::model()->findByAttributes(array('id_promo'=>$model->id));
		if($dataPromoKode){
			$promo_code = $dataPromoKode->kode_promo;
		}else{
			$promo_code = '';
		}
		
		$dataUserSegmentation = UserSegmentation::model()->allSegment()->findAllByAttributes(array('id_user'=>Yii::app()->user->id,'is_custom'=>0));
		
		$promoDetail = array();
		$promoImage = array();
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['Promo']))
			{
				
				/* echo '<pre>';
				print_r($_POST['Promo']);
				exit; */
				
				$transaction = Yii::app()->db->beginTransaction();
				try{
			
					
					$model->attributes=$_POST['Promo'];
					$model->id_company = Yii::app()->user->id;
					if ($model->promo_mode=='point'){
						$model->cash = 0;
					}else{
						$model->point = 0;
					}
					
					
					if($model->save()){						
					
						
						// save promo detail
						if (isset($_POST['user_segment'])){
							$this->saveDetailPromo($model->id);
						}
						
						// save promo kode
						for ($i = 1; $i <= $model->jumlah; $i++) {
							//echo $i,'<br>';
							$this->savePromoCode($model->id,$model->promo_code);
						}
						//exit;
						
						// upload images
						$fileObject=CUploadedFile::getInstances($model,'images');
						if($fileObject){
							/* echo '<pre>';
							print_r($fileObject);
							echo 'ada'; */
							
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							//echo 'ok';
							foreach ($fileObject as $key => $image){
								$timenow=date("YmdHis");
								$extension=$image->extensionName;
								$originalFileName = $judul =  str_replace(array(':', '\\', '/', '*','.',' '), '_', $image->name);
								$fileName = 'promo_image'.'_'.$timenow.'_'.$originalFileName;
								$newFileName = $fileName.'.'.$extension;
								//echo $newFileName.'<br>';
								
								$this->savePromoImage($model->id,$this->uploadDir.$newFileName);
								
								$image->saveAs($this->uploadDir.$newFileName); 
								chmod($this->uploadDir.$newFileName, 0777);
							}
							
						}
						
						Yii::app()->user->setFlash('success', " Data Saved ");
						$this->redirect(array('aPromotion'));
					}
					
					$transaction->commit();
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('id_company','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
			'totalPromoKode'=>$totalPromoKode,
			'promo_code'=>$promo_code,
			'dataUserSegmentation'=>$dataUserSegmentation,
			'promoDetail'=>$promoDetail,
			'promoImage'=>$promoImage,
		));
	}
	
	
	public function actionUpdate($id){
		$model=$this->loadModel($id);
		
		$totalPromoKode = PromoKode::model()->countByAttributes(array('id_promo'=>$model->id));
		$dataPromoKode = PromoKode::model()->findByAttributes(array('id_promo'=>$model->id));
		if($dataPromoKode){
			$promo_code = $dataPromoKode->kode_promo;
		}else{
			$promo_code = '';
		}
		
		$dataUserSegmentation = UserSegmentation::model()->allSegment()->findAllByAttributes(array('id_user'=>Yii::app()->user->id,'is_custom'=>0));
		
		$promoDetail = PromoDetail::model()->findAllByAttributes(array('id_promo'=>$model->id));
		$promoImage = PromoImage::model()->findAllByAttributes(array('id_promo'=>$model->id));
		
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['Promo']))
			{
				
				/* echo '<pre>';
				print_r($_POST['Promo']);
				exit; */
				
				$transaction = Yii::app()->db->beginTransaction();
				try{
			
					
					$model->attributes=$_POST['Promo'];
					$model->id_company = Yii::app()->user->id;
					if ($model->promo_mode=='point'){
						$model->cash = 0;
					}else{
						$model->point = 0;
					}
					
					
					if($model->save()){						
					
					
						//kalau update biar gampang di delete dulu data detail yang sebelumnya, lalu di replace sama yang baru
						$this->deleteDetailPromo($model->id);
						// save promo detail
						if (isset($_POST['user_segment'])){
							$this->saveDetailPromo($model->id);
						}
						
						
						// kalo update di delete dulu yang sebelumnya 
						$this->deletePromoCode($model->id);
						
						// save promo kode
						for ($i = 1; $i <= $model->jumlah; $i++) {
							//echo $i,'<br>';
							$this->savePromoCode($model->id,$model->promo_code);
						}
						//exit;
						
						// upload images
						$fileObject=CUploadedFile::getInstances($model,'images');
						if($fileObject){
							/* echo '<pre>';
							print_r($fileObject);
							echo 'ada'; */
							
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							//echo 'ok';
							foreach ($fileObject as $key => $image){
								$timenow=date("YmdHis");
								$extension=$image->extensionName;
								$originalFileName = $judul =  str_replace(array(':', '\\', '/', '*','.',' '), '_', $image->name);
								$fileName = 'promo_image'.'_'.$timenow.'_'.$originalFileName;
								$newFileName = $fileName.'.'.$extension;
								//echo $newFileName.'<br>';
								
								$this->savePromoImage($model->id,$this->uploadDir.$newFileName);
								
								$image->saveAs($this->uploadDir.$newFileName); 
								chmod($this->uploadDir.$newFileName, 0777);
							}
							
						}
						
						Yii::app()->user->setFlash('success', " Data Saved ");
						$this->redirect(array('aPromotion'));
					}
					
					$transaction->commit();
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('id_company','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
			'totalPromoKode'=>$totalPromoKode,
			'promo_code'=>$promo_code,
			'dataUserSegmentation'=>$dataUserSegmentation,
			'promoDetail'=>$promoDetail,
			'promoImage'=>$promoImage,
		));
	}
	
	public function savePromoCode($id_promo,$kode_promo){
			$PromoKode = new PromoKode;
			$PromoKode->id_promo = $id_promo;
			$PromoKode->kode_promo = $kode_promo;
			$PromoKode->save();
	}
	
	public function deletePromoCode($id_promo){
		PromoKode::model()->deleteAllByAttributes(array(
			'id_promo'=>$id_promo,
		));
	}
	
	public function savePromoImage($id_promo,$image){
		
			$imageData = new PromoImage;
			$imageData->id_promo = $id_promo;
			$imageData->image = $image;
			$imageData->save();
	}
	
	public function actionDeleteImage(){
		
		$key = $_POST['key'];
		$promoImage = PromoImage::model()->findByPk($key);
		$file = $_SERVER['DOCUMENT_ROOT'].''.Yii::app()->baseUrl.'/'.$promoImage->image;
		//echo $file;
		//exit;
		if ($promoImage){
			if (file_exists($file)){
				if ( unlink($file) ){
					$promoImage->delete();
					//echo "success";
				}else{
					//echo "fail";
					throw new CHttpException(500,'Kesalahan Terjadi. gagal delete');
				} 
			}else{
				throw new CHttpException(500,'Kesalahan Terjadi. file tidak ada');
			}
		}else{
			throw new CHttpException(500,'Kesalahan Terjadi. data tidak ada');
		}
		
		
		echo json_encode(array('status'=>'success'));
	}	

	
	public function saveDetailPromo($id_promo){
		$dataSegmentSelected = $_POST['user_segment'];
		foreach($dataSegmentSelected as $key => $value){
			$promoDetail = new PromoDetail;
			$promoDetail->id_promo = $id_promo;
			$promoDetail->id_user_segmentation = $value;
			$promoDetail->created_date = date("Y-m-d H:i:s");
			$promoDetail->status = 1;
			$promoDetail->save();
		}
	}
	
	public function deleteDetailPromo($id_promo){
		PromoDetail::model()->deleteAllByAttributes(array(
			'id_promo'=>$id_promo,
		));
	}
	
	public function actionAPromotion(){
		
		$year = date('Y');
		
		$model=new Promo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Promo']))
			$model->attributes=$_GET['Promo'];
		
		$model->id_company=Yii::app()->user->id;
		$dataProvider=$model->search();
		
		
		// -- grafik --//
		// total views
		$totalViewsData = $model->countTotalViewsData($year);
		//-- overview total views --//
		$overviewViewsData = $model->overviewViewsData($year);
		
		// total clicks
		$totalClicksData = $model->countTotalClicksData($year);
		//-- overview total clicks --//
		$overviewClicksData = $model->overviewClicksData($year);
		
		// total buyer
		$totalBuyerData = $model->countTotalBuyerData($year);
		//-- overview total Buyer --//
		$overviewBuyerData = $model->overviewBuyerData($year);
	
		

		$this->render('apromotion',array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
			'year'=>$year,
			'totalViewsData'=>$totalViewsData,
			'overviewViewsData'=>$overviewViewsData,
			'totalClicksData'=>$totalClicksData,
			'overviewClicksData'=>$overviewClicksData,
			'totalBuyerData'=>$totalBuyerData,
			'overviewBuyerData'=>$overviewBuyerData,
		));
		
	}
	
	public function actionGetDataTest(){
		$array = array();
		$array[]='ada';
		$array[]='ga ada';
		$array[]='aha';
		
		echo CJSON::encode($array);
	}
	
	
}