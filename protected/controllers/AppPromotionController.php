<?php

class AppPromotionController extends Controller
{
	public $layout='../layouts/mainadmin';
	public $uploadDir = '../app/images/promo_photo/';
	//public $uploadDir = 'upload/images/promo_kode/';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'view', 'create', 'update', 'deleteImage', 'aPromotion','getDataTest'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function loadModel($id)
	{
		$model=Promo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	public function actionView($id){
		
			
		$model = $this->loadModel($id);
		
		$promoImage = PromoImage::model()->findAllByAttributes(array('id_promo'=>$model->id));
		
		$criteria=new CDbCriteria;
		$criteria->compare('id_promo',$model->id);
		$dataProvider=new CActiveDataProvider('PromoDetail', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'created_date DESC',
			),
			'pagination'=>array(
				'pageSize'=>10,
			)
		));
		
		$totalPromoKode = PromoKode::model()->countByAttributes(array('id_promo'=>$model->id));
		$dataPromoKode = PromoKode::model()->findByAttributes(array('id_promo'=>$model->id));
		if($dataPromoKode){
			$promo_code = $dataPromoKode->kode_promo;
		}else{
			$promo_code = '';
		}
		
		
		$this->render('view',array(
			'model'=>$model,
			'promoImage'=>$promoImage,
			'totalPromoKode'=>$totalPromoKode,
			'promo_code'=>$promo_code,
			'dataProvider'=>$dataProvider,
		));
	}	
	
	public function actionIndex(){
		
		$totalDataPromo = Promo::model()->countByAttributes(array('id_company'=>Yii::app()->user->id));
		if ($totalDataPromo > 0){
			$this->actionAPromotion();
		}else{
			$this->render('index');
		}
	}	
	
	public function actionCreate(){
		$model=new Promo;
		
		$totalPromoKode = PromoKode::model()->countByAttributes(array('id_promo'=>$model->id));
		$dataPromoKode = PromoKode::model()->findByAttributes(array('id_promo'=>$model->id));
		if($dataPromoKode){
			$promo_code = $dataPromoKode->kode_promo;
		}else{
			$promo_code = '';
		}
		
		$dataUserSegmentation = UserSegmentation::model()->allSegment()->findAllByAttributes(array('id_user'=>Yii::app()->user->id,'is_custom'=>0));
		if (count($dataUserSegmentation) == 0){
			Yii::app()->user->setFlash('error', " Maaf, Anda belum mempunyai daftar segmentasi user Aplikasi, silahkan untuk membuat data segmenasti terlebih dahulu ");
			$this->redirect(array('index'));
		}
		
		$promoDetail = array();
		$promoImage = array();
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['Promo']))
			{
				
				/* echo '<pre>';
				print_r($_POST['Promo']);
				exit; */
				
				$transaction = Yii::app()->db->beginTransaction();
				try{
			
					
					$model->attributes=$_POST['Promo'];
					$model->id_company = Yii::app()->user->id;
					if ($model->promo_mode=='point'){
						$model->cash = 0;
					}else{
						$model->point = 0;
					}
					
					
					if($model->save()){						
					
						
						// save promo detail
						if (isset($_POST['user_segment'])){
							$this->saveDetailPromo($model->id);
						}
						
						// save promo kode
						for ($i = 1; $i <= $model->jumlah; $i++) {
							//echo $i,'<br>';
							$this->savePromoCode($model->id,$model->promo_code);
						}
						//exit;
						
						// upload images
						$fileObject=CUploadedFile::getInstances($model,'images');
						if($fileObject){
							/* echo '<pre>';
							print_r($fileObject);
							echo 'ada'; */
							
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							//echo 'ok';
							foreach ($fileObject as $key => $image){
								$timenow=date("YmdHis");
								$extension=$image->extensionName;
								$originalFileName = $judul =  str_replace(array(':', '\\', '/', '*','.',' '), '_', $image->name);
								$fileName = 'promo_image'.'_'.$timenow.'_'.$originalFileName;
								$newFileName = $fileName.'.'.$extension;
								//echo $newFileName.'<br>';
								
								$this->savePromoImage($model->id,$newFileName);
								
								$image->saveAs($this->uploadDir.$newFileName); 
								chmod($this->uploadDir.$newFileName, 0777);
							}
							
						}
						
						Yii::app()->user->setFlash('success', " Data Saved ");
						$this->redirect(array('aPromotion'));
					}
					
					$transaction->commit();
					$message ='Promo '.$model->title.' is waiting for approval, please check and do an action.';
					WA::sendNotificationToAdmin($message);
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('id_company','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
			'totalPromoKode'=>$totalPromoKode,
			'promo_code'=>$promo_code,
			'dataUserSegmentation'=>$dataUserSegmentation,
			'promoDetail'=>$promoDetail,
			'promoImage'=>$promoImage,
		));
	}
	
	
	public function actionUpdate($id){
		$model=$this->loadModel($id);
		
		$totalPromoKode = PromoKode::model()->countByAttributes(array('id_promo'=>$model->id));
		$dataPromoKode = PromoKode::model()->findByAttributes(array('id_promo'=>$model->id));
		if($dataPromoKode){
			$promo_code = $dataPromoKode->kode_promo;
		}else{
			$promo_code = '';
		}
		
		$dataUserSegmentation = UserSegmentation::model()->allSegment()->findAllByAttributes(array('id_user'=>Yii::app()->user->id,'is_custom'=>0));
		
		$promoDetail = PromoDetail::model()->findAllByAttributes(array('id_promo'=>$model->id));
		$promoImage = PromoImage::model()->findAllByAttributes(array('id_promo'=>$model->id));
		
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['Promo']))
			{
				
				/* echo '<pre>';
				print_r($_POST['Promo']);
				exit; */
				
				$transaction = Yii::app()->db->beginTransaction();
				try{
			
					
					$model->attributes=$_POST['Promo'];
					$model->id_company = Yii::app()->user->id;
					if ($model->promo_mode=='point'){
						$model->cash = 0;
					}else{
						$model->point = 0;
					}
					
					
					if($model->save()){						
					
					
						//kalau update biar gampang di delete dulu data detail yang sebelumnya, lalu di replace sama yang baru
						$this->deleteDetailPromo($model->id);
						// save promo detail
						if (isset($_POST['user_segment'])){
							$this->saveDetailPromo($model->id);
						}
						
						
						// kalo update di delete dulu yang sebelumnya 
						$this->deletePromoCode($model->id);
						
						// save promo kode
						for ($i = 1; $i <= $model->jumlah; $i++) {
							//echo $i,'<br>';
							$this->savePromoCode($model->id,$model->promo_code);
						}
						//exit;
						
						// upload images
						$fileObject=CUploadedFile::getInstances($model,'images');
						if($fileObject){
							/* echo '<pre>';
							print_r($fileObject);
							echo 'ada'; */
							
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							//echo 'ok';
							foreach ($fileObject as $key => $image){
								$timenow=date("YmdHis");
								$extension=$image->extensionName;
								$originalFileName = $judul =  str_replace(array(':', '\\', '/', '*','.',' '), '_', $image->name);
								$fileName = 'promo_image'.'_'.$timenow.'_'.$originalFileName;
								$newFileName = $fileName.'.'.$extension;
								//echo $newFileName.'<br>';
								
								$this->savePromoImage($model->id,$newFileName);
								
								$image->saveAs($this->uploadDir.$newFileName); 
								chmod($this->uploadDir.$newFileName, 0777);
							}
							
						}
						
						Yii::app()->user->setFlash('success', " Data Saved ");
						$this->redirect(array('aPromotion'));
					}
					
					$transaction->commit();
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('id_company','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
			'totalPromoKode'=>$totalPromoKode,
			'promo_code'=>$promo_code,
			'dataUserSegmentation'=>$dataUserSegmentation,
			'promoDetail'=>$promoDetail,
			'promoImage'=>$promoImage,
		));
	}
	
	public function savePromoCode($id_promo,$kode_promo){
			$PromoKode = new PromoKode;
			$PromoKode->id_promo = $id_promo;
			$PromoKode->kode_promo = $kode_promo;
			$PromoKode->save();
	}
	
	public function deletePromoCode($id_promo){
		PromoKode::model()->deleteAllByAttributes(array(
			'id_promo'=>$id_promo,
		));
	}
	
	public function savePromoImage($id_promo,$image){
		
			$imageData = new PromoImage;
			$imageData->id_promo = $id_promo;
			$imageData->image = 'images/promo_photo/'.$image;
			$imageData->save();
	}
	
	public function actionDeleteImage(){
		
		$key = $_POST['key'];
		$promoImage = PromoImage::model()->findByPk($key);
		$addPath = '../app/';
		$file = $_SERVER['DOCUMENT_ROOT'].''.Yii::app()->baseUrl.'/'.$addPath.$promoImage->image;
		//echo $file;
		//exit;
		if ($promoImage){
			if (file_exists($file)){
				if ( unlink($file) ){
					$promoImage->delete();
					//echo "success";
				}else{
					//echo "fail";
					throw new CHttpException(500,'Kesalahan Terjadi. gagal delete');
				} 
			}else{
				throw new CHttpException(500,'Kesalahan Terjadi. file tidak ada');
			}
		}else{
			throw new CHttpException(500,'Kesalahan Terjadi. data tidak ada');
		}
		
		
		echo json_encode(array('status'=>'success'));
	}	

	
	public function saveDetailPromo($id_promo){
		$dataSegmentSelected = $_POST['user_segment'];
		foreach($dataSegmentSelected as $key => $value){
			$promoDetail = new PromoDetail;
			$promoDetail->id_promo = $id_promo;
			$promoDetail->id_user_segmentation = $value;
			$promoDetail->created_date = date("Y-m-d H:i:s");
			$promoDetail->status = 1;
			$promoDetail->save();
		}
	}
	
	public function deleteDetailPromo($id_promo){
		PromoDetail::model()->deleteAllByAttributes(array(
			'id_promo'=>$id_promo,
		));
	}
	
	public function actionAPromotion(){
		
		$year = date('Y');
		$month = date ('m');
		
		$model=new Promo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Promo']))
			$model->attributes=$_GET['Promo'];
		
		$model->id_company=Yii::app()->user->id;
		$dataProvider=$model->search();
		
		
		// jika ada filter
		if (isset($_POST['Grafik'])){
			
			/* echo '<pre>';
			print_r($_POST['Grafik']);
			exit; */
			
			$result = array();
	
	
			$year = $_POST['Grafik']['year'];
			$month =$_POST['Grafik']['month'];
			$start_date = $_POST['Grafik']['start_date'];
			$end_date = $_POST['Grafik']['end_date'];
			$start_date_format = $year.'-'.$month.'-'.$start_date;
			$end_date_format = $year.'-'.$month.'-'.$end_date;
			
		
		
			// views Data
			$overviewViewsDataByDate = $model->overviewViewsDataByDate($year,$month,$start_date_format,$end_date_format);
			$arrayoverviewViewsDataByDateLabel = array();
			$arrayoverviewViewsDataByDateValue = array();
			foreach ($overviewViewsDataByDate as $key => $data){
				$arrayoverviewViewsDataByDateLabel[]=substr($key,-2);
			}
			//print_r($arrayoverviewViewsDataByDateLabel);
			foreach ($overviewViewsDataByDate as $key => $data){
				$arrayoverviewViewsDataByDateValue[]=$data;
			}
			//print_r($arrayoverviewViewsDataByDateValue);
			$labelsViewsDataByDate = $arrayoverviewViewsDataByDateLabel;
			//echo $labelsViewsDataByDate;
			$valuesViewsDataByDate = $arrayoverviewViewsDataByDateValue;
			$result['viewsData']['label']=$labelsViewsDataByDate;
			$result['viewsData']['value']=$valuesViewsDataByDate;
			// end views Data
			
			//-- clicks data//
			$overviewClicksDataByDate = $model->overviewClicksDataByDate($year,$month,$start_date_format,$end_date_format);
			$arrayoverviewClicksDataByDateLabel = array();
			$arrayoverviewClicksDataByDateValue = array();
			foreach ($overviewClicksDataByDate as $key => $data){
				$arrayoverviewClicksDataByDateLabel[]=substr($key,-2);
			}
			//print_r($arrayoverviewClicksDataByDateLabel);
			foreach ($overviewClicksDataByDate as $key => $data){
				$arrayoverviewClicksDataByDateValue[]=$data;
			}
			//print_r($arrayoverviewClicksDataByDateValue);
			$labelsClicksDataByDate = $arrayoverviewClicksDataByDateLabel;
			//echo $labelsViewsDataByDate;
			$valuesClicksDataByDate = $arrayoverviewClicksDataByDateValue;
			$result['clicksData']['label']=$labelsClicksDataByDate;
			$result['clicksData']['value']=$valuesClicksDataByDate;
			// end views Data
			
			//-- buyer data//
			$overviewBuyerDataByDate = $model->overviewBuyerDataByDate($year,$month,$start_date_format,$end_date_format);
			$arrayoverviewBuyerDataByDateLabel = array();
			$arrayoverviewBuyerDataByDateValue = array();
			foreach ($overviewBuyerDataByDate as $key => $data){
				$arrayoverviewBuyerDataByDateLabel[]=substr($key,-2);
			}
			//print_r($arrayoverviewBuyerDataByDateLabel);
			foreach ($overviewBuyerDataByDate as $key => $data){
				$arrayoverviewBuyerDataByDateValue[]=$data;
			}
			//print_r($arrayoverviewBuyerDataByDateValue);
			$labelsBuyerDataByDate = $arrayoverviewBuyerDataByDateLabel;
			//echo $labelsBuyerDataByDate;
			$valuesBuyerDataByDate = $arrayoverviewBuyerDataByDateValue;
			$result['buyerData']['label']=$labelsBuyerDataByDate;
			$result['buyerData']['value']=$valuesBuyerDataByDate;
			// end buyer Data
			
			
			
			echo CJSON::encode($result);
			exit;
			
			
		}else{
			$start_date = $year.'-'.$month.'-'.'01';
			$end_date = $year.'-'.$month.date("-t");
		}
		
		// end jika ada filter
		
		
		// -- grafik --//
		// total views
		$totalViewsData = $model->countTotalViewsData($year);
		//-- overview total views --//
		$overviewViewsData = $model->overviewViewsData($year);
		//-- overview total views // per tanggal --//
		$overviewViewsDataByDate = $model->overviewViewsDataByDate($year,$month);
		
		
		// total clicks
		$totalClicksData = $model->countTotalClicksData($year);
		//-- overview total clicks --//
		$overviewClicksData = $model->overviewClicksData($year);
		//-- overview total clicks // per tanggal --//
		$overviewClicksDataByDate = $model->overviewClicksDataByDate($year,$month);
		
		// total buyer
		$totalBuyerData = $model->countTotalBuyerData($year);
		//-- overview total Buyer --//
		$overviewBuyerData = $model->overviewBuyerData($year);
		//-- overview total Buyer // per tanggal --//
		$overviewBuyerDataByDate = $model->overviewBuyerDataByDate($year,$month);
	
		

		$this->render('apromotion',array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
			'year'=>$year,
			'month'=>$month,
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			
			'totalViewsData'=>$totalViewsData,
			'overviewViewsData'=>$overviewViewsData,
			'overviewViewsDataByDate'=>$overviewViewsDataByDate,
			
			'totalClicksData'=>$totalClicksData,
			'overviewClicksData'=>$overviewClicksData,
			'overviewClicksDataByDate'=>$overviewClicksDataByDate,
			
			'totalBuyerData'=>$totalBuyerData,
			'overviewBuyerData'=>$overviewBuyerData,
			'overviewBuyerDataByDate'=>$overviewBuyerDataByDate,
		));
		
	}
	
	public function actionGetDataTest(){
		$array = array();
		$array[]='ada';
		$array[]='ga ada';
		$array[]='aha';
		
		echo CJSON::encode($array);
	}
	
	
}