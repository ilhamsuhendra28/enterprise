<?php

class ClusteringCustomController extends Controller
{
	public $layout='../layouts/mainadmin';
	public $uploadDir = 'upload/files/excel/user_segmentation/';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create', 'update', 'getAndSaveDataSegmentationDetail', 'uOverview', 'viewUserSegmentation','delete','deleteDetail','updateDetail','createDetail'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function loadModelUserSegmentation($id)
	{
		$model=UserSegmentation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function loadModel($id)
	{
		$model=CustomSegmentation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionViewUserSegmentation($id){
		$model = $this->loadModelUserSegmentation($id);
		
		if ($model->is_custom == 1){
			$criteria=new CDbCriteria;
			$criteria->compare('id_custom_segmentation',$model->id);
			$dataProvider=new CActiveDataProvider('CustomSegmentationDetail', array(
				'criteria'=>$criteria,
				'sort'=>array(
					'defaultOrder'=>'created_date DESC',
				),
				'pagination'=>array(
					'pageSize'=>10,
				)
			));

		
			$this->render('viewUserSegmentation',array(
				'model'=>$model,
				'dataProvider'=>$dataProvider,
			));
		}else{
			throw new CHttpException(404,'The requested page does not exist (for user segement custom only)');
		}
		//var_dump($model);
	}
	
	public function actionIndex(){
        
		$totalDataClusteringCustom = CustomSegmentation::model()->countByAttributes(array('id_user'=>Yii::app()->user->id,'is_custom'=>1));
		/* echo $totalDataClusteringCustom;
		exit; */
		if ($totalDataClusteringCustom > 0){
			$this->actionUOverview();
		}else{
			$this->render('index');
		}
	}	
	
	public function actionCreate(){
		$model=new CustomSegmentation;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['CustomSegmentation']))
			{
				
				$transaction = Yii::app()->db->beginTransaction();
				
				try{
			
					$model->attributes=$_POST['CustomSegmentation'];
					$model->id_user = Yii::app()->user->id;
					$model->created_date = date("Y-m-d H:i:s");
					$model->is_custom=1;
					$model->status=1;
					
					
					if($model->save()){
						
						if(strlen(trim(CUploadedFile::getInstance($model,'file'))) > 0){
							$timenow=date("YmdHis");
							
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							$fileObject=CUploadedFile::getInstance($model,'file');
							$extension=$fileObject->extensionName;
							$fileName = 'file'.'_'.$timenow;
							$newFileName = $fileName.'.'.$extension;
							$fileObject->saveAs($this->uploadDir.$newFileName); 
							chmod($this->uploadDir.$newFileName, 0777);
							
							// disini proses save data detail dari file excel nya
							$this->actionGetAndSaveDataSegmentationDetail($model->id,$this->uploadDir,$extension,$fileName);
							
							
							// nanti setelah selesai di unlink (hapus)
							if (file_exists($this->uploadDir.$newFileName)){
								if ( unlink($this->uploadDir.$newFileName) ){
									//echo "success";
								}else{
									//echo "fail";
								}
							}
						}
						
						Yii::app()->user->setFlash('success', " Data Saved ");
						$this->redirect(array('uOverview'));
					}
					
					$transaction->commit();
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('file','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
		));
	}	
	
	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['CustomSegmentation']))
			{
				
				$transaction = Yii::app()->db->beginTransaction();
				
				try{
			
					$model->attributes=$_POST['CustomSegmentation'];
					//$model->id_user = Yii::app()->user->id;
					$model->updated_date = date("Y-m-d H:i:s");
					//$model->is_custom=1;
					//$model->status=1;
					
					
					if($model->save()){
						
						if(strlen(trim(CUploadedFile::getInstance($model,'file'))) > 0){
							$timenow=date("YmdHis");
							
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							$fileObject=CUploadedFile::getInstance($model,'file');
							$extension=$fileObject->extensionName;
							$fileName = 'file'.'_'.$timenow;
							$newFileName = $fileName.'.'.$extension;
							$fileObject->saveAs($this->uploadDir.$newFileName); 
							chmod($this->uploadDir.$newFileName, 0777);
							
							
							// di hapus dulu data sebelumnya 
							$this->deleteDetailSegementationCustom($model->id);
							
							// disini proses save data detail dari file excel nya
							$this->actionGetAndSaveDataSegmentationDetail($model->id,$this->uploadDir,$extension,$fileName);
							
							
							// nanti setelah selesai di unlink (hapus)
							if (file_exists($this->uploadDir.$newFileName)){
								if ( unlink($this->uploadDir.$newFileName) ){
									//echo "success";
								}else{
									//echo "fail";
								}
							}
						}
						
						Yii::app()->user->setFlash('success', " Data Saved ");
						$this->redirect(array('uOverview'));
					}
					
					$transaction->commit();
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('file','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
		));
	}	
	
	public function actionGetAndSaveDataSegmentationDetail($id_custom_segmentation,$uploadDir,$ext,$fileName){
		
			ini_set('max_execution_time', 0);
			Yii::import('application.extensions.PHPExcel.PHPExcel',true);
			
			$fileName = $fileName.'.'.$ext;
			$pathToFile = $uploadDir.$fileName;
			$field = array(
				'nama',
				'no_hp',
				'email',
			);
			
			
			//load data  excel 
			if($ext=='xls'){
				$objReader = PHPExcel_IOFactory::createReader('Excel5'); // untuk tipe file .xls
			}else{
				$objReader = PHPExcel_IOFactory::createReader('Excel2007'); // untuk tipe file .xlsx
			}

			$objPHPExcelUpdate = $objReader->load($pathToFile); 
			$objWorksheetUpdate = $objPHPExcelUpdate->getActiveSheet();
			//echo $objWorksheetUpdate->getCell('A2')->getValue().'<br>';
			
			// set propertis untuk read data / menampilkan data
			$highestRow = $objWorksheetUpdate->getHighestRow(); // e.g. 10
			$highestColumn = $objWorksheetUpdate->getHighestColumn(); // e.g 'F'
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
			
			
			//echo '<table>' . "\n";
				for ($row = 2; $row <= $highestRow; ++$row) { // $row = 2 karena judul ga di inputkan ke database
				  //echo '<tr>' . "\n";
					
					  $data = new CustomSegmentationDetail;
					  $data->id_custom_segmentation = $id_custom_segmentation ;
					  $data->created_date = date("Y-m-d H:i:s");
					  $data->status=1;
					  for ($col = 0; $col <= $highestColumnIndex; ++$col) {
						$coloumnAsIndex = $col;
						$coloumnAsString = PHPExcel_Cell::stringFromColumnIndex($coloumnAsIndex);
						$coloumnAndRowAsIndex= $coloumnAsIndex.$row;
						$coloumnAndRowAsString= $coloumnAsString.$row;
						$value = $objWorksheetUpdate->getCell($coloumnAndRowAsString)->getValue();
						//echo '<td>' . $value. '</td>' . "\n";
						if ($field[$col]){
							$fieldName = $field[$col];
							$data->$fieldName=$value;
						}
						
					  } 
					  if ($data->no_hp == '' && $data->email ==''){
						  // do nothing
					  }else{
						$data->save(false) ;
					  }

				  //echo '</tr>' . "\n";
				}
			//echo '</table>' . "\n"; 
		
		
		
	}
	
	public function deleteDetailSegementationCustom($id_custom_segmentation){
		CustomSegmentationDetail::model()->deleteAllByAttributes(array(
			'id_custom_segmentation'=>$id_custom_segmentation,
		));
	}
	
	public function actionUOverview(){
		
		$model=new CustomSegmentation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CustomSegmentation']))
			$model->attributes=$_GET['CustomSegmentation'];
		
		$model->id_user=Yii::app()->user->id;
		$model->is_custom=1;
		$dataProvider=$model->search();

		$this->render('uoverview',array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
		));
		
	}	
	
	public function actionDelete($id){
//      var_dump($id);exit;
        $segmen = UserSegmentation::model()->findByPk($id);
        $segmen->status = 2;
        $segmen->save();
		Yii::app()->user->setFlash('success', " Data Deleted ");
        $this->redirect(['clusteringCustom/index']);
    }
	
	public function actionDeleteDetail($id)
	{
		$UserSegmentationDetail = CustomSegmentationDetail::model()->findByPk($id);
		$UserSegmentationDetail->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionUpdateDetail($id)
	{
		$model=CustomSegmentationDetail::model()->findByPk($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CustomSegmentationDetail']))
		{
			$model->attributes=$_POST['CustomSegmentationDetail'];
			if($model->save()){
				Yii::app()->user->setFlash('success', " Data Updated ");
				$this->redirect(array('viewUserSegmentation','id'=>$model->id_custom_segmentation));
			}
		}

		$this->render('updateDetail',array(
			'model'=>$model,
		));
	}
	
	public function actioncreateDetail($id_segmentation)
	{
		$model= new CustomSegmentationDetail;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CustomSegmentationDetail']))
		{
			$model->attributes=$_POST['CustomSegmentationDetail'];
			$model->id_custom_segmentation = $id_segmentation ;
			$model->created_date = date("Y-m-d H:i:s");
			$model->status=1;
			if($model->save()){
				Yii::app()->user->setFlash('success', " Data Saved ");
				$this->redirect(array('viewUserSegmentation','id'=>$model->id_custom_segmentation));
			}
		}

		$this->render('updateDetail',array(
			'model'=>$model,
		));
	}
 
}