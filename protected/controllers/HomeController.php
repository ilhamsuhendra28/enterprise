<?php

class HomeController extends Controller
{
	public $layout='../layouts/mainadmin';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','profile'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex(){
		
		$this->redirect(array('clustering/index'));	
	}	
	
	
	public function actionProfile(){
		
		$userEnterprise = UserEnterprise::model()->findByPk(Yii::app()->user->id);
		
		
		// email account
		$email = UserEnterpriseEmail::model()->findByAttributes(array('id_user'=>Yii::app()->user->id));
		if(!$email) {
			$email=new UserEnterpriseEmail;
		}
		// default value email account
		if ($email->email ==''){
			$email->email = $userEnterprise->email;
		}
		
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['UserEnterpriseEmail']))
			{
				
				$transaction = Yii::app()->db->beginTransaction();
				try{
			
					
					$email->attributes=$_POST['UserEnterpriseEmail'];
					$email->id_user = Yii::app()->user->id;
					if ($email->isNewRecord){
						$email->created_date_time = date("Y-m-d H:i:s");
					}else{
						$email->updated_date_time = date("Y-m-d H:i:s");
					}
					
					
					if($email->validate()){						
					
						$email->save();
						Yii::app()->user->setFlash('success', " Data Saved ");
						$this->redirect(array('profile'));
					}
					
					$transaction->commit();
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('id_user','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('profile',array(
			'email'=>$email,
		));
	}
	
}
?>