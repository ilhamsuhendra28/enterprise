<?php

class BrandController extends Controller
{
	public $layout='../layouts/mainadmin';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'compare', 'eliminate'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex(){
		$tahun = Logic::getQueryTahun();
		$kategori = Logic::getQueryKategori();
		
		if(!empty($_POST)){
			$tampilbrand = Logic::getQueryTampilBrand($_POST['kategori'], $_POST['brand']);
			$ptahun = $_POST['tahun'];
			$pbulan = $_POST['bulan'];
			$pkategori = $_POST['kategori'];
			$pbrand = $tampilbrand;
			
			$tampilgender = Logic::getQueryGender($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilmarried = Logic::getQueryMarried($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilage = Logic::getQueryAge($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilreligion = Logic::getQueryReligion($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilsalary = Logic::getQuerySalary($ptahun, $pbulan, $pkategori, $pbrand);
			$show = true;
		}else{
			$ptahun = date('Y');
			$pbulan = date('m');
			// foreach($kategori as $kdx=>$krow){
			// 	if($kdx == 0){
			// 		$pkategori = $krow['kategori'];
			// 		$pbrand = $krow['brand'];
			// 	}
			// }	
			$pbrand = '';
			$tampilgender = Logic::getQueryGender($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilmarried = Logic::getQueryMarried($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilage = Logic::getQueryAge($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilreligion = Logic::getQueryReligion($ptahun, $pbulan, $pkategori, $pbrand);
			$tampilsalary = Logic::getQuerySalary($ptahun, $pbulan, $pkategori, $pbrand);
			$show = false;
		}
		
		$this->render('index', array(
			'tahun'=>$tahun,
			'kategori'=>$kategori,
			'tampilgender'=>$tampilgender,
			'tampilmarried'=>$tampilmarried,
			'tampilage'=>$tampilage,
			'tampilreligion'=>$tampilreligion,
			'tampilsalary'=>$tampilsalary,
			'ptahun'=>$ptahun,
			'pbulan'=>$pbulan,
			'pkategori'=>$pkategori,
			'pbrand'=>$pbrand,
			'show'=>$show
		));
	}	
	
	public function actionCompare(){
		$valbrand = Yii::app()->request->getParam('valbrand', NULL);
		$kategori = Yii::app()->request->getParam('kategori', NULL);
        $arrData = array();
        $i = 0;
        $param = '%' . ucwords($valbrand) . '%';

        $sql = "SELECT a.brand, a.kategori FROM(
			SELECT a.nama_barang brand, b.kategori_barang kategori 
			FROM receipt_retail_barang a
			JOIN kategori_barang b ON b.id_kategori_barang = a.id_kategori_barang
			UNION ALL
			SELECT a.nama_maskapai brand, c.upload_type kategori FROM receipt_pk_pesawat a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			UNION ALL
			SELECT a.nama_ka brand, c.upload_type FROM receipt_pk_ka a
			JOIN receipt_pk b ON b.id_pk = a.id_receipt_pk
			JOIN upload_type c ON c.id = b.id_upload_type
			) a
		WHERE a.brand LIKE :param1 AND a.kategori = :param2
		GROUP BY a.brand
		ORDER BY a.kategori ASC";
        $query = Yii::app()->db->createCommand($sql);
        $query->bindParam(':param1', $param);
        $query->bindParam(':param2', $kategori);
        $cmd = $query->queryAll();
		
        foreach ($cmd as $value) {
            $arrData[$i]['nama_brand']=$value['brand'];
            $i++;
        }
       
        $arrData = array_map("unserialize", array_unique(array_map("serialize", $arrData)));
        echo json_encode($arrData);
	}	
	
	public function actionEliminate(){
		$brand = $_POST['brand'];
		$pbrand = $_POST['pbrand'];
		
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$exp = explode(',', urldecode($pbrand));
				if($exp != ''){
				foreach($exp as $erow){
					if($erow != urldecode($brand)){
						$brands[] = $erow;
					}
				}
			
				$data['hasil'] = implode(',', $brands);
			}else{
				$data['hasil'] = '';
			}	
			echo urldecode(CJSON::encode($data));
			Yii::app()->end();
		}	
	}	
	
}
?>