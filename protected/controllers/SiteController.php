<?php

class SiteController extends Controller
{
	public $layout='../layouts/mainlogin';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('verifikasi', 'logout','cekotp', 'resendotp', 'verifikasiemail'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('login', 'signup', 'konfirmasi', 'forgot', 'forgotsend', 'changepassword','error'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	public function actionError()
	{
		$this->layout = "blank";
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function getToken(){
		$data = array(
			"grant_type" => "client_credentials",
		);
		
		$token = curl_init(); 
		curl_setopt($token, CURLOPT_URL, "https://api.mainapi.net/token"); 
		curl_setopt($token, CURLOPT_POST, 1 ); 
		curl_setopt($token, CURLOPT_POSTFIELDS, http_build_query($data)); 
		curl_setopt($token, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($token, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($token, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($token, CURLOPT_HTTPHEADER, array('Authorization: Basic dFJEbmhxMk9WOThmcmZSVlA0TmZ2YXZaVWVZYTpJa2JZX2NBeEMwWUU4Q1Jsd2NVNjNRRTBfZk1h','Content-Type: application/x-www-form-urlencoded'));
		$postToken = curl_exec($token); 
		curl_close($token);
		$responseToken = json_decode($postToken, true);
		$token = $responseToken['token_type'].' '.$responseToken['access_token']; 
		return $token;
	}	
	
	public function sendOtp($token, $no_hp, $id_user){
		$notp = Logic::setOtp();
		
		$wording = OtpWording::model()->find();
		$isi = str_replace('{otp}', $notp, $wording->wording_otp);
		$data = array(
			"msisdn" => $no_hp,
			"content" => $isi,
		);
		
		$otp = curl_init(); 
		curl_setopt($otp, CURLOPT_URL, "https://api.mainapi.net/smsnotification/1.0.0/messages"); 
		curl_setopt($otp, CURLOPT_POST, 1 ); 
		curl_setopt($otp, CURLOPT_POSTFIELDS, http_build_query($data)); 
		curl_setopt($otp, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($otp, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($otp, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($otp, CURLOPT_HTTPHEADER, array('Authorization: '.$token.'','Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));
		// curl_setopt($otp, CURLOPT_HTTPHEADER, array('Authorization: Bearer f70cbe312db1af949dad79e89a220453','Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));
		$postToken = curl_exec($otp); 
		curl_close($otp);
		$responseToken = json_decode($postToken, true);
		
		$userotp = OtpUser::model()->findByAttributes(array('id_user'=>$id_user));
		if(empty($userotp)) $userotp = new OtpUser;
		$userotp->id_user = $id_user;
		$userotp->otp = $notp;
		$userotp->durasi_otp = 10;
		$userotp->created_date = date('Y-m-d H:i:s');
		$userotp->otp_active = 1;
		if($userotp->save()){
			return true;
		}
	}	
	
	public function actionLogin()
	{
		$id = Yii::app()->user->isGuest;
		$cekotp = CekOtp::model()->find();
		if($cekotp->is_otp == 1){
			$otp = OtpUser::model()->findByAttributes(array('id_user'=>Yii::app()->user->id));
			
			if($otp->otp_active == 1 && !$id){
				$user = UserEnterprise::model()->findByPk(Yii::app()->user->id);
				
				$this->sendOtp($this->getToken(), $user->no_hp, $user->id);
				$this->redirect(array('site/verifikasi', array('id'=>'verifikasiotp', 'login'=>'belumlogin')));
				
			}else if($otp->otp_active == 2 && !$id){
				$this->redirect(array('home/index'));
			}	
		}else if($cekotp->is_otp == 2 && !$id){
			$this->redirect(array('home/index'));
		}	
			
		$model=new LoginForm;

		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			if(isset($_POST['LoginForm']))
			{
				$model->attributes=$_POST['LoginForm'];
				
				if($model->validate() && $model->login()){
					$data['hasil'] = 'success';
					$cekotp = CekOtp::model()->find();
					if(!empty($cekotp)){
						if($cekotp->is_otp == 1){
							$this->sendOtp($this->getToken(), $model->username, Yii::app()->user->id);
							$data['url'] = Yii::app()->createAbsoluteUrl('site/verifikasi', array('id'=>'verifikasiotp'));
						}else{
							$sub =explode('/', Yii::app()->user->returnUrl);
							if($sub[2] == ''){
								$data['url'] = Yii::app()->createAbsoluteUrl('home/index');
							}else{	
								$data['url'] = Yii::app()->user->returnUrl;
							}
						}	
					}
				}else{
					$data['hasil'] = 'gagal';
					$data['showerror'] = 'Username atau Password Salah';
				}
			}
			echo CJSON::encode($data);
			Yii::app()->end();
		}
		
		$this->render('login', array(
			'model'=>$model
		));
		
	}
	
	public function actionSignup(){
		$model=new UserEnterprise;

		if(isset($_POST['ajax']) && $_POST['ajax']==='signup-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		$data = array();
		
		if(Yii::app()->request->isAjaxRequest){
			$email = UserEnterprise::model()->findAll();
			if(!empty($email)){
				foreach($email as $erow){
					$cekemail[] = $erow->email;
				}	
			}else{
				$cekemail[] = 'none';
			}	
			if($_POST['agree'] == 'on'){
				if(in_array($_POST['UserEnterprise']['email'], $cekemail) == false){
					if(isset($_POST['UserEnterprise'])){
						$model=new UserEnterprise;
						$model->attributes = $_POST['UserEnterprise'];
						$password = Logic::getEncrypt($_POST['UserEnterprise']['new_password']);
						$model->password = $password['text'];
						$model->active = 2;		
						$model->created_date = date('Y-m-d H:i:s');
						if($model->save()){
							$data['hasil'] = 'success';
						}
					}	
				}else{
					$data['hasil'] = 'emailexist';
					$data['terms'] = 'Email already exist';
				}	
			}else{
				$data['hasil'] = 'disagree';
				$data['terms'] = 'Anda belum menyetejui persyaratan dan privacy data';	
			}	
			
			echo CJSON::encode($data);
			Yii::app()->end();
		}
		
		$this->render('signup', array(
			'model'=>$model
		));	
	}	
	
	public function actionVerifikasi($id, $login=null){
		$id = Yii::app()->user->isGuest;
		$cekotp = CekOtp::model()->find();
		if($cekotp->is_otp == 1){
			$otp = OtpUser::model()->findByAttributes(array('id_user'=>Yii::app()->user->id));
			
			if($otp->otp_active == 2 && !$id){
				$this->redirect(array('home/index'));
			}	
		}else if($cekotp->is_otp == 2 && !$id){
			$this->redirect(array('home/index'));
		}
		
		$user = UserEnterprise::model()->findByPk(Yii::app()->user->id);
		$otp = OtpUser::model()->findByAttributes(array('id_user'=>$user->id));
		
		$this->render('verifikasi', array(
			'user'=>$user,
			'otp'=>$otp,
			'login'=>$login
		));
	}		
	
	public function actionCekOtp($id, $time){
		$data = array();
		
		if(Yii::app()->request->isAjaxRequest){
			if($time > 0){
				$otpuser = OtpUser::model()->findByAttributes(array('id_user'=>$id));
				
				if($_POST['otp'] != ''){
					if($_POST['otp'] == $otpuser->otp){
						$otpuser->otp_active = 2;
						$otpuser->save();
						$data['hasil'] = 'success';
					}else{
						$data['hasil'] = 'invalid';
						$data['terms'] = 'Kode verifikasi yang anda masukkan tidak benar atau salah';
					}
				}else{
					$data['hasil'] = 'invalid';
					$data['terms'] = 'Anda belum memasukkan kode verifikasi';
				}
			}else{
				$data['hasil'] = 'invalid';
				$data['terms'] = 'Waktu telah habis, kode verifikasi telah expired silahkan ajukan pengiriman ulang kode verifiasi terbaru';
			}	
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}	
	
	public function actionResendOtp($id){
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$user = UserEnterprise::model()->findByPk($id);
			if($this->sendOtp($this->getToken(), $user->no_hp, $user->id) == true){
				$data['hasil'] = 'success';
			}		
			
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}	
	
	public function actionVerifikasiEmail($id, $random){
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$user = UserEnterprise::model()->findByPk($id);
			
			if(Logic::notifRegister($user->email) == 'success'){
				$data['hasil'] = 'success';
			}else{
				$data['hasil'] = 'maaf sistem sedang sibuk, silahkan hubungi contact center';
			}	
			
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}	
	
	public function actionKonfirmasi($params, $random = null){
		$model = UserManual::model()->findByAttributes(array('email'=>$params));
		$model->active = '1';
		$model->save();
		$this->render('konfirmasi');
	}
	
	public function actionForgot(){
		$forgot = new UserEnterprise;
		
		$this->render('forgot',array(
			'forgot'=>$forgot
		));
	}	
	
	public function actionForgotSend(){
		$data = array();
		
		$email = UserEnterprise::model()->findAll();
		foreach($email as $erow){
			$cekemail[] = $erow->email;
		}	
		
		if(Yii::app()->request->isAjaxRequest){
			if(in_array($_POST['UserEnterprise']['email'], $cekemail) == false){
				$data['hasil'] = 'emailexist';
				$data['terms'] = 'Email not found';
			}else{
				if(Logic::sendPassword($_POST['UserEnterprise']['email']) == 'success'){
					$data['hasil'] = 'success';
				}else{
					$data['terms'] = 'maaf sistem sedang sibuk, silahkan hubungi contact center';
				}	
			}
			
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}
	
	public function actionChangePassword($params, $random = null){
		$change = UserEnterprise::model()->findByAttributes(array('email'=>$params));
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($change);
			Yii::app()->end();
		}
		
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$change->attributes = $_POST['UserEnterprise'];
			$change->email = $params;
			$password = Logic::getEncrypt($_POST['UserEnterprise']['new_password']);
			$change->password = $password['text'];
			if($change->save()){
				$data['hasil'] = 'success';
			}	
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
		$this->render('changepassword',array(
			'change'=>$change,
			'params'=>$params
		));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}