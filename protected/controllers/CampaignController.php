<?php

class CampaignController extends Controller
{
	public $layout='../layouts/mainadmin';
	public $uploadDir = 'upload/images/campaign/';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'view', 'create', 'update', 'deleteImage', 'cOverview','viewUserSegmentation','delete','detailPaket','buyPaket','historyPaket','historyPaketDetail'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('cartcreate','clearcart','viewcart','deletecart','updatecart','buyPaketInCart','paidBuyPaket','traceSent','viewcontent','getCampaignTemplate','contentEmail','saveContentEmail','getContentEmail','redirectSavedContentEmail','changeTemplate','sendCampaign','holdCampaign','countDataReceiver','replicate','claimPaket'),
				'users'=>array('@'),
			),
			
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function loadModel($id)
	{
		$model=Campaign::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function loadModelUserSegmentation($id)
	{
		$model=UserSegmentation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionIndex($type='email'){
		
		$field_type = $type.'_blast';
		$totalDataCampaign = Campaign::model()->countByAttributes(array('id_user'=>Yii::app()->user->id,$field_type=>1));
		if ($totalDataCampaign > 0){
			$this->actionCOverview($type);
		}else{
			$this->render('index',array(
					'type'=>$type,
				)
			);
		}
	}	
	
	
	public function actionViewcontent($id){
		
		$model = $this->loadModel($id);
		echo $model->description;
	}
	
	public function actionGetCampaignTemplate($id){
		
		$data = array();
		$campaign_id = $id;
		$campaignDefault = KirimEmailMailchimp::getCampaignContentById($campaign_id);
		if ($createTemplate['status']=='error' || (isset($createTemplate['data']['type']) && $createTemplate['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){ // jika error // jika error
			$htmlCampaignDefault ='';
			$data['status']='FAILED';
			$data['message'] ='Kesalahan memuat Template';
		}else{
			$htmlCampaignDefault = $campaignDefault['data']['archive_html'];
			$htmlCampaignDefault = str_replace("<!doctype html>","",$htmlCampaignDefault);
			$htmlCampaignDefault = str_replace("View this email in your browser","",$htmlCampaignDefault);
			
			$data['status']='SUCCESS';
			$data['message'] ='Suskes memuat Template';
			$data['text'] =$htmlCampaignDefault;
		}
		//print_r($htmlCampaignDefault);
		//echo $htmlCampaignDefault;
		echo json_encode($data);
	}
	
	
	public function actionContentEmail($id,$type='email',$idTemplate=''){
		
		$model = $this->loadModel($id);

		$this->render('contentEmail',array(
			'model'=>$model,
			'type'=>$type,
			'idTemplate'=>$idTemplate,
		
		));
	}	
	
	public function actionSaveContentEmail($id,$type='email'){
		
		$model = $this->loadModel($id);
		$json = $_POST['json'];
		$template = $_POST['template'];
		$idTemplate = $_POST['idTemplate'];
		//print_r($_POST);
		$model->campaign_template_id = $idTemplate;
		$model->description = $template;
		$model->campaign_template_json = $json;
		if ($model->save()){
			echo json_encode(array('status'=>'SUCCESS','message'=>'Success save Content'));
		}else{
			$message = $model->getErrors();
			echo json_encode(array('status'=>'FAILED','message'=>$message));
		}

		
	}	
	
	public function actionGetContentEmail($id,$type='email',$idTemplate=''){
		
		$model = $this->loadModel($id);
		
		if ($idTemplate == ''){
			if ($model->campaign_template_json != ''){
				echo $model->campaign_template_json ;
			}else{
				$CampaignTemplate = CampaignTemplate::model()->findByAttributes(array('campaign_template_name'=>'Default','campaign_template_group'=>'bee'));
				if ($CampaignTemplate){
					echo $CampaignTemplate->campaign_template_json;
				}else{
					throw new CHttpException(500,'Default Content Tidak ada dalam sistem');
				}
			}
		}else{
			$CampaignTemplate = CampaignTemplate::model()->findByAttributes(array('id'=>$idTemplate,'campaign_template_group'=>'bee'));
			if ($CampaignTemplate){
				echo $CampaignTemplate->campaign_template_json;
			}else{
				throw new CHttpException(500,'Default Content Tidak ada dalam sistem');
			}
		}
	}
	
	public function actionChangeTemplate($id,$type='email'){
		
		$model = $this->loadModel($id);
		$idTemplate = $_POST['idTemplate'];
		// echo $idTemplate;
		// exit;
		$urlNext = Yii::app()->createAbsoluteUrl('/campaign/contentEmail', array('id'=>$model->id,'type'=>$type,'idTemplate'=>$idTemplate));
		echo json_encode(array('status'=>'SUCCESS','message'=>'Success change Content','urlNext'=>$urlNext));
		

		
	}	
	
	
	public function actionRedirectSavedContentEmail($id,$type='email'){
		
		Yii::app()->user->setFlash('success', " Data Campaign Saved ");
		$this->redirect(array('cOverview','type'=>$type));

		
	}	
	
	public function actionSendCampaign($id,$type='email'){
		
		$countReceiver = $this->actionCountDataReceiver($id,$type);
		
		$Pmodel=new Campaign('search');
		$Pmodel->unsetAttributes();  // clear any default values
		if(isset($_GET['Campaign']))
			$Pmodel->attributes=$_GET['Campaign'];
		
		$Pmodel->id_user=Yii::app()->user->id;
		$field_type = $type.'_blast';
		$Pmodel->$field_type = 1;
		// get paket kuota aktif
		$kuotaPaket = $Pmodel->countTotalPaketCampaign();
		$field_sisa = 'total_sisa_'.$type;
		$sisa_kuota = $kuotaPaket [$field_sisa];
		
		
		if($sisa_kuota >= $countReceiver){
			$model = $this->loadModel($id);
			$model->status = 1;
			$model->save(false);
			
			Yii::app()->user->setFlash('success', " Data Campaign has been on going to send ");
			$this->redirect(array('view','id'=>$id,'type'=>$type));
		}else{
			Yii::app()->user->setFlash('success', " Sorry.. Data Campaign Cannot Send, Because insufficient quota. Please Subscribe Package first ");
			$this->redirect(array('view','id'=>$id,'type'=>$type));
		}
		

		
	}	
	
	public function actionHoldCampaign($id,$type='email'){
		
		$model = $this->loadModel($id);
		
		if ($model->is_sent == 0){
			$model->status = 0;
			$model->save(false);
			
			Yii::app()->user->setFlash('success', " Data Campaign has been Hold");
			$this->redirect(array('view','id'=>$id,'type'=>$type));
		}else{
			Yii::app()->user->setFlash('success', " Sorry Data Campaign Cannot be Hold, Campaign has Sent");
			$this->redirect(array('view','id'=>$id,'type'=>$type));
		}
		

		
	}	
	
	public function actionCountDataReceiver($id,$type='email'){
		
		$model = $this->loadModel($id);
		//print_r($model->details);
		$dataInIdSegmentation = array();
		foreach ($model->details as $data){
			$dataInIdSegmentation[]=$data['id_user_segmentation'];
		}
		//print_r($dataInIdSegmentation);
		$dataReceiver = array();
		$countData = 0;
		if (count($dataInIdSegmentation) > 0){
			$inData = implode(',',$dataInIdSegmentation);
			//echo $inData;
			
			$segmentation = UserSegmentation::model()->allSegment()->findAll(array(
				'condition' => 'id IN ('.$inData.')',
				//'params'=>array(':listID'=>$inData),
			));
			/* echo '<pre>';
			print_r($segmentation);  */
			
			
			foreach ($segmentation as $segment){
				if ($segment->is_custom == 0){
					$phoneNumbers = $segment->listPhoneAllowed;
					$emails = $segment->listEmailAllowed;
					
					if($type =='email'){
						foreach ($emails as $email){
							$countData ++ ;
						}
					}else{ // wa atau sms
						foreach ($phoneNumbers as $phoneNumber){
							if($phoneNumber != '0'){
								$countData ++ ;
							}
						}
					}
					$dataReceiver[$segment->id]['emails'] = $emails;
					$dataReceiver[$segment->id]['phoneNumbers'] = $phoneNumbers;
				}else{
					$emailsAndphoneNumbers = $segment->detailsCustom;
					$dataReceiver[$segment->id]['emailsAndphoneNumbers'] = $emailsAndphoneNumbers;
					
					foreach ($emailsAndphoneNumbers as $data){
						$countData ++ ;
					}

				}
			}
		}
		
		//echo $countData;
		return $countData;
		
		
		/* echo '<pre>';
		print_r($dataReceiver); */
		

		
	}	
	
	public function actionReplicate($id,$type='email'){
		
		
		
		$attributesCampaign= Campaign::model()->getTableSchema()->getColumnNames();
		$attributesCampaignDetail= CampaignDetail::model()->getTableSchema()->getColumnNames();
		
		$model = $this->loadModel($id);
		$campaignDetail = CampaignDetail::model()->findAllByAttributes(array('id_campaign'=>$model->id));
		//$campaignImage = CampaignImage::model()->findAllByAttributes(array('id_campaign'=>$model->id));
		
		$newData = new Campaign ;
		foreach ($attributesCampaign as $attributeField){
			if ($attributeField != 'id'){ // id nya otomatis
			
				if ($attributeField == 'title'){
					$newData->$attributeField = $model->$attributeField. '- Copy'; 
				}
				else if ($attributeField == 'status' || $attributeField == 'total_sent' || $attributeField == 'is_sent' || $attributeField == 'approval'){
					$newData->$attributeField = 0; 
				}
				else if ($attributeField == 'approval_message'){
					$newData->$attributeField = ''; 
				}
				else{
					$newData->$attributeField = $model->$attributeField;
				}
			}
		}
		$newData->save(false);
		
		foreach($campaignDetail as $detail){
			
			$newDataDetail = new CampaignDetail ;
			
			foreach ($attributesCampaignDetail as $attributeField){
				if ($attributeField != 'id'){ // id nya otomatis
				
					if ($attributeField == 'id_campaign'){
						$newDataDetail->$attributeField = $newData->id; 
					}
					
					else{
						$newDataDetail->$attributeField = $detail->$attributeField;
					}
				}
			}
			
			$newDataDetail->save(false);
		}
		
		
		
		Yii::app()->user->setFlash('success', " Campaign Has been Replicated");
		$this->redirect(array('view','id'=>$newData->id,'type'=>$type));
				
		
	}
	
	public function actionView($id,$type='email'){
		
		$model = $this->loadModel($id);
		
		$campaignImage = CampaignImage::model()->findAllByAttributes(array('id_campaign'=>$model->id));
		
		$criteria=new CDbCriteria;
		$criteria->compare('id_campaign',$model->id);
		$dataProvider=new CActiveDataProvider('CampaignDetail', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'created_date DESC',
			),
			'pagination'=>array(
				'pageSize'=>10,
			)
		));
		
		
		$sentData=new CampaignSent('searchByCampaign');
		$sentData->unsetAttributes();  // clear any default values
		if(isset($_GET['CampaignSent']))
			$sentData->attributes=$_GET['CampaignSent'];

		$sentDataProvider = $sentData->searchByCampaign($id);
		
		
		
		
		$Pmodel=new Campaign('search');
		$Pmodel->unsetAttributes();  // clear any default values
		if(isset($_GET['Campaign']))
			$Pmodel->attributes=$_GET['Campaign'];
		
		$Pmodel->id_user=Yii::app()->user->id;
		$field_type = $type.'_blast';
		$Pmodel->$field_type = 1;
		// get paket kuota aktif
		$kuotaPaket = $Pmodel->countTotalPaketCampaign();
		
		
		
		
		$this->render('view',array(
			'model'=>$model,
			'campaignImage'=>$campaignImage,
			'dataProvider'=>$dataProvider,
			'type'=>$type,
			
			'sentData'=>$sentData,
			'sentDataProvider'=>$sentDataProvider,
			
			'kuotaPaket'=>$kuotaPaket,
		));
	}	
	
	public function actionViewUserSegmentation($id){
		$model = $this->loadModelUserSegmentation($id);
		
		if ($model->is_custom == 1){
			$criteria=new CDbCriteria;
			$criteria->compare('id_custom_segmentation',$model->id);
			$dataProvider=new CActiveDataProvider('CustomSegmentationDetail', array(
				'criteria'=>$criteria,
				'sort'=>array(
					'defaultOrder'=>'created_date DESC',
				),
				'pagination'=>array(
					'pageSize'=>10,
				)
			));

		
			$this->render('viewUserSegmentation',array(
				'model'=>$model,
				'dataProvider'=>$dataProvider,
			));
		}else{
			//echo 'Ini yang Clustering dari User, Nunggu Mas Adi cara nampilkan list user emil, no telp dll nya';
			// ini dihide
			//$segmentation = UserSegmentation::model()->cluster()->findByAttributes(['id'=>$model->id]);
			 
			 
			 /* var_dump($segmentation);
			 var_dump($segmentation->listPhoneAllowed);
			 var_dump($segmentation->listEmailAllowed); */
			 
			 // ini di hide
			 /* $phoneNumbers = $segmentation->listPhoneAllowed;
			 $emails = $segmentation->listEmailAllowed; */
			  
			$this->render('viewUserSegmentationApp',array(
				'model'=>$model,
				//'segmentation'=>$segmentation,
				
				// ini di hide
				/* 'phoneNumbers'=>$phoneNumbers,
				'emails'=>$emails, */
				
			));
		}
		//var_dump($model);
	}
	
	/* public function actionCreateOriginal(){
		$this->render('createOriginal');
	} */

	
	public function saveDynamicTypeSentField($model){
		if ($model->sent_type == 0){
			$model->begda =  new CDbExpression('NULL'); 
			$model->endda =  new CDbExpression('NULL'); 
		}
		else if ($model->sent_type == 1){
			$model->post_date =  '2018-01-'.$model->dayNumber; 
		}
		else if ($model->sent_type == 2){
			$arrayDateForNameDay = MyAppComponent::getArrayDataWeekDay();
			$date = $arrayDateForNameDay[$model->dayName];
			$model->post_date =  $date; 
		}
		else if ($model->sent_type == 3){
			$model->post_date =  new CDbExpression('NULL'); 
		}
		
		return $model;
	}
	
	public function actionCreate($type='email'){
		$model=new Campaign;
		
		
		/* $queryUserSegmentation = " SELECT * FROM user_segmentation WHERE id_user = :id_user order by created_date DESC";
		$params = array();
		$params[':id_user']= Yii::app()->user->id;
		
		$userSegmentation = Yii::app()->db->createCommand($queryUserSegmentation);
        $userSegmentation->params = $params;
        $dataUserSegmentation = $userSegmentation->queryAll(); */
		$dataUserSegmentation = UserSegmentation::model()->allSegment()->findAllByAttributes(array('id_user'=>Yii::app()->user->id));
		
		if (count($dataUserSegmentation) == 0){
			Yii::app()->user->setFlash('error', " Maaf, Anda belum mempunyai daftar segmentasi user, silahkan untuk membuat data segmenasti terlebih dahulu ");
			$this->redirect(array('index','type'=>$type));
		}
		
		$campaignDetail = array();
		$campaignImage = array();
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['Campaign']))
			{
				
				$transaction = Yii::app()->db->beginTransaction();
				try{
			
					
					$model->attributes=$_POST['Campaign'];
					$model->id_user = Yii::app()->user->id;
					$model->created_date = date("Y-m-d H:i:s");
					$model->status = 0;
					$field_type = $type.'_blast';
					$model->$field_type = 1;
					
					$this->saveDynamicTypeSentField($model);
					
					if($model->save()){						
					
						if (isset($_POST['user_segment'])){
							$this->saveDetailCampaign($model->id);
						}
						
						// upload image 
						$fileObject=CUploadedFile::getInstances($model,'images');
						if($fileObject){
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							//echo 'ok';
							foreach ($fileObject as $key => $image){
								$timenow=date("YmdHis");
								$extension=$image->extensionName;
								$originalFileName = $judul =  str_replace(array(':', '\\', '/', '*','.',' '), '_', $image->name);
								$fileName = 'campaign_image'.'_'.$timenow.'_'.$originalFileName;
								$newFileName = $fileName.'.'.$extension;
								//echo $newFileName.'<br>';
								
								$this->saveCampaignImage($model->id,$this->uploadDir.$newFileName);
								
								$image->saveAs($this->uploadDir.$newFileName); 
								chmod($this->uploadDir.$newFileName, 0777);
							}
						}
						
						if ($type =='email'){
							Yii::app()->user->setFlash('success', " Data Saved.. Please Create Content For Campaign");
							$this->redirect(array('contentEmail','id'=>$model->id,'type'=>$type));
						}else{
							Yii::app()->user->setFlash('success', " Data Saved ");
							$this->redirect(array('cOverview','type'=>$type));
						}
					}
					
					$transaction->commit();
					$message ='Campaign '.$model->title.' is waiting for approval, please check and do an action.';
					WA::sendNotificationToAdmin($message);
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('id_user','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
			'dataUserSegmentation'=>$dataUserSegmentation,
			'campaignDetail'=>$campaignDetail,
			'campaignImage'=>$campaignImage,
			'type'=>$type,
		));
	}
	
	public function actionUpdate($id,$type='email'){
		$model=Campaign::model()->findByPk($id);
		
		
		/* $queryUserSegmentation = " SELECT * FROM user_segmentation WHERE id_user = :id_user order by created_date DESC";
		$params = array();
		$params[':id_user']= Yii::app()->user->id;
		
		$userSegmentation = Yii::app()->db->createCommand($queryUserSegmentation);
        $userSegmentation->params = $params;
        $dataUserSegmentation = $userSegmentation->queryAll(); */
		$dataUserSegmentation = UserSegmentation::model()->allSegment()->findAllByAttributes(array('id_user'=>Yii::app()->user->id));
		
		$campaignDetail = CampaignDetail::model()->findAllByAttributes(array('id_campaign'=>$model->id));
		$campaignImage = CampaignImage::model()->findAllByAttributes(array('id_campaign'=>$model->id));
		
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

			if(isset($_POST['Campaign']))
			{
				
				
				$transaction = Yii::app()->db->beginTransaction();
				try{
			
					
					$model->attributes=$_POST['Campaign'];
					//$model->id_user = Yii::app()->user->id;
					$model->updated_date = date("Y-m-d H:i:s");
					//$model->status=1;
					
					$this->saveDynamicTypeSentField($model);
					
					if($model->save()){						
					
						//kalau update biar gampang di delete dulu data detail yang sebelumnya, lalu di replace sama yang baru
						$this->deleteDetailCampaign($model->id);
						
						if (isset($_POST['user_segment'])){
							$this->saveDetailCampaign($model->id);
						}
						
						// upload image 
						$fileObject=CUploadedFile::getInstances($model,'images');
						if($fileObject){
							if (!file_exists($this->uploadDir)){
								$oldmask = umask(0);
								mkdir($this->uploadDir, 0777, true);
								umask($oldmask);
							}
							//echo 'ok';
							foreach ($fileObject as $key => $image){
								$timenow=date("YmdHis");
								$extension=$image->extensionName;
								$originalFileName = $judul =  str_replace(array(':', '\\', '/', '*','.',' '), '_', $image->name);
								$fileName = 'campaign_image'.'_'.$timenow.'_'.$originalFileName;
								$newFileName = $fileName.'.'.$extension;
								//echo $newFileName.'<br>';
								
								$this->saveCampaignImage($model->id,$this->uploadDir.$newFileName);
								
								$image->saveAs($this->uploadDir.$newFileName); 
								chmod($this->uploadDir.$newFileName, 0777);
							}
						}
						
						
						
						if ($type =='email'){
							Yii::app()->user->setFlash('success', " Data Saved.. Please Create Content For Campaign");
							$this->redirect(array('contentEmail','id'=>$model->id,'type'=>$type));
						}else{
							Yii::app()->user->setFlash('success', " Data Saved ");
							$this->redirect(array('cOverview','type'=>$type));
						}
						
					}
					
					$transaction->commit();
				}catch(Exception $e){
					
					$transaction->rollback();
					$model->addError('id_user','Terjadi Kesalahan Dalam Menyimpan Data, Silahkan Coba Lagi. - '.$e->getMessage());
				}
			
			}

		$this->render('create',array(
			'model'=>$model,
			'dataUserSegmentation'=>$dataUserSegmentation,
			'campaignDetail'=>$campaignDetail,
			'campaignImage'=>$campaignImage,
			'type'=>$type,
		));
	}
	
	public function saveDetailCampaign($id_campaign){
		$dataSegmentSelected = $_POST['user_segment'];
		foreach($dataSegmentSelected as $key => $value){
			$campaignDetail = new CampaignDetail;
			$campaignDetail->id_campaign = $id_campaign;
			$campaignDetail->id_user_segmentation = $value;
			$campaignDetail->created_date = date("Y-m-d H:i:s");
			$campaignDetail->status = 1;
			$campaignDetail->save();
		}
	}
	
	public function deleteDetailCampaign($id_campaign){
		CampaignDetail::model()->deleteAllByAttributes(array(
			'id_campaign'=>$id_campaign,
		));
	}
	
	public function saveCampaignImage($id_campaign,$image){
		
			$imageData = new CampaignImage;
			$imageData->id_campaign = $id_campaign;
			$imageData->image = $image;
			$imageData->save();
	}
	
	public function actionDeleteImage(){
		
		$key = $_POST['key'];
		$campaignImage = CampaignImage::model()->findByPk($key);
		if ($campaignImage){
			if (file_exists($campaignImage->image)){
				if ( unlink($campaignImage->image) ){
					$campaignImage->delete();
					//echo "success";
				}else{
					//echo "fail";
					throw new CHttpException(500,'Kesalahan Terjadi. gagal delete');
				}
			}else{
				throw new CHttpException(500,'Kesalahan Terjadi. file tidak ada');
			}
		}else{
			throw new CHttpException(500,'Kesalahan Terjadi. data tidak ada');
		}
		
		
		echo json_encode(array('status'=>'success'));
	}	
	
	
	
	public function actionCOverview($type='email'){
		
		$year = date('Y');
		$month = date ('m');
		
		
		$model=new Campaign('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Campaign']))
			$model->attributes=$_GET['Campaign'];
		
		$model->id_user=Yii::app()->user->id;
		$field_type = $type.'_blast';
		$model->$field_type = 1;
		$dataProvider=$model->search();
		
		
		
		
		// jika ada filter
		if (isset($_POST['Grafik'])){
			
			/* echo '<pre>';
			print_r($_POST['Grafik']);
			exit; */
			
			$result = array();
	
			$year = $_POST['Grafik']['year'];
			$month =$_POST['Grafik']['month'];
			$start_date = $_POST['Grafik']['start_date'];
			$end_date = $_POST['Grafik']['end_date'];
			$start_date_format = $year.'-'.$month.'-'.$start_date;
			$end_date_format = $year.'-'.$month.'-'.$end_date;
			
			
			// Total Sent Data
			$overviewDataTotalSentByDate = $model->overviewDataTotalSentByDate($field_type,$year,$month,$start_date_format,$end_date_format);
			$arrayoverviewTotalSentByDateLabel = array();
			$arrayoverviewTotalSentByDateValue = array();
			foreach ($overviewDataTotalSentByDate as $key => $data){
				$arrayoverviewTotalSentByDateLabel[]=substr($key,-2);
			}
			//print_r($arrayoverviewTotalSentByDateLabel);
			foreach ($overviewDataTotalSentByDate as $key => $data){
				$arrayoverviewTotalSentByDateValue[]=$data;
			}
			//print_r($arrayoverviewTotalSentByDateValue);
			$labelsTotalSentByDate = $arrayoverviewTotalSentByDateLabel;
			//echo $labelsTotalSentByDate;
			$valuesTotalSentByDate = $arrayoverviewTotalSentByDateValue;
			// end Total Sent Data
			
			$result['totalSent']['label']=$labelsTotalSentByDate;
			$result['totalSent']['value']=$valuesTotalSentByDate;
			
			
			echo CJSON::encode($result);
			exit;
			
			
		}else{
			$start_date = $year.'-'.$month.'-'.'01';
			$end_date = $year.'-'.$month.date("-t");
		}
		
		// end jika ada filter
		
		
		
		// buat Grafik
		//$model->status=0;
		// -- total sent --//
		//$totalSent = $model->countTotalSentData($year); // diganti
		$totalSent = $model->countTotalSentData($year,$field_type);
		//echo $totalSent;
		
		//-- overview total sent --//
		//$overview = $model->overviewDataTotalSent($year); // diganti
		$overview = $model->overviewDataTotalSent($year,$field_type);
		//-- overview total sent by date --//
		$overviewDataTotalSentByDate = $model->overviewDataTotalSentByDate($field_type,$year,$month);
		
		// get paket kuota aktif
		$kuotaPaket = $model->countTotalPaketCampaign();
		
		
		$this->render('coverview',array(
			'model'=>$model,
			'dataProvider'=>$dataProvider,
			'type'=>$type,
			'totalSent'=>$totalSent,
			'overview'=>$overview,
			'overviewDataTotalSentByDate'=>$overviewDataTotalSentByDate,
			'year'=>$year,
			'month'=>$month,
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			
			'kuotaPaket'=>$kuotaPaket,
		));
		
	}	 
	
	public function actionDelete($id,$type){
//      var_dump($id);exit;
        $model = Campaign::model()->findByPk($id);
        $model->status = 2;
        $model->save();
		Yii::app()->user->setFlash('success', " Data Deleted ");
        $this->redirect(['campaign/index','type'=>$type]);
    }
	
	/* public function actionCOverviewOriginal(){
		$this->render('coverviewOriginal');
	}	 */
	
	
	public function actionDetailPaket(){
		
		$email = UserEnterpriseEmail::model()->findByAttributes(array('id_user'=>Yii::app()->user->id));
		if(!$email) { // jika belum punya email data
			Yii::app()->user->setFlash('success', " Sorry You Must Complete your  profile account first");
			$this->redirect(array('home/profile'));
		}
		
		
		$model=new Campaign;
		$model->id_user=Yii::app()->user->id;
		
		// get paket kuota aktif
		$kuotaPaket = $model->countTotalPaketCampaign();
		
		
		
		$paketAktif=new UserPaketCampaign('searchAktifPaket');
		$paketAktif->unsetAttributes();  // clear any default values
		if(isset($_GET['UserPaketCampaign']))
			$paketAktif->attributes=$_GET['UserPaketCampaign'];
		
		$paketAktif->id_user=Yii::app()->user->id;
		$userPaketAktif = $paketAktif->searchAktifPaket();
		
		
		$paketInProcess=new UserPaketCampaign('searchInProcessPaket');
		$paketInProcess->unsetAttributes();  // clear any default values
		if(isset($_GET['UserPaketCampaign']))
			$paketInProcess->attributes=$_GET['UserPaketCampaign'];
		
		$paketInProcess->id_user=Yii::app()->user->id;
		$userPaketInProcess = $paketInProcess->searchInProcessPaket();
		
		
		$listPaket=new PaketCampaign('search');
		$listPaket->unsetAttributes();  // clear any default values
		if(isset($_GET['PaketCampaign']))
			$listPaket->attributes=$_GET['PaketCampaign'];
		
		$listPaket->status=1;
		$listPaketData = $listPaket->search();
		
		
		$this->render('detailPaket',array(
				'kuotaPaket'=>$kuotaPaket,
				'paketAktif'=>$paketAktif,
				'userPaketAktif'=>$userPaketAktif,
				
				'paketInProcess'=>$paketInProcess,
				'userPaketInProcess'=>$userPaketInProcess,
				
				'listPaket'=>$listPaket,
				'listPaketData'=>$listPaketData,
			)
		);
		
	}	
	
	public function actionBuyPaket($id){
		
		/* echo 'haha';
		exit; */
		
		// inisiasi variable
		$userEnterprise = UserEnterprise::model()->findByPk(Yii::app()->user->id);
		// email account
		$email = UserEnterpriseEmail::model()->findByAttributes(array('id_user'=>Yii::app()->user->id));
		if(!$email) { // jika belum punya email data
			Yii::app()->user->setFlash('success', " Sorry You Must Complete your  profile account first");
			$this->redirect(array('home/profile'));
		}
		$cust_id = $userEnterprise->id;
		$cust_email = $userEnterprise->email;
		$cust_msisdn = $userEnterprise->no_hp;
		$cust_name = $email->email_name;
		$merchant_id = 'BD909';
		
		
    
		// save user paket campaign
		$PaketCampaign = PaketCampaign::model()->findByPk($id);
		
		$UserPaketCampaign = new UserPaketCampaign;
		$UserPaketCampaign->id_user = Yii::app()->user->id;
		$UserPaketCampaign->id_paket_campaign = $id;
        $UserPaketCampaign->status = 0;
		$UserPaketCampaign->created_date = date("Y-m-d H:i:s");
        $UserPaketCampaign->save();
		
		
		// request finPay
        $param = array();
        $param['add_info1'] = 'Buy Paket Campaign #'.$UserPaketCampaign->id.' '.$PaketCampaign->nama_paket.' For User '.$cust_name; 
        $param['amount'] = $PaketCampaign->harga; 
        $param['cust_email'] = $cust_email; 
        $param['cust_id'] = $cust_id; 
        $param['cust_msisdn'] = $cust_msisdn; 
        $param['cust_name'] = $cust_name; 
        $param['invoice'] = 'INV-Paket-'.$UserPaketCampaign->id; 
        $param['merchant_id'] = $merchant_id; 
        $param['return_url'] = Yii::app()->createAbsoluteUrl('cron/paymentResponse',['id'=>$UserPaketCampaign->id,'id_pc'=>$id]); 
        $param['sof_id'] = 'finpay021'; 
        $param['sof_type'] = 'pay'; 
        $param['timeout'] = '100000'; 
        $param['trans_date'] = date("Y-m-d H:i:s");  
		$finpay = new Finpay($param);
		
		$kode_transaksi = $finpay->requestPaymentCode();
		// update user paket campaign
		if($kode_transaksi != null){
			$UserPaketCampaign->kode_transaksi = $kode_transaksi;
			$json = $finpay->getJsonText();
			$UserPaketCampaign->json = $json;
			$UserPaketCampaign->save();
			
			Yii::app()->user->setFlash('success', " Paket ".$PaketCampaign->nama_paket." has been add to transaction, please paid soon");
		}else{
			$UserPaketCampaign->delete();
			Yii::app()->user->setFlash('success', " Paket ".$PaketCampaign->nama_paket." failed, please try again");
		}
		 
		
		
		
		$this->redirect(array('detailPaket'));
		
		
	}
	
	public function actionClaimPaket($id){
		
		$userid =Yii::app()->user->id;
		$PaketCampaign = PaketCampaign::model()->findByPk($id);
		$masa_aktif = $PaketCampaign->masa_aktif;
		$available = $PaketCampaign->available_use_per_user;
		$cekCount = UserPaketCampaign::model()->countByAttributes(array('id_paket_campaign'=>$id,'id_user'=>$userid));
		
		if($cekCount < $available){
		
			$dateTimeNow =  date("Y-m-d H:i:s");
			$kode = date("YmdHis");
			$status = 1;
			
			$dayDiff = '+'.$masa_aktif ;
			$dateNow = date('Y-m-d'); // ini dinamis
			$dateAfter = date('Y-m-d', strtotime($dayDiff.' days', strtotime($dateNow)));
			
			$tgl_aktif = $dateNow;
			$tgl_kadaluarsa = $dateAfter;
			
			$userpaket = new UserPaketCampaign;
			$userpaket->id_user = $userid;
			$userpaket->id_paket_campaign = $PaketCampaign->id;
			$userpaket->status = $status;
			$userpaket->tgl_aktif = $tgl_aktif;
			$userpaket->tgl_kadaluarsa = $tgl_kadaluarsa;
			$userpaket->sisa_email = $PaketCampaign->kuota_email;
			$userpaket->sisa_sms = $PaketCampaign->kuota_sms;
			$userpaket->sisa_wa = $PaketCampaign->kuota_wa;
			$userpaket->created_date =$dateTimeNow;
			$userpaket->updated_date =$dateTimeNow;
			$userpaket->kode_transaksi = 'F'.$userid.$kode;
			$userpaket->save();
			
			/* if(!$userpaket->save()){
				print_r($userpaket->getErrors());
				exit;
			} */
			
			Yii::app()->user->setFlash('success', " Paket ".$PaketCampaign->nama_paket." has been claim ");
			$this->redirect(array('paidBuyPaket','id'=>$userpaket->id));
		}else{
			Yii::app()->user->setFlash('success', " Paket ".$PaketCampaign->nama_paket." cannot be claim, because has limit available use per user ");
			$this->redirect(array('detailPaket'));
		}
		
	}
	
	public function actionHistoryPaket(){
		
		
		$paket=new UserPaketCampaign('searchHistoryPaket');
		$paket->unsetAttributes();  // clear any default values
		if(isset($_GET['UserPaketCampaign']))
			$paket->attributes=$_GET['UserPaketCampaign'];
		
		$paket->id_user=Yii::app()->user->id;
		$userPaket = $paket->searchHistoryPaket();
		
		
		$history=new HistoryBlastPaket('searchHistoryBlast');
		if(isset($_GET['HistoryBlastPaket']))
			$history->attributes=$_GET['HistoryBlastPaket'];
		
		
		$history->id_user=Yii::app()->user->id;
		$historyData = $history->searchHistoryBlast();
		
		
		
		
		$this->render('historyPaket',array(
				'paket'=>$paket,
				'userPaket'=>$userPaket,
				
				'history'=>$history,
				'historyData'=>$historyData,
			)
		);
		
	}
	
	
	public function actionHistoryPaketDetail($id){
		
		
		$history=new HistoryBlastPaket('searchHistoryBlastDetail');
		if(isset($_GET['HistoryBlastPaket']))
			$history->attributes=$_GET['HistoryBlastPaket'];
		
		
		$history->id_user=Yii::app()->user->id;
		$history->id_user_paket_campaign=$id;
		$historyData = $history->searchHistoryBlastDetail();
		
		
		
		
		$this->render('historyPaketDetail',array(

				'history'=>$history,
				'historyData'=>$historyData,
			)
		);
		
	}
	
	public function actionCartcreate($id)
	{
				
				$paket = PaketCampaign::model()->findByPk($id);
				Yii::app()->shoppingCart->put($paket); //1 item with id=1, quantity=1.
				Yii::app()->user->setFlash('success', "Paket  $paket->nama_paket Telah Disimpan di Keranjang Belanja ,Silahkan Lihat ".CHtml::link('Keranjang Belanja', array('campaign/viewcart'))." Untuk Mengubah Jumlah Paket Yang dibeli");
				echo "Paket  $paket->nama_paket Telah Disimpan di Keranjang Belanja ,Silahkan Lihat ".CHtml::link('Keranjang Belanja', array('campaign/viewcart'))." Untuk Mengubah Jumlah Paket Yang dibeli";
				
				//$this->redirect(array('viewProduct','id'=>$product->id_product,'title'=>$product->product_name_id));
		

		
	}
	
	public function actionClearcart()
	{
				
				Yii::app()->shoppingCart->clear();
				Yii::app()->user->setFlash('success', "Semua Isi dalam Keranjang Belanja dikosongkan");
				$this->redirect(array('detailPaket'));
		

		
	}
	
	public function actionViewcart()
	{

	$positions = Yii::app()->shoppingCart->getPositions();
		$arrayDataProvider=new CArrayDataProvider($positions, array(
			'id'=>'id',
			
			'pagination'=>array(
				'pageSize'=>10,
			),
		));

	$this->render('cart',array(
		'dataProvider'=>$arrayDataProvider,
	));
	}
	
	public function actionDeletecart($id)
	{
				
				$paket = PaketCampaign::model()->findByPk($id);
				Yii::app()->shoppingCart->remove($paket->getId()); 
				Yii::app()->user->setFlash('success', "Paket  $paket->nama_paket Telah  dihapus dari Keranjang Belanja.");
				
				
           		$this->redirect(array('viewcart'));
           	    

		
	}
	
	public function actionUpdatecart($id,$jumlah)
 	 {
 	 	
      
       			$paket = PaketCampaign::model()->findByPk($id);

       			
				Yii::app()->shoppingCart->update($paket,$jumlah);
				Yii::app()->user->setFlash('success', "Paket  $paket->nama_paket Telah  dirubah di Keranjang Belanja.");
				//$this->redirect(array('viewcart'));				
				echo 'ok';
				
				
	 }
	 
	 public function actionBuyPaketInCart(){
		 
		 // inisiasi variable
		$userEnterprise = UserEnterprise::model()->findByPk(Yii::app()->user->id);
		// email account
		$email = UserEnterpriseEmail::model()->findByAttributes(array('id_user'=>Yii::app()->user->id));
		if(!$email) { // jika belum punya email data
			Yii::app()->user->setFlash('success', " Sorry You Must Complete your  profile account first");
			$this->redirect(array('home/profile'));
		}
		$cust_id = $userEnterprise->id;
		$cust_email = $userEnterprise->email;
		$cust_msisdn = $userEnterprise->no_hp;
		$cust_name = $email->email_name;
		$merchant_id = 'BD909';
		
		
		
		$positions = Yii::app()->shoppingCart->getPositions();
		 /* $arrayDataProvider=new CArrayDataProvider($positions, array(
				'id'=>'id',
				
				'pagination'=>false,
			));
		 echo '<pre>'; 
		 print_r($positions);
		 */
		 
		 $arrayID = array();
		 foreach ($positions as $paket){
			 //echo $paket->id.' '.$paket->Quantity.'<br>';
			 $arrayID[]='#'.$paket->id.' ('.$paket->Quantity.')';
		 }
		 $listID = implode(', ',$arrayID);
		 //echo $listID;
		 $datenowData=date("YmdHis");
		 
		
		 // request finPay
        $param = array();
        $param['add_info1'] = 'Buy Pkt #'.$datenowData.' For '.$cust_name;
        $param['amount'] = Yii::app()->shoppingCart->getCost(); 
        $param['cust_email'] = $cust_email; 
        $param['cust_id'] = $cust_id; 
        $param['cust_msisdn'] = $cust_msisdn; 
        $param['cust_name'] = $cust_name; 
        $param['invoice'] = Yii::app()->user->id.$datenowData; 
        $param['merchant_id'] = $merchant_id; 
        $param['return_url'] = Yii::app()->createAbsoluteUrl('cron/paymentResponseCart'); 
        $param['sof_id'] = 'finpay021'; 
        $param['sof_type'] = 'pay'; 
        $param['timeout'] = '60'; 
        $param['trans_date'] = date("Y-m-d H:i:s");  
		$finpay = new Finpay($param);
		
		$kode_transaksi = $finpay->requestPaymentCode(); 
		
		
		// update user paket campaign
		if($kode_transaksi != null){
			$json = $finpay->getJsonText();
			
			foreach ($positions as $paket){
				
				
				for ($i = 1; $i <= $paket->Quantity; $i++) {
					//echo $paket->id.' ';
					$UserPaketCampaign = new UserPaketCampaign;
					$UserPaketCampaign->id_user = Yii::app()->user->id;
					$UserPaketCampaign->id_paket_campaign = $paket->id;
					$UserPaketCampaign->status = 0;
					$UserPaketCampaign->created_date = date("Y-m-d H:i:s");
					$UserPaketCampaign->kode_transaksi = $kode_transaksi;
					$UserPaketCampaign->json = $json;
					$UserPaketCampaign->save(false);
				}
			}
			
			Yii::app()->user->setFlash('success', " Paket has been add to transaction, please paid soon");
			Yii::app()->shoppingCart->clear();
			$this->redirect(array('paidBuyPaket','id'=>$UserPaketCampaign->id));
		}else{
			
			Yii::app()->user->setFlash('success', " Paket failed, please try again");
			$this->redirect(array('viewcart'));
		}
		
		
		
	 }
	
	
	public function actionPaidBuyPaket($id){
		$model = UserPaketCampaign::model()->findByPk($id);
		
		
		
		$this->render('paidBuyPaket',array(
			'model'=>$model,
			
		));
		
	}	 
	
	public function actionTraceSent($id){
		$trace = KirimEmailMailchimp::getCampaignById($id);
		if ($trace['status']=='error' || (isset($trace['data']['type']) && $trace['data']['type'] =='http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/') ){
			//echo 'error';
		}else{
			//echo '<pre>';
			//print_r($trace);
			echo '<b>Tracking</b><br>';
			$tracking = $trace['data']['tracking'];
			echo '<table class="materialize-items border-right bordered striped condensed responsive-table">';
			foreach($tracking as $keyTrace => $valTrace){
				echo '<tr>';
				echo '<td>'.$keyTrace . '</td><td> : </td><td>'.$valTrace.'</td>';
				echo '</tr>';
			}
			echo '</table>';
			
			echo '<b>Report Summary</b><br>';
			$report_summary = $trace['data']['report_summary'];
			$tracking = $trace['data']['tracking'];
			echo '<table class="materialize-items border-right bordered striped condensed responsive-table">';
			foreach($report_summary as $keyreport_summary => $valreport_summary){
				
				if (is_array($valreport_summary)){
					echo '<tr>';
					echo '<td>'.$keyTrace . '</td><td> : </td><td></td>';
					echo '</tr>';
					foreach($valreport_summary as $keyreport_valreport_summary => $valreport_valreport_summary){
						echo '<tr>';
						echo '<td>';
						echo '</td>';
						
						echo '<td>';
						echo $keyreport_valreport_summary . ' : '.$valreport_valreport_summary.'<br>';
						echo '</td>';
						
						echo '<td>';
						echo '</td>';
						
						echo '</tr>';
					}
				}else{
					echo '<tr>';
					echo '<td>'.$keyreport_summary . '</td><td> : </td><td>'.$valreport_summary.'</td>';
					echo '</tr>';
				}
				
			}
			echo '</table>';
			
		}
		
	}	 
}