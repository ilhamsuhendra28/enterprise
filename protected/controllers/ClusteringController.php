<?php

class ClusteringController extends Controller
{
	public $layout='../layouts/mainadmin';
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'saveEdit', 'create', 'uoverview', 'saveNew', 'countUser', 'view', 'edit', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
    public function actionView($id)
    {
        $segmen = UserSegmentation::model()->findByPk($id);
        $this->render('view_detail_segmen',[
            'segmen'=>$segmen,
        ]);
    }
	
	public function actionIndex()
    {
        $idUser = Yii::app()->user->id;
        $listSegmentation = UserSegmentation::model()->cluster()->findAllByAttributes(['id_user'=>$idUser],['order'=>'id DESC']);
        if (empty($listSegmentation)){
            $view = 'index';
            $render = [
                    'listSegmentation'=>$listSegmentation,
                ];
        } else {
            $view = 'overview';
            $render = [
                    'listSegmentation'=>$listSegmentation,
                    'overview'=>$this->overview($listSegmentation),
                ];
        }
//        var_dump($view,$idUser,$listSegmentation);exit;
		$this->render($view,$render);
	}
    
    private function overview($listSegmentation)
    {
        $listUser = [];
        foreach ($listSegmentation as $key => $value) {
            foreach ($value->listUser as $key2 => $value2) {
                $listUser[$value2->id] = $value2;
            }
        }
//        var_dump($listUser);exit;
        $male = 0; $female = 0;
        $listMale = []; $listFemale = [];
        foreach ($listUser as $key => $value) {
            if (!empty($value->gender)){
                if ($value->gender->value == '1')
                {$male++;$listMale[] = $value;}
                else 
                {$female++;$listFemale[] = $value;}
            }
        }
        $classUser = ['<15'=>0,'15-24'=>0,'25-34'=>0,'35-44'=>0,'>44'=>0];
        foreach ($listUser as $key => $value) {
            if ($value->age->age < 15)
                $classUser['<15']++;
            elseif (15<=$value->age->age AND $value->age->age<=24)
                $classUser['15-24']++;
            elseif (25<=$value->age->age AND $value->age->age<=34)
                $classUser['25-34']++;
            elseif (35<=$value->age->age AND $value->age->age<=44)
                $classUser['35-44']++;
            elseif ($value->age->age > 44)
                $classUser['>44']++;
        }
//        var_dump($classUser, array_sum($classUser));exit;
        $classSalary = []; $classSalar = [];
        $classSalary = ['<=1M'=>0,'>1M-3M'=>0,'>3M-5M'=>0,'>5M-8M'=>0,'>8M-15M'=>0,'>15M'=>0];
        foreach ($listMale as $key => $value) {
            if ($value->salary->salar <= 1000000)
                $classSalary['<=1M']++;
            elseif (1000000 < $value->salary->salar AND $value->salary->salar <= 3000000)
                $classSalary['>1M-3M']++;
            elseif (3000000 < $value->salary->salar AND $value->salary->salar <= 5000000)
                $classSalary['>3M-5M']++;
            elseif (5000000 < $value->salary->salar AND $value->salary->salar <= 8000000)
                $classSalary['>5M-8M']++;
            elseif (8000000 < $value->salary->salar AND $value->salary->salar <= 15000000)
                $classSalary['>8M-15M']++;
            elseif ($value->salary->salar >= 15000000)
                $classSalary['>15M']++;
            
        }
        $classSalar['male'] = $classSalary;
        
        $classSalary = [];
        $classSalary = ['<=1M'=>0,'>1M-3M'=>0,'>3M-5M'=>0,'>5M-8M'=>0,'>8M-15M'=>0,'>15M'=>0];
        foreach ($listFemale as $key => $value) {
            if ($value->salary->salar <= 1000000)
                $classSalary['<=1M']++;
            elseif (1000000 < $value->salary->salar AND $value->salary->salar <= 3000000)
                $classSalary['>1M-3M']++;
            elseif (3000000 < $value->salary->salar AND $value->salary->salar <= 5000000)
                $classSalary['>3M-5M']++;
            elseif (5000000 < $value->salary->salar AND $value->salary->salar <= 8000000)
                $classSalary['>5M-8M']++;
            elseif (8000000 < $value->salary->salar AND $value->salary->salar <= 15000000)
                $classSalary['>8M-15M']++;
            elseif ($value->salary->salar >= 15000000)
                $classSalary['>15M']++;
            
        }
        $classSalar['female'] = $classSalary;
        
        $classRelig = ['islam'=>0,'kristen'=>0,'katolik'=>0,'hindu'=>0,'budha'=>0];
        foreach ($listUser as $key => $value) {
            if ($value->religion->value == 8)
                $classRelig['islam']++;
            elseif ($value->religion->value == 9)
                $classRelig['kristen']++;
            elseif ($value->religion->value == 10)
                $classRelig['katolik']++;
            elseif ($value->religion->value == 11)
                $classRelig['hindu']++;
            elseif ($value->religion->value == 12)
                $classRelig['budha']++;
        }
        
        $classChild = ['0/1'=>0,'2'=>0,'3'=>0,'4'=>0,'5/6'=>0,'>6'];
        foreach ($listUser as $key => $value) {
            if ($value->children->value == 0 || $value->children->value == 1)
                $classChild['0/1']++;
            elseif ($value->children->value == 2)
                $classChild['2']++;
            elseif ($value->children->value == 3)
                $classChild['3']++;
            elseif ($value->children->value == 4)
                $classChild['4']++;
            elseif ($value->children->value == 5 || $value->children->value == 6)
                $classChild['5/6']++;
            elseif ($value->children->value > 6)
                $classChild['>6']++;
        }
        
        $classCity = [];
        $this->cityClass($listSegmentation);
        return [
            'male'=>$male, 
            'female'=>$female, 
            'classUser'=>$classUser, 
            'classSalar'=>$classSalar,
            'classRelig'=>$classRelig,
            'classChild'=>$classChild,
            'classCity'=>$this->cityClass($listSegmentation),
            ];
    }
    
    private function cityClass($listSegmentation){
        $listQ = [];
        foreach ($listSegmentation as $key => $value) {
            $listQ[] = $this->makeQuery($value->details);
        }
        $query = implode(' UNION ', $listQ);
        $queryOuter = 'SELECT
                            t.*,
                            count( t.id_user ) AS jml 
                        FROM
                            user_city t
                            JOIN ('.$query.') a ON t.id_user = a.id_user 
                        GROUP BY
                            kota 
                        ORDER BY
                            jml DESC';
        $query = Yii::app()->db->createCommand($queryOuter);
        $result = $query->queryAll();
        
        return $result;
    }
    
    private function ageClass()
    {
        
    }
    
    private function salarClass()
    {
        
    }
	
	public function actionCreate(){
		$listGrup = GroupSegmentation::model()->findAll(['order'=>'t.order ASC']);
        $countUser = User::model()->active()->count();
        $listKategoriBarang = KategoriBarang::model()->findAll();
        $listKota = MasterKota::model()->findAll();
        $listTambahan = ['TIKET KERETA'=>'TIKET KERETA','TIKET PESAWAT'=>'TIKET PESAWAT'];
        $listKategori = UserReceipt::model()->findAll(['select'=>'DISTINCT(kategori_barang)']);
        foreach ($listKategoriBarang as $key => $value) {
            $listTambahan[$value->kategori_barang] = $value->kategori_barang;
        }
        foreach ($listKategori as $key => $value) {
            $listTambahan[$value->kategori_barang] = $value->kategori_barang;
        }
//        var_dump($listTambahan);exit;
		$this->render('create',[
            'listGrup'=>$listGrup,
            'countUser'=>$countUser,
            'listKategoriBarang'=>$listKategoriBarang,
            'listTambahan'=>$listTambahan,
            'listKota'=>$listKota,
        ]);
	}	
	
	public function actionUOverview(){
		
		$this->render('uoverview');
	}	
    
    public function actionSaveNew(){
//        var_dump($_POST);
        $idUser = Yii::app()->user->id;
        $post = $_POST;
        array_shift($post);
//        var_dump($post);
        $userSegmentation = new UserSegmentation;
        $userSegmentation->id_user = $idUser;
        $userSegmentation->title = $post['1,title'];
        $userSegmentation->description = $post['2,descr'];
        $userSegmentation->created_date = date('Y-m-d H:i:s');
        $userSegmentation->status = 1;
        $userSegmentation->save();
        foreach ($post as $key => $value) {
            $idDetail = explode(',',$key);
            $userSegmen = new UserDetailSegmentation;
            $userSegmen->id_detail_segmentation = $idDetail[0];
            $userSegmen->fieldid = $idDetail[1];
            $userSegmen->id_segmentation = $userSegmentation->id;
            if ($idDetail[1] == 'relig')
                $userSegmen->value = CJSON::encode ($value);
            else
                $userSegmen->value = $value;
            $userSegmen->begda = date('Y-m-d');
            $userSegmen->endda = '9999-12-31';
            if ($value != '')
                $userSegmen->save();
//            var_dump($userSegmen,$userSegmen->errors);
        }
        $this->redirect(['clustering/index']);
    }
    
    public function actionEdit($id){
        Yii::app()->user->setState('idSegmen',$id);
        $segmen = UserSegmentation::model()->findByPk($id);
        $listGrup = GroupSegmentation::model()->findAll(['order'=>'t.order ASC']);
        $listKategoriBarang = KategoriBarang::model()->findAll();
        $listKota = MasterKota::model()->findAll();
        $listTambahan = ['TIKET KERETA'=>'TIKET KERETA','TIKET PESAWAT'=>'TIKET PESAWAT'];
        $listKategori = UserReceipt::model()->findAll(['select'=>'DISTINCT(kategori_barang)']);
        foreach ($listKategoriBarang as $key => $value) {
            $listTambahan[$value->kategori_barang] = $value->kategori_barang;
        }
        foreach ($listKategori as $key => $value) {
            $listTambahan[$value->kategori_barang] = $value->kategori_barang;
        }
//        var_dump($segmen);exit;
        $this->render('edit',[
            'listGrup'=>$listGrup,
            'segmen'=>$segmen,
            'listKategoriBarang'=>$listKategoriBarang,
            'listTambahan'=>$listTambahan,
            'listKota'=>$listKota,
        ]);
    }
    
    public function actionSaveEdit($id){
        $sql = 'UPDATE user_detail_segmentation SET endda = "'.date('Y-m-d',strtotime('-1 days')).'" WHERE id_segmentation = '.$id.' AND NOW() BETWEEN begda AND endda';
        $query = Yii::app()->db->createCommand($sql);
//        $query->params = array(':id_segmen'=>$id);
//        var_dump($query);exit;
        $query->execute();

        $idUser = Yii::app()->user->id;
        $post = $_POST;
        array_shift($post);
//        var_dump($post);
        $userSegmentation = UserSegmentation::model()->findByPk($id);
        $userSegmentation->id_user = $idUser;
        $userSegmentation->title = $post['1,title'];
        $userSegmentation->description = $post['2,descr'];
        $userSegmentation->updated_date = date('Y-m-d H:i:s');
        $userSegmentation->status = 1;
        $userSegmentation->save();
        foreach ($post as $key => $value) {
            $idDetail = explode(',',$key);
            $userSegmen = new UserDetailSegmentation;
            $userSegmen->id_detail_segmentation = $idDetail[0];
            $userSegmen->fieldid = $idDetail[1];
            $userSegmen->id_segmentation = $userSegmentation->id;
            if ($idDetail[1] == 'relig')
                $userSegmen->value = CJSON::encode ($value);
            else
                $userSegmen->value = $value;
            $userSegmen->begda = date('Y-m-d');
            $userSegmen->endda = '9999-12-31';
            if ($value != '')
                $userSegmen->save();
//            var_dump($userSegmen,$userSegmen->errors);
        }
        $this->redirect(['clustering/view','id'=>$id]);
    }
    
    public function actionCountUser(){        
        $listQuery = [];
        $listQuery[] = "SELECT id AS id_user FROM user WHERE active = 1";
        $params = [];
//        var_dump($_POST);exit;
        foreach ($_POST as $key => $value) {
            $_field = explode(',', $key);
            $field = $_field[1];
            
            switch ($field) {
                case 'gende':
                    if (empty($value))
                        break;
                    $listQuery[] = "SELECT id_user FROM user_personal_information "
                    . "WHERE fieldid = :field1 AND NOW() BETWEEN begda AND endda AND value = :value1";
                    $params[':field1'] = $field;
                    $params[':value1'] = $value;
                    break;
                
                case 'age':
                    $_value = explode('-', $value);
                    $min = $_value[0]; $max = $_value[1];
                    $listQuery[] = "SELECT id_user FROM user_age "
                    . "WHERE age BETWEEN $min AND $max";
//                    $params[':min'] = $min;
//                    $params[':max'] = $max;
                    break;
                
                case 'maria':
                    if (empty($value))
                        break;
                    $listQuery[] = "SELECT id_user FROM user_personal_information "
                    . "WHERE fieldid = :field2 AND NOW() BETWEEN begda AND endda AND value = :value2";
                    $params[':field2'] = $field;
                    $params[':value2'] = $value;
                    break;
                
                case 'relig':                    
                    $orCond = '';
                    $i = 0;
                    $allRelig = false;
                    foreach ($value as $key2 => $value2) {
                        if ($key2 == 0){
                            $allRelig = true;
                            break;
                        } else {
                            $orCond .= $i > 0 ? ' OR ':'';
                            $orCond .= 'value = :relig'.$i;
                            $params[':relig'.$i] = $key2;
                        }
                        $i++;
                    }
                    if (!$allRelig){
                        $listQuery[] = 'SELECT id_user FROM user_personal_information '
                            . 'WHERE fieldid = :field3 AND NOW() BETWEEN begda AND endda AND ('.$orCond.')';
                        $params[':field3'] = $field;
                    }
                    break;
                
                case 'salar':
                    $_value = explode('-', $value);
                    $min2 = $_value[0]*1000000; $max2 = $_value[1]*1000000;
//                    $listQuery[] = "SELECT id_user FROM user_personal_information "
//                    . "WHERE fieldid = :field4 AND NOW() BETWEEN begda AND endda AND REPLACE(REPLACE(REPLACE(value,'.',''),'<',''),'>','') BETWEEN $min2 AND $max2";
                    $listQuery[] = "SELECT id_user FROM user_salar WHERE salar BETWEEN $min2 AND $max2";
//                    $params[':field4'] = $field;
//                    $params[':min2'] = $min2;
//                    $params[':max2'] = $max2;
                    break;
                
                case 'purch':
                    $_value = explode('|', ltrim($value,'|'));
                    if (count($_value) > 0 AND $_value[0] != ''){
                        $query = "SELECT id_user FROM user_receipt WHERE ";
                        foreach ($_value as $keyP => $valueP) {
                            $query .= ' kategori_barang = :purch'.$keyP.' OR';
                            $params[':purch'.$keyP] = $valueP;
                        }
                        $listQuery[] = rtrim($query, 'OR').' GROUP BY id_user';
                    }
                    break;
                    
                case 'city':
                    $_value = explode('|', ltrim($value,'|'));
                    if (count($_value) > 0 AND $_value[0] != ''){
                        $query = "SELECT id_user FROM user_city WHERE ";
                        foreach ($_value as $keyP => $valueP) {
                            $query .= ' kota = :city'.$keyP.' OR';
                            $params[':city'.$keyP] = $valueP;
                        }
                        $listQuery[] = rtrim($query, 'OR').' GROUP BY id_user';
                    }
                    break;
                
                default:
                    $listQuery[] = "SELECT id AS id_user FROM user WHERE active = 1";
                    break;
            }
        }
        $result = $this->execQuery($listQuery,$params);
        echo count($result);
//        var_dump($listQuery,$params,$result);
    }
    
    private function execQuery($listQuery,$params){
        $query = '';
        foreach ($listQuery as $key => $value) {
            $query .= $key > 0 ? ' UNION ALL ':'';
            $query .= $value;
        }
        $queryOuter = 'SELECT * FROM
                        (SELECT id_user,count(id_user) jml FROM
                        ('.$query.') T
                        GROUP BY id_user) T2
                        WHERE jml >= '.count($listQuery);
        $pre = Yii::app()->db->createCommand($queryOuter);
        $pre->params = $params;
        $result = $pre->queryAll();
        return $result;
    }
    
    public function actionDelete($id){
//        var_dump($id);exit;
        $segmen = UserSegmentation::model()->findByPk($id);
        $segmen->status = 2;
        $segmen->save();
        $this->redirect(['clustering/index']);
    }
    
    private function makeQuery($details)
    {
        $listQuery = [];
        $listQuery[] = "SELECT id AS id_user FROM user WHERE active = 1";
        $params = [];
//        var_dump($detail);exit;
        foreach ($details as $key => $value) {
            $field = $value->fieldid;
            $value = $value->value;
            switch ($field) {
                case 'gende':
                    if (empty($value))
                        break;
                    $listQuery[] = "SELECT id_user FROM user_personal_information "
                    . "WHERE fieldid = '".$field."' AND NOW() BETWEEN begda AND endda AND value = '".$value."'";
                    $params[':field1'] = $field;
                    $params[':value1'] = $value;
                    break;
                
                case 'age':
                    $_value = explode('-', $value);
                    $min = $_value[0]; $max = $_value[1];
                    $listQuery[] = "SELECT id_user FROM user_age "
                    . "WHERE age BETWEEN $min AND $max";
                    break;
                
                case 'maria':
                    if (empty($value))
                        break;
                    $listQuery[] = "SELECT id_user FROM user_personal_information "
                    . "WHERE fieldid = '".$field."' AND NOW() BETWEEN begda AND endda AND value = '".$value."'";
                    $params[':field2'] = $field;
                    $params[':value2'] = $value;
                    break;
                
                case 'relig':                    
                    $orCond = '';
                    $i = 0;
                    $allRelig = false;
                    foreach (CJSON::decode($value) as $key2 => $value2) {
                        if ($key2 == 0){
                            $allRelig = true;
                            break;
                        } else {
                            $orCond .= $i > 0 ? ' OR ':'';
                            $orCond .= 'value = "'.$key2.'"';
                            $params[':relig'.$i] = $key2;
                        }
                        $i++;
                    }
                    if (!$allRelig){
                        $listQuery[] = 'SELECT id_user FROM user_personal_information '
                            . 'WHERE fieldid = "'.$field.'" AND NOW() BETWEEN begda AND endda AND ('.$orCond.')';
                        $params[':field3'] = $field;
                    }
                    break;
                
                case 'salar':
                    $_value = explode('-', $value);
                    $min2 = $_value[0]*1000000; $max2 = $_value[1]*1000000;
                    $listQuery[] = "SELECT id_user FROM user_salar WHERE salar BETWEEN $min2 AND $max2";
                    break;
                
                case 'purch':
                    $_value = explode('|', ltrim($value,'|'));
                    if (count($_value) > 0 AND $_value[0] != ''){
                        $query = "SELECT id_user FROM user_receipt WHERE ";
                        foreach ($_value as $keyP => $valueP) {
                            $query .= ' kategori_barang = "'.$valueP.'" OR';
                            $params[':purch'.$keyP] = $valueP;
                        }
                        $listQuery[] = rtrim($query, 'OR').' GROUP BY id_user';
                    }
                    break;
                    
                case 'city':
                    $_value = explode('|', ltrim($value,'|'));
                    if (count($_value) > 0 AND $_value[0] != ''){
                        $query = "SELECT id_user FROM user_city WHERE ";
                        foreach ($_value as $keyP => $valueP) {
                            $query .= ' kota = "'.$valueP.'" OR';
                            $params[':city'.$keyP] = $valueP;
                        }
                        $listQuery[] = rtrim($query, 'OR').' GROUP BY id_user';
                    }
                    break;
                    
                default:
                    $listQuery[] = "SELECT id AS id_user FROM user WHERE active = 1";
                    break;
            }
        }
        $result = $this->getFullQuery($listQuery,$params);
        return $result;
    }
    
    private function getFullQuery($listQuery,$params){
        $query = '';
        foreach ($listQuery as $key => $value) {
            $query .= $key > 0 ? ' UNION ALL ':'';
            $query .= $value;
        }
        $queryOuter = 'SELECT id_user FROM
                        (SELECT id_user,count(id_user) jml FROM
                        ('.$query.') T
                        GROUP BY id_user) T2
                        WHERE jml >= '.count($listQuery);
        
        return $queryOuter;
    }
    
}