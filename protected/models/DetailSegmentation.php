<?php

/**
 * This is the model class for table "detail_segmentation".
 *
 * The followings are the available columns in table 'detail_segmentation':
 * @property integer $id
 * @property string $fieldid
 * @property string $nama_field
 * @property string $tipe_field
 * @property integer $group
 * @property integer $col_size
 * @property integer $order
 * @property string $begda
 * @property string $endda
 */
class DetailSegmentation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detail_segmentation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group, col_size, order, min, max', 'numerical', 'integerOnly'=>true),
			array('fieldid', 'length', 'max'=>5),
			array('nama_field, tipe_field', 'length', 'max'=>255),
			array('begda, endda', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fieldid, nama_field, tipe_field, group, col_size, order, begda, endda', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        if (!empty(Yii::app()->request->getParam('id'))){
            $idSegmentation = Yii::app()->request->getParam('id');
        } else {
            $idSegmentation = 0;
        }
		return array(
            'value' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_detail_segmentation'=>'id'], 'condition'=>'value.id_segmentation = '.$idSegmentation.' AND NOW() between value.begda AND value.endda'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fieldid' => 'Fieldid',
			'nama_field' => 'Nama Field',
			'tipe_field' => 'Tipe Field',
			'group' => 'Group',
			'col_size' => 'Col Size',
			'order' => 'Order',
			'begda' => 'Begda',
			'endda' => 'Endda',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fieldid',$this->fieldid,true);
		$criteria->compare('nama_field',$this->nama_field,true);
		$criteria->compare('tipe_field',$this->tipe_field,true);
		$criteria->compare('group',$this->group);
		$criteria->compare('col_size',$this->col_size);
		$criteria->compare('order',$this->order);
		$criteria->compare('begda',$this->begda,true);
		$criteria->compare('endda',$this->endda,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetailSegmentation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
