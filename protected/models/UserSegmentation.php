<?php

/**
 * This is the model class for table "user_segmentation".
 *
 * The followings are the available columns in table 'user_segmentation':
 * @property integer $id
 * @property integer $id_user
 * @property string $title
 * @property string $created_date
 * @property string $updated_date
 * @property integer $status
 */
class UserSegmentation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_segmentation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, status, is_custom', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, title, created_date, updated_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'details' => array(self::HAS_MANY, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'NOW() between begda AND endda'),
            'gender' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'fieldid = "gende" AND NOW() between begda AND endda'),
            'age' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'fieldid = "age" AND NOW() between begda AND endda'),
            'mariage' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'fieldid = "maria" AND NOW() between begda AND endda'),
            'religion' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'fieldid = "relig" AND NOW() between begda AND endda'),
            'salary' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'fieldid = "salar" AND NOW() between begda AND endda'),
            'purchase' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'fieldid = "purch" AND NOW() between begda AND endda'),
            'city' => array(self::HAS_ONE, 'UserDetailSegmentation', ['id_segmentation'=>'id'], 'condition'=>'fieldid = "city" AND NOW() between begda AND endda'),
			'detailsCustom' => array(self::HAS_MANY, 'CustomSegmentationDetail', ['id_custom_segmentation'=>'id']),
		);
	}
    
    public function getListEmail()
    {
        $listUser = $this->getListUser();
        $emails = [];
        foreach ($listUser as $key => $value) {
            $emails[] = $value->email;
        }
        return $emails;
    }
    
    public function getListPhone()
    {
        $listUser = $this->getListUser();
        $phones = [];
        foreach ($listUser as $key => $value) {
            $phones[] = str_replace(' ', '', $value->phoneNumber);
        }
        return $phones;
    }
    
    public function getListEmailAllowed()
    {
        $listUser = $this->getListUser();
        $emails = [];
        foreach ($listUser as $key => $value) {
            if (!empty($value->emailSubs)){
                if($value->emailSubs->status_subscribe == 1){
                    $emails[] = $value->email;
                }
            }
        }
        return $emails;
    }
    
    public function getListPhoneAllowed()
    {
        $listUser = $this->getListUser();
        $phones = [];
        foreach ($listUser as $key => $value) {
            if (!empty($value->emailSubs)){
                if($value->smsSubs->status_subscribe == 1){
                    $phones[] = str_replace(' ', '', $value->phoneNumber);
                }
            }
        }
        return $phones;
    }
    
    public function getListWA()
    {
        $listUser = $this->getListUser();
        $phones = [];
        foreach ($listUser as $key => $value) {
            if (!empty($value->emailSubs)){
                if($value->waSubs->status_subscribe == 1){
                    $phones[] = str_replace(' ', '', $value->phoneNumber);
                }
            }
        }
        return $phones;
    }
    
    public function getListUser()
    {
        return $this->makeQuery($this->details);
    }
    
    public function getMales()
    {
        $gender = UserDetailSegmentation::model()->findByAttributes([
            'fieldid'=>'gende',
            'id_segmentation'=>$this->id            
        ], 'NOW() BETWEEN begda AND endda');
        
        switch ($gender->value) {
            case '1':
                return 100;
                break;
            
            case '2':
                return 0;
                break;
            
            default:
                return 100 - $this->getFemales();
                break;
        }
    }
    
    public function getFemales()
    {
        $gender = UserDetailSegmentation::model()->findByAttributes([
            'fieldid'=>'gende',
            'id_segmentation'=>$this->id            
        ], 'NOW() BETWEEN begda AND endda');
        
        switch ($gender->value) {
            case '1':
                return 0;
                break;
            
            case '2':
                return 100;
                break;
            
            default:
//                var_dump($this->details);exit;
                $result = $this->makeQuery($this->details);
                $male = 0; $female = 0;
                foreach ($result as $key => $value) {
                    if ($value->gender->value == 1)
                        $male++;
                    else
                        $female++;
                }
                return ceil($female / ($male + $female) * 100);
                break;
        }
    }
    
    public function getCountUser()
    {
        return count($this->makeQuery($this->details));
    }
    
    private function makeQuery($details)
    {
        $listQuery = [];
        $listQuery[] = "SELECT id AS id_user FROM user WHERE active = 1";
        $params = [];
//        var_dump($detail);exit;
        foreach ($details as $key => $value) {
            $field = $value->fieldid;
            $value = $value->value;
            switch ($field) {
                case 'gende':
                    if (empty($value))
                        break;
                    $listQuery[] = "SELECT id_user FROM user_personal_information "
                    . "WHERE fieldid = :field1 AND NOW() BETWEEN begda AND endda AND value = :value1";
                    $params[':field1'] = $field;
                    $params[':value1'] = $value;
                    break;
                
                case 'age':
                    $_value = explode('-', $value);
                    $min = $_value[0]; $max = $_value[1];
                    $listQuery[] = "SELECT id_user FROM user_age "
                    . "WHERE age BETWEEN $min AND $max";
                    break;
                
                case 'maria':
                    if (empty($value))
                        break;
                    $listQuery[] = "SELECT id_user FROM user_personal_information "
                    . "WHERE fieldid = :field2 AND NOW() BETWEEN begda AND endda AND value = :value2";
                    $params[':field2'] = $field;
                    $params[':value2'] = $value;
                    break;
                
                case 'relig':                    
                    $orCond = '';
                    $i = 0;
                    $allRelig = false;
                    foreach (CJSON::decode($value) as $key2 => $value2) {
                        if ($key2 == 0){
                            $allRelig = true;
                            break;
                        } else {
                            $orCond .= $i > 0 ? ' OR ':'';
                            $orCond .= 'value = :relig'.$i;
                            $params[':relig'.$i] = $key2;
                        }
                        $i++;
                    }
                    if (!$allRelig){
                        $listQuery[] = 'SELECT id_user FROM user_personal_information '
                            . 'WHERE fieldid = :field3 AND NOW() BETWEEN begda AND endda AND ('.$orCond.')';
                        $params[':field3'] = $field;
                    }
                    break;
                
                case 'salar':
                    $_value = explode('-', $value);
                    $min2 = $_value[0]*1000000; $max2 = $_value[1]*1000000;
                    $listQuery[] = "SELECT id_user FROM user_salar WHERE salar BETWEEN $min2 AND $max2";
                    break;
                
                case 'purch':
                    $_value = explode('|', ltrim($value,'|'));
                    if (count($_value) > 0 AND $_value[0] != ''){
                        $query = "SELECT id_user FROM user_receipt WHERE ";
                        foreach ($_value as $keyP => $valueP) {
                            $query .= ' kategori_barang = :purch'.$keyP.' OR';
                            $params[':purch'.$keyP] = $valueP;
                        }
                        $listQuery[] = rtrim($query, 'OR').' GROUP BY id_user';
                    }
                    break;
                    
                case 'city':
                    $_value = explode('|', ltrim($value,'|'));
                    if (count($_value) > 0 AND $_value[0] != ''){
                        $query = "SELECT id_user FROM user_city WHERE ";
                        foreach ($_value as $keyP => $valueP) {
                            $query .= ' kota = :city'.$keyP.' OR';
                            $params[':city'.$keyP] = $valueP;
                        }
                        $listQuery[] = rtrim($query, 'OR').' GROUP BY id_user';
                    }
                    break;
                    
                default:
                    $listQuery[] = "SELECT id AS id_user FROM user WHERE active = 1";
                    break;
            }
        }
        $result = $this->execQuery($listQuery,$params);
        return $result;
    }
    
    private function execQuery($listQuery,$params){
        $query = '';
        foreach ($listQuery as $key => $value) {
            $query .= $key > 0 ? ' UNION ALL ':'';
            $query .= $value;
        }
//        $queryOuter = 'SELECT * FROM
//                        (SELECT id_user,count(id_user) jml FROM
//                        ('.$query.') T
//                        GROUP BY id_user) T2
//                        WHERE jml >= '.count($listQuery);
        
        $listUser = User::model()->findAll('id IN (SELECT id_user FROM
                        (SELECT id_user,count(id_user) jml FROM
                        ('.$query.') T
                        GROUP BY id_user) T2
                        WHERE jml >= '.count($listQuery).')', $params);
//        $pre = Yii::app()->db->createCommand($queryOuter);
//        $pre->params = $params;
//        $result = $pre->queryAll();
        return $listUser;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'title' => 'Title',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserSegmentation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function scopes() {
        return array(
            'cluster'=>array(
                'condition'=>'is_custom = 0 AND status != 2',
            ),
			'allSegment'=>array(
                'condition'=>'status != 2',
            ),
        );
    }
}
