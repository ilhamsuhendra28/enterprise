<?php

/**
 * This is the model class for table "paket_campaign".
 *
 * The followings are the available columns in table 'paket_campaign':
 * @property integer $id
 * @property string $nama_paket
 * @property integer $kuota_email
 * @property integer $kuota_sms
 * @property integer $kuota_wa
 * @property integer $masa_aktif
 * @property integer $harga
 * @property string $created_date
 * @property string $updated_date
 * @property integer $status
 */
class PaketCampaign extends CActiveRecord implements IECartPosition
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paket_campaign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_paket', 'required'),
			array('kuota_email, kuota_sms, kuota_wa, masa_aktif, harga, status, is_free, available_use_per_user', 'numerical', 'integerOnly'=>true),
			array('nama_paket', 'length', 'max'=>255),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_paket, kuota_email, kuota_sms, kuota_wa, masa_aktif, harga, created_date, updated_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_paket' => 'Nama Paket',
			'kuota_email' => 'Kuota Email',
			'kuota_sms' => 'Kuota Sms',
			'kuota_wa' => 'Kuota Wa',
			'masa_aktif' => 'Masa Aktif',
			'harga' => 'Harga',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_paket',$this->nama_paket,true);
		$criteria->compare('kuota_email',$this->kuota_email);
		$criteria->compare('kuota_sms',$this->kuota_sms);
		$criteria->compare('kuota_wa',$this->kuota_wa);
		$criteria->compare('masa_aktif',$this->masa_aktif);
		$criteria->compare('harga',$this->harga);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>10),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaketCampaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	function getId(){
        return $this->id;
    }
	
	function getName(){
        return $this->nama_paket;
    }

    function getPrice(){
        return $this->harga;
    }
}
