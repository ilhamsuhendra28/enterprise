<?php

/**
 * This is the model class for table "history_blast_paket".
 *
 * The followings are the available columns in table 'history_blast_paket':
 * @property integer $id
 * @property integer $id_campaign
 * @property integer $id_user_paket_campaign
 * @property integer $total
 * @property string $created_date
 */
class HistoryBlastPaket extends CActiveRecord
{
	
	public $subscribed_id,$nama_paket,$total_blast_campaign,$id_user,$title,$total;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'history_blast_paket';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_campaign, id_user_paket_campaign, total', 'numerical', 'integerOnly'=>true),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_campaign, id_user_paket_campaign, total, created_date, id_user', 'safe', 'on'=>'search'),
			array('id, id_campaign, id_user_paket_campaign, total, created_date, id_user', 'safe', 'on'=>'searchHistoryBlast'),
			array('id, id_campaign, id_user_paket_campaign, total, created_date, id_user', 'safe', 'on'=>'searchHistoryBlastDetail'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_campaign' => 'Id Campaign',
			'id_user_paket_campaign' => 'Id User Paket Campaign',
			'total' => 'Total',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_campaign',$this->id_campaign);
		$criteria->compare('id_user_paket_campaign',$this->id_user_paket_campaign);
		$criteria->compare('total',$this->total);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function searchHistoryBlast()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias 		= 'a';
		$criteria->select 		= 'a.id_user_paket_campaign as subscribed_id, c.nama_paket, sum(a.total) as total_blast_campaign';
		$criteria->join 		= 'LEFT JOIN user_paket_campaign b on a.id_user_paket_campaign = b.id 
								   LEFT JOIN paket_campaign c on b.id_paket_campaign = c.id';
		$criteria->condition 	= 'b.id_user = :userid';
		$criteria->params 		= array(':userid'=>$this->id_user);
		$criteria->group 		= 'a.id_user_paket_campaign';
		
		/* select a.id_user_paket_campaign as subscribed_id, c.nama_paket, sum(a.total) as total_blast_campaign
		from history_blast_paket a
		LEFT JOIN user_paket_campaign b on a.id_user_paket_campaign = b.id
		LEFT JOIN paket_campaign c on b.id_paket_campaign = c.id
		#LEFT JOIN campaign d on a.id_campaign = d.id
		GROUP BY a.id_user_paket_campaign */

		/* $criteria->compare('id',$this->id);
		$criteria->compare('id_campaign',$this->id_campaign);
		$criteria->compare('id_user_paket_campaign',$this->id_user_paket_campaign);
		$criteria->compare('total',$this->total);
		$criteria->compare('created_date',$this->created_date,true); */

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchHistoryBlastDetail()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias 		= 'a';
		$criteria->select 		= 'a.id_user_paket_campaign as subscribed_id, c.nama_paket, a.id_campaign, d.title, sum(a.total) as total';
		$criteria->join 		= 'LEFT JOIN user_paket_campaign b on a.id_user_paket_campaign = b.id
								   LEFT JOIN paket_campaign c on b.id_paket_campaign = c.id
								   LEFT JOIN campaign d on a.id_campaign = d.id';
		$criteria->condition 	= 'b.id_user = :userid AND a.id_user_paket_campaign = :id_user_paket_campaign';
		$criteria->params 		= array(':userid'=>$this->id_user,':id_user_paket_campaign'=>$this->id_user_paket_campaign);
		$criteria->group 		= 'a.id_user_paket_campaign, a.id_campaign';
		
		/* select a.id_user_paket_campaign as subscribed_id, c.nama_paket, a.id_campaign, d.title, sum(a.total) as total
		from history_blast_paket a
		LEFT JOIN user_paket_campaign b on a.id_user_paket_campaign = b.id
		LEFT JOIN paket_campaign c on b.id_paket_campaign = c.id
		LEFT JOIN campaign d on a.id_campaign = d.id
		where b.id_user = 6 AND a.id_user_paket_campaign = 1
		GROUP BY a.id_user_paket_campaign, a.id_campaign */


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HistoryBlastPaket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
