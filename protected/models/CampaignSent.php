<?php

/**
 * This is the model class for table "campaign_sent".
 *
 * The followings are the available columns in table 'campaign_sent':
 * @property integer $id
 * @property integer $id_campaign
 * @property string $sent_date_time
 * @property string $campaign_guid
 * @property integer $total_sent
 */
class CampaignSent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'campaign_sent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_campaign, total_sent', 'numerical', 'integerOnly'=>true),
			array('campaign_guid', 'length', 'max'=>255),
			array('sent_date_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_campaign, sent_date_time, campaign_guid, total_sent', 'safe', 'on'=>'search'),
			array('id, id_campaign, sent_date_time, campaign_guid, total_sent', 'safe', 'on'=>'searchByCampaign'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_campaign' => 'Id Campaign',
			'sent_date_time' => 'Sent Date Time',
			'campaign_guid' => 'Campaign Guid',
			'total_sent' => 'Total Sent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_campaign',$this->id_campaign);
		$criteria->compare('sent_date_time',$this->sent_date_time,true);
		$criteria->compare('campaign_guid',$this->campaign_guid,true);
		$criteria->compare('total_sent',$this->total_sent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function searchByCampaign($id_campaign)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_campaign',$id_campaign);
		$criteria->compare('sent_date_time',$this->sent_date_time,true);
		$criteria->compare('campaign_guid',$this->campaign_guid,true);
		$criteria->compare('total_sent',$this->total_sent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>10),
			'sort'=>array(
				'defaultOrder'=>'sent_date_time DESC',
        	),
			
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CampaignSent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
