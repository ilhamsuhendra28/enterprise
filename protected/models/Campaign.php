<?php

/**
 * This is the model class for table "campaign".
 *
 * The followings are the available columns in table 'campaign':
 * @property integer $id
 * @property integer $id_user
 * @property integer $subject
 * @property string $title
 * @property string $description
 * @property integer $email_blast
 * @property integer $sms_blast
 * @property integer $wa_blast
 * @property string $post_date
 * @property string $end_date
 * @property string $post_time
 * @property string $created_date
 * @property string $updated_date
 * @property integer $status
 * @property integer $total_sent
 * @property integer $is_sent
 
 */
class Campaign extends CActiveRecord
{
	
	public $images ;
	
	public $month_number;
	public $month_name;
	public $countTotalSent ;
	
	public $email;
	public $email_name;
	
	public $dayNumber;
	public $dayName;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'campaign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, subject, title, sent_type', 'required'),
			array('id_user, email_blast, sms_blast, wa_blast, total_sent, status, is_sent, sent_type', 'numerical', 'integerOnly'=>true),
			array('subject, title', 'length', 'max'=>255),
			array('created_date, updated_date, images, post_date, end_date, post_time, total_sent, is_sent, sent_type, begda, endda, dayNumber, dayName, campaign_template_id, campaign_template_json', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, subject, title, description, email_blast, sms_blast, post_date, end_date, post_time, created_date, updated_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'details' => array(self::HAS_MANY, 'CampaignDetail', 'id_campaign'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'subject' => 'Subject',
			'title' => 'Title',
			'description' => 'Description',
			'email_blast' => 'Email Blast',
			'sms_blast' => 'Sms Blast',
			'wa_blast' => 'WA Blast',
			'post_date' => 'Post Date',
			'end_date' => 'End Date',
			'post_time' => 'Post Time',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'status' => 'Status',
			'total_sent' => 'Total Sent',
			'is_sent'=> 'Is Sent',
			'sent_type'=> 'Send Type',
			'begda'=> 'Begin Date',
			'endda'=> 'End Date',
			'dayNumber'=>'Send at date',
			'dayName'=>'Send at day',
			'campaign_template_id'=>'Template',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition = 'status != 2';
		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('email_blast',$this->email_blast);
		$criteria->compare('sms_blast',$this->sms_blast);
		$criteria->compare('wa_blast',$this->wa_blast);
		$criteria->compare('post_date',$this->post_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('post_time',$this->post_time,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('total_sent',$this->total_sent);
		$criteria->compare('is_sent',$this->is_sent);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'created_date DESC',
			),
			'pagination'=>array(
				'pageSize'=>10,
			)
		));
	}
	
	
	public function countTotalSentData($year = 2018,$field_type)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		/* 
		
		$criteria->select = "sum(total_sent) as countTotalSent";
		$criteria->condition = "year(post_date) = :post_date AND status != 2";
		$criteria->params = array(':post_date'=>$year);
		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('email_blast',$this->email_blast);
		$criteria->compare('sms_blast',$this->sms_blast);
		$criteria->compare('wa_blast',$this->wa_blast);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_sent',$this->is_sent); 
		
		$data = Campaign::model()->find($criteria);
		if($data->countTotalSent != null){
			return $data->countTotalSent;
		}else{
			return 0 ;
		}
		
		*/
		
		$sql = "	
				SELECT sum(a.total_sent) as countTotalSent
				FROM campaign_sent a
				LEFT JOIN campaign b ON a.id_campaign = b.id
				where 
				b.id_user = :id_user
				AND ".$field_type." = 1
				AND b.status != 2
				AND year(a.sent_date_time) = :year
				";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(
					':id_user'=>$this->id_user,
					':year'=>$year,
		);
        $row = $query->queryRow();
		
		if($row['countTotalSent'] != null){
			return $row['countTotalSent'];
		}else{
			return 0 ;
		}
		
		

		
	}
	
	
	public function overviewDataTotalSent($year = 2018,$field_type)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		

		$criteria=new CDbCriteria;

		/* 
		
		$criteria->select = "MONTH(post_date) as month_number, MONTHNAME(post_date) as month_name, sum(total_sent) as countTotalSent";
		$criteria->condition = "year(post_date) = :post_date AND status != 2";
		$criteria->params = array(':post_date'=>$year);
		$criteria->group = "month(post_date)";
		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('email_blast',$this->email_blast);
		$criteria->compare('sms_blast',$this->sms_blast);
		$criteria->compare('wa_blast',$this->wa_blast);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_sent',$this->is_sent);
		
		$data = Campaign::model()->findAll($criteria); 
		
		return $data ;
		*/
		
		$sql = "	
				
				SELECT  
				MONTH(a.sent_date_time) as month_number, 
				MONTHNAME(a.sent_date_time) as month_name,
				sum(a.total_sent) as countTotalSent
				FROM campaign_sent a
				LEFT JOIN campaign b ON a.id_campaign = b.id
				where 
				b.id_user = :id_user
				AND ".$field_type." = 1
				AND b.status != 2
				AND year(a.sent_date_time) = :year
				group by month(a.sent_date_time)
	
	
				";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(
					':id_user'=>$this->id_user,
					':year'=>$year,
		);
        $row = $query->queryAll();
		/* print_r($row);
		exit; */
		
		return $row;
		
		

		
	}
	
	
	public function overviewDataTotalSentByDate($field_type='email_blast',$year = 2018,$month='',$start_date='',$end_date='')
	{
		
		if ($month==''){
			$month = intval( date("m") );
		}else{
			$month = intval($month);
		}
		
		if ($start_date=='' || $end_date =='' ){
			$start_date = $year.date("-m-").'01';
			$end_date = $year.date("-m-t");
			
		}
		
		/* $sql = "
				SELECT 
				date(post_date) as date, 
				sum(total_sent) as countTotalSent 
				FROM campaign
				WHERE
				year(post_date) = :year
				AND MONTH(post_date) = :month
				AND ( date(post_date) >= :start_date AND date(post_date) <= :end_date )
				AND id_user = :id_user
				AND ".$field_type." = 1
				AND status != 2
				GROUP BY date(post_date)
				"; */
		
		$sql = "
				SELECT date(a.sent_date_time) as date, sum(a.total_sent) as countTotalSent
				FROM campaign_sent a
				LEFT JOIN campaign b ON a.id_campaign = b.id
				where 
				b.id_user = :id_user
				AND ".$field_type." = 1
				AND b.status != 2
				AND year(a.sent_date_time) = :year
				AND MONTH(a.sent_date_time) = :month
				AND ( date(a.sent_date_time) >= :start_date AND date(post_date) <= :end_date )
				group by date(a.sent_date_time)
				";
				
				
					
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(
					':id_user'=>Yii::app()->user->id,
					':year'=>$year,
					':month'=>$month,
					':start_date'=>$start_date,
					':end_date'=>$end_date,
		);
        $row = $query->queryAll();
		
		
		$arrayDataOverViewsTotalSentByDate= array();
		foreach($row as $total_view){
			$date = $total_view['date'];
			$arrayDataOverViewsTotalSentByDate[$date] = $total_view['countTotalSent'];
		}

		$arrayReturn = array();
		$start_date_flag = $start_date;
		while ($start_date_flag <= $end_date)
		{
			
			//echo $start_date_flag . "<br>";
			if (array_key_exists($start_date_flag,$arrayDataOverViewsTotalSentByDate)){
				$arrayReturn[$start_date_flag] = $arrayDataOverViewsTotalSentByDate[$start_date_flag];
			}else{
				$arrayReturn[$start_date_flag] = 0;
			}
			$start_date_flag = date('Y-m-d', strtotime($start_date_flag . ' +1 day'));
		}
		
		
		/* echo '<pre>';
		print_r($arrayReturn);
		exit; */ 
		return $arrayReturn;
		
	}
	
	
	public function countTotalPaketCampaign()
	{
		
		$sql = "	
				select 
				sum(a.sisa_email) total_sisa_email, sum(a.sisa_sms) as total_sisa_sms, sum(a.sisa_wa) as total_sisa_wa
				from user_paket_campaign a
				left join paket_campaign b on a.id_paket_campaign = b.id
				where 

				b.status = 1 AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa 
				AND  a.id_user = :id_user

				GROUP BY a.id_user

				";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(
					':id_user'=>$this->id_user,
		);
        $row = $query->queryRow();
		
		if($row!= null){
			return $row;
		}else{
			return array(); ;
		}
		
		
		

		
	}
	
	
	public function getStatusCampaign()
	{
		if ($this->status == 0 && $this->is_sent == 0 ){
			return 'Draft';
		}
		else if ($this->status == 1 && $this->is_sent == 0){
			return 'On-Going';
		}
		
		else if ($this->status == 1 && $this->is_sent == 1){
			return 'Sent';
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Campaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
