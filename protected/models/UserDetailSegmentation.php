<?php

/**
 * This is the model class for table "user_detail_segmentation".
 *
 * The followings are the available columns in table 'user_detail_segmentation':
 * @property integer $id
 * @property integer $id_detail_segmentation
 * @property string $fieldid
 * @property integer $id_segmentation
 * @property string $value
 * @property string $begda
 * @property string $endda
 */
class UserDetailSegmentation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_detail_segmentation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_detail_segmentation, id_segmentation', 'numerical', 'integerOnly'=>true),
			array('fieldid', 'length', 'max'=>5),
			array('value', 'length', 'max'=>255),
			array('begda, endda', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_detail_segmentation, fieldid, id_segmentation, value, begda, endda', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'text' => array(self::HAS_ONE, 'MasterValue', ['masterid'=>'fieldid', 'id'=>'value']),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_detail_segmentation' => 'Id Detail Segmentation',
			'fieldid' => 'Fieldid',
			'id_segmentation' => 'Id Segmentation',
			'value' => 'Value',
			'begda' => 'Begda',
			'endda' => 'Endda',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_detail_segmentation',$this->id_detail_segmentation);
		$criteria->compare('fieldid',$this->fieldid,true);
		$criteria->compare('id_segmentation',$this->id_segmentation);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('begda',$this->begda,true);
		$criteria->compare('endda',$this->endda,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserDetailSegmentation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function getTextRelig()
    {
        $listRelig = CJSON::decode($this->value);
        $textRelig = '';
        foreach ($listRelig as $key => $value) {
            switch ($key) {
                case 0:
                    $textRelig .= 'All, ';
                    break;
                
                case 8:
                    $textRelig .= 'Islam, ';
                    break;
                
                case 9:
                    $textRelig .= 'Kristen, ';
                    break;
                
                case 10:
                    $textRelig .= 'Katolik, ';
                    break;
                
                case 11:
                    $textRelig .= 'Hindu, ';
                    break;
                
                case 12:
                    $textRelig .= 'Budha, ';
                    break;

                default:
                    break;
            }
        }
        return rtrim($textRelig, ', ');
    }
}
