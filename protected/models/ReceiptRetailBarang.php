<?php

/**
 * This is the model class for table "receipt_retail_barang".
 *
 * The followings are the available columns in table 'receipt_retail_barang':
 * @property integer $id_barang
 * @property integer $id_receipt_retail
 * @property integer $id_kategori_barang
 * @property string $nama_barang
 * @property string $jumlah_barang
 * @property integer $id_satuan_barang
 * @property string $harga_barang
 * @property string $created_date
 * @property string $created_by
 */
class ReceiptRetailBarang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'receipt_retail_barang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_receipt_retail, id_kategori_barang, id_satuan_barang', 'numerical', 'integerOnly'=>true),
			array('nama_barang, jumlah_barang, harga_barang, created_by', 'length', 'max'=>255),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_barang, id_receipt_retail, id_kategori_barang, nama_barang, jumlah_barang, id_satuan_barang, harga_barang, created_date, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_barang' => 'Id Barang',
			'id_receipt_retail' => 'Id Receipt Retail',
			'id_kategori_barang' => 'Id Kategori Barang',
			'nama_barang' => 'Nama Barang',
			'jumlah_barang' => 'Jumlah Barang',
			'id_satuan_barang' => 'Id Satuan Barang',
			'harga_barang' => 'Harga Barang',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_barang',$this->id_barang);
		$criteria->compare('id_receipt_retail',$this->id_receipt_retail);
		$criteria->compare('id_kategori_barang',$this->id_kategori_barang);
		$criteria->compare('nama_barang',$this->nama_barang,true);
		$criteria->compare('jumlah_barang',$this->jumlah_barang,true);
		$criteria->compare('id_satuan_barang',$this->id_satuan_barang);
		$criteria->compare('harga_barang',$this->harga_barang,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReceiptRetailBarang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
