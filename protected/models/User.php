<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $point
 * @property string $cash
 * @property integer $active
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('active', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>255),
			array('point, cash', 'length', 'max'=>10),
			array('password', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, point, cash, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'gender' => array(self::HAS_ONE, 'UserPersonalInformation', ['id_user'=>'id'], 'condition'=>'fieldid = "gende" AND NOW() BETWEEN begda AND endda'),
            'religion' => array(self::HAS_ONE, 'UserPersonalInformation', ['id_user'=>'id'], 'condition'=>'fieldid = "relig" AND NOW() BETWEEN begda AND endda'),
            'children' => array(self::HAS_ONE, 'UserPersonalInformation', ['id_user'=>'id'], 'condition'=>'fieldid = "child" AND NOW() BETWEEN begda AND endda'),
            'city' => array(self::HAS_ONE, 'UserPersonalInformation', ['id_user'=>'id'], 'condition'=>'fieldid = "city" AND NOW() BETWEEN begda AND endda'),
            'mariage' => array(self::HAS_ONE, 'UserPersonalInformation', ['id_user'=>'id'], 'condition'=>'fieldid = "maria" AND NOW() BETWEEN begda AND endda'),
            'age' => array(self::HAS_ONE, 'UserAge', ['id_user'=>'id']),
            'salary' => array(self::HAS_ONE, 'UserSalar', ['id_user'=>'id']),
            'emailSubs' => array(self::HAS_ONE, 'UserSubscribe', ['id_user'=>'id'], 'condition'=>'id_media = 1'),
            'smsSubs' => array(self::HAS_ONE, 'UserSubscribe', ['id_user'=>'id'], 'condition'=>'id_media = 2'),
            'waSubs' => array(self::HAS_ONE, 'UserSubscribe', ['id_user'=>'id'], 'condition'=>'id_media = 3'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'point' => 'Point',
			'cash' => 'Cash',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('point',$this->point,true);
		$criteria->compare('cash',$this->cash,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function scopes() {
        return array(
            'active'=>array(
                'condition'=>'active = 1',
            ),
        );
    }
    
    public function getPhoneNumber()
    {
        $info = UserPersonalInformation::model()->findByAttributes(['id_user'=>$this->id,'fieldid'=>'phone'], 'NOW() BETWEEN begda AND endda');
        if (!empty($info))
            return $info->value;
        else 
            return 0;
    }
}
