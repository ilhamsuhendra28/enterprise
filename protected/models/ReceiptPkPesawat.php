<?php

/**
 * This is the model class for table "receipt_pk_pesawat".
 *
 * The followings are the available columns in table 'receipt_pk_pesawat':
 * @property integer $id_pesawat
 * @property integer $id_receipt_pk
 * @property string $kode_booking
 * @property string $no_identitas
 * @property string $nama_maskapai
 * @property string $kelas_pesawat
 * @property string $berangkat_dari
 * @property string $tgl_berangkat
 * @property string $jam_berangkat
 * @property string $tujuan_ke
 * @property string $tgl_sampai
 * @property string $jam_sampai
 * @property string $gate
 * @property string $no_penerbangan
 * @property string $no_kursi
 * @property string $harga_tiket
 * @property string $jenis_pembayaran
 * @property string $nomor_tiket
 * @property string $tgl_pembelian_tiket
 * @property string $jam_pembelian_tiket
 * @property string $created_date
 * @property string $created_by
 */
class ReceiptPkPesawat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'receipt_pk_pesawat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_receipt_pk', 'numerical', 'integerOnly'=>true),
			array('kode_booking, no_identitas, nama_maskapai, kelas_pesawat, no_penerbangan, no_kursi, jenis_pembayaran, created_by', 'length', 'max'=>100),
			array('berangkat_dari, tujuan_ke, harga_tiket, nomor_tiket', 'length', 'max'=>255),
			array('jam_berangkat, jam_sampai, gate, jam_pembelian_tiket', 'length', 'max'=>20),
			array('tgl_berangkat, tgl_sampai, tgl_pembelian_tiket, created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pesawat, id_receipt_pk, kode_booking, no_identitas, nama_maskapai, kelas_pesawat, berangkat_dari, tgl_berangkat, jam_berangkat, tujuan_ke, tgl_sampai, jam_sampai, gate, no_penerbangan, no_kursi, harga_tiket, jenis_pembayaran, nomor_tiket, tgl_pembelian_tiket, jam_pembelian_tiket, created_date, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pesawat' => 'Id Pesawat',
			'id_receipt_pk' => 'Id Receipt Pk',
			'kode_booking' => 'Kode Booking',
			'no_identitas' => 'No Identitas',
			'nama_maskapai' => 'Nama Maskapai',
			'kelas_pesawat' => 'Kelas Pesawat',
			'berangkat_dari' => 'Berangkat Dari',
			'tgl_berangkat' => 'Tgl Berangkat',
			'jam_berangkat' => 'Jam Berangkat',
			'tujuan_ke' => 'Tujuan Ke',
			'tgl_sampai' => 'Tgl Sampai',
			'jam_sampai' => 'Jam Sampai',
			'gate' => 'Gate',
			'no_penerbangan' => 'No Penerbangan',
			'no_kursi' => 'No Kursi',
			'harga_tiket' => 'Harga Tiket',
			'jenis_pembayaran' => 'Jenis Pembayaran',
			'nomor_tiket' => 'Nomor Tiket',
			'tgl_pembelian_tiket' => 'Tgl Pembelian Tiket',
			'jam_pembelian_tiket' => 'Jam Pembelian Tiket',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pesawat',$this->id_pesawat);
		$criteria->compare('id_receipt_pk',$this->id_receipt_pk);
		$criteria->compare('kode_booking',$this->kode_booking,true);
		$criteria->compare('no_identitas',$this->no_identitas,true);
		$criteria->compare('nama_maskapai',$this->nama_maskapai,true);
		$criteria->compare('kelas_pesawat',$this->kelas_pesawat,true);
		$criteria->compare('berangkat_dari',$this->berangkat_dari,true);
		$criteria->compare('tgl_berangkat',$this->tgl_berangkat,true);
		$criteria->compare('jam_berangkat',$this->jam_berangkat,true);
		$criteria->compare('tujuan_ke',$this->tujuan_ke,true);
		$criteria->compare('tgl_sampai',$this->tgl_sampai,true);
		$criteria->compare('jam_sampai',$this->jam_sampai,true);
		$criteria->compare('gate',$this->gate,true);
		$criteria->compare('no_penerbangan',$this->no_penerbangan,true);
		$criteria->compare('no_kursi',$this->no_kursi,true);
		$criteria->compare('harga_tiket',$this->harga_tiket,true);
		$criteria->compare('jenis_pembayaran',$this->jenis_pembayaran,true);
		$criteria->compare('nomor_tiket',$this->nomor_tiket,true);
		$criteria->compare('tgl_pembelian_tiket',$this->tgl_pembelian_tiket,true);
		$criteria->compare('jam_pembelian_tiket',$this->jam_pembelian_tiket,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReceiptPkPesawat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
