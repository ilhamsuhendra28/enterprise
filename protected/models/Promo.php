<?php

/**
 * This is the model class for table "promo".
 *
 * The followings are the available columns in table 'promo':
 * @property integer $id
 * @property integer $id_company
 * @property string $companyid
 * @property string $id_category
 * @property string $title
 * @property string $content
 * @property string $point
 * @property string $cash
 * @property string $begda
 * @property string $endda
 * @property integer $max_buy
 * @property integer $total_views
 * @property integer $total_clicks
 * @property integer $total_buys
 */
class Promo extends CActiveRecord
{
	public $promo_mode;
	public $promo_code;
	public $jumlah;
	public $images ;
	
	public $month_number;
	public $month_name;
	
	public $countTotalViews ;
	public $countTotalClicks ;
	public $countTotalBuyer;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('promo_code', 'required'),
			array('id_company, id_category, max_buy, jumlah, total_views, total_clicks, total_buys', 'numerical', 'integerOnly'=>true),
			array('point, cash', 'numerical'),
			array('companyid', 'length', 'max'=>8),
			array('title', 'length', 'max'=>255),
			array('point, cash', 'length', 'max'=>20),
			array('content, begda, endda, promo_mode, promo_code, jumlah, images', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_company, id_category, companyid, title, content, point, cash, begda, endda, max_buy, total_views, total_clicks, total_buys', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'PromoCategory', ['id_category'=>'id']),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_company' => 'Id Company',
			'companyid' => 'Companyid',
			'id_category'=> 'ID Category',
			'title' => 'Title',
			'content' => 'Content',
			'point' => 'Point',
			'cash' => 'Cash',
			'begda' => 'Begin Date',
			'endda' => 'End Date',
			'max_buy' => 'Max Buy',
			'total_views'=>'Total Views',
			'total_clicks'=>'Total Clicks',
			'total_buys'=>'Total Buys',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('id_category',$this->id_category);
		$criteria->compare('companyid',$this->companyid,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('point',$this->point,true);
		$criteria->compare('cash',$this->cash,true);
		$criteria->compare('begda',$this->begda,true);
		$criteria->compare('endda',$this->endda,true);
		$criteria->compare('max_buy',$this->max_buy);
		$criteria->compare('total_views',$this->total_views);
		$criteria->compare('total_clicks',$this->total_clicks);
		$criteria->compare('total_buys',$this->total_buys);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			//'pagination'=>array('pageSize'=>1),
			'sort'=>array(
				'defaultOrder'=>'begda DESC',
			),
		));
	}
	
	
	public function countTotalViewsData($year = 2018)
	{
		
		$sql = "SELECT count(*) as countTotalViews
			FROM promo_views_data a
			LEFT JOIN promo b ON a.id_promo = b.id
			where 
			b.id_company = :id_user
			AND year(a.views_date_time) = :year" ;
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(':id_user'=>Yii::app()->user->id,':year'=>$year);
        $row = $query->queryRow();
		
		return $row['countTotalViews'];
		
	}
	
	public function overviewViewsData($year = 2018)
	{
		$sql = "SELECT 
				MONTH(a.views_date_time) as month_number, MONTHNAME(a.views_date_time) as month_name, count(*) as countTotalViews
				FROM promo_views_data a
				LEFT JOIN promo b ON a.id_promo = b.id
				where 
				b.id_company = :id_user
				AND year(a.views_date_time) =:year
				group by month(a.views_date_time)";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(':id_user'=>Yii::app()->user->id,':year'=>$year);
        $row = $query->queryAll();
		
		/* echo '<pre>';
		print_r($row);
		exit; */
		return $row;
		
	}
	
	public function overviewViewsDataByDate($year = 2018,$month='',$start_date='',$end_date='')
	{
		
		if ($month==''){
			$month = intval( date("m") );
		}else{
			$month = intval($month);
		}
		
		if ($start_date=='' || $end_date =='' ){
			$start_date = $year.date("-m-").'01';
			$end_date = $year.date("-m-t");
			
		}
		
		$sql = "
				SELECT date(a.views_date_time) as date, count(*) as countTotalViews
				FROM promo_views_data a
				LEFT JOIN promo b ON a.id_promo = b.id
				where 
				b.id_company = :id_user
				AND year(a.views_date_time) = :year
				AND MONTH(a.views_date_time) = :month
				AND ( date(a.views_date_time) >= :start_date AND date(a.views_date_time) <= :end_date )
				group by date(a.views_date_time)
				";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(
					':id_user'=>Yii::app()->user->id,
					':year'=>$year,
					':month'=>$month,
					':start_date'=>$start_date,
					':end_date'=>$end_date,
		);
        $row = $query->queryAll();
		
		
		$arrayDataOverViewsDataByDate= array();
		foreach($row as $total_view){
			$date = $total_view['date'];
			$arrayDataOverViewsDataByDate[$date] = $total_view['countTotalViews'];
		}

		$arrayReturn = array();
		$start_date_flag = $start_date;
		while ($start_date_flag <= $end_date)
		{
			
			//echo $start_date_flag . "<br>";
			if (array_key_exists($start_date_flag,$arrayDataOverViewsDataByDate)){
				$arrayReturn[$start_date_flag] = $arrayDataOverViewsDataByDate[$start_date_flag];
			}else{
				$arrayReturn[$start_date_flag] = 0;
			}
			$start_date_flag = date('Y-m-d', strtotime($start_date_flag . ' +1 day'));
		}
		
		
		/* echo '<pre>';
		print_r($arrayReturn);
		exit; */ 
		return $arrayReturn;
		
	}
	
	
	
	public function countTotalClicksData($year = 2018)
	{
		
		$sql = "SELECT count(*) as countTotalClicks
				FROM promo_clicks_data a
				LEFT JOIN promo b ON a.id_promo = b.id
				where 
				b.id_company = :id_user
				AND year(a.clicks_date_time) = :year";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(':id_user'=>Yii::app()->user->id,':year'=>$year);
        $row = $query->queryRow();
		
		return $row['countTotalClicks'];
		
	}
	
	public function overviewClicksData($year = 2018)
	{
		$sql = "SELECT 
				MONTH(a.clicks_date_time) as month_number, MONTHNAME(a.clicks_date_time) as month_name, count(*) as countTotalClicks
				FROM promo_clicks_data a
				LEFT JOIN promo b ON a.id_promo = b.id
				where 
				b.id_company = :id_user
				AND year(a.clicks_date_time) = :year
				group by month(a.clicks_date_time)";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(':id_user'=>Yii::app()->user->id,':year'=>$year);
        $row = $query->queryAll();
		
		/* echo '<pre>';
		print_r($row);
		exit;  */
		return $row;
		
	}
	
	public function overviewClicksDataByDate($year = 2018,$month='',$start_date='',$end_date='')
	{
		
		if ($month==''){
			$month = intval( date("m") );
		}else{
			$month = intval($month);
		}
		
		if ($start_date=='' || $end_date =='' ){
			$start_date = $year.date("-m-").'01';
			$end_date = $year.date("-m-t");
			
		}
		
		$sql = "
			SELECT date(a.clicks_date_time) as date, count(*) as countTotalClicks
			FROM promo_clicks_data a
			LEFT JOIN promo b ON a.id_promo = b.id
			where 
			b.id_company = :id_user
			AND year(a.clicks_date_time) = :year
			AND MONTH(a.clicks_date_time) = :month
			AND ( date(a.clicks_date_time) >= :start_date AND date(a.clicks_date_time) <= :end_date )
			group by date(a.clicks_date_time)
			";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(
					':id_user'=>Yii::app()->user->id,
					':year'=>$year,
					':month'=>$month,
					':start_date'=>$start_date,
					':end_date'=>$end_date,
		);
        $row = $query->queryAll();
		
		
		$arrayDataOverClicksDataByDate= array();
		foreach($row as $total_view){
			$date = $total_view['date'];
			$arrayDataOverClicksDataByDate[$date] = $total_view['countTotalClicks'];
		}

		$arrayReturn = array();
		$start_date_flag = $start_date;
		while ($start_date_flag <= $end_date)
		{
			
			//echo $start_date_flag . "<br>";
			if (array_key_exists($start_date_flag,$arrayDataOverClicksDataByDate)){
				$arrayReturn[$start_date_flag] = $arrayDataOverClicksDataByDate[$start_date_flag];
			}else{
				$arrayReturn[$start_date_flag] = 0;
			}
			$start_date_flag = date('Y-m-d', strtotime($start_date_flag . ' +1 day'));
		}
		
		
		/* echo '<pre>';
		print_r($arrayReturn);
		exit;  */
		return $arrayReturn;
		
	}
	
	public function countTotalBuyerData($year = 2018)
	{
		
		$sql = "SELECT count(*) as countTotalBuyer
				FROM promo_kode a
				LEFT JOIN promo b ON a.id_promo = b.id
				where 
				a.used = 1
				AND b.id_company = :id_user
				AND year(a.used_date_time) = :year";
		
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(':id_user'=>Yii::app()->user->id,':year'=>$year);
        $row = $query->queryRow();
		
		return $row['countTotalBuyer'];
		
	}
	
	
	public function overviewBuyerData($year = 2018)
	{
		$sql = "SELECT 
				MONTH(a.used_date_time) as month_number, MONTHNAME(a.used_date_time) as month_name, count(*) as countTotalBuyer
				FROM promo_kode a
				LEFT JOIN promo b ON a.id_promo = b.id
				where 
				a.used = 1
				AND b.id_company = :id_user
				AND year(a.used_date_time) = :year
				group by month(a.used_date_time)";
		
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(':id_user'=>Yii::app()->user->id,':year'=>$year);
        $row = $query->queryAll();
		
		/* echo '<pre>';
		print_r($row);
		exit; */  
		return $row;
		
	}
	
	public function overviewBuyerDataByDate($year = 2018,$month='',$start_date='',$end_date='')
	{
		
		if ($month==''){
			$month = intval( date("m") );
		}else{
			$month = intval($month);
		}
		
		if ($start_date=='' || $end_date =='' ){
			$start_date = $year.date("-m-").'01';
			$end_date = $year.date("-m-t");
			
		}
		
		$sql = "
				SELECT date(a.used_date_time) as date, count(*) as countTotalBuyer
				FROM promo_kode a
				LEFT JOIN promo b ON a.id_promo = b.id
				where 
				a.used = 1
				AND b.id_company = :id_user
				AND year(a.used_date_time) = :year
				AND MONTH(a.used_date_time) = :month
				AND ( date(a.used_date_time) >= :start_date AND date(a.used_date_time) <= :end_date )
				group by date(a.used_date_time)";
		$query = Yii::app()->db->createCommand($sql);
        $query->params = array(
					':id_user'=>Yii::app()->user->id,
					':year'=>$year,
					':month'=>$month,
					':start_date'=>$start_date,
					':end_date'=>$end_date,
		);
        $row = $query->queryAll();
		
		
		$arrayDataOverBuyerDataByDate= array();
		foreach($row as $total_view){
			$date = $total_view['date'];
			$arrayDataOverBuyerDataByDate[$date] = $total_view['countTotalBuyer'];
		}

		$arrayReturn = array();
		$start_date_flag = $start_date;
		while ($start_date_flag <= $end_date)
		{
			
			//echo $start_date_flag . "<br>";
			if (array_key_exists($start_date_flag,$arrayDataOverBuyerDataByDate)){
				$arrayReturn[$start_date_flag] = $arrayDataOverBuyerDataByDate[$start_date_flag];
			}else{
				$arrayReturn[$start_date_flag] = 0;
			}
			$start_date_flag = date('Y-m-d', strtotime($start_date_flag . ' +1 day'));
		}
		
		
		/* echo '<pre>';
		print_r($arrayReturn);
		exit;   */
		return $arrayReturn;
		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Promo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
