<?php

/**
 * This is the model class for table "custom_segmentation_detail".
 *
 * The followings are the available columns in table 'custom_segmentation_detail':
 * @property integer $id
 * @property integer $id_custom_segmentation
 * @property integer $nama
 * @property string $no_hp
 * @property string $email
 * @property string $created_date
 * @property string $updated_date
 * @property integer $status
 */
class CustomSegmentationDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'custom_segmentation_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_custom_segmentation, status', 'required'),
			array('id_custom_segmentation, status', 'numerical', 'integerOnly'=>true),
			array('nama, no_hp', 'length', 'max'=>255),
			array('email', 'length', 'max'=>225),
			array('created_date, updated_date', 'safe'),
			array('email','email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_custom_segmentation, nama, no_hp, email, created_date, updated_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_custom_segmentation' => 'Id Custom Segmentation',
			'nama'=>'Nama',
			'no_hp' => 'No Hp',
			'email' => 'Email',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_custom_segmentation',$this->id_custom_segmentation);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomSegmentationDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
