<?php

/**
 * This is the model class for table "user_paket_campaign".
 *
 * The followings are the available columns in table 'user_paket_campaign':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_paket_campaign
 * @property integer $sisa_email
 * @property integer $sisa_sms
 * @property integer $sisa_wa
 * @property string $tgl_kadaluarsa
 * @property string $created_date
 * @property string $updated_date
 * @property integer $status
 * @property string $kode_transaksi
 * @property string $json
 * @property string $tgl_aktif
 */
class UserPaketCampaign extends CActiveRecord
{
	
	public $nama_paket,$kuota_email,$kuota_sms,$kuota_wa,$masa_aktif,$harga,$is_free;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_paket_campaign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, id_paket_campaign', 'required'),
			array('id_user, id_paket_campaign, sisa_email, sisa_sms, sisa_wa, status', 'numerical', 'integerOnly'=>true),
			array('kode_transaksi', 'length', 'max'=>255),
			array('tgl_kadaluarsa, created_date, updated_date, tgl_aktif, json', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, id_paket_campaign, sisa_email, sisa_sms, sisa_wa, tgl_kadaluarsa, created_date, updated_date, status, kode_transaksi, json. tgl_aktif', 'safe', 'on'=>'search'),
			array('id, id_user, id_paket_campaign, sisa_email, sisa_sms, sisa_wa, tgl_kadaluarsa, created_date, updated_date, status, kode_transaksi, json. tgl_aktif', 'safe', 'on'=>'searchAktifPaket'),
			array('id, id_user, id_paket_campaign, sisa_email, sisa_sms, sisa_wa, tgl_kadaluarsa, created_date, updated_date, status, kode_transaksi, json. tgl_aktif', 'safe', 'on'=>'searchInProcessPaket'),
			array('id, id_user, id_paket_campaign, sisa_email, sisa_sms, sisa_wa, tgl_kadaluarsa, created_date, updated_date, status, kode_transaksi, json. tgl_aktif', 'safe', 'on'=>'searchHistoryPaket'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			 'masterPaket' => array(self::BELONGS_TO, 'PaketCampaign', 'id_paket_campaign'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_paket_campaign' => 'Id Paket Campaign',
			'sisa_email' => 'Sisa Email',
			'sisa_sms' => 'Sisa Sms',
			'sisa_wa' => 'Sisa Wa',
			'tgl_kadaluarsa' => 'Tgl Kadaluarsa',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'status' => 'Status',
			'kode_transaksi' => 'Kode Transaksi',
			'json' => 'Json',
			'tgl_aktif'=>'Tgl Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_paket_campaign',$this->id_paket_campaign);
		$criteria->compare('sisa_email',$this->sisa_email);
		$criteria->compare('sisa_sms',$this->sisa_sms);
		$criteria->compare('sisa_wa',$this->sisa_wa);
		$criteria->compare('tgl_kadaluarsa',$this->tgl_kadaluarsa,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('kode_transaksi',$this->kode_transaksi,true);
		$criteria->compare('json',$this->json,true);
		$criteria->compare('tgl_aktif',$this->tgl_aktif,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function searchAktifPaket()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias 		= 'a';
		$criteria->select 		=  'a.id, a.id_user, b.nama_paket, 
									b.kuota_email, b.kuota_sms, b.kuota_wa,
									a.sisa_email, a.sisa_sms, a.sisa_wa,
									a.created_date, b.masa_aktif, b.is_free, a.tgl_aktif, a.tgl_kadaluarsa';
		$criteria->join 		= 'left join paket_campaign b on a.id_paket_campaign = b.id';
		$criteria->condition 	= 'b.status = 1  AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa 
								  AND  a.id_user = :userid';
		$criteria->params 		= array(':userid'=>$this->id_user);
		$criteria->order  		= 'a.created_date	ASC';
		
		
		
		/* $criteria->compare('sisa_email',$this->sisa_email);
		$criteria->compare('sisa_sms',$this->sisa_sms);
		$criteria->compare('sisa_wa',$this->sisa_wa);
		$criteria->compare('tgl_kadaluarsa',$this->tgl_kadaluarsa,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('kode_transaksi',$this->kode_transaksi,true);
		$criteria->compare('json',$this->json,true);
		$criteria->compare('tgl_aktif',$this->tgl_aktif,true); */
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function searchInProcessPaket()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias 		= 'a';
		$criteria->select 		=  'a.id, a.id_paket_campaign, a.id_user, b.nama_paket, 
									b.kuota_email, b.kuota_sms, b.kuota_wa,
									a.sisa_email, a.sisa_sms, a.sisa_wa,
									a.kode_transaksi,
									a.created_date, b.masa_aktif, a.tgl_aktif, a.tgl_kadaluarsa, a.status';
		$criteria->join 		= 'left join paket_campaign b on a.id_paket_campaign = b.id';
		$criteria->condition 	= 'b.status = 1  AND (a.status = 0 OR a.status = 2)
								  AND  a.id_user = :userid';
		$criteria->params 		= array(':userid'=>$this->id_user);
		$criteria->order  		= 'a.created_date	ASC';
		
		
		
		/* $criteria->compare('sisa_email',$this->sisa_email);
		$criteria->compare('sisa_sms',$this->sisa_sms);
		$criteria->compare('sisa_wa',$this->sisa_wa);
		$criteria->compare('tgl_kadaluarsa',$this->tgl_kadaluarsa,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('kode_transaksi',$this->kode_transaksi,true);
		$criteria->compare('json',$this->json,true);
		$criteria->compare('tgl_aktif',$this->tgl_aktif,true); */
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function searchHistoryPaket()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->alias 		= 'a';
		$criteria->select 		=  'a.id, a.id_user, b.nama_paket, 
									b.kuota_email, b.kuota_sms, b.kuota_wa, b.harga,
									a.sisa_email, a.sisa_sms, a.sisa_wa,
									a.created_date, b.masa_aktif, b.is_free, a.status, a.tgl_aktif, a.tgl_kadaluarsa';
		$criteria->join 		= 'left join paket_campaign b on a.id_paket_campaign = b.id';
		$criteria->condition 	= 'b.status = 1  AND a.status = 1 AND date(now()) <= a.tgl_kadaluarsa 
								  AND  a.id_user = :userid';
		$criteria->params 		= array(':userid'=>$this->id_user);
		$criteria->order  		= 'a.tgl_kadaluarsa	DESC';
		
		
		
		/* $criteria->compare('sisa_email',$this->sisa_email);
		$criteria->compare('sisa_sms',$this->sisa_sms);
		$criteria->compare('sisa_wa',$this->sisa_wa);
		$criteria->compare('tgl_kadaluarsa',$this->tgl_kadaluarsa,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('kode_transaksi',$this->kode_transaksi,true);
		$criteria->compare('json',$this->json,true);
		$criteria->compare('tgl_aktif',$this->tgl_aktif,true); */
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>30),
		));
	}
	
	public function getStatusString(){
		if ($this->status == 0){
			return 'Waiting to pay';
		}
		else if ($this->status == 1){
			if ($this->masterPaket->is_free == 0){
				return 'Paid';
			}else{
				return 'Claimed';
			}
		}
		else if ($this->status == 2){
			return 'Expired';
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserPaketCampaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
