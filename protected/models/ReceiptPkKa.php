<?php

/**
 * This is the model class for table "receipt_pk_ka".
 *
 * The followings are the available columns in table 'receipt_pk_ka':
 * @property integer $id_kereta
 * @property integer $id_receipt_pk
 * @property string $kode_booking
 * @property string $no_identitas
 * @property string $nama_ka
 * @property string $kelas_kereta
 * @property string $berangkat_dari
 * @property string $tgl_berangkat
 * @property string $jam_berangkat
 * @property string $tujuan_ke
 * @property string $tgl_sampai
 * @property string $jam_sampai
 * @property string $tipe_penumpang
 * @property string $no_gerbong
 * @property string $no_kursi
 * @property string $tgl_pembelian_tiket
 * @property string $jam_pembelian_tiket
 * @property string $created_date
 * @property string $created_by
 */
class ReceiptPkKa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'receipt_pk_ka';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_receipt_pk', 'numerical', 'integerOnly'=>true),
			array('kode_booking, no_identitas, nama_ka, kelas_kereta, tipe_penumpang, no_gerbong, no_kursi, created_by', 'length', 'max'=>100),
			array('berangkat_dari, tujuan_ke', 'length', 'max'=>255),
			array('jam_berangkat, jam_sampai, jam_pembelian_tiket', 'length', 'max'=>20),
			array('tgl_berangkat, tgl_sampai, tgl_pembelian_tiket, created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kereta, id_receipt_pk, kode_booking, no_identitas, nama_ka, kelas_kereta, berangkat_dari, tgl_berangkat, jam_berangkat, tujuan_ke, tgl_sampai, jam_sampai, tipe_penumpang, no_gerbong, no_kursi, tgl_pembelian_tiket, jam_pembelian_tiket, created_date, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kereta' => 'Id Kereta',
			'id_receipt_pk' => 'Id Receipt Pk',
			'kode_booking' => 'Kode Booking',
			'no_identitas' => 'No Identitas',
			'nama_ka' => 'Nama Ka',
			'kelas_kereta' => 'Kelas Kereta',
			'berangkat_dari' => 'Berangkat Dari',
			'tgl_berangkat' => 'Tgl Berangkat',
			'jam_berangkat' => 'Jam Berangkat',
			'tujuan_ke' => 'Tujuan Ke',
			'tgl_sampai' => 'Tgl Sampai',
			'jam_sampai' => 'Jam Sampai',
			'tipe_penumpang' => 'Tipe Penumpang',
			'no_gerbong' => 'No Gerbong',
			'no_kursi' => 'No Kursi',
			'tgl_pembelian_tiket' => 'Tgl Pembelian Tiket',
			'jam_pembelian_tiket' => 'Jam Pembelian Tiket',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_kereta',$this->id_kereta);
		$criteria->compare('id_receipt_pk',$this->id_receipt_pk);
		$criteria->compare('kode_booking',$this->kode_booking,true);
		$criteria->compare('no_identitas',$this->no_identitas,true);
		$criteria->compare('nama_ka',$this->nama_ka,true);
		$criteria->compare('kelas_kereta',$this->kelas_kereta,true);
		$criteria->compare('berangkat_dari',$this->berangkat_dari,true);
		$criteria->compare('tgl_berangkat',$this->tgl_berangkat,true);
		$criteria->compare('jam_berangkat',$this->jam_berangkat,true);
		$criteria->compare('tujuan_ke',$this->tujuan_ke,true);
		$criteria->compare('tgl_sampai',$this->tgl_sampai,true);
		$criteria->compare('jam_sampai',$this->jam_sampai,true);
		$criteria->compare('tipe_penumpang',$this->tipe_penumpang,true);
		$criteria->compare('no_gerbong',$this->no_gerbong,true);
		$criteria->compare('no_kursi',$this->no_kursi,true);
		$criteria->compare('tgl_pembelian_tiket',$this->tgl_pembelian_tiket,true);
		$criteria->compare('jam_pembelian_tiket',$this->jam_pembelian_tiket,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReceiptPkKa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
