<html>
    <head>
        <title>Bagidata | Indonesia Data Exchange</title>
    	<link rel="icon" type="image/png" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/favicon.png">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/style.css"  media="screen,projection"/>
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>
		<style type="text/css">
			#main-loader:before {
			  content: '';
			  display: block;
			  position: fixed;
			  top: 0;
			  left: 0;
			  width: 100%;
			  height: 100%;
			  background-color: rgba(255, 255, 255, 0.5);
			}
			#main-loader {
			  position: fixed;
			  z-index: 99999;
			  height: 2em;
			  width: 2em;
			  overflow: show;
			  margin: auto;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  right: 0;
			  display : none;
			}
		</style>
    </head>
	 <body>
        <div class="row row-front">
			<?php echo $content; ?>
		</div>
		
		<div id="main-loader">
			<div class="preloader-wrapper small active">
			  <div class="spinner-layer spinner-blue">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>

			  <div class="spinner-layer spinner-red">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>

			  <div class="spinner-layer spinner-yellow">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>

			  <div class="spinner-layer spinner-green">
			    <div class="circle-clipper left">
			      <div class="circle"></div>
			    </div><div class="gap-patch">
			      <div class="circle"></div>
			    </div><div class="circle-clipper right">
			      <div class="circle"></div>
			    </div>
			  </div>
			</div>
		</div>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/materialize.min.js"></script>
    </body>
</html>