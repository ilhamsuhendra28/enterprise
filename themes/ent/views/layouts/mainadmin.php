<html>
    <head>
    	<title>Bagidata | Indonesia Data Exchange</title>
    	<link rel="icon" type="image/png" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/favicon.png">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/style.css"  media="screen,projection"/>
		<?php /*<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">*/ ?>
  		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/plugin/font-awesome/4.5.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
        <link type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/materialize-stepper.min.css"  media="screen,projection"/>	
		<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" /> -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css" />
        <link type="text/css" rel="stylesheet" href="https://materializecss.com/extras/noUiSlider/nouislider.css"  media="screen,projection"/>
        <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.js"></script> -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>
		<style type="text/css">
			#main-loader:before {
			  content: '';
			  display: block;
			  position: fixed;
			  top: 0;
			  left: 0;
			  width: 100%;
			  height: 100%;
			  background-color: rgba(255, 255, 255, 0.5);
			}
			#main-loader {
			  position: fixed;
			  z-index: 99999;
			  height: 2em;
			  width: 2em;
			  overflow: show;
			  margin: auto;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  right: 0;
			  display : none;
			}
		</style>
    </head>
	<body>
		<?php
			$activehome = array();
			$activesegment = array();
			$activesegmentCustom = array();
			$activecampaign = array();
			$activeapppromotion = array();
			$activebrand = array();
			
			if(Yii::app()->controller->id=='home' && Yii::app()->controller->action->id=='index'){
				$activehome = 'menu-side-act';
			}else if(Yii::app()->controller->id=='clustering' || Yii::app()->controller->id=='clusteringCustom'){
				$activesegment = 'menu-side-act';
			}else if(Yii::app()->controller->id=='campaign' || Yii::app()->controller->id=='appPromotion'){
				$activecampaign = 'menu-side-act';
			}else if(Yii::app()->controller->id=='brand' && Yii::app()->controller->action->id=='index'){
				$activebrand = 'menu-side-act';
			}	
		?>
		<div class="row row-segment">
       		<div class="col s2 side-segment">
       			<div class="row side-logo">
       				<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/logo.png" class="responsive-img">
       			</div>
       			<div class="row menu-side">
       				<ul class="collapsible collapsible-accordion">
					
       					<!-- <li class="<?php //echo $activehome; ?>"><a href="<?php //echo Yii::app()->createUrl('home/index'); ?>"><img src="<?php //echo Yii::app()->theme->baseUrl; ?>/assets/images/icon-menu/bagidata-icon-menu-user-home.svg" width="16"> Home</a></li> -->
						
						<li class="<?php echo $activesegment; ?> menu-drop">
				    		<a class="collapsible-header"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/icon-menu/bagidata-icon-menu-user-segment.svg" width="16"> User Segment<i class="material-icons">arrow_drop_down</i></a>
					   		<div class="collapsible-body">
					   			<ul>
					   				<li><a href="<?php echo Yii::app()->createUrl('clustering/index'); ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> User Segment App</a></li>
					   				<li><a href="<?php echo Yii::app()->createUrl('clusteringCustom/index'); ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> User Segment Custom</a></li>
					   			</ul>
					   		</div>
				    	</li>
						
						<?php /*
						<li class="<?php echo $activesegment; ?> menu-drop">
				    		<a class="collapsible-header">User Segment<i class="material-icons">arrow_drop_down</i></a>
					   		<div class="collapsible-body">
					   			<ul>
					   				<li><a href="<?php echo Yii::app()->createUrl('clustering/index'); ?>">List User Segment</a></li>
					   				<li><a href="<?php echo Yii::app()->createUrl('clustering/uoverview'); ?>">User Overview</a></li>
					   			</ul>
					   		</div>
				    	</li>
						*/ ?>
						
						
						<?php /*
				    	<li class="<?php echo $activecampaign; ?> menu-drop">
				    		<a class="collapsible-header">Campaign<i class="material-icons">arrow_drop_down</i></a>
					   		<div class="collapsible-body">
					   			<ul>
					   				<li><a href="<?php echo Yii::app()->createUrl('campaign/index'); ?>">List Campaign</a></li>
					   				<li><a href="<?php echo Yii::app()->createUrl('campaign/cOverview'); ?>">Campaign Overview</a></li>
					   				<li><a href="<?php echo Yii::app()->createUrl('campaign/aPromotion'); ?>">App Promotions</a></li>
					   			</ul>
					   		</div>
				    	</li>
						*/?>
						
						<?php /*<li class="<?php echo $activecampaign; ?>"><a href="<?php echo Yii::app()->createUrl('campaign/index'); ?>">Campaign</a></li>*/ ?>
						<?php /*<li class="<?php echo $activeapppromotion; ?>"><a href="<?php echo Yii::app()->createUrl('appPromotion/index'); ?>">App Promotions</a></li>*/?>
						
						<li class="<?php echo $activecampaign; ?> menu-drop">
				    		<a class="collapsible-header"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/icon-menu/bagidata-icon-menu-campaign.svg" width="16"> Campaign<i class="material-icons">arrow_drop_down</i></a>
					   		<div class="collapsible-body">
					   			<ul>
					   				<li><a href="<?php echo Yii::app()->createUrl('campaign/index',array('type'=>'email')); ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> Email</a></li>
					   				<li><a href="<?php echo Yii::app()->createUrl('campaign/index',array('type'=>'sms')); ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> SMS</a></li>
									<li><a href="<?php echo Yii::app()->createUrl('campaign/index',array('type'=>'wa')); ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> Whatsapp</a></li>
									<li><a href="<?php echo Yii::app()->createUrl('appPromotion/index'); ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> Apps</a></li>
					   			</ul>
					   		</div>
				    	</li>
						
				    	<li class="<?php echo $activebrand; ?>"><a href="<?php echo Yii::app()->createUrl('brand/index'); ?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/icon-menu/bagidata-icon-menu-user-brand.svg" width="16"> Brand</a></li>
						<?php $user = UserEnterprise::model()->findByPk(Yii::app()->user->id); ?>
				    	<!-- <li><a href="<?php //echo Yii::app()->createUrl('site/logout'); ?>">Logout (<?php //echo $user->full_name; ?>)</a></li> -->
					</ul>
       			</div>
       		</div>
       		<div class="col s10 content-seg">
       			<nav>
	    			<div class="nav-wrapper">
	      				<div class="search-head brand-logo">
	      					<div class="input-field inline">
	      						<i class="fa fa-search prefix" aria-hidden="true"></i>
	      						<input id="icon_prefix" type="text" class="validate search-nav">
          						<label for="icon_prefix">Search Campaign or User Segment</label>
          					</div>
	      				</div>
	      				<ul id="nav-mobile" class="right hide-on-med-and-down">
	       					<li><a class="dropdown-trigger" href="#!" data-target="dropdown1"><?php echo $user->full_name; ?> <i class="material-icons right">expand_more</i></a>
	       						<ul id="dropdown1" class="dropdown-content" tabindex="0" style="">
							      	<li tabindex="0">
						        		<a href="<?php echo Yii::app()->createUrl('/home/profile'); ?>">Profile</a>
							      	</li>
									<li tabindex="1">
						        		<a href="<?php echo Yii::app()->createUrl('site/logout'); ?>">Logout</a>
							      	</li>
							    </ul>
	       					</li>
	       					<li class="img-profile">
								<a href="#">
									<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/profile-photo.svg">
								</a>
							</li>
	      				</ul>
	    			</div>
	  			</nav>
				
				<div id="main-loader">
					<div class="preloader-wrapper small active">
					  <div class="spinner-layer spinner-blue">
						<div class="circle-clipper left">
						  <div class="circle"></div>
						</div><div class="gap-patch">
						  <div class="circle"></div>
						</div><div class="circle-clipper right">
						  <div class="circle"></div>
						</div>
					  </div>

					  <div class="spinner-layer spinner-red">
						<div class="circle-clipper left">
						  <div class="circle"></div>
						</div><div class="gap-patch">
						  <div class="circle"></div>
						</div><div class="circle-clipper right">
						  <div class="circle"></div>
						</div>
					  </div>

					  <div class="spinner-layer spinner-yellow">
						<div class="circle-clipper left">
						  <div class="circle"></div>
						</div><div class="gap-patch">
						  <div class="circle"></div>
						</div><div class="circle-clipper right">
						  <div class="circle"></div>
						</div>
					  </div>

					  <div class="spinner-layer spinner-green">
						<div class="circle-clipper left">
						  <div class="circle"></div>
						</div><div class="gap-patch">
						  <div class="circle"></div>
						</div><div class="circle-clipper right">
						  <div class="circle"></div>
						</div>
					  </div>
					</div>
				</div>
				
				<?php if($user->active == 2){ ?>
					<style>
					span.badge.email {
						font-weight: 300;
						font-size: 0.8rem;
						color: #fff;
						background-color: #26a69a;
						border-radius: 2px;
					}
					</style>

					<center>
						<a href="#" onclick="verifemail()"><span class="email badge" style="float:none;text-decoration:underline;font-size:18px;">Anda belum memverifikasi email anda, Silahkan konfirmasi email anda dengan mengklik link ini</span></a>
					</center>
					
					<script>
						function verifemail(){
							jQuery.ajax({
								type: 'POST',
								dataType: 'json',
								url: '<?php echo Yii::app()->createAbsoluteUrl("site/verifikasiemail", array('id'=>$user->id, 'random'=>Logic::randomChar())); ?>',
								data:data,
								
								beforeSend: function() {
									$("#main-loader").show();
								},
								
								success:function(data){
									if(data.hasil == 'success'){
										$("#main-loader").hide();
										swal(
											'Success',
											'Notifikasi verifikasi telah dikirim ke email anda, silahkan diperiksa. Terimakasih',
											'success'
										).catch(swal.noop);
									}else{
										$("#main-loader").hide();
										 swal(
											'Gagal',
											data.hasil,
											'error'
										).catch(swal.noop);
									}	
								},
						  });
						}	
					</script>
				<?php } ?>
				<?php echo $content; ?>
			</div>
       	</div>
		
		<?php /*<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>*/?>
		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		<?php Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' ); ?>		
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/materialize.min.js"></script>
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script> -->
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/materialize-stepper.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/tinymce.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.tinymce.min.js"></script>
        <script type="text/javascript" src="https://materializecss.com/extras/noUiSlider/nouislider.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/wNumb.js"></script>
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/Chart.min.js"></script>
	
		<script>

			$(function(){
			   $('.stepper').activateStepper();
			});
        
			$(document).ready(function(){
				$('select').formSelect();
			});
			
			
			
			$(document).on('click','.con-check', function(){
				if($(this).hasClass('aktif')){
					$(this).removeClass('aktif');
					$(this).find('.con-num-count .center-cont .check').addClass("hidden");
					$(this).find('.con-num-count .center-cont .num').removeClass("hidden");
					$(this).find('.con-num-count .center-cont input.checkboxlist').prop('checked',false);
					
				}else{
					$(this).addClass('aktif');
					$(this).find('.con-num-count .center-cont .check').removeClass("hidden");
					$(this).find('.con-num-count .center-cont .num').addClass("hidden");
					$(this).find('.con-num-count .center-cont input.checkboxlist').prop('checked',true);
					
				}
				
				//alert('load setelah ajax');
				
				
				if ($(this).data("url")){
					//alert($(this).data("url"));
					window.location.href = $(this).data("url");
				}
			}); 
			
			/*
            $(".con-check").click(function(){
				if($(this).hasClass('aktif')){
					$(this).removeClass('aktif');
					$(this).find('.con-num-count .center-cont .check').addClass("hidden");
					$(this).find('.con-num-count .center-cont .num').removeClass("hidden");
					$(this).find('.con-num-count .center-cont input.checkboxlist').attr('checked',false);
					
				}else{
					$(this).addClass('aktif');
					$(this).find('.con-num-count .center-cont .check').removeClass("hidden");
					$(this).find('.con-num-count .center-cont .num').addClass("hidden");
					$(this).find('.con-num-count .center-cont input.checkboxlist').attr('checked',true);
					
				}
				
				if ($(this).data("url")){
					//alert($(this).data("url"));
					window.location.href = $(this).data("url");
				}
				
			});
			*/ 
			
			
			
      
			$('.chips-placeholder').chips({
				placeholder: 'Purchasing interes',
				secondaryPlaceholder: 'Add More +',
			});
       
			$(document).ready(function(){
				$('.datepicker').datepicker({
					'format':'yyyy-mm-dd',
				});
			});
       
            $(document).ready(function(){
                $('.timepicker').timepicker({
					'twelveHour':false,
				});
            });
        
            tinymce.init({
				selector: '.ck',
				height: 200,
				menubar: false,
				plugins: [
					'advlist autolink lists link image charmap print preview anchor textcolor',
					'searchreplace visualblocks code fullscreen',
					'insertdatetime media table contextmenu paste code help wordcount'
				],
				toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
			});
			
			tinymce.init({
				selector: '.cktemplate',
				height: 900,
				menubar: false,
				plugins: [
					'advlist autolink lists link image charmap print preview anchor textcolor',
					'searchreplace visualblocks code fullscreen',
					'insertdatetime media table contextmenu paste code help wordcount'
				],
				toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
			});
      
        	var elem = document.querySelector('.collapsible');
			var instance = M.Collapsible.init(elem);
			var elem = document.querySelector('.dropdown-trigger');
  			var instance = M.Dropdown.init(elem);
       
			if ($('#mybarChart').length ){ 		  
				var ctx = document.getElementById("mybarChart");
				var mybarChart = new Chart(ctx, {
					type: 'line',
					data: {
						labels: ["1M+", "1.1m - 3M", "3.1M - 5M", "5.1M - 8M", "8.1M - 15M", ">15M"],
						datasets: [{
							label: "Data1",
							backgroundColor: "rgba(255, 255, 255, 0)",
							borderColor: "#ffb60a",
							pointBorderColor: "#ffb60a",
							pointBackgroundColor: "#ffb60a",
							pointHoverBackgroundColor: "#fff",
							pointHoverBorderColor: "rgba(220,220,220,1)",
							pointBorderWidth: 1,
							data: [31, 74, 6, 39, 20, 85, 7]
						}, {
							label: "Data 2",
							backgroundColor: "rgba(255, 255, 255, 0)",
							borderColor: "#3aa8b8",
							pointBorderColor: "#3aa8b8",
							pointBackgroundColor: "#3aa8b8",
							pointHoverBackgroundColor: "#fff",
							pointHoverBorderColor: "rgba(151,187,205,1)",
							pointBorderWidth: 1,
							data: [82, 23, 66, 9, 99, 4, 2]
						},
						{
							label: "Data 2",
							backgroundColor: "rgba(255, 255, 255, 0)",
							borderColor: "#c74793",
							pointBorderColor: "#c74793",
							pointBackgroundColor: "#c74793",
							pointHoverBackgroundColor: "#fff",
							pointHoverBorderColor: "rgba(151,187,205,1)",
							pointBorderWidth: 1,
							data: [82, 13, 65, 21, 49, 10, 15]
						}]
					},

					options: {
						legend:{
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true
								}
							}]
						}
					}
				});
			} 
			
			if ($('#c2').length ){ 	  
				var ctx = document.getElementById("c2");
				var data = {
					labels: [
						"Budget",
						"None"
					],
					datasets: [{
						data: [80,20], // Persen
						backgroundColor: [
							"#a3a1fb",
							"#edecfe"
						],
						hoverBackgroundColor: [
							"#8280ea",
							"#edecfe"
						]
					}]
				};

				var canvasDoughnut = new Chart(ctx, {
					type: 'doughnut',
					tooltipFillColor: "#edecfe",
					data: data
				});
			} 
			
			
			
			$(".con-check").click(function(){
				if($(this).hasClass('aktif')){
					$(this).removeClass('aktif');
					$(this).find('.con-num-count .center-cont .check').addClass("hidden");
					$(this).find('.con-num-count .center-cont .num').removeClass("hidden");
					$(this).find('.con-num-count .center-cont input.checkboxlist').prop('checked',false);
				}else{
					$(this).addClass('aktif');
					$(this).find('.con-num-count .center-cont .check').removeClass("hidden");
					$(this).find('.con-num-count .center-cont .num').addClass("hidden");
					$(this).find('.con-num-count .center-cont input.checkboxlist').prop('checked',true);
				}
				//alert('load awal');
				
				var boxes = $('.con-num-count .center-cont input.checkboxlist:checked').length;
				//console.log(boxes);
				$('.con-num-count .center-cont input.checkboxlist').each(function () {
					if (boxes > 0){
						$(this).prop('required',false);
					}else{
						$(this).prop('required',true);
					}
				});
				
				
				if ($(this).data("url")){
					//alert($(this).data("url"));
					window.location.href = $(this).data("url");
				} 
				
			});
			
			
			
		
			if ($('#mybarChart1').length ){ 		  
				var ctx1 = document.getElementById("mybarChart1");
				var mybarChart1 = new Chart(ctx1, {
					type: 'bar',
					data: {
						labels: ["1M+", "1.1m - 3M", "3.1M - 5M", "5.1M - 8M", "8.1M - 15M", ">15M"],
						datasets: [{
							label: 'Male',
							backgroundColor: "#5ee2a0",
							data: [51, 30, 40, 28, 92, 50]
						}, {
							label: 'Female',
							backgroundColor: "#ffb60a",
							data: [41, 56, 25, 48, 72, 34]
						}]
					},

					options: {
						legend:{
							labels:{
								FontSize : '5px'
							}
						},
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true
								}
							}]
						}
					}
				});
			} 
			
			$(".con-check").click(function(){
				if($(this).hasClass('aktif')){
					$(this).removeClass('aktif');
					$(this).find('.con-num-count .center-cont .check').addClass("hidden");
					$(this).find('.con-num-count .center-cont .num').removeClass("hidden");
				}else{
					$(this).addClass('aktif');
					$(this).find('.con-num-count .center-cont .check').removeClass("hidden");
					$(this).find('.con-num-count .center-cont .num').addClass("hidden");
				}
			});
		</script>
    </body>
</html>